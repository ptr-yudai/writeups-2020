(module
  (type (;0;) (func (param i32)))
  (type (;1;) (func))
  (type (;2;) (func (result i32)))
  (type (;3;) (func (param i32) (result i32)))
  (type (;4;) (func (param i32 i32 i32) (result i32)))
  (import "env" "_print_char" (func $_print_char (type 0)))
  (import "env" "_flush" (func $_flush (type 1)))
  (import "env" "_get_char" (func $_get_char (type 2)))
  (func $__wasm_call_ctors (type 1))
  (func $initialize (type 1)
    i32.const 0
    i32.const 1024
    i32.store offset=3144
    i32.const 1024
    i32.const 0
    i32.const 100
    call $memset
    drop
    i32.const 1136
    i32.const 0
    i32.const 1000
    call $memset
    drop
    i32.const 2144
    i32.const 0
    i32.const 1000
    call $memset
    drop)
  (func $execute (type 3) (param i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      local.get 0
      i32.const 1
      i32.lt_s
      br_if 0 (;@1;)
      i32.const 0
      local.set 1
      loop  ;; label = @2
        local.get 1
        i32.const 2144
        i32.add
        call $_get_char
        i32.store8
        local.get 0
        local.get 1
        i32.const 1
        i32.add
        local.tee 1
        i32.ne
        br_if 0 (;@2;)
      end
      local.get 0
      i32.const 1
      i32.lt_s
      br_if 0 (;@1;)
      i32.const 0
      local.set 2
      i32.const 0
      local.set 1
      i32.const 0
      local.set 3
      loop  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            i32.const 2144
            i32.add
            i32.load8_s
            i32.const -43
            i32.add
            local.tee 4
            i32.const 50
            i32.gt_u
            br_if 0 (;@4;)
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          block  ;; label = @12
                            block  ;; label = @13
                              block  ;; label = @14
                                block  ;; label = @15
                                  local.get 4
                                  br_table 0 (;@15;) 11 (;@4;) 1 (;@14;) 2 (;@13;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 3 (;@12;) 11 (;@4;) 4 (;@11;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 11 (;@4;) 5 (;@10;) 11 (;@4;) 6 (;@9;) 0 (;@15;)
                                end
                                local.get 3
                                i32.const 1136
                                i32.add
                                local.tee 4
                                local.get 4
                                i32.load8_u
                                i32.const 1
                                i32.add
                                i32.store8
                                local.get 1
                                i32.const 1
                                i32.add
                                local.tee 1
                                local.get 0
                                i32.ge_s
                                br_if 13 (;@1;)
                                br 11 (;@3;)
                              end
                              local.get 3
                              i32.const 1136
                              i32.add
                              local.tee 4
                              local.get 4
                              i32.load8_u
                              i32.const -1
                              i32.add
                              i32.store8
                              local.get 1
                              i32.const 1
                              i32.add
                              local.tee 1
                              local.get 0
                              i32.lt_s
                              br_if 10 (;@3;)
                              br 12 (;@1;)
                            end
                            local.get 3
                            i32.const 1136
                            i32.add
                            i32.load8_u
                            local.set 5
                            block  ;; label = @13
                              i32.const 0
                              i32.load offset=3144
                              local.tee 4
                              i32.const 4
                              i32.add
                              i32.const 1124
                              i32.lt_u
                              br_if 0 (;@13;)
                              block  ;; label = @14
                                i32.const 0
                                i32.load8_u offset=1024
                                local.tee 4
                                i32.eqz
                                br_if 0 (;@14;)
                                i32.const 1025
                                local.set 6
                                loop  ;; label = @15
                                  local.get 4
                                  i32.const 24
                                  i32.shl
                                  i32.const 24
                                  i32.shr_s
                                  call $_print_char
                                  local.get 6
                                  i32.load8_u
                                  local.set 4
                                  local.get 6
                                  i32.const 1
                                  i32.add
                                  local.set 6
                                  local.get 4
                                  br_if 0 (;@15;)
                                end
                              end
                              call $_flush
                              i32.const 1024
                              local.set 4
                              i32.const 0
                              i32.const 1024
                              i32.store offset=3144
                              i32.const 1024
                              i32.const 0
                              i32.const 100
                              call $memset
                              drop
                            end
                            local.get 5
                            i32.const 2
                            i32.or
                            i32.const 62
                            i32.ne
                            br_if 4 (;@8;)
                            local.get 4
                            i32.const 38
                            i32.store8
                            i32.const 0
                            i32.load offset=3144
                            i32.const 108
                            i32.const 103
                            local.get 5
                            i32.const 60
                            i32.eq
                            select
                            i32.store8 offset=1
                            i32.const 0
                            i32.load offset=3144
                            i32.const 116
                            i32.store8 offset=2
                            i32.const 0
                            i32.load offset=3144
                            i32.const 59
                            i32.store8 offset=3
                            i32.const 4
                            local.set 4
                            br 5 (;@7;)
                          end
                          local.get 3
                          i32.const -1
                          i32.add
                          local.set 3
                          local.get 1
                          i32.const 1
                          i32.add
                          local.tee 1
                          local.get 0
                          i32.lt_s
                          br_if 8 (;@3;)
                          br 10 (;@1;)
                        end
                        local.get 3
                        i32.const 1
                        i32.add
                        local.set 3
                        local.get 1
                        i32.const 1
                        i32.add
                        local.tee 1
                        local.get 0
                        i32.lt_s
                        br_if 7 (;@3;)
                        br 9 (;@1;)
                      end
                      local.get 3
                      i32.const 1136
                      i32.add
                      i32.load8_u
                      br_if 5 (;@4;)
                      local.get 1
                      i32.const 1
                      i32.add
                      local.set 4
                      local.get 1
                      i32.const 2145
                      i32.add
                      i32.load8_u
                      local.tee 5
                      i32.const 93
                      i32.ne
                      br_if 4 (;@5;)
                      local.get 4
                      i32.const 1
                      i32.add
                      local.tee 1
                      local.get 0
                      i32.lt_s
                      br_if 6 (;@3;)
                      br 8 (;@1;)
                    end
                    local.get 3
                    i32.const 1136
                    i32.add
                    i32.load8_u
                    i32.eqz
                    br_if 4 (;@4;)
                    local.get 1
                    i32.const -1
                    i32.add
                    local.set 4
                    local.get 1
                    i32.const 2143
                    i32.add
                    i32.load8_u
                    local.tee 5
                    i32.const 91
                    i32.ne
                    br_if 2 (;@6;)
                    local.get 4
                    i32.const 1
                    i32.add
                    local.tee 1
                    local.get 0
                    i32.lt_s
                    br_if 5 (;@3;)
                    br 7 (;@1;)
                  end
                  local.get 4
                  local.get 5
                  i32.store8
                  i32.const 1
                  local.set 4
                end
                i32.const 0
                i32.const 0
                i32.load offset=3144
                local.get 4
                i32.add
                i32.store offset=3144
                local.get 1
                i32.const 1
                i32.add
                local.tee 1
                local.get 0
                i32.lt_s
                br_if 3 (;@3;)
                br 5 (;@1;)
              end
              i32.const 0
              local.set 7
              loop  ;; label = @6
                local.get 5
                i32.const 255
                i32.and
                local.set 6
                local.get 4
                i32.const 2143
                i32.add
                i32.load8_u
                local.set 5
                local.get 4
                i32.const -1
                i32.add
                local.tee 1
                local.set 4
                local.get 7
                local.get 6
                i32.const 93
                i32.eq
                i32.add
                local.get 6
                i32.const 91
                i32.eq
                i32.sub
                local.tee 7
                i32.const 0
                i32.gt_s
                br_if 0 (;@6;)
                local.get 1
                local.set 4
                local.get 5
                i32.const 255
                i32.and
                i32.const 91
                i32.ne
                br_if 0 (;@6;)
                br 2 (;@4;)
              end
            end
            i32.const 0
            local.set 7
            loop  ;; label = @5
              local.get 5
              i32.const 255
              i32.and
              local.set 6
              local.get 4
              i32.const 2145
              i32.add
              i32.load8_u
              local.set 5
              local.get 4
              i32.const 1
              i32.add
              local.tee 1
              local.set 4
              local.get 7
              local.get 6
              i32.const 91
              i32.eq
              i32.add
              local.get 6
              i32.const 93
              i32.eq
              i32.sub
              local.tee 7
              i32.const 0
              i32.gt_s
              br_if 0 (;@5;)
              local.get 1
              local.set 4
              local.get 5
              i32.const 255
              i32.and
              i32.const 93
              i32.ne
              br_if 0 (;@5;)
            end
          end
          local.get 1
          i32.const 1
          i32.add
          local.tee 1
          local.get 0
          i32.ge_s
          br_if 2 (;@1;)
        end
        local.get 2
        i32.const 1
        i32.add
        local.tee 2
        i32.const 100000
        i32.lt_u
        br_if 0 (;@2;)
      end
    end
    block  ;; label = @1
      i32.const 0
      i32.load8_u offset=1024
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      i32.const 1025
      local.set 4
      loop  ;; label = @2
        local.get 1
        i32.const 24
        i32.shl
        i32.const 24
        i32.shr_s
        call $_print_char
        local.get 4
        i32.load8_u
        local.set 1
        local.get 4
        i32.const 1
        i32.add
        local.set 4
        local.get 1
        br_if 0 (;@2;)
      end
    end
    call $_flush
    i32.const 0
    i32.const 1024
    i32.store offset=3144
    i32.const 1024
    i32.const 0
    i32.const 100
    call $memset
    drop
    i32.const 0)
  (func $memset (type 4) (param i32 i32 i32) (result i32)
    (local i32 i32 i32 i64)
    block  ;; label = @1
      local.get 2
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      local.get 1
      i32.store8
      local.get 0
      local.get 2
      i32.add
      local.tee 3
      i32.const -1
      i32.add
      local.get 1
      i32.store8
      local.get 2
      i32.const 3
      i32.lt_u
      br_if 0 (;@1;)
      local.get 0
      local.get 1
      i32.store8 offset=2
      local.get 0
      local.get 1
      i32.store8 offset=1
      local.get 3
      i32.const -3
      i32.add
      local.get 1
      i32.store8
      local.get 3
      i32.const -2
      i32.add
      local.get 1
      i32.store8
      local.get 2
      i32.const 7
      i32.lt_u
      br_if 0 (;@1;)
      local.get 0
      local.get 1
      i32.store8 offset=3
      local.get 3
      i32.const -4
      i32.add
      local.get 1
      i32.store8
      local.get 2
      i32.const 9
      i32.lt_u
      br_if 0 (;@1;)
      local.get 0
      i32.const 0
      local.get 0
      i32.sub
      i32.const 3
      i32.and
      local.tee 4
      i32.add
      local.tee 3
      local.get 1
      i32.const 255
      i32.and
      i32.const 16843009
      i32.mul
      local.tee 1
      i32.store
      local.get 3
      local.get 2
      local.get 4
      i32.sub
      i32.const -4
      i32.and
      local.tee 4
      i32.add
      local.tee 2
      i32.const -4
      i32.add
      local.get 1
      i32.store
      local.get 4
      i32.const 9
      i32.lt_u
      br_if 0 (;@1;)
      local.get 3
      local.get 1
      i32.store offset=8
      local.get 3
      local.get 1
      i32.store offset=4
      local.get 2
      i32.const -8
      i32.add
      local.get 1
      i32.store
      local.get 2
      i32.const -12
      i32.add
      local.get 1
      i32.store
      local.get 4
      i32.const 25
      i32.lt_u
      br_if 0 (;@1;)
      local.get 3
      local.get 1
      i32.store offset=16
      local.get 3
      local.get 1
      i32.store offset=12
      local.get 3
      local.get 1
      i32.store offset=20
      local.get 3
      local.get 1
      i32.store offset=24
      local.get 2
      i32.const -24
      i32.add
      local.get 1
      i32.store
      local.get 2
      i32.const -28
      i32.add
      local.get 1
      i32.store
      local.get 2
      i32.const -20
      i32.add
      local.get 1
      i32.store
      local.get 2
      i32.const -16
      i32.add
      local.get 1
      i32.store
      local.get 4
      local.get 3
      i32.const 4
      i32.and
      i32.const 24
      i32.or
      local.tee 5
      i32.sub
      local.tee 2
      i32.const 32
      i32.lt_u
      br_if 0 (;@1;)
      local.get 1
      i64.extend_i32_u
      local.tee 6
      i64.const 32
      i64.shl
      local.get 6
      i64.or
      local.set 6
      local.get 3
      local.get 5
      i32.add
      local.set 1
      loop  ;; label = @2
        local.get 1
        local.get 6
        i64.store
        local.get 1
        i32.const 8
        i32.add
        local.get 6
        i64.store
        local.get 1
        i32.const 16
        i32.add
        local.get 6
        i64.store
        local.get 1
        i32.const 24
        i32.add
        local.get 6
        i64.store
        local.get 1
        i32.const 32
        i32.add
        local.set 1
        local.get 2
        i32.const -32
        i32.add
        local.tee 2
        i32.const 31
        i32.gt_u
        br_if 0 (;@2;)
      end
    end
    local.get 0)
  (table (;0;) 1 1 funcref)
  (memory (;0;) 2)
  (global (;0;) (mut i32) (i32.const 68688))
  (global (;1;) i32 (i32.const 68688))
  (global (;2;) i32 (i32.const 3148))
  (export "memory" (memory 0))
  (export "__heap_base" (global 1))
  (export "__data_end" (global 2))
  (export "initialize" (func $initialize))
  (export "execute" (func $execute))
  (data (;0;) (i32.const 1024) "\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00")
  (data (;1;) (i32.const 3144) "\00\04\00\00"))
