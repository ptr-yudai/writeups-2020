const fs = require('fs')
let bytes = new Uint8Array(fs.readFileSync('./main.wasm'));

const getchar = program => {
    const it = (function* () {
        for (const c of program) {
            yield c.charCodeAt(0);
        }
    })();
    return () => it.next().value;
};

const execute = program => {
    if (program.length > 1000) {
        alert('too long');
        return;
    }

    let buffer = '';
    const importObject = {
        env: {
            _get_char: getchar(program),
            _print_char(arg) {
                buffer += String.fromCharCode(arg);
            },
            _flush() {
                console.log(buffer);
                buffer = '';
            }
        }
    };

    WebAssembly.instantiate(bytes, importObject).then(result => {
        const instance = result.instance;
        instance.exports.initialize();
        instance.exports.execute(program.length);
    });
};

execute(new String(fs.readFileSync('./exploit.bf')));
