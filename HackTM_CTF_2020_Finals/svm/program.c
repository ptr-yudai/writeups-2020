unsigned char registers[5];
unsigned char memory[0x100];

void assert_in_range(int value, int min, int max) {
  if (value < min || value > max) exit(1);
}

void ope_div(unsigned char arg1, unsigned char arg2, unsigned char arg3) {
  switch(arg1) {
  case 1: // reg[arg3] /= arg2;
    assert_in_range(arg3, 0, 4);
    registers[arg3] /= arg2;
    break;
  case 2: // mem[arg3] /= mem[arg2]
    assert_in_range(arg2, 0, 0xff);
    assert_in_range(arg3, 0, 0xff);
    memory[arg3] /= memory[arg2];
    break;
  case 3: // mem[arg3] /= arg2
    assert_in_range(arg3, 0, 0xff);
    memory[arg3] /= arg2;
    break;
  case 4: // mem[arg3] /= reg[arg2]
    assert_in_range(arg2, 0, 4);
    assert_in_range(arg3, 0, 0xff);
    memory[arg3] /= registers[arg2];
    break;
  case 5: // reg[arg3] /= mem[arg2]
    assert_in_range(arg2, 0, 0xff);
    assert_in_range(arg3, 0, 4);
    memory[arg3] /= registers[arg2];
    break;
  case 6: // reg[arg3] /= reg[arg2]
    assert_in_range(arg2, 0, 4);
    assert_in_range(arg3, 0, 4);
    registers[arg3] /= registers[arg2];
    break;
  default:
    exit(1);
  }
}

void ope_inc(unsigned char arg1, unsigned char arg2) {
  switch(arg1) {
  case 1: // reg[arg2]++;
    assert_in_range(arg2, 0, 4);
    registers[arg2]++;
    break;
  case 2: // mem[arg2]++;
    assert_in_range(arg2, 0, 0xff);
    memory[arg2]++;
  default:
    exit(1);
  }
}

void ope_strcpy(unsigned char arg1, unsigned char arg2, int pos, char *code) {
  int offset = (arg1 << 8) + arg2;
  assert_in_range(offset, 0, 0x3e8);
  memcpy(&code[pos], &pcode[offset], strlen(&code[offset]));
  code[pos + strlen(&pcode[offset])] = 0;
}

void ope_read(unsigned char arg1) {
  // OOB write of memory
  registers[0] = read(0, &memory[arg1], (int)registers[1]);
}

void ope_write(unsigned char arg1) {
  // possible OOB
  write(1, &memory[arg1], (int)registers[2]);
  registers[0] = registers[2];
}

void ope_write_str(unsigned char arg1) {
  assert_in_range(arg1, 0, 0xff);
  for(registers[3] = 0; memory[arg1 + registers[3]]; registers[3]++) {
    write(1, memory[arg1 + registers[3]], 1);
  }
  registers[0] = registers[3];
}

int run_code(unsigned char *code) {
  int i;
  unsigned int arg1, arg2, arg3;

  for(i = 0; code[i] && i < 0x3e8;) {
    switch(code[i]) {
    case 0x21:
      arg3 = code[++i];
      arg2 = code[++i];
      arg1 = code[++i];
      ope_XXX(arg1, arg2, arg3);
      break;
    case 0x22:
      arg3 = code[++i];
      arg2 = code[++i];
      arg1 = code[++i];
      ope_XXX(arg1, arg2, arg3);
      break;

    case 0x24:
      arg3 = code[++i];
      arg2 = code[++i];
      arg1 = code[++i];
      ope_mul(arg1, arg2, arg3);
      break;
    case 0x25:
      arg3 = code[++i];
      arg2 = code[++i];
      arg1 = code[++i];
      ope_div(arg1, arg2, arg3);
      break;
    case 0x26:
      arg2 = code[++i];
      arg1 = code[++i];
      ope_inc(arg1, arg2);
      break;
    case 0x27:
      arg2 = code[++i];
      arg1 = code[++i];
      ope_dec(arg1, arg2);
      break;
    case 0x28:
      arg3 = i + 1;
      arg2 = code[++i];
      arg1 = code[++i];
      ope_strcpy(arg1, arg2, arg3, code);
      break;
    case 0x29:
      ope_read(code[++i]);
      break;
    case 0x2a:
      ope_write(code[++i]);
      break;
    case 0x2b:
      ope_write_str(code[++i]);
      break;
      
    case 0x90:
      break;
    }
  }
}
