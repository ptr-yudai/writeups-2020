from ptrlib import p16

def MEM(index):
    assert 0 <= index <= 0xff
    return ('MEM', index)
def REG(index):
    assert 0 <= index <= 4
    return ('REG', index)
def IMM(value):
    assert 0 <= value <= 0xff
    return ('IMM', value)

def ope_mov(dst, src):
    output = b'\x20'
    if dst[0] == 'MEM':
        if src[0] == 'MEM':
            output += bytes([dst[1], src[1], 0x02])
        elif src[0] == 'REG':
            output += bytes([dst[1], src[1], 0x04])
        elif src[0] == 'IMM':
            output += bytes([dst[1], src[1], 0x03])
    elif dst[0] == 'REG':
        if src[0] == 'MEM':
            output += bytes([dst[1], src[1], 0x05])
        elif src[0] == 'REG':
            output += bytes([dst[1], src[1], 0x06])
        elif src[0] == 'IMM':
            output += bytes([dst[1], src[1], 0x01])
    assert len(output) == 4
    return output

def ope_add(dst, src):
    output = b'\x21'
    if dst[0] == 'MEM':
        if src[0] == 'MEM':
            output += bytes([dst[1], src[1], 0x02])
        elif src[0] == 'REG':
            output += bytes([dst[1], src[1], 0x04])
        elif src[0] == 'IMM':
            output += bytes([dst[1], src[1], 0x03])
    elif dst[0] == 'REG':
        if src[0] == 'MEM':
            output += bytes([dst[1], src[1], 0x05])
        elif src[0] == 'REG':
            output += bytes([dst[1], src[1], 0x06])
        elif src[0] == 'IMM':
            output += bytes([dst[1], src[1], 0x01])
    assert len(output) == 4
    return output

def ope_sub(dst, src):
    output = b'\x22'
    if dst[0] == 'MEM':
        if src[0] == 'MEM':
            output += bytes([dst[1], src[1], 0x02])
        elif src[0] == 'REG':
            output += bytes([dst[1], src[1], 0x04])
        elif src[0] == 'IMM':
            output += bytes([dst[1], src[1], 0x03])
    elif dst[0] == 'REG':
        if src[0] == 'MEM':
            output += bytes([dst[1], src[1], 0x05])
        elif src[0] == 'REG':
            output += bytes([dst[1], src[1], 0x06])
        elif src[0] == 'IMM':
            output += bytes([dst[1], src[1], 0x01])
    assert len(output) == 4
    return output

def ope_xor(dst, src):
    output = b'\x23'
    if dst[0] == 'MEM':
        if src[0] == 'MEM':
            output += bytes([dst[1], src[1], 0x02])
        elif src[0] == 'REG':
            output += bytes([dst[1], src[1], 0x04])
        elif src[0] == 'IMM':
            output += bytes([dst[1], src[1], 0x03])
    elif dst[0] == 'REG':
        if src[0] == 'MEM':
            output += bytes([dst[1], src[1], 0x05])
        elif src[0] == 'REG':
            output += bytes([dst[1], src[1], 0x06])
        elif src[0] == 'IMM':
            output += bytes([dst[1], src[1], 0x01])
    assert len(output) == 4
    return output

def ope_mul(dst, src):
    output = b'\x24'
    if dst[0] == 'MEM':
        if src[0] == 'MEM':
            output += bytes([dst[1], src[1], 0x02])
        elif src[0] == 'REG':
            output += bytes([dst[1], src[1], 0x04])
        elif src[0] == 'IMM':
            output += bytes([dst[1], src[1], 0x03])
    elif dst[0] == 'REG':
        if src[0] == 'MEM':
            output += bytes([dst[1], src[1], 0x05])
        elif src[0] == 'REG':
            output += bytes([dst[1], src[1], 0x06])
        elif src[0] == 'IMM':
            output += bytes([dst[1], src[1], 0x01])
    assert len(output) == 4
    return output

def ope_div(dst, src):
    output = b'\x25'
    if dst[0] == 'MEM':
        if src[0] == 'MEM':
            output += bytes([dst[1], src[1], 0x02])
        elif src[0] == 'REG':
            output += bytes([dst[1], src[1], 0x04])
        elif src[0] == 'IMM':
            output += bytes([dst[1], src[1], 0x03])
    elif dst[0] == 'REG':
        if src[0] == 'MEM':
            output += bytes([dst[1], src[1], 0x05])
        elif src[0] == 'REG':
            output += bytes([dst[1], src[1], 0x06])
        elif src[0] == 'IMM':
            output += bytes([dst[1], src[1], 0x01])
    assert len(output) == 4
    return output

def ope_inc(dst):
    output = b'\x26'
    if dst[0] == 'MEM':
        output += bytes([dst[1], 0x02])
    if dst[0] == 'REG':
        output += bytes([dst[1], 0x01])
    assert len(output) == 3
    return output

def ope_dec(dst):
    output = b'\x27'
    if dst[0] == 'MEM':
        output += bytes([dst[1], 0x02])
    if dst[0] == 'REG':
        output += bytes([dst[1], 0x01])
    assert len(output) == 3
    return output

def ope_strcpy(offset):
    output = b'\x28'
    output += p16(offset, 'little')
    return output

def ope_read(offset):
    assert 0 <= offset <= 0xff
    output = bytes([0x29, offset])
    return output

def ope_write(offset):
    assert 0 <= offset <= 0xff
    output = bytes([0x2a, offset])
    return output

def ope_write_str(offset):
    assert 0 <= offset <= 0xff
    output = bytes([0x2b, offset])
    return output

def ope_nop():
    return b'\x90'
