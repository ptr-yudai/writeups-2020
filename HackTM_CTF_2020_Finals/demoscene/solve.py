import string

def put(output, flag, index, a, b):
    if index >= len(flag): return True
    ys = (flag[index] + b) % 128
    ye = (flag[index] + a) % 128
    if ys > ye or ys > 0x30 or ye > 0x30:
        #print(f"[-] Index {index} is not {chr(flag[index])}")
        for i in range(0, 0x30):
            output[i][index] = '?'
        return False
    for i in range(ys, ye):
        output[i][index] = '#'
    return True

def draw(flag):
    output = [
        [' ' for i in range(80)]
        for j in range(0x30)
    ]
    if not put(output, flag,  0, 77, 71): return False
    if not put(output, flag,  1, 54, 44): return False
    if not put(output, flag,  2, 53, 41): return False
    if not put(output, flag,  3, 46, 32): return False
    if not put(output, flag,  4, 70, 55): return False
    if not put(output, flag,  5, 78, 73): return False
    if not put(output, flag,  5, 66, 61): return False
    if not put(output, flag,  6, 19, 15): return False
    if not put(output, flag,  6, 32, 28): return False
    if not put(output, flag,  7, 76, 72): return False
    if not put(output, flag,  7, 89, 85): return False
    if not put(output, flag,  8, 107, 103): return False
    if not put(output, flag,  8, 94, 90): return False
    if not put(output, flag,  9, 28, 24): return False
    if not put(output, flag,  9, 41, 37): return False
    if not put(output, flag, 10, 32, 28): return False
    if not put(output, flag, 10, 45, 41): return False
    if not put(output, flag, 11, 87, 83): return False
    if not put(output, flag, 11, 74, 70): return False
    if not put(output, flag, 12, 104, 100): return False
    if not put(output, flag, 12, 91, 87): return False
    if not put(output, flag, 13, 56, 52): return False
    if not put(output, flag, 13, 43, 39): return False
    if not put(output, flag, 15, 43, 38): return False
    if not put(output, flag, 15, 55, 50): return False
    if not put(output, flag, 14, 91, 87): return False
    if not put(output, flag, 14, 78, 74): return False
    if not put(output, flag, 16, 53, 37): return False
    if not put(output, flag, 17, 38, 23): return False
    if not put(output, flag, 18, 68, 56): return False
    if not put(output, flag, 18, 55, 54): return False
    if not put(output, flag, 19, 40, 30): return False
    if not put(output, flag, 19, 28, 27): return False
    if not put(output, flag, 20, 28, 27): return False
    if not put(output, flag, 20, 38, 32): return False
    if not put(output, flag, 21, 63, 62): return False
    if not put(output, flag, 22, 42, 41): return False
    if not put(output, flag, 23, 100, 83): return False
    if not put(output, flag, 24, 54, 37): return False
    if not put(output, flag, 25, 85, 68): return False
    if not put(output, flag, 26, 44, 27): return False
    if not put(output, flag, 27, 34, 30): return False
    if not put(output, flag, 28, 81, 75): return False
    if not put(output, flag, 29, 49, 41): return False
    if not put(output, flag, 30, 42, 38): return False
    if not put(output, flag, 30, 36, 32): return False
    if not put(output, flag, 31, 33, 29): return False
    if not put(output, flag, 31, 41, 37): return False
    if not put(output, flag, 32, 107, 103): return False
    if not put(output, flag, 32, 117, 113): return False
    if not put(output, flag, 33, 55, 51): return False
    if not put(output, flag, 33, 43, 39): return False
    if not put(output, flag, 34, 40, 37): return False
    if not put(output, flag, 34, 54, 50): return False
    if not put(output, flag, 35, 45, 42): return False
    if not put(output, flag, 35, 30, 28): return False
    if not put(output, flag, 36, 88, 87): return False
    if not put(output, flag, 36, 104, 102): return False
    if not put(output, flag, 37, 85, 84): return False
    if not put(output, flag, 37, 69, 68): return False
    if not put(output, flag, 38, 87, 86): return False
    if not put(output, flag, 39, 30, 29): return False
    if not put(output, flag, 39, 40, 31): return False
    if not put(output, flag, 39, 45, 44): return False
    if not put(output, flag, 40, 50, 37): return False
    if not put(output, flag, 40, 54, 51): return False
    if not put(output, flag, 41, 29, 28): return False
    if not put(output, flag, 41, 14, 13): return False
    if not put(output, flag, 41, 24, 15): return False
    return '\n'.join([''.join(line) for line in output])

mask = b"__________________?????__________________"
flag = b"HackTM{B0rnD3c@desTooLa7eForDemo$cen3F4me}"
for c in string.printable[:-6]:
    s = draw(flag + bytes([ord(c)]))
    if s == False: continue
    print(c)
    print(s)
