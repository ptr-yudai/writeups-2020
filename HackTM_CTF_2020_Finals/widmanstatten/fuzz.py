import random
import string
import time
from ptrlib import *
import re

CATEGORY = {
    1: ["Plasma field", "Schumann frequency crystals", "Neutron self-destruction device", "Unstable quasispace teleportation"],
    2: ["Gamma ray generator", "Thermonuclear missile", "Space pigeon shit thrower", "Pew pew lasers"],
    3: ["Flux Capacitor", "Micro black hole bundle", "Timelord policebox"],
    4: ["Resonant cavity thruster", "Ununpentium wedge", "Nuclear pulse", "Retro encabulator", "Specific impulse magnetoplasma"]
}

def add(category, name, count):
    sock.sendlineafter("inventory\n", "1")
    sock.sendlineafter("[4]: ", str(category))
    sock.sendlineafter("?\n", name)
    sock.sendlineafter(":\n", str(count))
    return int(sock.recvlineafter("index "))
def update(index, count, size, description):
    sock.sendlineafter("from inventory\n", "2")
    result = []
    while True:
        l = sock.recvline()
        if b"Entry" not in l:
            sock.unget(l + b"\n")
            break
        r = re.findall(b"Entry \[\d+\]: Description \[(.+)\]", l)
        if r:
            result.append(r[0])
        else:
            print(l)
    sock.sendlineafter("?\n", str(index))
    sock.sendlineafter(":\n", str(count))
    sock.sendlineafter(":\n", str(size))
    sock.sendlineafter(":\n", description)
    return result
def delete(index):
    sock.sendlineafter("inventory\n", "3")
    sock.sendlineafter("?\n", str(index))

sock = Process(["node", "./challenge.js"])
#sock = Socket("localhost", 9999)

table = [0x41 if has_space(chr(i)) else i for i in range(0x80)]
randstr = lambda n: ''.join([random.choice(string.ascii_letters)
                             for i in range(n)])
randbytes = lambda n: bytes([random.choice(table)
                             for i in range(n)])
input("> ")
state = []
for i in range(500):
    choice = random.randrange(0, 3)
    if choice == 0:
        category = random.randint(1, 4)
        name = random.choice(CATEGORY[category])
        count = random.randint(-0xffffffff, 0xffffffff)
        print(f"add({category}, '{name}', {count})")
        state.append(add(category, name, count))
    elif choice == 1 and len(state) > 0:
        index = random.choice(state)
        count = random.randint(-0xffffffff, 0xffffffff)
        size = random.randint(120, 121)
        description = randstr(random.randint(0, size))
        print(f"update({index}, {count}, {size}, '{description}')")
        for desc in update(index, count, size, description):
            if b"FLAG" in desc:
                print(desc)
                input(":eyes:")
    elif len(state) > 0:
        index = random.choice(state)
        print(f"delete({index})")
        delete(index)
        state.remove(index)
    else:
        index = random.randint(0, 10)
        print(f"delete({index})")
        delete(index)

sock.interactive()
