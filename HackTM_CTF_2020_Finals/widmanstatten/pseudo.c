char *mem_1fc0; // 32-bit pointer

unsigned int read_line(char *ptr, int size) {
  for(int count = 0; i < size; i++) {
    read(0, ptr, 1);
  }
}

void select_part() {
  memset(buf, 0, 0x81);
  if (read_line(buf, 0x80) > 5) {
    for(int i = 0; i < 16; i++) {
      if (strstr(CATEGORY[i], buf) != NULL) {
        // ...
      }
    }
  }
}

void add_entry() {
  puts("What part category do you choose?");
  for(int i = 1; i < 5; i++) {
    printf("        [%d] ========== %s =========\n", i, cat[i]);
  }
  unsigned category = user_get_uint();
  int index = select_part(&category); // maybe
  if (index > 0) {
    printf("Enter part count:\n");
    unsigned count = user_get_uint();
    if (category < 3) {
      
      malloc(0x7c);
    } else {
      malloc(0x7e);
    }
  }
}

void update_entry() {
  int i;
  for(i = 0; i < mem_29e0; i++) {
    if (mem_1fc0[i]) {
      // print thing
    }
  }

  printf("Which entry to update?\n");
  unsigned index = user_get_uint();
  if ((index < mem_29e0) && (mem_1fc0[index])) {
    printf("Enter new part count:\n");
    unsigned count = user_get_uint();
    printf("Enter new description size:\n");
    unsigned size = user_get_uint();

    if (size <= 0x79) {
      char *ptr = malloc(size);
      if (ptr) {
        puts("GO:");
        unsigned length = read_line(ptr, size);
        // blahblah
        *(unsigned short*)mem_1fc0[index+122] = count & 0xffff;
        memcpy(mem_1fc0[index] + 1, ptr, length);
      }
      free(ptr);
    }
  }
}

void remove_entry() {
  printf("Which entry to delete?\n");
  int index = user_get_uint();
  if (index < mem_29e0) {
    free(mem_1fc0[index]);
    mem_1fc0[index] = NULL;
    printf("Done!\n");
  }
}

int main() {
  setup();
  print_banner();
  while(1) {
    print_menu();
    switch(user_get_uint()) {
    case 1: add_entry(); break;
    case 2: update_entry(); break;
    case 3: remove_entry(); break;
    case 4: puts("END BANNER"); return 0;
    }
  }
}

unsigned int user_get_uint() {
  unsigned int choice;
  scanf("%u", &choice);
  return choice;
}

void setup() {
  setbuf(stdin, NULL);
  setbuf(stdout, NULL);
  setbuf(stderr, NULL);
}
void print_banner() {
  puts("================== Widmanstatten's Automated Spaceship Management System ==================");
  puts("                   =====================================================");
}
