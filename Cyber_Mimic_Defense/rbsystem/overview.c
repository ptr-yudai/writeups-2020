void*buflist[16];
long sizelist[16];
FILE *rd;
long rd_check;

long read_long() {
    long x;
    scanf("%ld", &x);
    return x;
}

void  allocate() {
    printf("Index: ");
    long index = read_long();
    if (index > 15) {
        return;
    }
    if (buflist[index] != 0) {
        return;
    }

    printf("Size: ");
    long size = read_long();
    if (size > 0x1000) {
        return;
    }
    buflist[index] = malloc(size);
    sizelist[index] = size;

}

void edit() {
    if (rd_check == 0) {
        return;
    }

    printf("Index: ");
    long index = read_long();
    if (index > 15) {
        return;
    }
    if (buflist[index] == 0) {
        return;
    }

    printf("Offset: ");
    long offset = read_long();

    printf("Size: ");
    long size = read_long();

    if (offset + size > sizelist[index]) {
        return;
    }
    fread(buflist[index] + offset, size, 1, rd);
}

void show() {
    printf("Index: ");
    long index = read_long();
    if (index > 15) {
        return;
    }
    if (buflist[index] == 0) {
        return;
    }

    printf("Content: %s\n", buflist[index]);
}

void open() {
    if (rd_check != 0) {
        return;
    }
    rd = fopen("/dev/urandom", "rb");
    rd_check = 1;
}

void close() {
    if (rd_check == 0) {
        return;
    }
    fclose(rd);
    rd_check = 0;
    rd = 0;
}
