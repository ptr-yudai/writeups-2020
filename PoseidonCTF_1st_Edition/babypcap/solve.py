with open('queries.txt') as f:
  s = f.read().splitlines()

res = ''
y = 0
for line in s:
  ip = line.split('.')[:4]
  y = (y + 1) % 4
  if y != 0: continue
  lip = 0
  try:
      for i, x in enumerate(ip[::-1]):
          lip |= int(x) << 8 * i
  except:
      continue

  if int(ip[1]) > 255:
    res += chr(int(ip[0], 8))

print(res)
