char hoge[] = "\x55\x40\x89\xe5\x90\x90\x40\x89\xec\x5d";
char answer[] = "\x84\xd3\xb8\xca\xe2\x36...";
char rep_hoge[0x100];
char password[0x20];
char encrypted[0x20];

int main() {
  int i;
  char j;
  write(1, "Enter psasword: ", 0x10);

  for(i = 0; i < 0x100; i++) {
    box[i] = i;
  }
  j = 0;
  for(i = 0; i < 0x100; i++) {
    if (j >= 10) j = 0;
    rep_hoge[i] = hoge[j];
  }
  j = 0;
  for(i = 0; i < 0x100; i++) {
    j += rep_hoge[i] + box[i];
    char tmp = box[i];
    box[i] = box[j];
    box[j] = tmp;
  }

  char temp_pass[0x20];
  read(0, password, 0x20);

  for(i = 0; i < 0x20; i++) {
    password[i] = temp_pass[i];
  }
  j = 0;
  for(i = 0; i < 0x20; i++) {
    j += box[i + 1];
    char tmp = box[i+1];
    box[i+1] = box[j];
    box[j] = box[i+1];
    encrypted[i] = box[box[i+1] + box[j]] ^ password[i];
  }

  assert(memcmp(encrypted, answer, 0x20) == 0);
}
