with open("mixer", "rb") as f:
    buf = f.read()
for i in range(0x12000, 0x12000 + 0x791):
    buf = buf[:i] + bytes([buf[i] ^ 0x2a]) + buf[i+1:]
with open("unpacked", "wb") as f:
    f.write(buf)
