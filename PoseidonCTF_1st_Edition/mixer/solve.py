with open("./unpacked", "rb") as f:
    f.seek(0x12147)
    key = f.read(10)
    answer = f.read(0x20)

password = b''
box = [i for i in range(0x100)]

j = 0
for i in range(0x100):
    j = (j + key[i % 10] + box[i]) % 256
    box[i], box[j] = box[j], box[i]

j = 0
for i in range(0x20):
    j = (j + box[i+1]) % 256
    box[i+1], box[j] = box[j], box[i+1]
    password += bytes([
        box[(box[i+1] + box[j]) % 256] ^ answer[i]
    ])

print(password)
