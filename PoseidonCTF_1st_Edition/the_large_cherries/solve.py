from z3 import *
from ptrlib import *

magic = [BitVec("magic{}".format(i), 8) for i in range(8)]
s = Solver()

s.add(magic[3] + magic[0] == 0xab)
s.add(magic[3] == 0x37)
s.add(magic[1] ^ magic[2] == 0x5d)
s.add(magic[4] - magic[2] == 0x05)
s.add(magic[4] + magic[6] == 0xa2)
s.add(magic[5] == magic[6])
s.add(magic[6] == 0x30)
s.add(magic[7] == 0x7a)

r = s.check()
if r == sat:
    m = s.model()
else:
    print(r)
    exit(1)

secret = ['?' for i in range(8)]
for d in m.decls():
    secret[int(d.name()[5:])] = chr(m[d].as_long())

print(''.join(secret))

sock = Socket("poseidonchalls.westeurope.cloudapp.azure.com", 9003)
sock.sendlineafter(": ", ''.join(secret))
sock.sendlineafter(": ", ''.join(secret) + '\x00A')
sock.interactive()
