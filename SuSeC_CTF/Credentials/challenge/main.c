#include "main.h"

unsigned char key[0x20];
unsigned char iv[0x20];
SECRET *cred = NULL;

/**
 * Main program
 */
int set_username(void) {
  if (cred == NULL) {
    print("No credential set\n");
    return 1;
  }

  if (input("Username: ", cred->username, LEN_USERNAME - 1) == 0) {
    print("Enter username\n");
    return 1;
  }
  return 0;
}

int set_password(void) {
  EVP_CIPHER_CTX *ctx;
  char secret[LEN_PASSWORD];
  int len = 0, pad = 0;

  if (cred == NULL) {
    print("No credential set\n");
    return 1;
  }

  ctx = EVP_CIPHER_CTX_new();
  EVP_CIPHER_CTX_init(ctx);
  EVP_EncryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, key, iv);

  if (input("Password: ", cred->password, LEN_PASSWORD - 1) == 0) {
    print("Enter password\n");
    EVP_CIPHER_CTX_cleanup(ctx);
    EVP_CIPHER_CTX_free(ctx);
    return 1;
  }

  memset(secret, 0, LEN_PASSWORD);
  EVP_EncryptUpdate(ctx,
                    secret, &len,
                    cred->password, strlen(cred->password));
  EVP_EncryptFinal_ex(ctx, secret + len, &pad);
  EVP_CIPHER_CTX_cleanup(ctx);
  EVP_CIPHER_CTX_free(ctx);
  
  memcpy(cred->password, secret, len + pad);
  memset(secret, 0, LEN_PASSWORD);
}

void new_cred(void) {
  if (cred) {
    free(cred->username);
    free(cred->password);
    free(cred);
  }
  cred = (SECRET*)malloc(sizeof(SECRET));

  cred->username = (char*)malloc(LEN_USERNAME);
  if (set_username()) return;
  cred->password = (char*)malloc(LEN_PASSWORD);
  if (set_password()) return;
}

void del_cred(void) {
  if (cred) {
    free(cred->username);
    free(cred->password);
    free(cred);
    cred = NULL;
  } else {
    print("No credential set\n");
  }
}

int menu(void) {
  print("1. New credential\n2. Delete credential\nx. Exit\n");
  return read_int("> ");
}

void setup(void) {
  memset(key, 0, 0x10);
  memset(iv, 0, 0x10);
  input("Key: ", key, 0x11);
  input("IV: ", iv, 0x11);
}

/**
 * Entry point
 */
int main(void) {
  setup();

  while(1) {
    switch(menu()) {
    case 1: new_cred(); break;
    case 2: del_cred(); break;
    default: print("Bye!\n"); return 0;
    }
  }
}

/**
 * Utilities
 */
int input(const char* msg, char *buf, unsigned short size) {
  char *ptr;
  
  print(msg);
  if (read(0, buf, size) == 0) exit(1);
  
  ptr = strchr(buf, '\n');
  if (ptr) *ptr = 0;
  
  return strlen(buf);
}

int read_int(const char* msg) {
  char buf[0x20];
  
  memset(buf, 0, 0x20);
  input(msg, buf, 0x1f);
  
  return atoi(buf);
}

void print(const char* msg) {
  write(1, msg, strlen(msg));
}
