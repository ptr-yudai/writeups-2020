#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <openssl/engine.h>
#include <openssl/aes.h>
#include <stdlib.h>
#define LEN_USERNAME 0xa0
#define LEN_PASSWORD 0xa0

typedef struct {
  char *username;
  char *password;
} SECRET;

int input(const char*, char*, unsigned short);
int read_int(const char*);
void print(const char*);
