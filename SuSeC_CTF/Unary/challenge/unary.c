#include <stdio.h>
#include <stdlib.h>

__attribute__((constructor)) void setup(void) {
  setbuf(stdin, NULL);
  setbuf(stdout, NULL);
  setbuf(stderr, NULL);
}

void inc(int operand, int *result) { *result = operand + 1; }
void dec(int operand, int *result) { *result = operand - 1; }
void not(int operand, int *result) { *result = ~operand; }
void neg(int operand, int *result) { *result = -operand; }
static void (*ope[])(int, int*) = {inc, dec, not, neg};

void show_menu(void) {
  puts("0. EXIT");
  puts("1. x++");
  puts("2. x--");
  puts("3. ~x");
  puts("4. -x");
}

int read_int(char *msg) {
  int v;
  printf("%s", msg);
  if (scanf("%d", &v) != 1) exit(0);
  return v;
}

int main(void) {
  int operand, result, index, i;

  while(1) {
    show_menu();
    index = read_int("Operator: ");
    if (index == 0) break;
    operand = read_int("x = ");
    ope[index - 1](operand, &result);
    printf("f(x) = %d\n", result);
  }

  return 0;
}
