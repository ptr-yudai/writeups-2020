from z3 import *

def p32(l):
    return (l[0] & 0xff) | ((l[1] & 0xff) << 8) | ((l[2] & 0xff) << 16) | ((l[3] & 0xff) << 24)

answer = [
    0x74d8, 0xec55, 0x04b5, 0x421a, 0x6d11, 0x02ba, 0x055f, 0x8105,
    0x6c28, 0xeda0, 0x0499, 0x6ae0, 0x55e7, 0x18a9, 0x3591, 0x71d6,
    0xa864, 0x4537
]

ds = [BitVec(f"ds{j}", 32) for j in range(36)]
s = Solver()

for i in range(0x81):
    for j in range(9):
        jpp = (j + 1) % 9
        a = p32(ds[j*4:(j+1)*4])
        b = p32(ds[jpp*4:(jpp+1)*4])
        if i % 3 == 0:
            # c = (a | b) & (~a | ~b)
            c = (a | b) & ~(a & b)
            # d = ~(~c & ~0x24114514) & ~(c & 0x24114514)
            d = (c | 0x24114514) & ~(c & 0x24114514)
        elif i % 3 == 1:
            # c = ~(~a & ~b) & ~(a & b)
            c = (a | b) & ~(a & b)
            # d = (c & ~0x1919810) | (~c & 0x1919810)
            d = (~c & 0x1919810) | (c & (0xffffffff ^ 0x1919810))
        else:
            # c = (a & ~b) | (~a & b)
            c = (a & ~b) | (~a & b)
            # d = (c | 0x19260817) & (~c | ~0x19260817)
            d = (c | 0x19260817) & ~(c & 0x19260817)
        ds[j*4    ] = d & 0xff
        ds[j*4 + 1] = (d >> 8) & 0xff
        ds[j*4 + 2] = (d >> 16) & 0xff
        ds[j*4 + 3] = (d >> 24) & 0xff

for i in range(0x12):
    s.add(ds[i*2] == answer[i] & 0xff)
    s.add(ds[i*2+1] == answer[i] >> 8)

print("[+] solving...")
r = s.check()
if r == sat:
    m = s.model()
    print(m)
else:
    print(r)
    exit(1)

real_ds = [-1 for i in range(36)]
for d in m.decls():
    real_ds[int(d.name()[2:])] = m[d].as_long()

print(real_ds)

real_ds = [150, 165, 97, 105, 96, 117, 183, 96, 122, 120, 105, 183, 179, 97, 134, 89, 89, 96, 153, 154, 96, 97, 131, 117, 166, 96, 102, 158, 105, 89, 111, 107, 97, 104, 89, 112]

es = [-1 for i in range(36)]
for i in range(6):
    for j in range(6):
        es[6*i + j] = real_ds[6*j + i] - 0x55

with open("WMware/.src/disk", "rb") as f:
    f.seek(0x761)
    keypad = f.read(59)

print(keypad)
flag = b''
for c in es:
    if c > 59:
        flag += bytes([keypad[c - 0x30] - 0x20])
    else:
        flag += bytes([keypad[c]])

print(flag)
