unsigned int *ds; // data segment
unsigned char *es;

/**
   0x55555957, 0x55735555
 */

void copy() {
  int i, j;
  for(i = 0; i < 6; i++) {
    for(j = 0; j < 6; j++) {
      ds[6*j + i] = es[6*i + j] + 0x55;
    }
  }
}

void check_input() {
  unsigned int i, j, k, a, b, c;

  for(i = 0; i != 0x81; i++) {
    for(j = 0; j != 9; j++) {
      int jpp = (j + 1) % 9;
      switch(i % 3) {
      case 0:
        a = ds[j*4];
        b = ds[jpp*4];
        c = (a | b) & (~a | ~b);
        ds[j*4] = ~(~c & ~0x24114514) & ~(c & 0x24114514);
        break;
      case 1:
        a = ds[j*4];
        b = ds[jpp*4];
        c = ~(~a & ~b) & ~(a & b);
        ds[j*4] = (~c & 0x1919810) | (c & ~0x1919810);
        break;
      case 2:
        a = ds[j*4];
        b = ds[jpp*4];
        c = (a & ~b) | (~a & b);
        ds[j*4] = (c | 0x19260817) & (~c | ~0x19260817);
        break;
      }
    }
  }

  int fail = 0;
  for(i = 0; i != 0x12; i++) {
    if (ds[2*i - 0x12354cd] != ds[i*2]) i += 1;
  }

  if (fail == 0) {
    puts("Access");
  } else {
    puts("Fail");
  }
}
