<?php
error_reporting(0);
if ($_POST["text"]!=NULL) {
    $decode_data=$_POST["text"];
    // var_dump($_POST['text']);
    die(base64decode($decode_data));
}

function dir_count($filename){
    $depath = 0;
    foreach (explode('/', $filename) as $part) {
        if ($part == '..') {
            $depath--;
        } elseif ($part != '.') {
            $depath++;
        }
    }
    return $depath;
}

if ($_GET['filename']!=NULL) {
    $filename=$_GET['filename'];
    if(strlen($filename)>50){
        die("You're over the limit");
    }
    $filename=preg_replace("/(flag)|(proc)|(self)|(maps)/i","w&m",$filename);
    if(dir_count($filename)>0){
        echo file_get_contents('./hint/'.$filename);
    }
    else {
        echo "You're over the limit";
    }
}