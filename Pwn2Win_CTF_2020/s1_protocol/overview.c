int size_128 = 0x80;
int size_21  = 0x15;

// sub_401CAD
void bytes2long(mpz_t a, char *bytes, int len) {
  char *hex = alloca(len * 2);
  for(int i = 0; i < len; i++) {
    sprintf(&hex[i*2], "%02X", bytes[i]);
  }
  mpz_set_string(a, hex, 16);
}

void getRandomBytes(char *buf, int size) {
  FILE *fp = fopen("/dev/urandom", "r");
  fread(buffer, 1, size, fp);
  fclose(fp);
}

int main() {
  mpz_t p, q, n, e, r, mod, result;
  mpz_init(p);
  mpz_init(q);
  mpz_init(n);
  mpz_init(e);
  mpz_init(x);
  mpz_init(mod);
  mpz_init(result);

  char *rndBuffer = alloca(size_128);
  char *rnd21bytes = alloca(size_21);
  char *flag = alloca(size_128);

  // generate p and q
  do {
    getRandomBytes(rnd21bytes, size_21);
    for(int i = 0; i < size_128 / size_21; i++) {
      memcpy(&rndBuffer[i*size_21], size_21, rnd21bytes);
    }
    getRandomBytes(&rndBuffer[i*size_21], size_128 % size_21);
    bytes2long(p, rndBuffer, size_128);
    mpz_setbit(p, size_128 * 8 - 2);
    mpz_setbit(p, size_128 * 8 - 1);
    mpz_setbit(p, 0);
  } while(mpz_probab_prime_p(p, 30));
  do {
    getRandomBytes(rndBuffer, size_128);
    bytes2long(q, rndBuffer, size_128);
    mpz_setbit(q, size_128 * 8 - 2);
    mpz_setbit(q, size_128 * 8 - 1);
    mpz_setbit(q, 0);
  } while(mpz_probab_prime_p(q, 30));

  FILE *fp = fopen("flag.txt", "r");
  int len_flag = fread(flag, 1, size_128, fp);
  fclose(fp);

  /*
    e = bytes2long(flag)
    r = bytes2long(os.urandom(0x7f))

    n = p * q
    mod = n ** 3
    r = pow(r, n, mod)
    n += 1
    mod *= n
    result = pow(n, e, mod)

    print(n, r, result)
   */
  bytes2long(e, flag, len_flag);
  getRandomBytes(rndBuffer, 0x7f);
  bytes2long(r, rndBuffer, 0x7f);

  mpz_mul(n, p, q);
  mpz_mul(mod, n, n);
  mpz_mul(mod, n, mod);
  mpz_powm(r, r, n, mod);

  mpz_add_ui(n, n, 1);
  mpz_mul(mod, n, mod);
  mpz_powm(result, n, e, mod);

  // n = p * q
  // r = r^n mod n^3
  // c = (n+1)^e mod (n^3 * (n+1))
  gmp_printf("%Zd\n\n", n);
  gmp_printf("%Zd\n\n", r);
  gmp_printf("%Zd\n\n", result);
}
