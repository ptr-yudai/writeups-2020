
ulonglong TA_InvokeCommandEntryPoint
                    (longlong cmd_id,uint param_2,undefined8 params,ulonglong param_4)

{
  uint uVar1;
  uint *puVar2;
  ulonglong uVar3;
  longlong lVar4;
  ulonglong retval;
  uint local_44;
  undefined params_copy [64];
  
  if (cmd_id == 1) {
    lVar4 = FUN_00102290((ulonglong)param_2);
    if (lVar4 != 0) {
      retval = 0;
      FUN_001001dc(*(undefined8 *)(lVar4 + 8));
      Destructor((ulonglong)param_2);
      goto LAB_001024e4;
    }
  }
  else {
    if (cmd_id == 0) {
      lVar4 = FUN_00102290((ulonglong)param_2);
      if (lVar4 == 0) {
        if (DAT_00117498 == '\0') {
          DAT_00117498 = '\x01';
          FUN_00100380();
          FUN_00100488();
          FUN_00108464(&DAT_0010f498,0x8000);
          FUN_00102e44();
          uVar3 = FUN_00100110();
          retval = uVar3 & 0xffffffff;
          if ((int)uVar3 != 0) goto LAB_001024e4;
        }
        puVar2 = (uint *)FUN_00101dd4(0x20,0x80000000);
        if (puVar2 == (uint *)0x0) {
          retval = 0xffff000c;
          goto LAB_001024e4;
        }
        *puVar2 = param_2;
        *(undefined8 *)(puVar2 + 2) = 0;
        *(undefined8 *)(puVar2 + 4) = 0;
        *(undefined **)(puVar2 + 6) = PTR_DAT_0010c390;
        *(uint **)PTR_DAT_0010c390 = puVar2;
        PTR_DAT_0010c390 = (undefined *)(puVar2 + 4);
      }
      lVar4 = FUN_00102290((ulonglong)param_2);
      if (lVar4 != 0) {
        FUN_00102330(params_copy,&local_44,params);
        uVar1 = local_44;
        DAT_0010c448 = local_44;
        FUN_00100388(&DAT_0010c450,params_copy,0x40);
        retval = Constructor((ulonglong)uVar1,params_copy,lVar4 + 8);
        retval = retval & 0xffffffff;
        FUN_001022b8(params,(ulonglong)local_44,params_copy);
        if ((int)retval != 0) {
          Destructor((ulonglong)param_2);
        }
        goto LAB_001024e4;
      }
    }
    else {
      if (cmd_id != 2) {
        retval = 0xffffffff;
        FUN_00101e54(0);
        goto LAB_001024e4;
      }
      lVar4 = FUN_00102290((ulonglong)param_2);
      if (lVar4 != 0) {
        FUN_00102330(params_copy,&local_44,params);
        uVar1 = local_44;
        DAT_0010c448 = local_44;
        FUN_00100388(&DAT_0010c450,params_copy,0x40);
        retval = inc_value(*(undefined8 *)(lVar4 + 8),param_4 & 0xffffffff,(ulonglong)uVar1,
                           params_copy);
        retval = retval & 0xffffffff;
        FUN_001022b8(params,(ulonglong)local_44,params_copy);
        goto LAB_001024e4;
      }
    }
  }
  retval = 0xffff0007;
LAB_001024e4:
  DAT_0010c448 = 0;
  FUN_0010242c();
  return retval;
}



undefined8 inc_value(undefined8 param_1,int param_2,int param_type,undefined8 *params)

{
  undefined8 retval;
  uint value [2];
  undefined8 local_8;
  
  if (param_2 == 0) {
    IMSG("inc_value",0x6c,1,1,"Executing function at %p",0x100020);
    if (param_type == 0x665) {
      value[0] = 0;
      TEE_MemMove(value,*params,(ulonglong)*(uint *)(params + 1));
      IMSG("inc_value",0x72,2,1,"Got value: %u from NW",(ulonglong)value[0]);
      value[0] = value[0] + 1;
      IMSG("inc_value",0x74,2,1,"Increase value to: %u");
      TEE_MemMove(params[2],value,4);
      local_8 = 0x100020;
      TEE_MemMove(params[4],&local_8,4);
      retval = 0;
    }
    else {
      retval = 0xffff0006;
    }
    return retval;
  }
  return 0xffff0006;
}



void get_secret(void)

{
  uint ret;
  int ret_;
  undefined8 uVar1;
  uint local_11c;
  undefined auStack280 [16];
  undefined *local_108;
  undefined4 local_100;
  undefined auStack252 [52];
  undefined op [200];
  
  IMSG("get_secret",0x38,1,1,"Here is the key, Android! Long live the rebellion!");
  memset(op,0,200);
  uVar1 = FUN_00100388(auStack280,&DAT_0010a6d8,0x10);
  ret = TEEC_InvokeCommand(uVar1,0,0,0,&DAT_0010c428,&local_11c);
  if (ret != 0) {
    IMSG("get_secret",0x4c,1,1,"TEE_InvokeCommand failed with code 0x%x origin 0x%x",(ulonglong)ret,
         (ulonglong)local_11c);
  }
  memset(auStack252,0,0x34);
  local_100 = 0xbe;
  local_108 = op;
  ret_ = FUN_00101a80(DAT_0010c428,0,0,6,&local_108,&local_11c);
  if (ret_ != 0) {
    IMSG("get_secret",0x5a,1,1,"WTF?!");
  }
  IMSG("get_secret",0x5c,1,1,&DAT_0010aace,op);
  FUN_001017f0(DAT_0010c428);
  FUN_00101e54(0);
  return;
}

