from ptrlib import *

with open("engine", "rb") as f:
    f.seek(0x2220)
    b = f.read(0x012c)

encoded = b''
for i in range(0, len(b), 4):
    encoded += bytes([b[i]])

v = 0xffffff91
decoded = b''
esi = 0
ecx = 0
edx = 0x91
r8 = 0x8080808080808081
for esi in range(len(encoded)):
    decoded += bytes([encoded[esi] ^ (edx & 0xff)])
    ecx = ((edx & 0xff) + esi) & 0xffffffff
    edx = (((ecx * r8) >> (64 + 7)) + ecx) & 0xffffffff

print(decoded)
