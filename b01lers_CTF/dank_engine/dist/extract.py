import struct

pack = open('DankEngine.pck', 'rb')
pack.read(0x54)

file_count = struct.unpack('<I', pack.read(4))[0]

for i in range(file_count):
    name_len = struct.unpack('<I', pack.read(4))[0]
    name = pack.read(name_len).rstrip(b'\x00').decode('utf-8')
    offset = struct.unpack('<I', pack.read(4))[0]
    pack.read(4)
    size = struct.unpack('<I', pack.read(4))[0]
    pack.read(20)
    print(name)

    x = pack.tell()
    pack.seek(offset)
    content = pack.read(size)
    if ".import" in name:
        name = name.replace(".import/", "")
    with open("extracted/{}".format(name[6:]), "wb") as f:
        f.write(content)
    pack.seek(x)
