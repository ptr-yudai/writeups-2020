with open("output.txt", "r") as f:
    encoded = f.read()

def encrypt(inp, key):
    out = ""
    for c in inp:
        if ord('a') <= ord(c) <= ord('z'):
            out += chr(ord('a') + (ord(c) - ord('a') + key) % 26)
        elif ord('A') <= ord(c) <= ord('Z'):
            out += chr(ord('A') + (ord(c) - ord('A') + key) % 26)
        elif ord('가') <= ord(c) <= ord('힣'):
            out += chr(ord('가') + (ord(c) - ord('가') + key) % 11172)
        else:
            out += c
    return out
    
for key in range(11172):
    out = ''
    for c in encoded:
        if ord('a') <= ord(c) <= ord('z'):
            out += chr(ord('a') + (ord(c) - ord('a') - key) % 26)
        elif ord('A') <= ord(c) <= ord('Z'):
            out += chr(ord('A') + (ord(c) - ord('A') - key) % 26)
        elif ord('가') <= ord(c) <= ord('힣'):
            out += chr(ord('가') + (ord(c) - ord('가') - key) % 11172)
        else:
            out += c

    if not out.startswith("Bingo"):
        continue
    o_ok = False
    t_ok = False
    for c in out:
        if 0xc544 <= ord(c) <= 0xC655:
            o_ok = True
        elif 0xc790 <= ord(c) <= 0xCE6D:
            t_ok = True
    if o_ok and t_ok:
        #print(key, out)
        print(out[10])
