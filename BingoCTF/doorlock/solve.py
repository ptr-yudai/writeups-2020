from z3 import *

s = Solver()

val1 = BitVec("val1", 32)
val2 = BitVec("val2", 32)
val3 = BitVec("val3", 32)
val4 = BitVec("val4", 32)

s.add((val1 + val2) ^ (val3 + val4) + val1 == 71717953)
s.add((val1 + val4) ^ (val2 + val3) + val2 == 232749735)
s.add((val2 + val4) ^ (val1 + val3) + val3 == 4310406)
s.add((val1 ^ val2 ^ val3 ^ val4) + val4 == 145747802)

while True:
    r = s.check()
    if r == sat:
        m = s.model()
        a, b, c, d = m[val1].as_long(), m[val2].as_long(), m[val3].as_long(), m[val4].as_long()
        print("{:08}{:08}{:08}{:08}".format(a, b, c, d))
        s.add(Not(And(val1 == a, val2 == b, val3 == c, val4 == d)))
    else:
        print(r)
        break
