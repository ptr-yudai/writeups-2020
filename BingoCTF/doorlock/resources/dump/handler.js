function validate_input() {
    let inputs = $("#inputed_password").val();
    if (inputs.length != 32) return;
    let val1 = Number(inputs.substr(0, 8));
    let val2 = Number(inputs.substr(8, 8));
    let val3 = Number(inputs.substr(16, 8));
    let val4 = Number(inputs.substr(24, 8));
    
    let result = [];
    result.push(((val1 + val2) ^ (val3 + val4) + val1) >>> 0);
    result.push(((val1 + val4) ^ (val2 + val3) + val2) >>> 0);
    result.push(((val2 + val4) ^ (val1 + val3) + val3) >>> 0);
    result.push(((val1 ^ val2 ^ val3 ^ val4) + val4) >>> 0);

    if (result[0] == 71717953 && result[1] == 232749735 && result[2] == 4310406 && result[3] == 145747802) {
        alert("Flag is Bingo{" + inputs + "}");
    }
}

function init() {
    for (let n=1; n<=9; n++) {
        $("#pwd"+n).on("click", function() {
            $("#inputed_password").val($("#inputed_password").val() + n);
            validate_input();
        });
    }
}