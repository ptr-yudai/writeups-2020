from ptrlib import *
import ctypes

glibc = ctypes.cdll.LoadLibrary('./libc-2.27.so')

flag = ''
while True:
    sock = Socket("fun1.bingo.hypwnlab.com:10101")

    sock.sendlineafter(": ", "LOVEHODU")
    r = sock.recvlineafter("srand(")[:-1]
    seed = int(r)
    glibc.srand(seed)

    v = glibc.rand() & 0xff
    target = xor("\x1e\x01\xc7\xa2\x02\xc2\xa8\x15", chr(v))
    key = xor(target, "\x55\x48\x89\xe5\x48\x83\xec\x40")

    sock.sendlineafter(": ", key)
    sock.sendlineafter(": ", flag + 'A')

    delta = int(sock.recvlineafter(": "))
    flag += chr(0x41 + delta)
    print(flag)

    sock.close()
