with open("1.secret", "rb") as f:
    buf = f.read()

ofs = 0xb8a
key = b'KINGJADU'
for i in range(0x14):
    print(hex(buf[ofs+i]))
    buf = buf[:ofs+i] + bytes([buf[ofs+i] ^ key[i%8]]) + buf[ofs+i+1:]

with open("2.secret", "wb") as f:
    f.write(buf)
