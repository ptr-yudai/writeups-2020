with open("secret", "rb") as f:
    buf = f.read()

key = b'LOVEHODU'
for i in range(0x14):
    buf = buf[:0xcd2+i] + bytes([buf[0xcd2+i] ^ key[i%8]]) + buf[0xcd3+i:]

with open("1.secret", "wb") as f:
    f.write(buf)
