from ptrlib import *
import ctypes
glibc = ctypes.cdll.LoadLibrary('/lib/x86_64-linux-gnu/libc-2.27.so')

seed = int(input("> "))
glibc.srand(seed)
v = glibc.rand() & 0xff

print(hex(v))
target = xor("\x1e\x01\xc7\xa2\x02\xc2\xa8\x15", chr(v))
key = xor(target, "\x55\x48\x89\xe5\x48\x83\xec\x40")[::-1]

print(key.hex())
