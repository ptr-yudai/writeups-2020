import angr
import claripy
from logging import getLogger, WARN

getLogger("angr").setLevel(WARN + 1)
getLogger("claripy").setLevel(WARN + 1)

flag = claripy.BVS("flag", 8 * 33)
p = angr.Project("./Automorphism")
state = p.factory.entry_state(args=[p.filename], stdin=flag)
simgr = p.factory.simulation_manager(state)

flags = list(flag.chop(8))
state.add_constraints(flags[0] == 'T')
state.add_constraints(flags[1] == 'S')
state.add_constraints(flags[2] == 'G')
state.add_constraints(flags[3] == 'L')
state.add_constraints(flags[4] == 'I')
state.add_constraints(flags[5] == 'V')
state.add_constraints(flags[6] == 'E')
state.add_constraints(flags[7] == '{')
state.add_constraints(flags[31] == '}')

for i, c in enumerate(flag.chop(8)):
    state.add_constraints(
        claripy.Or(
            claripy.And(
                0x20 <= c, c <= 0x7f
            ),
            c == 0
        )
    )

simgr.explore(find=0x4013b7)

try:
    found = simgr.found[0]
    print(found.solver.eval(flag, cast_to=bytes))
except IndexError:
    print("Not Found")
