import base64
from ptrlib import *

TYPE_DIR   = 0
TYPE_FILE  = 1
TYPE_CLON  = 2
TYPE_SYM   = 3
TYPE_HARD  = 4
TYPE_LABEL = 5

def segment(type, id, offset, length):
    return p32(type) + p32(id) + p32(offset) + p32(length)
def directory(name, entries):
    d = p32(len(name)) + p32(len(entries)) + name
    for entry in entries:
        d += p32(entry)
    return d
def file(name, contents):
    return p32(len(name)) + p32(len(contents)) + name + contents
def clone(name, inode):
    return p32(len(name)) + p32(inode) + name
def symlink(name, target):
    return p32(len(name)) + p32(len(target)) + name + target
def hardlink(name, target):
    return p32(len(name)) + p32(target) + name
def label(name):
    return name

import random

def randstr(l):
    return b''.join([bytes([random.randrange(0, 0x100)]) for i in range(l)])

while True:
    n = random.randint(1, 30)

    data = [
        (TYPE_DIR, 0, directory(b"", [i+1 for i in range(n)])),
    ]
    fileList = []
    dirList = []
    for i in range(n):
        type = random.randrange(0, 4)
        name = randstr(random.randint(0, 20))
        if type == TYPE_DIR:
            print(f"DIR: id={i+1}, name={name} / list={fileList}")
            dirList.append(i+1)
            data.append((
                TYPE_DIR, i+1,
                directory(name, fileList)
            ))
        elif type == TYPE_FILE:
            cont = randstr(random.randint(0, 20))
            print(f"FILE: id={i+1}, name={name} / contents={cont}")
            fileList.append(i+1)
            data.append((
                TYPE_FILE, i+1,
                file(name, cont)
            ))
        elif type == TYPE_CLON:
            to = random.randint(1, n)
            print(f"CLONE: id={i+1}, name={name} / to={to}")
            fileList.append(i+1)
            data.append((
                TYPE_CLON, i+1,
                clone(name, to)
            ))
        elif type == TYPE_SYM or type == TYPE_HARD:
            if n == 1:
                continue
            to = i + 1
            while to == i + 1:
                to = random.randint(1, n)
            print(f"HARD: id={i+1}, name={name} / to={to}")
            fileList.append(i+1)
            data.append((
                TYPE_HARD, i+1,
                hardlink(name, to)
            ))

    fs =  b"FEATHER\x00"
    fs += p32(len(data))
    ofs = 0
    for datum in data:
        fs += segment(type=datum[0], id=datum[1], offset=ofs, length=len(datum[2]))
        ofs += len(datum[2])
    for datum in data:
        fs += datum[2]

    b = base64.b64encode(fs)

    sock = Process("./feather")
    sock.recvline()

    sock.sendline(b + b"\n")

    time.sleep(0.3)
    r = sock._poll()
    if r == -11:
        print("[!] ===== CRASH =====")
        print(data)
        print(b)
        break
    elif r != -6:
        print(b)
    sock.close()
