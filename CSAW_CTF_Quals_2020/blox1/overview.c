char data_403700[] = {
  3, 2, 3, 2, 2,
  0, 1, 3, 1, 0,
  0, 2, 2, 2, 2,
  0, 3, 0, 1, 0
};
char data_403718[] = {
  1, 2, 3,
  1, 7, 4,
  1, 1, 1,
  3, 7, 5
}
char data_403730[] = {
  2, 2, 2, 2, 2,
  3, 1, 2, 1, 3,
  3, 1, 1, 1, 1,
  3, 1, 3, 1, 3
};
char data_403748[] = {
  5, 2, 3,
  5, 3, 2,
  1, 5, 1,
  4, 3, 4
};

// 0x402637
int huga(int x) {
  for(int i = 0; i < 3; i++) {
    char a = 0, b = 0;
    for(int j = 0; j < 5; j++) {
      if (board[j+15][x*3+i]) { // block exists
        a ^= (j + 1);
        b += 1;
      }
    }
    if (data_403718[x*3 + i] != a) return 0;
    if (data_403748[x*3 + i] != b) return 0;
  }
}

// 0x402554
int hoge(int x) {
  for(int i = 0; i < 5; i++) {
    char a = 0, b = 0;
    for(int j = 0; j < 3; j++) {
      if (board[i+15][x*3+j]) { // block exists
        a ^= (j + 1);
        b += 1;
      }
    }
    if (data_403700[x*5 + i] != a) return 0;
    if (data_403730[x*5 + i] != b) return 0;
  }
  return 1;
}

int check_cheat_codes() {
  for(int i = 0; i < 4; i++) {
    if (hoge(i) == 0) return 0;
    if (huga(i) == 0) return 0;
  }
}
