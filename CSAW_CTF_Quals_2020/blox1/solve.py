from z3 import *
from ptrlib import *

array1 = [
  3, 2, 3, 2, 2,
  0, 1, 3, 1, 0,
  0, 2, 2, 2, 2,
  0, 3, 0, 1, 0
]
array2 = [
  1, 2, 3,
  1, 7, 4,
  1, 1, 1,
  3, 7, 5
]
array3 = [
  2, 2, 2, 2, 2,
  3, 1, 2, 1, 3,
  3, 1, 1, 1, 1,
  3, 1, 3, 1, 3
]
array4 = [
  5, 2, 3,
  5, 3, 2,
  1, 5, 1,
  4, 3, 4
]

s = Solver()
board = [
    [BitVec(f"flag[{i},{j}]", 8) for j in range(12)]
    for i in range(20)
]

for i in range(20):
    for j in range(12):
        if i < 15:
            s.add(board[i][j] == 0)
        else:
            s.add(Or(board[i][j] == 0, board[i][j] == 1))

for x in range(4):
    # huga
    for i in range(3):
        a = b = BitVec(0, 8)
        for j in range(5):
            a ^= board[j+15][x*3+i] * (j + 1)
            b += board[j+15][x*3+i]
        s.add(a == array2[x*3+i])
        s.add(b == array4[x*3+i])

    # hoge
    for i in range(5):
        a = b = BitVec(0, 8)
        for j in range(3):
            a ^= board[i+15][x*3+j] * (j + 1)
            b += board[i+15][x*3+j]
        s.add(a == array1[x*5+i])
        s.add(b == array3[x*5+i])

r = s.check()
if r == sat:
    m = s.model()
    output = ""
    for i in range(20):
        for j in range(12):
            output += str(m[board[i][j]])
        output += "\n"
    print(output)
else:
    print(r)
    exit()

mem = b''
for i in range(20):
    for j in range(12):
        if m[board[i][j]] == 1:
            mem += b'\x06'
        else:
            mem += b'\x00'

for i, block in enumerate(chunks(mem, 8, b'\x00')):
    print("set *{}={}".format(hex(0x4037e0 + i*8), hex(u64(block))))
