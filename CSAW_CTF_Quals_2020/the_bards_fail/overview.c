typedef struct {
  char type;   // +0
  short nazo1; // +2
  int nazo2;   // +4
  char name[0x20]; // +8
  double nazo3;    // +28h
} GoodWeapon;

typedef struct {
  char type;    // +0
  double nazo3; // +8
  int nazo2;    // +10h
  short nazo1;  // +14h
  char name[0x20]; // +16h
} EvilWeapon;

void good(GoodWeapon *weapon)
{
  switch(readword()) {
  case '1':
    weapon->type = 0x6c;
    break;
  case '2':
    weapon->type = 0x78;
    break;
  default:
    exit(1);
  }

  weapon->nazo1 = 20;
  weapon->nazo2 = 15;
  weapon->nazo3 = 18.0;
  read(0, weapon->name, 0x20);
}

void evil(EvilWeapon *weapon)
{
  switch(readword()) {
  case '1':
    weapon->type = 0x63;
    break;
  case '2':
    weapon->type = 0x73;
    break;
  default:
    exit(1);
  }

  weapon->nazo1 = 20;
  weapon->nazo2 = 15;
  weapon->nazo3 = 18.0;
  read(0, weapon->name, 0x20);
}

int decide(char *buf, int n)
{
  switch(readword()) {
  case 'g':
    bardList[n] = 'g';
    good((GoodWeapon*)buf);
    return 0x30;
  case 'e':
    bardList[n] = 'e';
    good((EvilWeapon*)weapon);
    return 0x38;
  default:
    exit(1);
  }
}

void bard()
{
}
