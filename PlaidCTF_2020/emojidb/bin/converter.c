#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <unistd.h>

int main(int argc, char **argv) {
  int i;
  unsigned char out[0x10] = {0};
  unsigned char in[0x10] = {0};
  setlocale(0, "en_US.UTF-8");

  if (argc < 2) {
    printf("Usage: %s [1|2]\n", argv[0]);
    return 1;
  }

  if (argv[1][0] == '1') {
    read(0, in, 0x10);
    mbstowcs((wchar_t*)out, in, 0x10);
    write(1, out, 8);
  } else {
    read(0, in, 8);
    wcstombs(out, (wchar_t*)in, 0x10);
    write(1, out, 0x10);
  }
  return 0;
}
