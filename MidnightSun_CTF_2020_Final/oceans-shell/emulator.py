from unicorn import *
from unicorn.x86_const import *
from capstone import *
from pwn import *
from arc4 import ARC4

HALT_ADDRESS = 0x8000000
CODE_ADDRESS = 0x1000000
CODE_SIZE = 4 * 0x1000

STACK_ADDRESS = 0x4000000
STACK_SIZE = 8 * 0x1000

BLOCKS_SIZE = 0x1000
MESSAGE_ADDRESS = 0x2000000
OUT_ADDRESS = 0x3000000
SECRET_ADDRESS = 0xF000000

def hook_rdtsc(uc, address, size, user_data):
    mu, _ = user_data
    mu.reg_write(UC_X86_REG_RAX, 0)
    mu.reg_write(UC_X86_REG_RDX, 0)
    rip = mu.reg_read(UC_X86_REG_RIP)
    mu.reg_write(UC_X86_REG_RIP, rip + size)

BLOCKLIST = {
    'rdtsc': hook_rdtsc
}

def hook_code64(uc, address, size, user_data):
    mu, md = user_data
    if address == HALT_ADDRESS:
        mu.emu_stop()
        return

    instruction = mu.mem_read(address, size)
    for i in md.disasm(instruction, address):
        if i.mnemonic in BLOCKLIST:
            BLOCKLIST[i.mnemonic](uc, address, size, user_data)

# callback for tracing memory access (READ or WRITE)
def hook_mem_access(uc, access, address, size, value, user_data):
    #print(hex(address), size, access, UC_MEM_READ)
    if access == UC_MEM_READ:
        if address in range(SECRET_ADDRESS, SECRET_ADDRESS+BLOCKS_SIZE):
            user_data['touched'] = True

def emulate_code(code, message, secret, rc4_key, can_touch_secret=False):
    assert len(code) <= CODE_SIZE
    try:
        # Initialize emulator in X86-64bit mode
        md = Cs(CS_ARCH_X86, CS_MODE_64)
        mu = Uc(UC_ARCH_X86, UC_MODE_64)

        secret_guard = {
            'touched': False
        }

        mu.mem_map(HALT_ADDRESS, BLOCKS_SIZE, UC_PROT_READ | UC_PROT_EXEC)
        mu.mem_write(HALT_ADDRESS, b'\0'*BLOCKS_SIZE)

        mu.mem_map(CODE_ADDRESS, CODE_SIZE, UC_PROT_READ | UC_PROT_EXEC)
        mu.mem_write(CODE_ADDRESS, code.ljust(CODE_SIZE, b'\0'))

        # setup stack
        mu.mem_map(STACK_ADDRESS, STACK_SIZE, UC_PROT_READ | UC_PROT_WRITE)
        mu.mem_write(STACK_ADDRESS, struct.pack('<Q', HALT_ADDRESS).rjust(STACK_SIZE, b'\0'))
        mu.reg_write(UC_X86_REG_RSP, STACK_ADDRESS + STACK_SIZE - 8)

        mu.mem_map(MESSAGE_ADDRESS, BLOCKS_SIZE, UC_PROT_READ)
        mu.mem_write(MESSAGE_ADDRESS, message.ljust(BLOCKS_SIZE, b'\0'))

        fix_length_key = rc4_key.ljust(16, b'\0')
        rc4 = ARC4(fix_length_key)
        ciphertext = fix_length_key + rc4.encrypt(secret)
        mu.mem_map(SECRET_ADDRESS, BLOCKS_SIZE, UC_PROT_READ)
        mu.mem_write(SECRET_ADDRESS, ciphertext.ljust(BLOCKS_SIZE, b'\0'))
        mu.mem_map(OUT_ADDRESS, BLOCKS_SIZE, UC_PROT_WRITE)
        mu.mem_write(OUT_ADDRESS, b'\0'*BLOCKS_SIZE)

        # initialize machine registers
        mu.reg_write(UC_X86_REG_RAX, 0)
        mu.reg_write(UC_X86_REG_RBX, 0)
        mu.reg_write(UC_X86_REG_RCX, 0)
        mu.reg_write(UC_X86_REG_RDX, 0)
        mu.reg_write(UC_X86_REG_RSI, 0)
        mu.reg_write(UC_X86_REG_RDI, 0)
        mu.reg_write(UC_X86_REG_R8,  0)
        mu.reg_write(UC_X86_REG_R9,  0)
        mu.reg_write(UC_X86_REG_R10, 0)
        mu.reg_write(UC_X86_REG_R11, 0)
        mu.reg_write(UC_X86_REG_R12, 0)
        mu.reg_write(UC_X86_REG_R13, 0)
        mu.reg_write(UC_X86_REG_R14, 0)
        mu.reg_write(UC_X86_REG_R15, 0)

        mu.hook_add(UC_HOOK_CODE, hook_code64, (mu, md), CODE_ADDRESS, CODE_ADDRESS + CODE_SIZE)
        mu.hook_add(UC_HOOK_CODE, hook_code64, (mu, md), HALT_ADDRESS, HALT_ADDRESS + BLOCKS_SIZE)

        mu.hook_add(UC_HOOK_MEM_WRITE, hook_mem_access)
        mu.hook_add(UC_HOOK_MEM_READ, hook_mem_access, user_data=secret_guard)

        try:
            mu.emu_start(CODE_ADDRESS, CODE_ADDRESS + len(code), timeout=5*UC_SECOND_SCALE, count=1000000)
        except UcError as e:
            print('Error', e)
            return (False, 'Error during emulation')

        if can_touch_secret or not secret_guard['touched']:
            return (True, mu.mem_read(OUT_ADDRESS, 0x1000))
        else:
            return (False, 'Tried to touch secrets')

    except UcError as e:
        #print('Error', e)
        return (False, 'Error during emulation')
