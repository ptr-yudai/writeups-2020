from sage.all import *
import re

N = 166340340660877519226247260987065058211371932250499241074633026863387096385721863889904212902371869444274842128829082337552709506851212430322777753756706525557339442213042322434951929492350910439291882068704178535251621706072605607671593003649095340546077718624006859019891494745600258808326203381366558360403
with open("output.txt", "r") as f:
    C = eval(re.findall("(\[.+\])", f.read())[0])

def gcd(a, b): 
    while b:
        a, b = b, a % b
    return a.monic()

def franklinreiter(C1, C2, e, N, a, b):
    P, X = PolynomialRing(Zmod(N), name='X').objgen()
    g1 = (a*X + b)**e - C1
    g2 = X**e - C2
    result = -gcd(g1, g2).coefficients()[0]
    return int(result)

"""
c0 = (Z)^3 mod N
c1 = (Z + 's'^3)^3 mod N
Z = 1 * m1 + 's'^3
"""
Z = franklinreiter(C[1], C[0], 3, N, 1, ord('s')**3)
flag = ""
for c in C:
    for m in range(0x100):
        if pow((Z+m**3), 3, N) == c:
            flag += chr(m)
            break
print(flag)
