with open("secure_db", "rb") as f:
    buf = f.read()

buf = buf[:0x20] + b'\xa0' + buf[0x21:]

with open("fixed_secure_db", "wb") as f:
    f.write(buf)
