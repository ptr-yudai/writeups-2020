from ptrlib import *

with open("shellcode.bin", "rb") as f:
    shellcode = f.read()

sclist = []
for block in chunks(shellcode, 8, b'\x00'):
    sclist.append(u64(block, type=float))
print(sclist)
