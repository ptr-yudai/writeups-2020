from ptrlib import *

MODE_MEM2REG = 0x7d # memory --> register
MODE_REG2MEM = 0x7c # register --> memory
MODE_REG2REG = 0x7b # register --> register
MODE_IMM     = 0x7a # immediate --> register
TYPE_LONG  = 4
TYPE_INT   = 3
TYPE_SHORT = 2
TYPE_CHAR  = 1

def disasm(code):
    output = ''
    pos = 0
    while pos < len(code):
        ope = code[pos]
        if ope == 0x11: # MOVE
            output += "mov "
            mode = code[pos+1]
            if mode == MODE_MEM2REG:
                output += f"R{code[pos+3]}, [0x{code[pos+2]:x}]"
                pos += 4
            elif mode == MODE_REG2MEM:
                output += f"[{code[pos+3]}], R{code[pos+2]}"
                pos += 4
            elif mode == MODE_REG2REG:
                output += f"R{code[pos+3]}, R{code[pos+2]}"
                pos += 4
            else:
                type = code[pos+2]
                if type == TYPE_LONG:
                    output += f"R{code[pos+10]}, 0x{u64(code[pos+3:pos+10]):x}"
                    pos += 11
                elif type == TYPE_INT:
                    output += f"R{code[pos+7]}, 0x{u64(code[pos+3:pos+7]):x}"
                    pos += 8
                elif type == TYPE_INT:
                    output += f"R{code[pos+5]}, 0x{u64(code[pos+3:pos+5]):x}"
                    pos += 6
                elif type == TYPE_INT:
                    output += f"R{code[pos+4]}, 0x{code[pos+3]:x}"
                    pos += 5

        elif ope == 0x14:
            output += "add "
            mode = code[pos+1]
            if mode == MODE_REG2REG:
                output += f"R{code[pos+3]}, R{code[pos+2]}"
                pos += 4
            else:
                output += f"R{code[pos+9]}, 0x{u64(code[pos+2:pos+9]):x}"
                pos += 10

        elif ope == 0x15:
            output += "sub "
            mode = code[pos+1]
            if mode == MODE_REG2REG:
                output += f"R{code[pos+3]}, R{code[pos+2]}"
                pos += 4
            else:
                output += f"R{code[pos+9]}, 0x{u64(code[pos+2:pos+9]):x}"
                pos += 10

        elif ope == 0x16:
            output += "xor "
            mode = code[pos+1]
            if mode == MODE_REG2REG:
                output += f"R{code[pos+3]}, R{code[pos+2]}"
                pos += 4
            else:
                output += f"R{code[pos+9]}, 0x{u64(code[pos+2:pos+9]):x}"
                pos += 10

        elif ope == 0x19:
            output += "cmp "
            mode = code[pos+1]
            size = code[pos+2]
            if mode == MODE_MEM2REG:
                output += f"R{code[pos+3]}, [0x{code[pos+2]:x}]"
                pos += 4
            elif mode == MODE_REG2MEM:
                output += f"[{code[pos+3]}], R{code[pos+2]}"
                pos += 4
            elif mode == MODE_REG2REG:
                output += f"R{code[pos+3]}, R{code[pos+2]}"
                pos += 4
            elif mode == MODE_IMM:
                output += f"R{code[pos+3+size]}, 0x{u64(code[pos+3:pos+3+size]):x}"
                pos += 4 + size

        elif ope == 0x1c:
            output += f"jz {code[pos+1]}"
            pos += 3

        elif ope == 0x21:
            rw = code[pos+1]
            mode = code[pos+2]
            fd = code[pos+3]
            size = code[pos+5]
            if rw == 0:
                output += f"read({fd}, [0x{code[pos+4]:x}], 0x{size:x})"
            else:
                output += f"write({fd}, R{code[pos+4]}, 0x{size:x})"
            pos += 7

        elif ope == 0x23:
            output += "exit()"
            pos += 1

        else:
            print(f"Unknown: 0x{ope:x}")
            print(output)
            print(code[pos:])
            exit(1)
        output += "\n"
    return output

with open("./opcode", "rb") as f:
    code = f.read()

print(disasm(code))

