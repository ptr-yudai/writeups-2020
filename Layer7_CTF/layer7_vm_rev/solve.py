from z3 import *
from ptrlib import *

def add(a, b):
    c = 0
    for i in range(7):
        c |= ((((a >> (8*i)) & 0xff) + ((b >> (8*i)) & 0xff)) & 0xff) << (8*i)
    return c

def sub(a, b):
    c = 0
    for i in range(7):
        c |= ((((a >> (8*i)) & 0xff) - ((b >> (8*i)) & 0xff)) & 0xff) << (8*i)
    return c

s = Solver()
flag = [BitVec(f"part{i}", 56) for i in range(3)]

a = flag[0] ^ 0x45728976235614
b = flag[1] ^ 0x06997d5a209478
c = flag[2] ^ 0x5065711f2a7964
a = sub(a, c)
b = add(b, a)
c = sub(c, b)
s.add(a == 0x9d3290b2501151)
s.add(b == 0xf60fa1da60f478)
s.add(c == 0x6df98d9dbd1c9b)

while True:
    r = s.check()
    if r == sat:
        m = s.model()
        ans = [m[part].as_long() for part in flag]
        out = b""
        for i in range(3):
            out += int.to_bytes(ans[i], length=7, byteorder='big')[::-1]
        print(out)
        s.add(Not(And([part == m[part].as_long() for part in flag])))
    else:
        break
