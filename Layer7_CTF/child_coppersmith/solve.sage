with open("enc.txt", "r") as f:
    N, e, c = eval(f.readline())
    hint = eval(f.readline())

low_size = hint.bit_length()
kbits = 512 * 2 - low_size

PR.<x> = PolynomialRing(Zmod(N), implementation='NTL')
f = x*2^low_size + hint
f = f.monic()
set_verbose(2)

#print(f)
#s = f.small_roots(2^kbits, beta=0.3, epsilon=0.01)
#print(s)

x0 = 94373865754922499897817115100334125251009562382499702611396215285774935900254978486028717004912632770689515314851325844365046848
pq = x0*2^low_size + hint
p = N / pq
q = pq / p

print(p)
print(q)

piN = p * (p-1) * (q-1)
d = inverse_mod(e, piN)
print(bytes.fromhex(hex(pow(c, d, N))[2:]))
