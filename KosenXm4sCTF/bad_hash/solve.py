from z3 import *
from ptrlib import *

def hash(base):
    xor_sum = 0
    mod_sum = 0
    for c in base:
        xor_sum ^= c
        mod_sum += c
        mod_sum = URem(mod_sum, 100)
    return (xor_sum, mod_sum)

sock = Socket("27.133.155.191", 30010)

r = sock.recvregex("ans_x (\d+), ans_m (\d+)")
x, m = int(r[0]), int(r[1])
print(x, m)

s = Solver()
b = [BitVec(f'b_{i}', 8) for i in range(4)]
for c in b:
    s.add(0x20 <= c, c <= 0x7e)
h = hash(b)
s.add(h[0] == x, h[1] == m)
r = s.check()
if r == sat:
    m = s.model()
    ans = ""
    for c in b:
        ans += chr(m[c].as_long())
    sock.sendline(ans)
    print(ans)
else:
    print(r)

sock.interactive()
