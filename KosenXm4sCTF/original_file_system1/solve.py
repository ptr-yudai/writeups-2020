from ptrlib import xor

with open("prob1.img", "rb") as f:
    f.seek(0x26)
    buf = f.read(32)
    key = xor("xm4s{", buf)
    print(xor(buf, key))
