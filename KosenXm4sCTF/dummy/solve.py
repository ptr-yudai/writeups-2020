import subprocess
import re

flag = b''
output = subprocess.check_output(["objdump", "-S", "-M", "intel", "dummy"])
prevline = ''
for line in output.split(b'\n'):
    line = line.decode()
    if 'je' in line and 'cmp' in prevline:
        r = re.findall("eax,0x([0-9a-f]+)", prevline)
        if r:
            flag += bytes.fromhex(r[0])[::-1]
    prevline = line

print(flag)
