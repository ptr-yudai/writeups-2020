from Crypto.Util.number import *
from ptrlib import *
import base64

sock = Socket("nc 27.133.155.191 30013")

N = int(sock.recvregex("\((\d+), \d+\)")[0])

c = bytes_to_long(base64.b64decode(sock.recvlineafter("is ")))
sock.sendline(base64.b64encode(long_to_bytes(c * (N-1))))
m = bytes_to_long(base64.b64decode(sock.recvlineafter("is ")))

print(long_to_bytes(N - m))

sock.interactive()
