import numpy as np
from PIL import Image

image_array = np.asarray(Image.open("./output.png")).copy()

flag = ''


for x in range(len(image_array)):
    for y in range(len(image_array[0])):
        r, g, b = image_array[x][y]
        c = ((b & 0b111) << 4) | ((g & 0b11) << 2) | (r & 0b11)
        flag += chr(c)
        if chr(c) == '}':
            print(flag)
            exit()
