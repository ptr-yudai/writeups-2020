import base64
from ptrlib import xor

cipher = base64.b64decode(b'aW4kYHpQUDt+dUVuC0tSamoWX0RjexFBT307HzwI')

flag = b''
key = "paswd"
iv  = "abcde"
for i in range(0, len(cipher), len(key)):
    block = xor(iv, xor(key, cipher[i:i+len(key)]))
    iv = cipher[i:i+len(key)]
    flag += block

print(flag)
