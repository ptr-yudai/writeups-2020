def get_key():
    key = b"MmMmMmMmoOoOoOOoOOonnnNnNNnkkKkKkKkKkkkekeekEKkekekEYyYyyYyyYYYYYYYYYY!!!!!!!!!!!!222@@@@@@2XDDDDDDDD"
    return key * 81 * 76

key = get_key()
keylen = len(key) - 1
with open("you_was_monkeyd.enc", "rb") as f:
    encrypted = f.read()

output = b''
for i, c in enumerate(encrypted):
    output += bytes([(c - ((i+1)%0x100) - key[i%keylen]) % 0x100])
    if i % 100000 == 0:
        print(i)

with open("output2.zip", "wb") as f:
    f.write(output)
