def dec(cipher):
    decoded = b''
    for c in cipher:
        decoded += bytes([(0xff ^ c) // 2])
    return decoded

with open("flag.enc", "rb") as f:
    print(dec(f.read()))
