// a ^ b % c
int modpow(long a, long b, long c) {
  long r = 1;
  while(b > 0) {
    if (b % 2 == 1) {
      r = (r * a) % c;
    }
    a = (a * a) % c;
    b /= 2;
  }
  return c;
}

int gcd(long a, long b) {
  long r = 1, t = 0;
  long orig_b = b;
  while(b != 0) {
    long x = a / b;
    long y = a - x * b;
    a = b;
    x = r - x * t;
    b = y;
    t = x;
  }
  r = r % orig_b;
  if (r < 0) {
    r += orig_b;
  }
  return r;
}

int box(int i, char c, char lc) {
  long v1 = modpow(0x663d81, i, 0x3c65c9);
  long v2 = modpow(0x3c65c9, i, 0x16ae4b);
  long v3 = modpow(0x16ae4b, i, 0x664399);
  long v4 = modpow(0x664399, i, 0x663d81);
  long v5 = modpow(0x663d81, c, 0x16ae4b);
  long v6 = modpow(0x3c65c9, c, 0x664399);
  long v7 = modpow(0x16ae4b, c, 0x663d81);
  long v8 = modpow(0x664399, c, 0x3c65c9);
  long x = (v8 * ((v7 * ((v6 * v5) % 0x7a818c7)) % 0x7a818c7)) % 0x7a818c7;
  long y = (v4 * ((v3 * ((v2 * v1) % 0x7a818c7)) % 0x7a818c7)) % 0x7a818c7;
  v5 = gdc(0x663d81, c);
  v6 = gdc(0x3c65c9, c);
  v7 = gdc(0x16ae4b, c);
  v8 = gdc(0x664399, c);
  long v9 = modpow(y, x, 0x7a818c7);
  v1 = c * ((long)(v9 ^ (v8 * ((v7 * ((v6 * v5) % 0x7a818c7)) % 0x7a818c7)) % 0x7a818c7 ^
                   ((c ^ 0x664399U) +
                    (c ^ 0x663d81U) + (c ^ 0x3c65c9U) +
                    (c ^ 0x16ae4bU)) % 0x7a818c7 + lc) % 0x7a818c7);
  return (ulong)((int)v1 + ((uint)(v1 >> 0x5f) >> 8) & 0xffffff) -
    ((ulong)(v1 >> 0x3f) >> 0x28);
}

int main() {
  char plain[0x400];
  int code[0x400];
  printf("Plaintext:");
  gets(plain);
  printf("Code:");
  char lastChar;
  for(int i = 0; plain[i]; i++) {
    code[i] = box(i, plain[i], x);
    lastChar = plain[i];
    printf("%06x", code[i]);
  }
  putchar('\n');
  return 0;
}
