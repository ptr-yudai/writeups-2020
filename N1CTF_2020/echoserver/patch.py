with open("./pwn22", "rb") as f:
    buf = f.read()

target = buf.find(b"\x48\x01\xdc\xc5")
buf = buf[:target-4] + b'\x38\x60\x00\x00' + buf[target:]

target = buf.find(b"\x48\x01\xdc\xe5")
buf = buf[:target] + b'\x38\x60\x00\x23' + buf[target+4:]

with open("./patched-pwn22", "wb") as f:
    f.write(buf)

import os
os.system("chmod +x patched-pwn22")
