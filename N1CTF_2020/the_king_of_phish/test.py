import base64

with open("cmd.txt", "r") as f:
    buf = f.read()

print(base64.b64encode(buf.encode("utf-16le")).decode())

