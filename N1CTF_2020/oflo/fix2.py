from ptrlib import xor

with open("patched-oflo", "rb") as f:
    buf = f.read()

buf = buf[:0xa69] + xor(buf[0xa69:0xa69+10], "n1ctf") + buf[0xa69+10:]

with open("2-patched-oflo", "wb") as f:
    f.write(buf)

