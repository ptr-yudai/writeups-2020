arr = [0x35, 0x2d, 0x11, 0x1a, 0x49, 0x7d, 0x11, 0x14, 0x2b, 0x3b, 0x3e, 0x3d, 0x3c, 0x5f]

"""
assert flag[i] ^ (version[i] + 2) == arr[i]
"""

versions = b'Linux version 5.4.0-51-generic'

flag = ""
for i in range(len(arr)):
    flag += chr(arr[i] ^ (versions[i] + 2))

print(flag)
