
with open("oflo", "rb") as f:
    buf = f.read()

buf = buf[:0xbb1] + b'\x90' + buf[0xbb2:] # jmp obfuscation
buf = buf[:0xbb7] + b'\x90'*6 + buf[0xbb7+6:] # call obfuscation

with open("patched-oflo", "wb") as f:
    f.write(buf)

