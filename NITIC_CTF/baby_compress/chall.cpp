//g++ pwn2.cpp
#include <stdlib.h>
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <vector>
#include <cassert>
using namespace std;

class Data{
    public:
        Data();
        ~Data();
        virtual void compress();
        virtual void decompress();
        long length;
        char *content;
        bool is_compressed;
};

Data::Data(){
    string buf;
    cout << "Input content: ";
    cin >> buf;
    this->length = buf.length();
    this->content = new char[this->length];
    this->is_compressed = false;
    memcpy(this->content, buf.data(), buf.length());
}

Data::~Data(){
    delete(this->content);
    this->content = nullptr;
    this->length = 0;
}

void Data::compress(){
    int c = 1;
    int pos = 0;
    vector<char> result;
    for (int i = 0; i <= this->length; i++) {
        if (i == 0){
            result.push_back(this->content[i]);
            pos++;
            continue;
        }
        if (content[i] == this->content[i-1]) {
            c++;
        }
        if (content[i] != this->content[i-1] || c == 255) {
            result.push_back(c);
            result.push_back(content[i]);
            pos += 2;
            c = 1;
        }
    }
    result.push_back('\0');
    this->length = pos - 1;
    this->is_compressed = true;
    memcpy(this->content, result.data(), pos);
}

void Data::decompress(){
    if (!this->is_compressed){
        return;
    }
    int pos = 0;
    vector<char> result;
    for (int i = 0; i < this->length; i += 2) {
        char k = this->content[i+1];
        for (int j = 0; j < k; j++){
            result.push_back(this->content[i]);
            pos++;
        }
    }
    result.push_back('\0');
    this->length = pos;
    memcpy(this->content, result.data(), this->length);
}

void init(){
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
}

void menu(){
    cout << "1. add data\n";
    cout << "2. compress data\n";
    cout << "3. decompress data\n";
    cout << "4. clear data\n";
    cout << "5. read data\n";
    cout << "6. exit\n";
    cout << "> ";
}

int main() {
    init();
    int input;
    Data *ds[10];
    while (true){
        menu();
        cin >> input;
        switch(input){
            case 1:
                cout << "index: ";
                cin >> input; 
                assert(input >= 0 && input < 10);
                ds[input] = new Data();
                break;
            case 2:
                cout << "index: ";
                cin >> input; 
                assert(input >= 0 && input < 10);
                ds[input]->compress();
                break;
            case 3:
                cout << "index: ";
                cin >> input; 
                assert(input >= 0 && input < 10);
                ds[input]->decompress();
                break;
            case 4:
                cout << "index: ";
                cin >> input; 
                assert(input >= 0 && input < 10);
                delete ds[input];
                break;
            case 5:
                cout << "index: ";
                cin >> input; 
                assert(input >= 0 && input < 10);
                assert(input >= 0);
                cout << "length: " << ds[input]->length << "\n";
                cout << "content: " << ds[input]->content << "\n";
                break;
            case 6:
                exit(0);
            default:
                continue;
        }
    }
}
