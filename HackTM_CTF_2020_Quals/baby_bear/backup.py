# gdb -n -q -x backup.py ./baby_bear
import gdb
import re
import string

def to_hash(s):
    output = ""
    for x in re.findall("0x([0-1]+)", s)[:6]:
        for i in range(len(x)-1, 0, -2):
            output += x[i]
    return output

gdb.execute("set pagination off")
gdb.execute("break *0x4004d0")
gdb.execute("break *0x4005dd")
gdb.execute("break *0x400617")
gdb.execute("run")

x = gdb.execute("x/2xg 0x600770", to_string=True)
r = re.findall("0x([0-9a-f]+)", x)
key = r[1].decode("hex")[::-1] + r[2].decode("hex")[::-1]
gdb.execute("continue")
print(key.encode("hex"))

# 0x3f3f3f3fe75a3894
"""
data = '?' * 0x10
for index in range(len(data)):

    for c in range(0x100):

        data = data[:index] + chr(c) + data[index:]
        
        for i in range(len(data)):
            gdb.execute("set {{char}}{}={}".format(0x600770 + i, ord(data[i])))

        gdb.execute("x/4xg 0x600770")
        gdb.execute("continue")

        print(to_hash(gdb.execute("x/7xg 0x600801", to_string=True)))
        print(to_hash(gdb.execute("x/7xg 0x600832", to_string=True)))
"""
