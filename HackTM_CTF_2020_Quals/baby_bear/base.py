# gdb -n -q -x base.py ./baby_bear
import gdb
import random
import re
import string

def to_hash(s):
    output = ""
    for x in re.findall("0x([0-1]+)", s)[:6]:
        for i in range(len(x)-1, 0, -2):
            output += x[i]
    return output

answer = raw_input("> ")

gdb.execute("set pagination off")
gdb.execute("break *0x4005dd")
gdb.execute("break *0x400617")

data = '?' * 0x10

def search(index=0, data='????????????'):
    for c in range(0x100):
        data = data[:index] + chr(c) + data[index + 1:]
        gdb.execute("run")
        for i in range(len(data)):
            gdb.execute("set {{char}}{}={}".format(0x600770 + i, ord(data[i])))

        gdb.execute("continue")
        w = to_hash(gdb.execute("x/7xg 0x600832", to_string=True))
        if w[:4 + index*4] == answer[:4 + index*4]:
            lastdata = data.encode("hex")
            last = w

            for result in search(index+1, data):
                yield result

        elif w[:-2] == answer:
            yield data

result = next(search())
print(result.encode("hex"))
exit()
