from ptrlib import *
import string
import time

# socat TCP-L:9999,reuseaddr,fork EXEC:./papa_bear

def pretty(x):
    x = x.replace(b"\n", b"")
    x = x.replace(b" ", b"")
    x = x.replace(b"d", b"")
    x = x.replace(b"b", b"")
    x = x.replace(b"-", b"")
    x = x.replace(b"=", b"")
    x = x.replace(b"P", b"")
    x = x.replace(b"Q", b"")
    x = x.replace(b"p", b"")
    x = x.replace(b"q", b"")
    x = x.replace(b"W", b"#")
    x = x.replace(b"M", b".")
    return x

with open("answer.txt", "rb") as f:
    code = pretty(f.read())

visited = []
def search(flag='', ofs=0):
    global visited
    if (flag, ofs) in visited:
        return
    else:
        visited.append((flag, ofs))
    for c in " " + string.printable[:-6]:
        time.sleep(0.01)
        sock = Socket("localhost", 9999)
        sock.sendline(str2bytes(flag + c).hex())
        for i in range(7):
            sock.recvline()
        result = b''
        for i in range(7):
            result += pretty(sock.recvline())
        sock.close()

        xx = result[:ofs + 7]
        yy = code[:ofs + 7]
        x = result[:ofs + 6]
        y = code[:ofs + 6]
        if xx == yy:
            print(repr(flag + c), ofs + 7)
            for r in search(flag + c, ofs + 7):
                yield r

        if x == y:
            print(repr(flag + c), ofs + 6)
            for r in search(flag + c, ofs + 6):
                yield r

        if c == ' ' and x[:-1] == y[:-1]:
            print(repr(flag + c), ofs + 5)
            for r in search(flag + c, ofs + 5):
                yield r
        
        if result == code:
            yield flag
    
logger.level = 0
flag = ''#'HackTM{F4th3r'
ofs = 0#81

print(next(search(flag, ofs)))
