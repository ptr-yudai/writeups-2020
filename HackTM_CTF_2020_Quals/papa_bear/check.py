from ptrlib import *
import string
import time

def pretty(x):
    x = x.replace(b"\n", b"")
    x = x.replace(b" ", b"")
    x = x.replace(b"d", b"")
    x = x.replace(b"b", b"")
    x = x.replace(b"-", b"")
    x = x.replace(b"=", b"")
    x = x.replace(b"P", b"")
    x = x.replace(b"Q", b"")
    x = x.replace(b"p", b"")
    x = x.replace(b"q", b"")
    x = x.replace(b"W", b"#")
    x = x.replace(b"M", b".")
    return x

with open("answer.txt", "rb") as f:
    code = pretty(f.read())

# 6, 6, 6, 7, 7, 6, 7, 7, 6, 7, 6, 7, 7, 7, 6, 7, 7, 6, 7
# 7, 7, 6
logger.level = 0

sock = Socket("localhost", 9999)
sock.sendline((b"HackTM{F4th3r_ __").hex())
for i in range(7):
    sock.recvline()
result = b''
for i in range(7):
    result += pretty(sock.recvline())
sock.close()
print(code[70:128])
print(result[70:128])
