from ptrlib import *
import string
import time

def pretty(x):
    x = x.replace(b"\n", b"")
    x = x.replace(b" ", b"")
    x = x.replace(b"d", b"")
    x = x.replace(b"b", b"")
    x = x.replace(b"-", b"")
    x = x.replace(b"=", b"")
    x = x.replace(b"P", b"")
    x = x.replace(b"Q", b"")
    x = x.replace(b"p", b"")
    x = x.replace(b"q", b"")
    x = x.replace(b"W", b"#")
    x = x.replace(b"M", b".")
    return x

with open("answer.txt", "rb") as f:
    code = pretty(f.read())

logger.level = 0
flag = 'HackTM{F4th'
for index in range(1 + len(flag), len(code) // 6 + 1):
    
    for c in "_" + string.printable[:-6]:
        time.sleep(0.1)
        sock = Socket("localhost", 9999)
        sock.sendline(flag + c)
        for i in range(7):
            sock.recvline()
        result = b''
        for i in range(7):
            result += pretty(sock.recvline())
        sock.close()

        if len(flag) < 4:
            delta = 1
        elif len(flag) < 8:
            delta = 2
        
        x = result[:1 + delta + 6*index]
        y = code[:1 + delta + 6*index]
        print("Trying {}: {}, {}".format(c, x, y))
        if x == y:
            flag += c
            print(flag)
            break

    else:
        print("Not found!")
        exit()
