#!/usr/bin/env python
import os

flag = bytes.fromhex(input())
encoded = '\\x'.join(['{:02x}'.format(c) for c in flag])
os.system("bash -c \"./papa_bear $'{}'\"".format('\\x'+encoded))
