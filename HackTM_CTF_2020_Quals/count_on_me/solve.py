from Crypto.Cipher import AES
iv = '42042042042042042042042042042042'.decode('hex')
c = "059fd04bca4152a5938262220f822ed6997f9b4d9334db02ea1223c231d4c73bfbac61e7f4bf1c48001dca2fe3a75c975b0284486398c019259f4fee7dda8fec".decode("hex")
key = ((3, 4),(0, 3),(2, 0),(3, 0),(0, 2),(-1, -1),(3, 1),(3, 1),(3, 3),(2, 2),(3, 4),(1, 1),(3, 4),(2, 2),(1, 3),(-1, -1),(1, 0),(1, 3),(3, 2),(3, 3),(3, 3),(1, 0),(1, 4),(0, 3),(2, 1),(2, 0),(0, 1),(2, 0),(2, 0),(0, 0),(2, 0),(-1, -1),(0, 0),(1, 3),(3, 3),(2, 0),(0, 0),(3, 0),(3, 3),(1, 0),(3, 3),(2, 4),(3, 4),(0, 1),(0, 1),(0, 0),(0, 4),(1, 1),(3, 0),(0, 4),(2, 1),(3, 1),(2, 0),(1, 3),(2, 4),(1, 0),(2, 3),(3, 1),(1, 4))

for x1 in range(20):
    print(x1)
    for x2 in range(20):
        for x3 in range(20):
            cnt = 0
            n = 0
            for pair in key:
                if -1 in pair:
                    if cnt == 0:
                        x = x1
                    elif cnt == 1:
                        x = x2
                    elif cnt == 2:
                        x = x3
                    cnt += 1
                else:
                    x = pair[0]*5 + pair[1]
                n *= 20
                n += x
            k = hex(n).rstrip('L')[2:].decode("hex")
            aes = AES.new(k, AES.MODE_CBC, iv)
            m = aes.decrypt(c)
            if 'HackTM' in m:
                print(m)
                exit()
