t = [
    0x67, 0xc6, 0x69, 0x73, 0x51, 0xff, 0x4a, 0xec, 0x29, 0xcd,
    0xba, 0xab, 0xf2, 0xfb, 0xe3, 
]

cred = b'\x8f\x55\x29\xa2\x9f\x7a\x2e\x93\xfb\xa8\xba\x10\xa8\x7c\x19'
password = b""
for i in range(15):
    for c in range(0x100):
        if (c * 10 ^ i + t[i]) & 0xff == cred[i]:
            password += bytes([c])
            break

print(password)
