from ptrlib import *
from datetime import datetime, timezone, timedelta
import ctypes
import string

def encrypt(data):
    out = b''
    for c in data:
        out += bytes([c ^ ((1 + (glibc.rand() % 1638)) & 0xff)])
    return out

with open("out.txt", "rb") as f:
    encrypted = f.read()

glibc = ctypes.cdll.LoadLibrary('/lib/x86_64-linux-gnu/libc-2.27.so')

time = '2020/04/10 19:01:59'
date = datetime.strptime(time, '%Y/%m/%d %H:%M:%S')
print(date)
date += timedelta(hours=9)
print(date)
for t in range(int(date.timestamp()), 0, -1):
    glibc.srand(t)
    m = encrypt(encrypted)
    if consists_of(m, string.printable, per=0.9):
        print("Hit: " + str(t))
        break
    if t % 1000 == 0:
        print(t)

glibc.srand(t)
print(encrypt(encrypted))
