using System;
using System.IO;
using System.Text.Encoding;

class Program
{
    File piFile;
    private static void Main(string []args)
    {
        piFile = File("one-million-digits.txt");
        Hide("original.bmp", "result.bmp", "<CENSORED>");
        return;
    }

    private static void Hide(string srcPath, string dstPath, string secret)
    {
        BitArray secret_bits = new BitArray(GetBytes(secret)); // stack.0
        Bytes buf[] = ReadAllBytes(srcPath); // stack.1
        int hoge = src[14] + 14; // stack.2
        for(int i = 0; i < secret_bits.length(); i++) { // stack.5
            int ofs = GetNextPiDigit() + hoge; // stack.3
            char x = src[ofs] & 0xfe; // stack.4
            buf[ofs] = (char)secret_bits[i] + x;
            hoge += 10;
        }
        WriteAllBytes(dstPath, buf);
    }

    private static int GetNextPiDigit() {
        int digit = piFile.ReadByte(); // stack.0
        if (digit == 0x0A) {
            digit = piFile.ReadByte();
        }
        return digit - 0x30;
    }
}
