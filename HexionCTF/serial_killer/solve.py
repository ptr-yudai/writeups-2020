import re
a = """
  DAT_c0a0 = 0x2a;
  DAT_c0a1 = 0x27;
  DAT_c0a2 = 0x3a;
  DAT_c0a3 = 1;
  DAT_c0a4 = 0x16;
  DAT_c0a5 = 4;
  DAT_c0a6 = 0x39;
  DAT_c0a7 = 0x71;
  DAT_c0a8 = 0x34;
  DAT_c0a9 = 0x27;
  DAT_c0aa = 0x2c;
  DAT_c0ab = 0x1d;
  DAT_c0ac = 0x77;
  DAT_c0ad = 0x27;
  DAT_c0ae = 0x30;
  DAT_c0af = 0x73;
  DAT_c0b0 = 0x23;
  DAT_c0b1 = 0x2e;
  DAT_c0b2 = 0x1d;
  DAT_c0b3 = 0x29;
  DAT_c0b4 = 0x73;
  DAT_c0b5 = 0x2e;
  DAT_c0b6 = 0x2e;
  DAT_c0b7 = 0x71;
  DAT_c0b8 = 0x30;
  DAT_c0b9 = 0x31;
  DAT_c0ba = 0x1d;
  DAT_c0bb = 0x32;
  DAT_c0bc = 0x2e;
  DAT_c0bd = 0x76;
  DAT_c0be = 0x3b;
  DAT_c0bf = 0x1d;
  DAT_c0c0 = 0x25;
  DAT_c0c1 = 0x76;
  DAT_c0c2 = 0x2f;
  DAT_c0c3 = 0x71;
  DAT_c0c4 = 0x31;
  DAT_c0c5 = 0x3f;
"""

flag = ""
for x in re.findall("= 0x([0-9a-f]+)", a):
    flag += chr(0x42 ^ int(x, 16))

print(flag)
