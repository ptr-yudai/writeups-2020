from ptrlib import bytes2str, str2bytes
from Crypto.Cipher import AES

with open("src/iv", "rb") as f:
    iv = f.read(0x10)
with open("src/key", "rb") as f:
    key = f.read(0x10)
collection = []
with open("src/collection", "rb") as f:
    data = f.read()
    for line in data.split(b'\n')[:6]:
        category, link = line.split(b' ')
        collection.append((category, link))

# up to 0x100-1 bytes
my_category = b'a'
# up to 0x400-1 bytes
my_link = b'b'
collection.append((my_category, my_link))

sorted_collection = sorted(collection, key = lambda x: x[0])

buf = ''
for category, link in sorted_collection:
    buf += "%s{%s} " % (bytes2str(category), bytes2str(link))
buf = buf.rstrip(" ") + '\x00\x00'
buf += '\x00' * (0x20 - (len(buf) % 0x10))
print(buf)
print(hex(len(buf)))

aes = AES.new(key, AES.MODE_CBC, iv)
cipher = aes.encrypt(str2bytes(buf))

print("encrypted: " + cipher.hex())
