void Backdoor()
{
  GetVariableDataPtr();
}

// sub_1045
void AdminOTPCheck(char *otp, int *correct)
{
  int ok;
  unsigned long value, otp_key;

  opt[0x10] = 0;
  unsigned long part1 = AsciiStrHexToUint64(opt);
  unsigned long part2 = AsciiStrHexToUint64(&opt[0x11]);
  part2 = little2big(part2) + part1;

  ok = 0;
  unknown_method(&otp_key, 0);

  value = 0;
  value += ((otp_key >> 0x00) & 0xffff) << 0x20;
  value += ((otp_key >> 0x10) & 0xff) << 0x18;
  value += ((otp_key >> 0x18) & 0xff) << 0x10;
  value += ((otp_key >> 0x20) & 0xff) << 0x08;

  for(int i = -2; i <= 2; i++) {
    if (part2 == value + i) {
      ok = 1;
    }
  }

  *correct = ok;
}
