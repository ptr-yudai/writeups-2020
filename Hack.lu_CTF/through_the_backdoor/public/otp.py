from ptrlib import u64, p64

year = 2020
month = 10
day = 24
hour = 9
minute = 22

# superSecretPassword123

value = (year << 32) | (month << 24) | (day << 16) | (hour << 8) | minute

print("{:016x}-{:016x}".format(1, u64(p64(value, 'big'))))
