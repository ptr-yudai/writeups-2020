#!/bin/sh
 
mount -t proc none /proc
mount -t sysfs none /sys
mount -t devtmpfs none /dev
mount -t efivarfs none /sys/firmware/efi/efivars/

get_user_creds() {
    echo "Enter Username:"
    read -r name || exit

    echo "Enter Password:"
    read -r password || exit
     
    echo "Enter One Time Password:"
    read -r otp || exit
}

check_creds() {
    res=0
    echo -ne "$name\x00$password\x00$otp\x00">var
    efivar-static -w --name 13371337-1337-1337-1337-133713371337-AuthEndpoint -f var
    res="$(efivar-static -p -d --name 13371337-1337-1337-1337-133713371337-AuthEndpoint)"
    if [ "$res" -gt 0 ]; then
        auth_level="$res"
    else
        echo "$name is not allowed here!"
    fi
}

get_var() {
    echo "Enter Name:"
    read -r var_name || exit
    efivar-static -p --name "13371337-1337-1337-1337-133713371337-$var_name"
}

set_var() {
    echo "Enter Name:"
    read -r var_name || exit
    echo "Enter Data:"
    read -r var_data || exit
    echo -ne "$var_data" > var
    efivar-static -w --name "13371337-1337-1337-1337-133713371337-$var_name" -f var
}

clear

echo "  _____                 _           _        ";
echo " |  __ \               | |         | |       ";
echo " | |__) |_      ___ __ | |__  _   _| |__     ";
echo " |  ___/\ \ /\ / / '_ \| '_ \| | | | '_ \    ";
echo " | |     \ V  V /| | | | | | | |_| | |_) |   ";
echo " |_|____  \_/\_/ |_|_|_|_| |_|\__,_|_.__/    ";
echo " |  ____|          | |     (_) |             ";
echo " | |__  __  ___ __ | | ___  _| |_            ";
echo " |  __| \ \/ / '_ \| |/ _ \| | __|           ";
echo " | |____ >  <| |_) | | (_) | | |_            ";
echo " |______/_/\_\ .__/|_|\___/|_|\__|           ";
echo "             | |                             ";
echo "  _____      |_|        _                    ";
echo " |  __ \      | |      | |                   ";
echo " | |  | | __ _| |_ __ _| |__   __ _ ___  ___ ";
echo " | |  | |/ _\` | __/ _\` | '_ \ / _\` / __|/ _ \ ";
echo " | |__| | (_| | || (_| | |_) | (_| \__ \  __/";
echo " |_____/ \__,_|\__\__,_|_.__/ \__,_|___/\___|";
echo "                                             ";
echo "            Strangers keep out!";


auth_level=0

while true; do
    if [ "$auth_level" -eq 0 ]; then
        get_user_creds
        check_creds
        continue
    fi
    echo "
What's it today?
login   Login
time    Get current system time
load    Load data
save    Save data
shell   Get shell access
"
    read -r action || exit
    case "$action" in
        login)
            auth_level=0
            ;;
        time)
            date
            ;;
        load)
            if [ "$auth_level" -gt 1 ]; then
                get_var
                continue
            fi
            echo "Permission denied!"
            ;;
        save)
            if [ "$auth_level" -gt 1 ]; then
                set_var
                continue
            fi
            echo "Permission denied!"
            ;;
        shell)
            if [ "$auth_level" -gt 1 ]; then
                echo "Access granted!"
                echo "System is currently under maintenance ... removing root permissions"
                chpst -u nobody /bin/busybox ash
                continue
            fi
            echo "Permission denied!"
            ;;
        *)
            echo "$action is not a valid action"
    esac
done

