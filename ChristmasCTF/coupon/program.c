typedef struct {
  char outbuf[0x100]; // +0000h
  char buf[0x100]; // +0100h
  char key[0x21];  // +0200h
  char nonce[0x0d];   // +0221h
  char ver[0x11];  // +022eh
  char tag[0x11];  // +023fh
  int outlen;      // +0250h
  int len;         // +0254h
} Cipher;

void unhexlify(char *output, char *hex, int len) {
  char str[3];
  for(int i = 0; i < size; i++) {
    str[0] = hex[i*2];
    str[1] = hex[i*2+1];
    str[2] = '\0';
    sscanf(str, "%2hhx", &output[i]);
  }
  output[i] = '\0';
}
void randomize(char *ptr, int len) {
  for(int i = 0; i < size; i++) {
    ptr[i] = rand();
  }
}
void read_key(char *binkey) {
  char key[0x40];
  FILE *fp;
  if (access("key.txt", O_RDONLY) == -1) {
    fp = fopen("key.txt", "w");
    randomize(binkey, 0x20);
    hexlify(key, binkey, 0x20);
    fwrite(key, 1, 0x40, fp);
  } else {
    fp = fopen("key.txt", "r");
    fread(key, 1, 0x40, fp);
    unhexlify(binkey, key, 0x20);
  }
}
void setup(Cipher *cipher) {
  setvbuf(stdout, NULL, _IONBF, 0);
  setvbuf(stdin, NULL, _IONBF, 0);
  srand(time(0));
  randomize(cipher->nonce, 0x0c);
  read_key(cipher->key);
  strcpy(cipher->ver, "coupon center v1");
}
int myread(int fd, char *buf, int len) {
  int s = read(fd, buf, len);
  if (s > 0 && buf[s-1] == '\n') {
    buf[s-1] = '\0';
  }
  return s;
}
void dump(const char *str, char *buf, int len) {
  printf("%s : ", str);
  for(int i = 0; i < len; i++) {
    printf("%02x", buf[i]);
  }
  putchar('\n');
}

void show_gifts() {
  struct dirent *de;
  DIR *dirp;
  puts("Here's a gift list that we have :)");
  puts("==== gift list ====");
  dirp = opendir("./GiftBag");
  if (dirp == NULL) return;

  while(de = readdir(dirp)) {
    if (de->d_name[0] != '.') {
      puts(de->d_name[0]);
    }
  }
  closedir(dirp);
}

void reset_cipher(Cipher *cipher) {
  memset(cipher->outbuf, 0, 0x100);
  memset(cipher->buf, 0, 0x100);
  memset(cipher->tag, 0, 0x11);
  cipher->len = 0;
  cipher->outlen = 0;
}
int check_lowercase(char *ptr) {
  int ok = 1;
  while(*ptr) {
    ok &= ('a' <= *ptr) && (*ptr <= 'z');
    ptr++;
  }
  return ok;
}

void choose_gift(Cipher *cipher) {
  char msg[0x100] = {0}, name[0x40] = {0}, gift[0x40] = {0};
  // input
  puts("What's your name?"); printf(" : ");
  myread(0, name, 0x30);
  puts("What do you want for the gift?"); printf(" : ");
  myread(0, gift, 0x30);
  sprintf(msg, "Santa's coupon for %s :)\nPresent: %s", name, gift);
  // check
  if (strstr(name, "flag") || strstr(gift, "flag")) {
    puts("That's not for you~~");
    exit(0);
  }
  if (check_lowercase(name) == 0 || check_lowercase(gift) == 0) {
    puts("Bad kid nono...");
    exit(0);
  }
  // copy
  strncpy(cipher->buf, msg, strlen(msg));
  if (len & 0xf) {
    len = ((len + 0xf) / 0x10) * 0x10; // maybe (loc_1a02)
  }
  cipher->len = len;
}

void encrypt(Cipher *cipher) {
  int pdf;
  EVP_CIPHER_CTX *ctx;

  ctx = EVP_CIPHER_CTX_new();
  EVP_EncryptInit_ex(ctx, EVP_aes_256_gcm(ctx), NULL, 0, 0);
  EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, 0xc, 0);
  EVP_EncryptInit_ex(ctx, 0, NULL, cipher->buf, cipher->nonce);
  EVP_EncryptUpdate(ctx,
                    NULL, &cipher->out_len,
                    cipher->ver, 0x10);
  EVP_EncryptUpdate(ctx,
                    cipher->outbuf, &cipher->outlen,
                    cipher->buf, cipher->len);
  EVP_EncryptFinal_ex(ctx, &x, &pad);
  EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_GET_TAG, 0x10, cipher->tag);
  EVP_CIPHER_CTX_free(ctx);
}
int decrypt(Cipher *cipher) {
  EVP_CIPHER_CTX *ctx;
  ctx = EVP_CIPHER_CTX_new();
  EVP_DecryptInit_ex(ctx, EVP_aes_256_gcm(), 0, 0, 0);
  EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, 0xc, 0);
  EVP_DecryptInit_ex(ctx, 0, 0, cipher->key, cipher->nonce);
  EVP_DecryptUpdate(ctx,
                    NULL, &cipher->outlen,
                    cipher->ver, 0x10);
  EVP_DecryptUpdate(ctx,
                    cipher->outbuf, &cipher->outlen,
                    cipher->buf, cipher->len);
  EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, 0x10, cipher->tag);
  int r = EVP_DecryptFinal_ex(ctx, cipher->outbuf, cipher->outlen);
  EVP_CIPHER_CTX_free(ctx);
  return r;
}

void make_coupon(Cipher *cipher) {
  puts("\nOkay then, let's make some coupon for you");
  encrypt(cipher);
  dump("Coupon", cipher->buf , cipher->outlen);
  dump("Tag", cipher->tag);
}

void read_coupon(Cipher *cipher) {
  int size;
  char coupon[0x200], tag[0x20];
  puts("Hello, would you hand me the coupon you received?");
  printf("Coupon number : ");
  if ((size = myread(0, coupon, 0x200)) % 2 == 1) {
    puts("Coupon must be an even-length string!!");
    exit(0);
  }
  unhexlify(cipher->buf, coupon, size);

  printf("Coupon's tag : ");
  if (myread(0, tag, 0x20) != 0x20) {
    puts("Tag must be 16 bytes hex string!!");
    exit(0);
  }
  unhexlify(cipher->tag, tag, 0x10);
}

void check_coupon(Cipher *cipher) {
  if (decrypt(cipher)) {
    puts("===== coupon contents =====");
    printf("%s", cipher->outbuf);
    puts("===========================");
  } else {
    puts("Maybe this coupon is expired...");
    exit(0);
  }
}

void get_present(Cipher *cipher) {
  char path[0x100], buf[0x800];
  char *name = strstr(cipher->outbuf, "Present: ") + 9;
  strcat(path, "./GiftBag/", name);
  if (access(path, O_RDONLY) == -1) {
    printf("There's no such thing like '%s' :(\n", name);
  }
  FILE *fp = fopen(path, "r");
  fread(buf, 1, 0x800, fp);
  puts(buf);
}

int main(void) {
  int choice;
  unsigned coupon_flag = 0;
  Cipher cipher = {0};

  setup_cipher(&cipher);
  show_banner();
  while(1) {
    scanf("%d", &choice); getchar();
    switch (choice) {
    case 1: // show gifts
      show_gifts();
      break;
    case 2: // get coupon
      if (coupon_flag <= 1) {
        reset_cipher(&cipher);
        choose_gift(&cipher);
        make_coupon(&cipher);
        coupon_flag++;
      } else {
        puts("You such a greedy!!!");
        exit(0);
      }
    case 3: // use coupon
      reset_cipher(&cipher);
      read_coupon(&cipher);
      check_coupon(&cipher);
      break;
    case 4: // quit
      puts("Thank you for using our service :)");
      puts("Have a Merry Christmas!");
      return 0;
    default:
      puts("Pardon me?");
    }
  }
}
