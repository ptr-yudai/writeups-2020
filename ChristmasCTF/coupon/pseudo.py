from Crypto.Cipher import AES

"""
pwndbg> x/32xg 0x7fffffffd880 + 0x200
0x7fffffffda80: 0xca57e24a58459dc8      0x91f3882e0d1298c2
0x7fffffffda90: 0x70a3d35ca33fae49      0xfef8e23549327630
0x7fffffffdaa0: 0x41d12d060805f200      0x6f6300dddcf1a816
0x7fffffffdab0: 0x6e6563206e6f7075      0xcf00317620726574
0x7fffffffdac0: 0xa38dfef06800e745      0x00f7dce1ca444bfd
"""
key = bytes.fromhex('C89D45584AE257CAC298120D2E88F39149AE3FA35CD3A3703076324935E2F8FE')
nonce = bytes.fromhex('f20508062dd14116a8f1dcdd')

coupon = bytes.fromhex('3cfa7d5d814b93c052e822933080c218d3fb44e00a1647cff4ffe0bee0885e29658e68e4acbfa44ebbe5b4e50673868c')
tag = bytes.fromhex('7b6a965b11de227c65431eae620fde77')

coupon = bytes.fromhex('3cfa7d5d814b93c052e822933080c218d3fb44e00a164782afa49fcea8c4271c799f21a1aaa4f90bd38ad3800673868c')
tag = bytes.fromhex('cf45e70068f0fe8da3fd4b44cae1dcf7')

cipher = AES.new(key, AES.MODE_GCM, nonce)
plain = cipher.decrypt(coupon)

cipher = AES.new(key, AES.MODE_GCM, nonce)
encrypted, MACtag = cipher.encrypt_and_digest(plain[:16])
print(coupon)
print(encrypted)
print(MACtag)
print(tag)

print(plain)
print(cipher.decrypt_and_verify(coupon, tag))
