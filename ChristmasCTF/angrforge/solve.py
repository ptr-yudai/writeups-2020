import gdb
import string

flag = ['?' for i in range(0x38)]

gdb.execute('set pagination off')
gdb.execute('break *0x555555557a2c')

i = 0
while i < 0x38:
    for c in string.printable[:-5]:
        flag[i] = c
        with open("input", "w") as f:
            f.write(''.join(flag))
        gdb.execute('run < input > /dev/null')

        for j in range(i):
            gdb.execute('continue')

        al = gdb.parse_and_eval('$al')
        dl = gdb.parse_and_eval('$dl')
        if int(al) == int(dl):
            i += 1
            print(''.join(flag))
            break
    else:
        print(''.join(flag))
        exit(1)
