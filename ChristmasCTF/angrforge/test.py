def xor(s, key):
    i = 0
    output = b''
    while True:
        p = s.find(b'\x00')
        if i >= len(s) or (p != -1 and i >= p): break
        output += bytes([s[i] ^ ord(key[i % len(key)])])
        i += 1
    output += s[i+1:]
    return output

with open("angrforge", "rb") as f:
    f.seek(0x4080)
    encoded = f.read(0x38)

# pattern 1
s = encoded
s = xor(s, "soldierarmywarriorknight")
s = xor(s, "armywarriorknight")
s = xor(s, "herosoldierarmywarriorknight")
s = xor(s, "warriorknight")
s = xor(s, "knight")
s = xor(s, "dragonfour")
s = xor(s, "veteranssantarudolfsled")
s = xor(s, "santarudolfsled")
s = xor(s, "rudolfsled")
s = xor(s, "sled")
print(s)

from ptrlib import *
sock = Process("./angrforge")
input()
sock.sendline(s[:-1])
sock.interactive()
