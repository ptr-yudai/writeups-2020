#include <stdio.h>

char encoded[] = "..."; // unk_4080

int check(char *ptr) {
  for(int i = 0; i <= 0x37; i++) {
    if (ptr[i] != encoded[i]) return 0;
  }
  return 1;
}

void sub_33e9(char *ptr) {
  for(int i = 0; i < strlen(ptr); i++) {
    ptr[i] ^= "<KEY>"[i % strlen("<KEY>")];
  }
}

void encode(char *ptr) {
  if (strlen(ptr) < 0x38) {
    sub_33e9(ptr);
    sub_3481(ptr);
    sub_3519(ptr);
    sub_35b1(ptr);
    sub_3649(ptr);
    sub_36e1(ptr);
    sub_3779(ptr);
    sub_3811(ptr);
    sub_38a9(ptr);
    sub_3941(ptr);
  } else {
    sub_11c9(ptr);
    sub_1396(ptr);
    sub_1563(ptr);
    // ...
  }
}

int main() {
  char buf[0x40];
  fgets(buf, 0x39, stdin);

  if (check(buf) == 1) {
    puts("OMG, Thank you for your good works :)");
  } else {
    fatal();
  }
  return 0;
}
