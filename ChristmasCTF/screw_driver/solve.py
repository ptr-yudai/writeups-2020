import angr
import claripy
from logging import getLogger, WARN

getLogger("angr").setLevel(WARN + 1)
getLogger("claripy").setLevel(WARN + 1)

with open("file", "rb") as f:
    buf = list(f.read()) + [0]

p = angr.Project("./Screw_Driver.sys")

#state = p.factory.blank_state(addr=0x140003440)
state = p.factory.blank_state(addr=0x14000301b)
state.memory.store(0x140005060, bytes(buf))
state.memory.store(0x140005060, b'XMAS')
state.memory.store(0x140005040, bytes(buf))

simgr = p.factory.simulation_manager(state)

global x
class hook_dbgprint(angr.SimProcedure):
    def run(self):
        global x
        print("[+] DbgPrint")
        arg1 = b""
        i = 0
        while True:
            c = self.state.memory.load(self.state.regs.rcx + i, 1)
            c = self.state.solver.eval(c, cast_to=bytes)
            if c == b'\x00': break
            arg1 += c
            i += 1
        print("arg1 = ", arg1)
        if b'%s' in arg1:
            arg2 = b""
            i = 0
            while True:
                c = self.state.memory.load(self.state.regs.rdx + i, 1)
                c = self.state.solver.eval(c, cast_to=bytes)
                if c == b'\x00': break
                arg2 += c
                i += 1
            print("arg2 = ", arg2)
        x = self.state.regs.rdx
        return self.state.regs.rax

class hook_strcmp(angr.SimProcedure):
    def run(self):
        print("[+] strcmp")
        m = self.state.memory.load(self.state.regs.rcx, 0x1d)
        self.state.memory.store(self.state.regs.rdx, m)
        return claripy.BVV(0, 32)

class hook_memcpy(angr.SimProcedure):
    def run(self):
        print("[+] memcpy")
        m = self.state.memory.load(self.state.regs.rdx, self.state.regs.r8)
        self.state.memory.store(self.state.regs.rcx, m)
        m = self.state.memory.load(self.state.regs.rcx, 0x1d)
        print(self.state.solver.eval(m, cast_to=bytes))
        return self.state.regs.rax

@p.hook(0x140003481, length=0)
def hook_at_pon(state):
    print("initialize")
    state.memory.store(state.regs.rax, b"@_@-Round_aNd_roUnd_@nD_r0uNd\0")
"""
@p.hook(0x14000362d, length=0)
def hook_at_pon(state):
    print(state.regs.eax)
"""

p.hook_symbol("DbgPrint", hook_dbgprint())
#p.hook_symbol("strcmp", hook_strcmp())
#p.hook(0x140003940, hook_memcpy())

#simgr.explore(find=0x14000378f)
simgr.explore()

try:
    print("[+] DONE")
except IndexError:
    print("Not Found")
