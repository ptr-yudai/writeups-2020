from ptrlib import *

"""
typedef struct {
  char name[0x20];
  func *matcher; // +20h
  int age; // +28h
  int min; // +2Ch
  int max; // +30h
  int sex; // +38h
  Profile* list; // +40h (male_list / female_list)
  char h1[0x10]; // +48h
  char h2[0x10]; // +58h
  char h3[0x10]; // +68h
  int match_found; // +78h
} Profile;
"""

def make(age, name, min, max, sex, h1, h2, h3):
    sock.sendlineafter("> ", "0")
    sock.sendlineafter(": ", str(age))
    sock.sendafter(": ", name)
    sock.sendlineafter(": ", str(min))
    sock.sendlineafter(": ", str(max))
    sock.sendlineafter("> ", str(sex))
    sock.sendafter(": ", h1)
    sock.sendafter(": ", h2)
    sock.sendafter(": ", h3)
def save():
    sock.sendlineafter("> ", "1")
def find():
    sock.sendlineafter("> ", "2")
def show():
    sock.sendlineafter("> ", "3")
def users():
    sock.sendlineafter("> ", "4")

sock = Socket("localhost", 9999)

import random
import string
rndstr = lambda n: ''.join([random.choice(string.ascii_letters)
                            for i in range(n)])
males = 0
females = 0
while True:
    r = random.randint(1, 6)
    if r == 1 and males < 8 and females < 8:
        print("save()")
        save()
    elif r == 2:
        print("find()")
        find()
    elif r == 3:
        print("show()")
        show()
    elif r == 4:
        print("users()")
        users()
    else:
        age = random.randint(18, 100)
        name = rndstr(random.randint(1, 0x1f))
        min = random.randint(0, age)
        max = random.randint(age, 200)
        sex = random.randint(0, 1)
        h1 = rndstr(random.randint(1, 0xf))
        h2 = rndstr(random.randint(1, 0xf))
        h3 = rndstr(random.randint(1, 0xf))
        print(f"make({age}, {name}, {min}, {max}, {sex}, {h1}, {h2}, {h3})")
        make(age, name, min, max, sex, h1, h2, h3)
        if sex:
            females += 1
        else:
            males += 1

sock.interactive()
