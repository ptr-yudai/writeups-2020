from ptrlib import *

flag = b''
flag += p16(0x10000 - 0xb0bd)
flag += p16(0x10000 - 0xbabc)
flag += p16(0x10000 - 0xbeb9)
flag += p16(0x10000 - 0xbaac)
flag += p16(0x10000 - 0xcfce)
flag += p16(0x10000 - 0xcfce)
keyList = (
    (0x63f7, 0xf974),
    (0xa419, 0x2b9d),
    (0xec2b, 0x4caf),
    (0x347d, 0xbee1),
    (0x5c87, 0xfc0d),
    (0xe589, 0x6e48),
    (0x2e9b, 0xe03c),
    (0x73ad, 0xd322),
    (0x94f7, 0x1979),
    (0xbd19, 0x36d6),
    (0xc72b, 0x40e8),
    (0x497d, 0xcbf7),
)
for key in keyList:
    for x in range(0x10000):
        if (x ^ key[0]) + key[1] == 0x10000:
            break
    flag += p16(x)
    print(flag)
