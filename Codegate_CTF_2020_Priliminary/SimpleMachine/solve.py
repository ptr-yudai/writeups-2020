# gdb -n -q -x solve.py ./simple_machine
import gdb
import re

def get_arg():
    v = int(re.findall("\t0x([0-9a-f]{8})", gdb.execute("x/1xw $rdi + 0x34", to_string=True))[0], 16)
    return v & 0xffff, v >> 16

gdb.execute("break *0x00005555555558c0")
gdb.execute("break *0x00005555555558a0")
gdb.execute("break *0x0000555555555888")
gdb.execute("break *0x0000555555555870")
gdb.execute("break *0x0000555555555858")
gdb.execute("break *0x0000555555555848")
gdb.execute("break *0x0000555555555830")
gdb.execute("break *0x0000555555555810")
gdb.execute("break *0x00005555555557e0")
gdb.execute("run ./target < input")

flag = ''
log = ''
skip = False
while True:
    rip = int(re.findall("0x([0-9a-f]+)", gdb.execute("i r $rip", to_string=True))[0], 16)
    if rip == 0x00005555555557e0:
        arg1, arg2 = get_arg()
        log += "[+] read 0x{:04x}, 0x{:04x}\n".format(arg1, arg2)
    elif rip == 0x0000555555555848:
        arg1, arg2 = get_arg()
        log += "[+] mov 0x{:04x}, 0x{:04x}\n".format(arg1, arg2)
    elif rip == 0x0000555555555858:
        arg1, arg2 = get_arg()
        log += "[+] add 0x{:04x}, 0x{:04x}\n".format(arg1, arg2)
        #w = 0x10000 - arg2
        #gdb.execute("set {{short}}*{{$rdi + 0x34}} = {}".format(w))
        #flag += hex(w)[2:].decode("hex")[::-1]
        #print("[!] flag = {}".format(flag))
    elif rip == 0x0000555555555870:
        arg1, arg2 = get_arg()
        log += "[+] mul 0x{:04x}, 0x{:04x}\n".format(arg1, arg2)
    elif rip == 0x0000555555555888:
        arg1, arg2 = get_arg()
        log += "[+] xor 0x{:04x}, 0x{:04x}\n".format(arg1, arg2)
    elif rip == 0x00005555555558a0:
        arg1, arg2 = get_arg()
        if arg2 == 0xc:
            skip = True
        else:
            skip = False
        log += "[+] cmp 0x{:04x}, 0x{:04x}\n".format(arg1, arg2)
    elif rip == 0x00005555555558c0:
        arg1, arg2 = get_arg()
        log += "[+] jnz 0x{:04x}, 0x{:04x}\n".format(arg1, arg2)
        if not skip:
            gdb.execute("set {short}*{$rdi + 0x34} = 0")
    elif rip == 0x0000555555555810:
        with open("trace.txt", "w") as f:
            f.write(log)
        break
    gdb.execute("continue")
