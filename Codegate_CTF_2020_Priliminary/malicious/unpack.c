#include <openssl/camellia.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned char rawKey[] = "p4y1oad_3nc_key!";
unsigned char code[0x4400];
int main() {
  FILE *fp = fopen("hoge.exe", "rb");
  fseek(fp, 0x4e40, SEEK_SET);
  fread(code, 1, 0x4400, fp);
  fclose(fp);
  
  CAMELLIA_KEY keyTable = {0};
  Camellia_set_key(rawKey, 0x80, &keyTable);
  for(int i = 0; i < 0x4400; i += 0x10) {
    Camellia_decrypt(&code[i], &code[i], &keyTable);
  }

  fp = fopen("bootloader", "wb");
  fwrite(code, 1, 0x4400, fp);
  fclose(fp);
}
