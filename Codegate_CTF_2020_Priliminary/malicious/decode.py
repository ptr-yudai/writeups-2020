with open("bootloader", "rb") as f:
    output = f.read(0x30)
    code = f.read(0xe0 - 0x30)

    for c in code:
        output += bytes([c ^ 0xf4])

    output += f.read()

with open("decoded_bl", "wb") as f:
    f.write(output)

# P%|'3bTIeXz+CZn:|F4KX[=^?#rD-Dj\l
