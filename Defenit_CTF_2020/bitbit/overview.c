typedef struct {
  int bfType;
  int bfSize;
  short bfReserved1;
  short bfReserved2;
  int bfOffBits;
} BMPHeader;

typedef struct {
  int biSize;
  int biWidth;
  int biHeight;
  short biPlanes;
  short biBitCount;
  int biCompression;
  int biSizeImage;
  int biXPixPerMeter;
  int biYPixPerMeter;
  int biClrUsed;
  int biCirImportant;
} BMPInfoHeader;

typedef struct {
  BMPHeader *bf;
  BMPInfoHeader *bi;
  void *img;
} BMPFile;

FILE *fp;
BMPFile *bmp;

int read_bmp() {
  int i, j;
  if ((bmp->bf.bfType = readBlock(2)) != 0x4d42)
    return 1;

  bmp->bf.bfSize = readBlock(4);
  bmp->bf.bfReserved1 = readBlock(2);
  bmp->bf.bfReserved2 = readBlock(2);
  bmp->bf.bfOffBits = (short)readBlock(4);

  bmp->bi.biSize = readBlock(4);
  bmp->bi.biWidth = readBlock(4);
  bmp->bi.biHeight = readBlock(4);
  bmp->bi.biPlanes = readBlock(2);
  //...
  bmp->bi.biCirImportant = readBlock(4);

  bmp->img = malloc(bmp->bi.biHeight); // vuln?
  for(int y = 0; y < bmp->bi.biHeight; y++) {
    bmp->img[i] = malloc(8);
  }

  switch(bmp->bi.biBitCount) {
  case 24:
    for(i = 0; i < bmp->bi.biHeight; i++) {
      bmp->img[i] = malloc(bmp->bi.biWidth); // vuln?
      for(j = 0; j < bmp->bi.biWidth; j++) {
        bmp->img[i][j] = malloc(3);
        bmp->img[i][j][0] = readBlock(1);
        bmp->img[i][j][1] = readBlock(1);
        bmp->img[i][j][2] = readBlock(1);
      }
    }
    break;
  case 32:
    for(i = 0; i < bmp->bi.biHeight; i++) {
      bmp->img[i] = malloc(bmp->bi.biWidth); // vuln?
      for(j = 0; j < bmp->bi.biWidth; j++) {
        bmp->img[i][j] = malloc(4);
        bmp->img[i][j][0] = readBlock(1);
        bmp->img[i][j][1] = readBlock(1);
        bmp->img[i][j][2] = readBlock(1);
        bmp->img[i][j][3] = readBlock(1);
      }
    }
    break;
  }
}

void open_bmp(const char *path) {
  fp = fopen(path, "rb");
  bmp = malloc(sizeof(BMPFile));
  bmp->header = malloc(0x10);
  bmp->b = malloc(0x28);
}
