from ptrlib import ror, rol
import re

code = """mov     byte ptr [ebp-5Ch], 0E9h
mov     byte ptr [ebp-5Bh], 0AEh
mov     byte ptr [ebp-5Ah], 8Eh
mov     byte ptr [ebp-59h], 0Eh
mov     byte ptr [ebp-58h], 0AEh
mov     byte ptr [ebp-57h], 8Eh
mov     byte ptr [ebp-56h], 88h
mov     byte ptr [ebp-55h], 0ACh
mov     byte ptr [ebp-54h], 4Ch
mov     byte ptr [ebp-53h], 0AEh
mov     byte ptr [ebp-52h], 0ECh
mov     byte ptr [ebp-51h], 6Ah
mov     byte ptr [ebp-50h], 8Eh
mov     byte ptr [ebp-4Fh], 4Eh
mov     byte ptr [ebp-4Eh], 2Dh
mov     byte ptr [ebp-4Dh], 0CDh
mov     byte ptr [ebp-4Ch], 0ECh
mov     byte ptr [ebp-4Bh], 28h
mov     byte ptr [ebp-4Ah], 0
mov     byte ptr [ebp-49h], 0
mov     byte ptr [ebp-48h], 0
mov     byte ptr [ebp-7Ch], 88h
mov     byte ptr [ebp-7Bh], 0ACh
mov     byte ptr [ebp-7Ah], 0CCh
mov     byte ptr [ebp-79h], 0ACh
mov     byte ptr [ebp-78h], 0CDh
mov     byte ptr [ebp-77h], 2Dh
mov     byte ptr [ebp-76h], 8Eh
mov     byte ptr [ebp-75h], 6Fh
mov     byte ptr [ebp-74h], 86h
mov     byte ptr [ebp-73h], 0EEh
mov     byte ptr [ebp-72h], 66h
mov     byte ptr [ebp-71h], 6Eh
mov     byte ptr [ebp-70h], 6
mov     byte ptr [ebp-6Fh], 0ADh
mov     byte ptr [ebp-6Eh], 66h
mov     byte ptr [ebp-6Dh], 0EBh
mov     byte ptr [ebp-6Ch], 86h
mov     byte ptr [ebp-6Bh], 0E6h
mov     byte ptr [ebp-6Ah], 6
mov     byte ptr [ebp-69h], 0ADh
mov     byte ptr [ebp-68h], 4Ch
mov     byte ptr [ebp-67h], 6
mov     byte ptr [ebp-66h], 0ADh
mov     byte ptr [ebp-65h], 4Ch
mov     byte ptr [ebp-64h], 26h
mov     byte ptr [ebp-63h], 0CDh
mov     byte ptr [ebp-62h], 27h
mov     byte ptr [ebp-61h], 0AFh
mov     byte ptr [ebp-60h], 0
"""

output = ""
for line in code.split('\n'):
    r = re.findall(', ([0-9A-F]+)', line)
    if r:
        c = int(r[0], 16)
        output += chr(rol(c, 3, bits=8))
print(output)

# OutputDebugStringA
# Defenit{4w3s0m3_470mb0mb1n9}
