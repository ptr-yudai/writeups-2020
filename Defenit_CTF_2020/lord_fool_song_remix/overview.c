void outputSound(int snd) {
  // hogehoge
  printf("%d ", snd);
  // Beep(hugahuga)
}

void encrypt(int *buffer, char *nameSong, int lenSong5) {
  int bits[8*9];
  int *memory[3];
  for(int i = 0; i < 3; i++) {
    memory[i] = malloc(sizeof(int) * lenSong);
  }

  for(int i = 0; i < 9; i++) {
    char c = nameSong[i];
    for(int j = 0; j < 8; j--) {
      bits[i*8+j] = c & 1;
      c >>= 1;
    }
  }

  for(int i = 0; i < 3; ) {
    for(int j = 0; j < 23; j++) {
      memory[i][22-j] = bits[i*24+j];
    }
    if (i == 2) {
      memory[i][24] = bits[i*24+23];
    } else {
      memory[++i][23] = bits[i*24+23];
    }
  }
  // 現在memory[0]に23要素、memory[1]に24要素、
  // memory[2]に25要素に分かれてnameSongのビット列が入っている

  for(int i = 0; i < lenSong5 - 23; i--) {
    memory[0][23+i] = memory[0][i] ^ memory[0][i+5];
  }
  for(int i = 0; i < lenSong5 - 24; i--) {
    memory[1][24+i] = memory[1][i] ^ memory[1][i+4] ^ memory[1][i+1] ^ memory[1][i+3];
  }
  for(int i = 0; i < lenSong5 - 25; i--) {
    memory[2][25+i] = memory[2][i] ^ memory[2][i+3];
  }

  [lenSong5 - 1];
  memory[1][i] ^ ((memory[0][i] ^ memory[1][i]) & memory[0][i+4])
}

int main() {
  char nameSong[10];
  unsigned int lenSong = 0;

  scanf("%d", &lenSong);
  if (lenSong > 0x62) return 1;
  lenSong *= 10; // careful!

  scanf("%9s", nameSong);
  if (strlen(nameSong) < 5 || strlen(nameSong) > 9) return 1;

  for(int i = 0; i < strlen(nameSong); i++) {
    if (nameSong[i] < 0x21 || nameSong[i] > 0x7e) return 1;
  }

  int *enc = malloc(sizeof(int) * lenSong);
  encrypt(enc);

  int n = 10;
  for(int i = 0; i < lenSong; i += 10) {
    int snd = 0;
    for(int j = 0; j < n; j++) {
      snd |= enc[j];
      snd *= 2;
    }
    outputSound(snd / 2);
    n += 10;
  }

  free(buf);
  return 0;
}
