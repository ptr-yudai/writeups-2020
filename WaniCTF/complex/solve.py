# strlen(flag) = 0x2b
# flag.startswith("FLAG{")
# flag.endswith("}")
from ptrlib import p64

_arr = [0x3131393431333637, 0x3435313837393235, 0x3635313836343636, 0x3834303131353334, 0x3734323435]
_ans = [0x6E44564D6E575F53, 0x576A48545B585747, 0x535D45675D57535E, 0x675A42444550416B, 0x52415E5543]

arr = b"".join([p64(v) for v in _arr])
ans = b"".join([p64(v) for v in _ans])

print(arr)
print(ans)
flag = "FLAG{"
for x, y in zip(arr, ans):
    # flag[i] + 
    flag += chr(x ^ y)
flag += "}"
print(flag)
