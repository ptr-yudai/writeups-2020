s = """mov     [rbp+answers], 63C1D9CBh
mov     [rbp+var_18C], 383F1BB2h
mov     [rbp+var_188], 4107DD90h
mov     [rbp+var_184], 34841FB5h
mov     [rbp+var_180], 3EBDF538h
mov     [rbp+var_17C], 31565585h
mov     [rbp+var_178], 4DEF055Eh
mov     [rbp+var_174], 1BFDEB79h
mov     [rbp+var_170], 24118FF9h
mov     [rbp+var_16C], 272298E8h
mov     [rbp+var_168], 7ABCB5E2h
mov     [rbp+var_164], 9466371h
mov     [rbp+var_160], 7799B008h
mov     [rbp+var_15C], 172289A0h
mov     [rbp+var_158], 401A25A3h
mov     [rbp+var_154], 39CE61B8h
mov     [rbp+var_150], 56EC69A8h
mov     [rbp+var_14C], 106F1FBCh
mov     [rbp+var_148], 77FC40DDh
mov     [rbp+var_144], 4828AE9Dh
mov     [rbp+var_140], 2252BAB7h
mov     [rbp+var_13C], 45935DCCh
mov     [rbp+var_138], 7565BD9Ah
mov     [rbp+var_134], 5AE240C0h
mov     [rbp+var_130], 20EDD601h
mov     [rbp+var_12C], 47362402h
mov     [rbp+var_128], 0B61FCC7h
mov     [rbp+var_124], 7C7607B7h
mov     [rbp+var_120], 6CF7737Dh
mov     [rbp+var_11C], 522262FAh
mov     [rbp+var_118], 5EE1319Bh
mov     [rbp+var_114], 50B94CA2h
mov     [rbp+var_110], 0A617E04h
mov     [rbp+var_10C], 1FE90F3Ch
mov     [rbp+var_108], 53D6C81h
mov     [rbp+var_104], 491F731Dh
mov     [rbp+var_100], 513F6544h
mov     [rbp+var_FC], 532C71B5h
mov     [rbp+var_F8], 651D5EFBh
mov     [rbp+var_F4], 7550F572h
mov     [rbp+var_F0], 7A4F0AFFh
mov     [rbp+var_EC], 5FDA144Eh
mov     [rbp+var_E8], 7E975877h
mov     [rbp+var_E4], 71E8BA89h
mov     [rbp+var_E0], 76FC9DB7h
mov     [rbp+var_DC], 3EB17E6Fh
mov     [rbp+var_D8], 2BB71C42h
mov     [rbp+var_D4], 4DE907F2h"""

import re
answers = []
for line in s.split("\n"):
    r = re.findall(", ([0-9A-F]+)h", line)
    answers.append(int(r[0], 16))
print(answers)
