from ptrlib import *

def oracle(n):
    sock.sendlineafter(":", str(n))
    l = sock.recvline()
    if b"too small" in l:
        return True
    elif b"too big" in l:
        return False
    else:
        print(l)
        sock.interactive()
        exit()

sock = Socket("nc number.wanictf.org 60000")

left, right = 1, 1<<16
while True:
    print(left, right)
    middle = (left + right) // 2
    if oracle(middle):
        left = middle
    else:
        right = middle

sock.interactive()
