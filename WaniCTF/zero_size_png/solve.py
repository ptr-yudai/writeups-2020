import binascii

def p32(data, byteorder='big', signed=False):
    return data.to_bytes(4, byteorder=byteorder, signed=signed)

def u32(data, byteorder='big', signed=False):
    return int.from_bytes(data, byteorder=byteorder, signed=signed)

with open("dyson.png", "rb") as f:
    buf = f.read()

crcval = u32(buf[0x1d:0x21])
print("CRC = " + hex(crcval))

crc = binascii.crc32(buf[0x0c:0x1d])
for width in range(1, 0x1000):
    if width % 100 == 0:
        print(width)
    for height in range(1, 0x1000):
        buf = buf[:0x10] + p32(width) + p32(height) + buf[0x18:]
        crc = binascii.crc32(buf[0x0c:0x1d])
        if crc == crcval:
            print(width, height)
            with open("output.png", "wb") as f:
                f.write(buf)
            exit()

