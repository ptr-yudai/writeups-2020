from ptrlib import *
from functools import reduce
from math import gcd

def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, x, y = egcd(b % a, a)
        return (g, y - (b // a) * x, x)

def modinv(b, n):
    g, x, _ = egcd(b, n)
    if g == 1:
        return x % n

def gen():
    sock.sendlineafter("> ", "1")
    return int(sock.recvline())

sock = Socket("nc lcg.wanictf.org 50001")

def crack_unknown_increment(states, modulus, multiplier):
    increment = (states[1] - states[0]*multiplier) % modulus
    return modulus, multiplier, increment

def crack_unknown_multiplier(states, modulus):
    multiplier = (states[2] - states[1]) * modinv(states[1] - states[0], modulus) % modulus
    return crack_unknown_increment(states, modulus, multiplier)

def crack_unknown_modulus(states):
    diffs = [s1 - s0 for s0, s1 in zip(states, states[1:])]
    zeroes = [t2*t0 - t1*t1 for t0, t1, t2 in zip(diffs, diffs[1:], diffs[2:])]
    modulus = abs(reduce(gcd, zeroes))
    return crack_unknown_multiplier(states, modulus)

nums = []
for i in range(6):
    nums.append(gen())

m, a, b = crack_unknown_modulus(nums)

x = nums[-1]

next = lambda x: (a * x + b) % m

sock.sendlineafter("> ", "2")
for i in range(10):
    x = next(x)
    print(x)
    sock.sendlineafter("> ", str(x))

sock.interactive()
