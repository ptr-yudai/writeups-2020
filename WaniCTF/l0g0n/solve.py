from ptrlib import *

sock = Socket("nc l0g0n.wanictf.org 50002")

for i in range(0x100):
    sock.sendlineafter("> ", "00" * 16)
    sock.sendlineafter("> ", "00" * 16)
    l = sock.recvline()
    if b"Failed" not in l:
        print(l)
        break
    else:
        print(l)
