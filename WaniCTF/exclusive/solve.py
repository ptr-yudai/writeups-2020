from ptrlib import xor

with open("output.txt", "rb") as f:
    enc = f.read()

key = xor(enc, "FLAG{")[:3]
print(xor(enc, key))
