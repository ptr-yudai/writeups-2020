from scapy.all import *

store = {}

def analyse(pkt):
    global store
    if pkt[ICMP].type == 8:
        id = pkt[ICMP].id
        seq = pkt[ICMP].seq
        #pkt[ICMP].show()
        if id not in store:
            store[id] = (seq, pkt[ICMP][Raw].load)
        elif store[id][0] < seq:
            store[id] = (seq, pkt[ICMP][Raw].load)
        #store[id][seq] = pkt[ICMP][Raw].load

sniff(offline="hoge.pcap", filter="icmp", store=0, prn=analyse)

for key in store:
    with open(f"file{key}", "wb") as f:
        f.write(store[key][1])
