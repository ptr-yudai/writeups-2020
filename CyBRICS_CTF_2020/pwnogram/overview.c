typedef struct {
  long size;
  char data[1]; // actually variable length
} STRING;

typedef struct {
  long unk;
  STRING *data; // char* for recv usage
  long len;
} MSG;

MSG *create_register_message(char *username) {
  MSG *msg;
  msg = malloc(0x18); // sizeof MSG
  msg->unk = 0;
  msg->len = strlen(username) + 8;
  msg->data = malloc(msg->len); // sizeof STRING
  msg->data->size = msg->len - 8;
  memcpy(msg->data->data, username, msg->len - 8);
  return msg;
}

MSG *read_msg(int fd) {
  int size; MSG *msg;
  read(fd, &size, 4);
  msg = malloc(0x18);
  read(fd, &msg->unk, 8);
  read(fd, &msg->len, 8);
  msg->data = malloc(msg->len);
  read(fd, msg->data, msg->len);
  return msg;
}

MSG *send_msg(int fd, MSG *msg) {
  int size = msg->len + 8;
  write(fd, &size, 4);
  write(fd, &msg->unk, 8);
  write(fd, msg->data, msg->len);
  return read_msg(fd);
}

char is_msg_ok(MSG *msg, int type) {
  if (msg->unk == type) {
    if (msg->len == 2) {
      if (strncmp(msg->data, "ok", 2) == 0) {
        // this is weird
        // perhaps something like
        // union {
        //    char *data;
        //    STRING *str;
        // } u;
        return 1;
      }
    }
  }
  return 0;
}

int main() {
  char is_ok;
  MSG *m_reg, *new;

  puts("Enter your name: ");
  readline(name, 0x20);
  m_reg = create_register_message(name);
  new = send_msg(fd, m_reg);
  is_ok = is_msg_ok(new);
  release_msg(m_reg);
  release_msg(new);

  return 0;
}
