import json
import hashlib
import subprocess

"""
uiuctf{time_to_take_april_fools_seriously_next_time}
"""

with open("output.json", "r") as f:
    b64list = json.load(f)

#target = b64list[95][0] + b64list[17][0]
target = b64list[95][0] + b64list[106][0]
for i, b64 in enumerate(b64list):
    p = subprocess.Popen(["base64", "-d"],
                         stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)
    r = p.communicate((target + b64[0]).encode())
    p.wait()
    with open(f"{i}.jpeg", "wb") as f:
        f.write(r[0])

"""
i = -1
filelist = set()
orphanlist = set()
while i > -len(b64list)-1:
    b64 = b64list[i][0]
    if '=' in b64:
        # end of file
        filelist.add(b64)
    else:
        # proc orphan
        candidate = set()
        fosterlist = set()
        removelist = set()
        for orphan in orphanlist:
            for j, jpeg in enumerate(filelist):
                try:
                    base64.b64decode(orphan + jpeg)
                    candidate.add(orphan + jpeg)
                    removelist.add(jpeg)
                    fosterlist.add(orphan)
                except:
                    pass
        for orphan in fosterlist:
            orphanlist.remove(orphan)
            # proc current
        for j, jpeg in enumerate(filelist):
            try:
                base64.b64decode(b64 + jpeg)
                candidate.add(b64 + jpeg)
                removelist.add(jpeg)
            except:
                pass
        if candidate == []:
            orphanlist.add(b64)
        for victim in removelist:
            filelist.remove(victim)
        filelist = filelist.union(candidate)
    i -= 1

print(orphanlist)
for jpeg in filelist:
    data = base64.b64decode(jpeg)
    with open(hashlib.md5(data).hexdigest() + ".jpeg", "wb") as f:
        f.write(data)
"""
