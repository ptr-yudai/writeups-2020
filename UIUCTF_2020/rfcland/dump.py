from scapy.all import *
import json
import base64
import subprocess

cnt = 0

b64list = []

def analyze(pkt):
    global b64list
    tcp = pkt[IP][TCP]
    if Raw in tcp:
        try:
            payload = json.loads(tcp[Raw].load)
            b64list.append([payload["data"],])
        except Exception as e:
            b64list[-1].append(tcp[Raw].load.decode())

sniff(offline="challenge.pcap", filter="tcp", store=0, prn=analyze)

with open("output.json", "w") as f:
    json.dump(b64list, f)
