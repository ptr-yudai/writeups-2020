Hello fellow zoomers. All of the files on my calculator are those in the files_on_calculator directory. If you want to build FRY.8xp yourself, go for it. The source is in the fry_src directory. I used v8.8 of the toolchain to compile FRY.8xp.

IMPORTANT NOTE IMPORTANT NOTE IMPORTANT NOTE:
If you are using the CEmu emulator instead of a real calculator, the latest release and master branch will NOT work. You will need to pull from the usb branch and build from source.
