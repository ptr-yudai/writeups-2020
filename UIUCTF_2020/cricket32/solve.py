f = open("cricket32", "rb")
f.seek(0x5cc)

flag = b''
for lp in range(8):
    block = f.read(13)
    seed = (block[0] << 24) | (block[1] << 16) | (block[4] << 8) | block[5]
    # Find block=flag[lp*4:lp*4 + 4] such that crc32_intel(block, seed) == block
    print(hex(seed))

f.close()
