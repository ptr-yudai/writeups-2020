import functools

B = 'hoge - 15 chars'
B = sorted(list(B))
assert len(B) == 15

def u(li):
 Q = [sorted(li[i::3])[::-1] for i in range(3)]
 return Q

keys = u(B)
flag = open('input.txt', 'r').read()

with open("output.txt", "w") as wf:
 cipher = b''
 for i in range(len(flag)):
  c = 0
  for key in keys:
   c ^= (ord(flag[i]) & ~ord(key[i%5])) | (~ord(flag[i]) & ord(key[i%5]))
  cipher += bytes([c])
 wf.write(repr(cipher))

"""
(m[0]~key[0][0]) ^ (m[0]~key[1][0]) ^ (m[0]~key[2][0])
"""
