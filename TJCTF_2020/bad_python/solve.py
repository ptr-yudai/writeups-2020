from z3 import *

with open("input_two.txt", "rb") as f:
    m2 = f.read()
with open("output_two.txt", "r") as f:
    c2 = eval(f.read())

s = Solver()

keys = [
    [BitVec(f"keys@({i},{j})", 8) for j in range(5)]
    for i in range(3)
]
for key in keys:
    for k in key:
        s.add(0x20 < k, k < 0x7f)
    for i in range(len(key) - 1):
        s.add(key[i] >= key[i+1])

for i in range(len(m2)):
    c = 0
    for key in keys:
        c ^= (m2[i] & ~key[i % 5]) | (~m2[i] & key[i % 5])
    s.add(c == c2[i])

r = s.check()
if r == sat:
    m = s.model()
else:
    print(r)
    exit(1)

keys = [
    [m[keys[i][j]] for j in range(5)]
    for i in range(3)
]
print(keys)

with open("output_one.txt", "r") as f:
    c1 = eval(f.read())

m1 = [BitVec(f"m1@{i}", 8) for i in range(len(c1))]
for c in m1:
    s.add(0x20 < c, c < 0x7f)
for i in range(len(m1)):
    c = 0
    for key in keys:
        c ^= (m1[i] & ~key[i % 5]) | (~m1[i] & key[i % 5])
    s.add(c == c1[i])

r = s.check()
if r == sat:
    m = s.model()
else:
    print(r)
    exit(1)

flag = ""
for c in m1:
    flag += chr(m[c].as_long())
print(flag)
