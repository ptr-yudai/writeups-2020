from ptrlib import *

sock = Socket("p1.tjctf.org", 8003)

sock.recvuntil("what is ")
ans = eval(sock.recvline()[:-1])
sock.sendline(str(ans))

print(sock.recv())
print(sock.recv())
print(sock.recv())

sock.interactive()
