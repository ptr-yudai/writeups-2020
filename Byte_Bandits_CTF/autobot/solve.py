from ptrlib import *
import base64
import angr

sock = Socket("pwn.byteband.it", 6000)

for i in range(100):
    elf = sock.recvline()
    if len(elf) < 0x80:
        print(elf)
    with open("test", "wb") as f:
        f.write(base64.b64decode(elf))

    def ok(state):
        if b"good job" in state.posix.dumps(1):
            return True
        return False

    def ng(state):
        if b"Wrong pass" in state.posix.dumps(1):
            return True
        return False

    p = angr.Project("./test", load_options={"auto_load_libs": False})
    state = p.factory.entry_state()
    simgr = p.factory.simulation_manager(state)
    simgr.explore(find=ok, avoid=ng)

    try:
        found = simgr.found[0]
        password = found.posix.dumps(0)
    except IndexError:
        print("Not Found")
        exit()

    print(i, password)
    sock.sendline(password)

sock.interactive()
