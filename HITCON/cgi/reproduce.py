import json
import requests
import msgpack
import base64
import pprint

URL = "http://0.0.0.0:5566"
header = {'Content-Type': 'application/json'}

with open("crash.txt", "r") as f:
    payload = json.loads(f.read())
for key in payload:
    pprint.pprint(msgpack.unpackb(base64.b64decode(payload[key]), raw=False))

r = requests.post(f"{URL}/debug%2fmain.cgi", data=json.dumps(payload))
print(r.text)
