struct lh_table {
  int size;       // 0x00
  int count;      // 0x04
  int collisions; // 0x08
  int resizes;    // 0x0c
  int lookups;    // 0x10
  int inserts;    // 0x14
  int deletes;    // 0x18
  const char *name;      // 0x20
  struct lh_entry *head; // 0x28
  struct lh_entry *tail;
  struct lh_entry *table;
  lh_entry_free_fn *free_fn;
  lh_hash_fn *hash_fn;
  lh_equal_fn *equal_fn;
};

char is_debug() {
  char *p;
  if (!getenv("DEBUG")) return 0;
  if (!(p = getenv("REQUEST_URI"))) return 0;
  if (!(p = strtok(p, "/"))) return 0;
  return strcmp(p, "debug") != 0;
}

void show_login() {
  char html[0x2000];
  FILE *fp = fopen("login.html", "r");
  if (fp == NULL) {
    puts("gg");
  } else {
    fread(html, 1, 0x2000, fp);
    puts("Content-type: text/html\r\n\r");
    printf("%s", html);
  }
}

char try_login() {
  char hash[32];
  char buf[0x2000];
  char *addr = getenv("REMOTE_ADDR");
  if (addr == NULL) return 0;
  if (read(0, buf, 0x2000) <= 0) return 0;
  if (sodium_init() == -1) return 0;
  if (crypto_generichash(hash, 32, buf, strlen(buf), NULL, 0)) return 0;

  if ((*(unsigned short*)&hash[0] == 0xffff)
      && (*(unsigned short*)&hash[16] == 0xffff)) {
    sprintf(buf, "/run/cgi/session/%s", addr);
    return open(path, O_RDWR) != -1;
  }

  return 0;
}

char try_logout() {
  char path[0x2000];
  char *addr = getenv("REMOTE_ADDR");
  if (addr == NULL) return 0;
  sprintf(path, "/run/cgi/session/%s", addr);
  return unlink(path) == 0;
}

char is_logged_in(int is_debug) {
  char path[0x2000];
  if (is_debug) return 1;
  char *addr = getenv("REMOTE_ADDR");
  if (addr == NULL) return 0;
  sprintf(path, "/run/cgi/session/%s", addr);
  return access(path, 0) == 0;
}

void unpack(msgpack_object *obj, char *binary, int size) {
  msgpack_unpacker *unpacker = msgpack_unpacker_new(0x100000);
  if (unpacker == NULL) return 0;

  if (unpcker->free < size) {
    if (msgpack_realloc(unpacker, size) == 1) return 0;
  }

  memcpy(&unpacker->buffer[unpacker->used], binary, size);
  unpacker->used += size;
  unpacker->free -= size;
  memset(obj, 0, 0x20);

  if (msgpack_unpacker_next(unpacker, msgpack) != 2) return 0;

  msgpack_unpacker_free(unpacker);
  return 1;
}

void show_checksum(char *binary) {
  char buffer[0x2000];
  char buf2[0x50];
  char hash[0x20];
  memset(buffer, 0, 0x2000);
  memset(buf2, 0, 0x41);
  memcpy(buffer, binary, 0x2000);

  crypto_generichash(hash, 0x20, buffer, 0x2000, NULL, 0);
  printf("checksum: %s\n", sodium_bin2hex(buf2, 0x41, hash, 0x20));
}

int main(int argc, char **argv) {
  char msgpack[0x2000];
  char isdebug = is_debug();
  if (strcmp(getenv("REQUEST_METHOD"), "GET") == 0) {

    /* GET: Show login page */
    show_login();

  } else if (strcmp(getenv("REQUEST_METHOD"), "PUT") == 0) {

    /* PUT: Login */
    printf("Content-type: text/html\r\n\r\n");
    printf("login %s", try_login() ? "success" : "failed");

  } else if (strcmp(getenv("REQUEST_METHOD"), "DELETE") == 0) {

    /* DELETE: Logout */
    printf("Content-type: text/html\r\n\r\n");
    printf("logout %s", try_logout() ? "success": "failed");

  } else if (strcmp(getenv("REQUEST_METHOD"), "POST") == 0) {

    /* POST: */
    printf("Content-type: text/html\r\n\r\n");
    if (is_logged_in(is_debug) == 0) {

      // if is_debug is true, login is not necessary
      printf("login needed");
      return 0;

    }

    /* Get content length */
    char *ptr = getenv("CONTENT_LENGTH");
    if (ptr == NULL) {
      printf("error 0");
      return 0;
    }
    unsigned int size = strtoul(ptr);
    char *buf = malloc(size);
    if (buf == NULL) {
      printf("error 1");
      return 0;
    }

    /* Get POST data */
    if (read(0, buf, size) != size) {
      printf("error 2");
      return 0;
    }
    close(0);

    json_object *data;
    if (!(data = json_tokener_parse(buf))) {
      printf("error 3");
      return 0;
    }

    struct lh_table *hash_data;
    hash_data = json_object_get_object(data);

    void *key;
    void *value;
    struct lh_entry *cursor;
    for(cursor = hash_data->head; cursor != NULL; cursor = hash_data->next) {
      key = cursor->key;
      value = cursor->value;
      if (json_object_get_type(value) != json_type_string)
        continue;

      // Get value as string
      char *str_value
        = json_object_get_string(json_object_object_get(data, key));
      long len_value = strlen(str_value);
      char *binary;
      if (!(binary = malloc(len_value))) {
        printf("error 4");
        return 1;
      }

      long len_binary;
      if (sodium_base642bin(binary, len_value,   // result binary
                            str_value, len_value // base64 string
                            "\r\n", // ignore
                            &len_binary, NULL, // bin_len, b64_end
                            1)) {
        printf("error 5");
        return 1;
      }

      if (isdebug != 1) {
        show_checksum(binary);
      }
      msgpack_object msgpack_obj;
      if (unpack(&msgpack_obj, binary, len_binary) == 1) {
        printf("unpack failed");
        return 1;
      }

      memset(msgpack, 0, 0x2000);
      if (isdebug != 1) {
        msgpack_object_print_buffer(msgpack, 0x2000, msgpack_obj);
        printf("msgpack: %s\n", msgpack);
      }

      json_object *tmp = sub_401aa0(msgpack_obj);
      printf("%s: %s\n", key, json_object_to_json_string_ext(tmp, 2));

      free_msgpack(msgpack_obj);
      json_object_put(tmp);
      free(binary);
    }

  } else {

    printf("Content-type: text/html\r\n\r\n");
    printf("unsupported");

  }

  return 0;
}
