from ptrlib import *
import requests
import msgpack
import json
import base64
import random
        
randbytes = lambda n: bytes([random.randrange(0x40, 0x60)
                             for i in range(random.randrange(1, n))])

def create_structure(max_depth=5):
    if max_depth == 0 or (max_depth != 5 and random.random() < 0.5):
        T = random.randrange(0, 4)
        if T == 0:
            return random.choice([True, False])
        elif T == 1:
            return random.uniform(-1000.0, 1000.0)
        elif T == 2:
            return random.randint(-100000, 100000)
        elif T == 3:
            return randbytes(0x10)
    else:
        T = random.randrange(0, 2)
        if T == 0:
            obj = {}
            for i in range(random.randint(0, (6-max_depth) * 5)):
                obj[randbytes(0x10)] = create_structure(max_depth-1)
        elif T == 1:
            obj = []
            for i in range(random.randint(0, (6-max_depth) * 5)):
                obj.append(create_structure(max_depth-1))
        return obj

URL = "http://0.0.0.0:5566"
#URL = "http://3.115.95.159:5566"

exploit = {
    "hoge": "huga",
    "taro": [
        "goro",
        "jiro"
    ]
}
data = msgpack.packb(exploit)

header = {'Content-Type': 'application/json'}

while True:
    payload = {"a": base64.b64encode(data).decode(), "z": base64.b64encode(data).decode()}
    for i in range(random.randrange(1, 2)):
        key = bytes2str(randbytes(0x10))
        x = create_structure()
        data = msgpack.packb(x)
        payload[key] = base64.b64encode(data).decode()

    r = requests.post(f"{URL}/debug%2fmain.cgi", data=json.dumps(payload))
    if r.status_code != 200:
        print("[!] ALERT")
        with open("crash.txt", "w") as f:
            f.write(json.dumps(payload))
        import os
        os.system("dmesg | tail")
        input("> ")
    else:
        print(".", end="", flush=True)
