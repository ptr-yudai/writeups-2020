#include <sodium.h>
#include <string.h>
#include <stdlib.h>

// 0xBCC74D1A3431E480A1F77C550F3F7882
// 0x98503E87BD38DF161EB8106157EFA22D
// 82783f0f557cf7a180e431341a4dc7bc2da2ef576110b81e16df38bd873e5098
int main(void) {
  if (sodium_init() == -1) {
    perror("sodium_init");
    return 1;
  }

  unsigned short hash[16];
  char buf[0x10] = { 0 };
  for(unsigned long c = 0; c < 0x100000000; c++) {
    if ((c & 0xffffff) == 0) printf("c = %lx\n", c);
    *(unsigned long*)buf = c;
    crypto_generichash((char*)hash, 32, buf, strlen(buf), NULL, 0);
    if (hash[0] == 0xffff && hash[8] == 0xffff) {
      printf("%lx\n", c);
      printf("%lx-%lx\n", *(unsigned long*)&hash[0], *(unsigned long*)&hash[8]);
      printf("====\n%s\n====\n", buf);
      break;
    }
  }

}
