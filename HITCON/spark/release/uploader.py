from os import system

init = """#!/bin/sh

chown 0:0 -R /
chown 1000 /home/spark
chmod 400 /flag

mount -t proc none /proc
mount -t sysfs none /sys
mount -t devtmpfs none /dev
mount -t tmpfs tmpfs /tmp

sleep 1

dmesg -n 1
insmod spark.ko
chmod 0666 /dev/node
cd /home/spark
timeout -t 60 -s KILL setsid cttyhack su -s /bin/sh root
poweroff -f
"""

assert system("musl-gcc exploit.c -o pwn -static") == 0
assert system("musl-gcc leak.c -o leak -static") == 0
system("cp original.cpio.gz mount")
system("cd mount && gunzip original.cpio.gz")
system("cd mount/root && cpio -idv < ../original.cpio 2>/dev/null")
system("mv pwn mount/root/home/spark")
system("mv leak mount/root/home/spark")
with open("mount/root/init", "w") as f:
    f.write(init)
system("cd mount/root && find . -print0 | cpio --null -o --format=newc > ../original.cpio 2>/dev/null")
system("cd mount && gzip original.cpio")
system("cp mount/original.cpio.gz initramfs.cpio.gz")

system("./run.sh")
