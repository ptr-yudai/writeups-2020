x = """finalize(41) == 0
link(4, 43, -559038737) == 0
finalize(25) == 0
close(53) (and re-open)
finalize(41) == -1
link(19, 50, -559038737) == 0
finalize(8) == 0
link(27, 22, -559038737) == -1
finalize(31) == 0
close(44) (and re-open)
finalize(29) == 0
link(60, 23, -559038737) == -1
finalize(16) == 0
finalize(19) == 0
close(38) (and re-open)
link(49, 48, -559038737) == -1
finalize(33) == 0
close(61) (and re-open)
finalize(43) == 0
link(10, 21, -559038737) == 0
close(58) (and re-open)
finalize(5) == 0
close(57) (and re-open)
link(45, 55, -559038737) == 0
finalize(48) == 0
close(39) (and re-open)
finalize(34) == 0
link(53, 46, -559038737) == -1
close(20) (and re-open)
link(34, 10, -559038737) == -1
finalize(46) == 0
link(59, 43, -559038737) == -1
finalize(50) == -1
close(38) (and re-open)
link(54, 23, -559038737) == -1
finalize(23) == 0
close(2) (and re-open)
link(60, 31, -559038737) == -1
finalize(57) == 0
close(39) (and re-open)
link(57, 2, -559038737) == -1
finalize(13) == 0
close(37) (and re-open)
link(22, 33, -559038737) == -1
finalize(50) == -1
close(10) (and re-open)
link(50, 36, -559038737) == -1
finalize(44) == 0
close(58) (and re-open)
link(16, 35, -559038737) == -1
finalize(55) == 0
link(14, 12, -559038737) == -1
close(9) (and re-open)
finalize(42) == 0
link(29, 5, -559038737) == -1
finalize(6) == 0
close(38) (and re-open)
finalize(13) == -1
link(48, 46, -559038737) == -1
close(25) (and re-open)
link(34, 14, -559038737) == -1
finalize(22) == 0
close(15) (and re-open)
link(16, 35, -559038737) == -1
finalize(21) == close(14) (and re-open)
link(62, 28, -559038737) == -1
close(52) (and re-open)"""

for line in x.split("\n"):
    if "-1" in line: continue
    if line.startswith("close"):
        print(line.split()[0] + ";")
    else:
        line = line.replace("link", "link_")
        print(line[:line.index(" ==")] + ";")
