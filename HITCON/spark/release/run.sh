#!/bin/bash

qemu-system-x86_64 \
  -kernel ./bzImage \
  -initrd ./initramfs.cpio.gz \
  -nographic \
  -monitor none \
  -cpu qemu64 \
  -append "loglevel=3 console=ttyS0 nokaslr panic=1" \
  -m 256M -gdb tcp::1234 \
#  -no-reboot \
