#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/random.h>
#include <unistd.h>

#define SPARK_LINK        0x4008D900
#define SPARK_QUERY       0xC010D903
#define SPARK_GETINFO     0x8018D901
#define SPARK_FINALIZE    0xD902

#define DEV_PATH "/dev/node"

#define N 0x110
static int fd[N];

struct spark_ioctl_query {
  int fd1;
  int fd2;
  long long distance;
};

static void link_(int a, int b, unsigned int weight) {
  printf("link(%d, %d, %d) == ", a, b, weight);
  int r = ioctl(fd[a], SPARK_LINK, fd[b] | ((unsigned long long) weight << 32));
  printf("%d\n", r);
}

static void query(int a, int b) {
  struct spark_ioctl_query qry = {
    .fd1 = fd[a],
    .fd2 = fd[b],
  };
  printf("query(%d, %d) == ", a, b);
  int r = ioctl(fd[0], SPARK_QUERY, &qry);
  printf("%d\n", r);
  if (qry.distance > 0xffffffff) {
    printf("[ALERT] 0x%llx\n", qry.distance);
    exit(1);
  }
}

static void getinfo(int a) {
  unsigned long arr[3];
  printf("getinfo(%d) == ", a);
  int r = ioctl(fd[a], SPARK_GETINFO, &arr);
  printf("%d\n", r);
  if (arr[0] > 0x101
      || arr[1] > 0x101
      || arr[2] > 0x101) {
    printf("[ALERT] 0x%lx / 0x%lx / 0x%lx\n", arr[0], arr[1], arr[2]);
    exit(1);
  }
}

static void finalize(int a) {
  printf("finalize(%d) == ", a);
  int r = ioctl(fd[a], SPARK_FINALIZE);
  printf("%d\n", r);
}

int main(int argc, char *argv[]) {
  int seed;
  setbuf(stdout, NULL);
  getrandom(&seed, sizeof(int), 0);
  srand(seed);

  for(int i = 0; i < N; i++) {
    fd[i] = open(DEV_PATH, O_RDWR);
  }

  int t;
  while(1) {
    switch(rand() % 8) {
    case 0:
      t = rand() % N;
      close(fd[t]);
      fd[t] = open(DEV_PATH, O_RDWR);
      break;
    case 1:
    case 2:
      link_(rand() % N, rand() % N, rand());
      break;
    case 3:
    case 4:
      query(rand() % N, rand() % N);
      break;
    case 5:
    case 6:
      getinfo(rand() % N);
      break;
    case 7:
      finalize(rand() % N);
      break;
    }
  }

  return 0;
}
