#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/random.h>
#include <unistd.h>

#define SPARK_LINK        0x4008D900
#define SPARK_QUERY       0xC010D903
#define SPARK_GETINFO     0x8018D901
#define SPARK_FINALIZE    0xD902

#define DEV_PATH "/dev/node"

#define N 0x110
static int fd[N];

struct spark_ioctl_query {
  int fd1;
  int fd2;
  long long distance;
};

static void link_(int a, int b, unsigned int weight) {
  printf("link(%d, %d, %d) == ", a, b, weight);
  int r = ioctl(fd[a], SPARK_LINK, fd[b] | ((unsigned long long) weight << 32));
  printf("%d\n", r);
}

static void query(int a, int b) {
  struct spark_ioctl_query qry = {
    .fd1 = fd[a],
    .fd2 = fd[b],
  };
  printf("query(%d, %d) == ", a, b);
  int r = ioctl(fd[0], SPARK_QUERY, &qry);
  printf("%d\n", r);
  if (qry.distance > 0xffffffff) {
    printf("[ALERT] 0x%llx\n", qry.distance);
    exit(1);
  }
}

static void getinfo(int a) {
  unsigned long arr[3];
  printf("getinfo(%d) == ", a);
  int r = ioctl(fd[a], SPARK_GETINFO, &arr);
  printf("%d\n", r);
  if (arr[0] > 0x101
      || arr[1] > 0x101
      || arr[2] > 0x101) {
    printf("[ALERT] 0x%lx / 0x%lx / 0x%lx\n", arr[0], arr[1], arr[2]);
    exit(1);
  }
}

static void finalize(int a) {
  printf("finalize(%d) == ", a);
  int r = ioctl(fd[a], SPARK_FINALIZE);
  printf("%d\n", r);
}

int main(int argc, char *argv[]) {
  int seed;
  setbuf(stdout, NULL);
  getrandom(&seed, sizeof(int), 0);
  srand(seed);

  for(int i = 0; i < N; i++) {
    fd[i] = open(DEV_PATH, O_RDWR);
  }

link_(58, 87, 2067260398);
link_(43, 268, 1076931688);
close(159);
link_(154, 244, 858872557);
close(65);
link_(73, 109, 2069034372);
close(53);
link_(39, 73, 1270951734);
link_(77, 88, 1958435543);
link_(67, 240, 195410071);
close(149);
finalize(254);
close(88);
close(6);
finalize(14);
close(212);
close(146);
close(67);
link_(173, 235, 1349498917);
link_(118, 261, 1073798624);
close(154);
link_(10, 21, 853596999);
close(223);
finalize(201);
close(59);
close(222);
close(145);
close(78);
link_(33, 114, 1342095240);
close(162);
link_(48, 261, 1804732117);
close(3);
close(239);
close(224);
close(27);
link_(182, 245, 723395664);
close(65);
close(143);
close(264);
link_(21, 40, 1717850774);
close(223);
close(233);
close(42);
close(221);
link_(66, 84, 191415004);
finalize(164);
close(158);
finalize(62);
finalize(174);
finalize(226);
close(88);
finalize(132);
close(130);
close(104);
close(20);
close(122);
close(48);
finalize(223);
link_(232, 250, 776576590);
close(228);
close(99);
link_(260, 219, 2140971027);
close(134);
link_(38, 56, 1633274667);
close(238);
link_(72, 122, 1484128890);
link_(81, 131, 1740737922);
close(43);
link_(145, 203, 1436538540);
close(134);
close(23);
link_(19, 72, 987891160);
close(93);
finalize(138);
close(167);
close(159);
finalize(175);
link_(261, 17, 463887833);
close(28);
close(177);
link_(8, 143, 592206864);
close(182);
close(131);
link_(8, 169, 1968847000);
link_(102, 251, 1019554010);
close(64);
close(238);
link_(134, 260, 749358159);
close(25);
close(10);
close(246);
close(121);
link_(104, 218, 1645817516);
close(143);
close(160);
finalize(30);
close(198);
close(245);
close(199);
close(92);
close(171);
close(210);
finalize(259);
link_(68, 111, 1385676869);
close(124);
finalize(247);
link_(236, 85, 241000867);
close(232);
close(208);
close(51);
close(94);
link_(123, 215, 473155713);
link_(77, 7, 675866867);
finalize(93);
close(202);
link_(67, 91, 1623776108);
close(274);
finalize(6);
finalize(258);
finalize(246);
link_(234, 20, 212070441);
link_(169, 119, 1603470229);
close(239);
close(271);
link_(41, 105, 722296133);
close(221);
finalize(46);
link_(70, 144, 1356026750);
finalize(261);
link_(173, 180, 2093460336);
finalize(160);
finalize(115);
finalize(270);
close(52);
close(215);
finalize(265);
close(97);
finalize(41);
link_(143, 230, 1627031236);
close(53);
finalize(225);
link_(143, 235, 1392618553);
close(199);
link_(12, 145, 824921721);
close(61);
link_(117, 101, 2013714082);
finalize(61);
link_(248, 56, 1932446375);
link_(55, 107, 1674247163);
finalize(216);
close(268);
close(168);
finalize(237);
close(123);
close(228);
close(99);
close(85);
close(44);
link_(116, 123, 513548572);
close(124);
link_(244, 164, 555877959);
link_(145, 167, 979125640);
close(131);
close(160);
finalize(241);
finalize(113);
close(137);
close(70);
finalize(131);
finalize(222);
close(32);
close(182);
close(14);
link_(161, 251, 1320039042);
finalize(146);
finalize(82);
close(15);
close(56);
link_(124, 127, 241346632);
close(18);
link_(54, 179, 1230152885);
link_(116, 154, 168533694);
close(184);
close(139);
close(7);
link_(162, 213, 164845459);
close(106);
close(129);
finalize(150);
link_(97, 168, 1504630066);
finalize(253);
close(68);
finalize(176);
close(61);
finalize(249);
close(21);
close(78);
close(159);
link_(108, 170, 788557318);
close(104);
close(164);
finalize(127);
close(61);
close(24);
close(247);
link_(123, 251, 1030343072);
finalize(128);
close(188);
close(53);
close(66);
close(135);
close(117);
close(264);
finalize(143);
close(116);
close(146);
finalize(94);
finalize(204);
finalize(130);
close(13);
link_(1, 78, 1222322837);
close(88);
link_(80, 121, 260740088);
close(48);
close(93);
close(265);
finalize(149);
link_(152, 248, 1363439460);
finalize(134);
close(208);
close(107);
finalize(177);
finalize(38);
close(11);
close(21);
finalize(114);
close(62);
finalize(213);
finalize(174);
close(76);
close(42);
close(179);
close(177);
finalize(214);
finalize(40);
link_(9, 24, 1073211756);
link_(159, 15, 1495381531);
link_(125, 196, 598713598);
link_(79, 240, 1814182498);
finalize(147);
close(173);
close(30);
close(268);
link_(1, 79, 1579711221);
finalize(27);
finalize(187);
close(77);
link_(234, 74, 339429010);
close(136);
finalize(117);
link_(72, 167, 232792532);
close(74);
close(270);
close(141);
close(178);
link_(220, 174, 783059985);
close(187);
close(107);
finalize(231);
close(168);
finalize(229);
link_(95, 202, 881896214);
link_(107, 267, 994420960);
close(70);
link_(197, 71, 391037844);
close(108);
close(163);
close(87);
finalize(252);
finalize(92);
close(259);
close(218);
close(148);
close(68);
finalize(63);
close(151);
finalize(161);
close(59);
close(265);
finalize(210);
link_(171, 49, 537288766);
close(38);
close(233);
close(43);
close(107);
close(47);
link_(86, 197, 368258357);
close(98);
close(87);
link_(54, 123, 1176177096);
finalize(52);
link_(203, 121, 1096532554);
close(15);
close(87);
close(113);
close(223);
link_(5, 261, 736073123);
close(108);
close(118);
link_(225, 170, 596082956);
close(52);
link_(257, 41, 626007108);
link_(184, 256, 1730204972);
close(133);
finalize(179);
close(198);
close(181);
link_(112, 267, 2120151661);
close(188);
close(214);
close(221);
link_(8, 71, 981257932);
link_(83, 151, 1703775395);
finalize(160);
close(241);
link_(34, 268, 852027677);
finalize(33);
link_(26, 11, 1071474129);
finalize(255);
close(93);
close(59);
close(50);
close(183);
close(139);
finalize(268);
close(85);
close(23);
finalize(87);
close(90);
close(264);
link_(118, 196, 603561317);
close(162);
link_(26, 224, 869312055);
finalize(48);
close(173);
finalize(202);
finalize(55);
close(246);
close(173);
close(96);
close(218);
link_(83, 22, 1260189016);
close(72);
close(128);
close(100);
link_(197, 256, 1062274862);
close(164);
close(226);
close(133);
close(168);
finalize(120);
finalize(211);
close(217);
close(230);
finalize(70);
close(4);
close(88);
close(136);
close(148);
close(89);
link_(193, 4, 1519636540);
close(84);
close(33);
finalize(215);
close(23);
close(219);
finalize(39);
finalize(263);
close(47);
close(244);
close(233);
close(202);
close(239);
finalize(234);
close(143);
close(163);
close(69);
link_(256, 84, 1042545244);
finalize(207);
finalize(84);

 return 0;
}
