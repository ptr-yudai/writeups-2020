from ptrlib import *

def create_node(pred_id):
    sock.sendlineafter(">\n", "1")
    sock.sendlineafter(">\n", str(pred_id))
    return int(sock.recvline())
def connect_node(pred_id, succ_id):
    sock.sendlineafter(">\n", "2")
    sock.sendlineafter(">\n", str(pred_id))
    sock.sendlineafter(">\n", str(succ_id))
def disconnect_node(pred_id, succ_id):
    sock.sendlineafter(">\n", "3")
    sock.sendlineafter(">\n", str(pred_id))
    sock.sendlineafter(">\n", str(succ_id))
def write_text(node_id, data):
    sock.sendlineafter(">\n", "4")
    sock.sendlineafter(">\n", str(node_id))
    sock.sendlineafter(">\n", str(len(data)))
    sock.sendafter(">\n", data)
def write_bin(node_id, data):
    sock.sendlineafter(">\n", "5")
    sock.sendlineafter(">\n", str(node_id))
    sock.sendlineafter(">\n", str(len(data)))
    sock.sendafter(">\n", data)
def read_text(node_id):
    sock.sendlineafter(">\n", "6")
    sock.sendlineafter(">\n", str(node_id))
    text = sock.recvuntil("op>\n")
    sock.unget(">\n")
    return text[:-4]
def gc():
    sock.sendlineafter(">\n", "7")

node_id = 0
class Node(object):
    def __init__(self):
        global node_id
        self.links = set()
        self.nid = node_id
        node_id += 1
    def link(self, i):
        self.links.add(i)
    def unlink(self, i):
        self.links.discard(i)

def update_available(available, root, flags):
    if root in flags: return
    available.add(root)
    flags.append(root)
    for node in root.links:
        update_available(available, node, flags)

import random
root = Node()
available = set()
pickup = lambda: random.choice(list(available))
craft = lambda: bytes([random.randrange(0, 0x100)
                       for i in range(random.randrange(0, 0x10))])

sock = Process("./dual-2df8d4005c5d4ffc03183a96a5d9cb55ac4ee56dfb589d65b0bf4501a586a4b0")

while True:
    available = set()
    flags = []
    update_available(available, root, flags)

    r = random.randrange(0, 7)
    if r == 0:
        parent = pickup()
        node = Node()
        parent.link(node)
        print(f"create_node({parent.nid})")
        if create_node(parent.nid) > 100:
            print("[-] Too large")
            exit()
    elif r == 1:
        src, dst = pickup(), pickup()
        print(f"connect_node({src.nid}, {dst.nid})")
        connect_node(src.nid, dst.nid)
        src.link(dst)
    #elif r == 2:
    #    src = pickup()
    #    if len(src.links) == 0:
    #        continue
    #    dst = random.choice(list(src.links))
    #    print(f"disconnect_node({src.nid}, {dst.nid})")
    #    disconnect_node(src.nid, dst.nid)
    #    src.unlink(dst)
    elif r == 3:
        target = pickup()
        data = craft()
        print(f"write_text({target.nid}, {data})")
        write_text(target.nid, data)
    elif r == 4:
        target = pickup()
        data = craft()
        print(f"write_bin({target.nid}, {data})")
        write_bin(target.nid, data)
    #elif r == 5:
    #    target = pickup()
    #    print(f"read_text({target.nid})")
    #    read_text(target.nid)
    elif r == 6:
        print("gc()")
        gc()
