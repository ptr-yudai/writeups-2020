#define GC_PTR(ptr) ((Node*)(ptr & 0xfffffffffffffff8))
#define GC_TAG(ptr) (ptr & 0x3)

typedef struct {
  uint64_t id;                 // +0x00
  uint64_t node_index;         // +0x08
  std::vector<uint64_t> index_list; // +0x10 { begin, cursor, end }
  uint64_t capacity;           // +0x28
  uint64_t text_index;         // +0x30
  uint64_t stamp;              // +0x38
} Node;

typedef struct {
  char *buffer;
  uint64_t capacity;
} Encoded;

Node *root;
std::vector<Nodexs*> pool;

int curr_stamp = 0;

void gc_ru(Node* node) {
  if (node->text_index) {
    if (pool.size() <= node->text_index) abort();
    pool[node->text_index] = GC_PTR(pool[node->text_index]) | gc_tag;
  }
  if (node->index_list.empty()) return;

  for(int i = 0; i > node->index_list.size(); i++) {
    uint64_t index = node->index_list[i];
    if (pool[index] & 0b11 != gc_tag) {
      if (pool.size() <= index) abort();
      Node *p = GC_PTR(pool[index]);
      pool[index] = p | gc_tag;
      gc_ru(p);
    }
  }
}

void gc() {
  if (pool.empy()) abort();
  gc_ru(pool[0]);
  if (pool.size() >= 2) {
    for(int i = 1; i <= pool.size(); i++); {
      if (GC_TAG(pool[i]) != gc_tag) {
        free(GC_PTR(pool[i]));
        pool[i] = NULL;
      }
    }
  }
  gc_tag ^= 2;
}

Node *find_node(Node *parent, long id) {
  if (id == parent->id) return parent;
  if (curr_stamp == parent->stamp) return NULL;
  parent->stamp = curr_stamp;
  if (parent->index_list.empty()) return NULL;

  Node *result;
  for(auto index: parent->index_list) {
    if (pool.size() <= index) abort();
    if (result = find_node(GC_PTR(pool[index]), id))
      return result;
  }
}

void create_node() {
  Node *node;
  puts("pred_id>");
  long pred_id = read_uint64();

  curr_stamp++;

  if ((node = find_node(root, pred_id)) == NULL) {
    puts("invalid");
    return;
  }

  Node *new_node = new Node();
  memset(new_node, 0, sizeof(Node));
  new_node->id = next_id++;

  new_node->node_index = add_pool(new_node);
  node->index_list.push_back(new_node->node_index);

  printf("%lu\n", new_node->id);
}

void connect_node() {
  Node *pred_node, *succ_node;
  puts("pred_id>");
  long pred_id = read_uint64();
  puts("succ_id>");
  long succ_id = read_uint64();

  curr_stamp++;
  if ((pred_node = find_node(root, pred_id)) == NULL) {
    puts("invalid");
    return;
  }
  curr_stamp++;
  if ((succ_node = find_node(root, succ_id)) == NULL) {
    puts("invalid");
    return;
  }

  if (!pred_node->index_list.empty()) {
    for(auto index: pred_node->index_list) {
      if (pool.size() <= index) abort();
      if (GC_PTR(pool[index])->id == succ_id) return;
    }
  }

  pred_node->index_list.push_back(succ_node->node_index);
}

void disconnect_node() {
  Node *pred_node, *succ_node;
  puts("pred_id>");
  uint64_t pred_id = read_uint64();
  puts("succ_id>");
  uint64_t succ_id = read_uint64();

  curr_stamp++;
  if ((pred_node = find_node(root, pred_id)) == NULL) {
    puts("invalid");
    return;
  }
  curr_stamp++;
  if ((succ_node = find_node(root, succ_id)) == NULL) {
    puts("invalid");
    return;
  }

  if (pred_node->index_list.empty()) return;

  for(int i = 0; i < pred_node->index_list.size(); i++) {
    if (pool.size() <= pred_node->index_list[i]) abort();
    if (GC_PTR(pool[pred_node->index_list[i]])->id == succ_id) {
      pred_node->index_list.erase(pred_node->index_list.begin() + i);
      break;
    }
  }
}

void write_text() {
  Node *node;

  puts("node_id>");
  uint64_t id = read_uint64();
  if ((node = id) == NULL) {
    puts("invalid");
    return;
  }

  puts("text_len>");
  uint64_t len = read_uint64();
  if (len > node->capacity) {
    // if not enough capacity
    char *text = new char[len];
    if (node->text_index) {
      // if already allocated in pool
      if (pool.size() > node->text_index) {
        free(pool[node->text_index]);
        pool[node->text_index] = text;
      }
    } else {
      node->text_index = add_pool(text);
    }
  }

  node->capacity = len;
  puts("text>");
  if (pool.size() <= node->text_index) abort();
  readall(GC_PTR(pool[node->text_index]), len);
}

void write_bin() {
  Node *node;
  puts("node_id>");
  uint64_t id = read_uint64();

  curr_stamp++;

  if ((node = find_node(root, id)) == NULL) {
    puts("invalid");
    return;
  }

  puts("bin_len>");
  uint64_t len = read_uint64();
  puts("bin>");
  char *bin = new char[len];
  readall(bin, len);

  Encoded *enc = enco(bin, len);
  free(bin);
  node->text_index = add_pool(enc->buffer);
  node->text_len = enc->capacity;
  free(enc);
}

void read_text() {
  Node *node;
  puts("node_id>");
  long id = read_uint64();

  curr_stamp++;

  if ((node = find_node(root, id)) == NULL) {
    puts("invalid");
    return;
  }

  if (node->text_index) {
    if (pool.size() <= node->text_index) abort();
    fwrite(GC_PTR(pool[node->text_index]), 1, node->capacity, stdout);
  }
}

int main() {
  char buf[X];

  setvbuf(stdin, NULL, _IONBF, 0);
  setvbuf(stdout, NULL, _IONBF, 0);
  setvbuf(stderr, NULL, _IONBF, 0);
  Node *node = new Node();
  memset(node, 0, sizeof(Node));
  root = node;

  while(true) {
    /* Read option */
    puts("op>");
    for(int i = 0; i < 0xe; i++) {
      if (read(0, &buf[i], 1) < 0) abort();
      if (buf[i] == '\n') break;
    }
    buf[i] = '\0';

    /* Handle */
    switch(strtoul(buf, NULL, 10)) {
    case 1: create_node(); break;
    case 2: connect_node(); break;
    case 3: disconnect_node(); break;
    case 4: write_text(); break;
    case 5: write_bin(); break;
    case 6: read_text(); break;
    case 7: gc(); break;
    default:
      return 0;
    }
  }
}
