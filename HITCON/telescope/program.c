char *slots[1024];
int slot_sizes[1024];

/**
 * Telescope is a class that extends protobuf::MessageLite
   typedef struct {
     void *vtable; // 0x4060f8
     void *tagged_ptr;
   } Telescope;
 */

int get_slot() {
  puts("slot>");
  unsigned int slot = read64();
  if (slot >= 0x400) abort();
  if (slots[slot] == NULL) abort();
  return slot;
}

/* loc_4038f8 */
void new_slot() {
  puts("slot>");
  int slot = read64(); // ebx
  if (slot < 0 || slot >= 0x400) abort();
  puts("size>");
  int size = read64(); // r12d->ebp
  if (size < 0) abort();

  char *ptr; // r15
  if ((ptr = malloc(size)) == NULL) abort();
  memset(ptr, 0, size);

  slots[slot] = ptr;
  slot_sizes[slot] = size;
}

/* loc_403b16 */
void write_slot() {
  int slot = get_slot(); // ebp
  puts("data>");
  int size = slot_sizes[slot]; // ebx
  if (size <= 0) return;
  char *ptr = slots[slot]; // rbp

  for(long i = 0; i < size; i++) {
    read(0, &ptr[i], 1);
  }
}

/* loc_403b76 */
void delete_slot() {
  int slot = get_slot();
  free(slots[slot]);
  slots[slot] = NULL;
  slot_sizes[slot] = 0;
}

/* loc_403ba7 */
void do_telescope() {
  Telescope telescope();
  int slot = get_slot(); // ebp
  telescope.ParseFromArray(slots[slot], slot_sizes[slot]);
  /* validate parsed buffer here?
  if (cond:loc_403be8) {
    if (cond:loc_403bf7) abort();
  }
  */
  if (telescope.pass != 0xdeadbeef) abort();

  printf("# of Lens: %d\n", telescope.lens.size());

  string s = telescope.SerializeAsString();
  memcpy(slots[slot], s.c_str(), s.size());
}

/* loc_4037e4 */
void show_slot() {
  int slot = get_slot();
  write(1, slots[slot], slot_sizes[slot]);
}

int main() {
  setvbuf(stdin, NULL, _IONBF, 0);
  setvbuf(stdout, NULL, _IONBF, 0);
  setvbuf(stderr, NULL, _IONBF, 0);

  while(1) {
    puts("op>");
    switch(read64()) {
    case 1: new_slot(); break;
    case 2: write_slot(); break;
    case 3: delete_slot(); break;
    case 4: do_telescope(); break;
    case 5: show_slot(); break;
    default: return 0;
    }
  }
}
