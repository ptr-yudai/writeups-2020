# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: telescope.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='telescope.proto',
  package='',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\x0ftelescope.proto\"/\n\tTelescope\x12\x10\n\x04lens\x18\x01 \x03(\x03\x42\x02\x10\x01\x12\x10\n\x08password\x18\x02 \x01(\x03')
)




_TELESCOPE = _descriptor.Descriptor(
  name='Telescope',
  full_name='Telescope',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='lens', full_name='Telescope.lens', index=0,
      number=1, type=3, cpp_type=2, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=_b('\020\001'), file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='password', full_name='Telescope.password', index=1,
      number=2, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=19,
  serialized_end=66,
)

DESCRIPTOR.message_types_by_name['Telescope'] = _TELESCOPE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Telescope = _reflection.GeneratedProtocolMessageType('Telescope', (_message.Message,), dict(
  DESCRIPTOR = _TELESCOPE,
  __module__ = 'telescope_pb2'
  # @@protoc_insertion_point(class_scope:Telescope)
  ))
_sym_db.RegisterMessage(Telescope)


_TELESCOPE.fields_by_name['lens']._options = None
# @@protoc_insertion_point(module_scope)
