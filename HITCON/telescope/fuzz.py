from ptrlib import *
from telescope_pb2 import Telescope

def new_slot(slot, size):
    sock.sendlineafter(">\n", "1")
    sock.sendlineafter(">\n", str(slot))
    sock.sendlineafter(">\n", str(size))
def write_slot(slot, data):
    sock.sendlineafter(">\n", "2")
    sock.sendlineafter(">\n", str(slot))
    sock.sendafter(">\n", data)
def delete_slot(slot):
    sock.sendlineafter(">\n", "3")
    sock.sendlineafter(">\n", str(slot))
def telescope(slot):
    sock.sendlineafter(">\n", "4")
    sock.sendlineafter(">\n", str(slot))
def show_slot(slot):
    sock.sendlineafter(">\n", "5")
    sock.sendlineafter(">\n", str(slot))
    l = sock.recvuntil("op>\n")[:-4]
    sock.unget("op>\n")
    return l

sock = Socket("localhost", 9999)

import random
while True:
    t = Telescope()
    for i in range(random.randrange(0, 0x100)):
        t.lens.append(random.randrange(0, 0x100000000))
    t.password = 0xdeadbeef
    payload = t.SerializeToString()
    print(hex(len(payload)), payload)
    new_slot(0, len(payload))
    write_slot(0, payload)
    telescope(0)
    r = show_slot(0)
    if len(r) != len(payload):
        print("[!] ALERT")
        print(hex(len(r)), r)
        print(t)
        input("> ")
    delete_slot(0)

sock.interactive()

