#!/usr/bin/python3
import os
import sys
import random
import subprocess
import string
import shutil

def main():
    os.system("./exploit/compile.sh")
    with open("./exploit/exploit", "rb") as f:
        data = f.read()
    os.system("rm -rf ./home")
    os.makedirs("./home/user/")
    shutil.copyfile("./app/rooted.cpio", "./rootfs.img")
    with open("./pwn", "wb") as initramfs:
        initramfs.write(data)

    os.system("chmod 755 ./pwn && echo ./pwn | cpio -R +1000:+1000 -H newc -o >> ./rootfs.img");
    print("Starting computer...")
    os.execvp("qemu-system-aarch64",
              ["qemu-system-aarch64",
               "-m", "64M",
               "-kernel", "./app/Image",
               "-initrd", "./rootfs.img",
               "-nographic",
               "-machine", "virt",
               "-cpu", "max",
               "-smp", "1",
               "-monitor", "none",
               "-no-reboot",
               "-nodefaults", "-snapshot",
               "-chardev", "stdio,id=char0,mux=off,signal=off",
               "-serial", "chardev:char0",
               "-sandbox", "on,obsolete=deny,elevateprivileges=deny,spawn=deny,resourcecontrol=deny",
               "-append", "oops=panic loglevel=2 panic=1 kaslr",
               "-gdb", "tcp::1234"]
    )

if __name__ == "__main__":
    main()
