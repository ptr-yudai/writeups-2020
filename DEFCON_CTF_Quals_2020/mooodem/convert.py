from ptrlib import *

with open("output.bin", "rb") as f:
    buf = f.read()

output = ''
for c in buf:
    output += str(c)

data = b''
output = output[10*46+5:]
#output = output[9:]
for i in range(0, len(output), 10):
    block = output[i:i+10]
    if block[0] != '0' or block[9] != '1': #assert
        break
    c = int(block[1:9][::-1], 2) ^ 0xff
    data += bytes([ror(c, 1, bits=8)])
print(bytes2str(data))
