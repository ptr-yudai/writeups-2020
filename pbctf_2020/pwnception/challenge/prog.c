int kernel_interruption() {
  if (signum == 0x70) {

    if (rax == 0x9e && rdi == 0x1002) {
      nazo_bool = 1;
      nazo_qword = rsi;
    } else if (rax == 0x0f) {
      nazo_bool_2 = 1;
      for(i = 0; i <= 0x10; i++) {
        uc_mem_read(uc, rsp+i*8+0x28, &regs[i*8], 8);
      }
    } else if (rax == 0x0a) {
      uc_mem_protect(uc, rdi, rsi, rdx & 0b111);
    } else if (rax == 0x09) {
      uc_mem_map(uc, rdi, rsi, rdx & 0b111);
    }

  } else if (signum == 0x71) {

    if (rax == 0) {
      ptr = malloc(rdi);
    } else if (ptr != NULL && rax == 1) {
      uc_mem_read(uc, rdi, ptr, rsi);
    } else if (ptr != NULL && rax == 2) {
      uc_mem_write(uc, rdi, ptr, rsi);
    } else if (ptr != NULL && rax == 3) {
      free(ptr);
    }

  }
}
