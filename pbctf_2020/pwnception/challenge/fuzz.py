from ptrlib import *
import random

def craft_block(depth=5):
    if depth < 0: return ""
    code = ""
    for i in range(10):
        ope = random.randrange(0, 10)
        if 0 <= ope < 4:
            code += ">"
        elif 4 <= ope < 5:
            code += "<"
        elif 5 <= ope < 6:
            code += "+"
        elif 6 <= ope < 7:
            code += "-"
        elif 7 <= ope < 8:
            code += "."
        elif depth != 0:
            code += "[" + craft_block(depth-1) + "]"
    return code
            

sock = Process(["./main", "./kernel", "./userland"])

code = craft_block()
sock.sendafter(": ", code + "!")

try:
    while True:
        r = sock.recv()
        if r:
            print(r)
except:
    print(code)

sock.interactive()
