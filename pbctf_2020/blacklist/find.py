import subprocess

output = subprocess.check_output(["python3", "ls.py", "/flag_dir"])

dir1 = []
for line in set(output.decode().split("\n")):
    if line == '' or line == '..' or line == '.':
        continue
    dir1.append(f"/flag_dir/{line}")
print(dir1)

dir2 = []
for path in dir1:
    print(path)
    output = subprocess.check_output(["python3", "ls.py", path])
    for line in set(output.decode().split("\n")):
        if line == '' or line == '..' or line == '.':
            continue
        dir2.append(f"{path}/{line}")
print(dir2)

files = []
for path in dir2:
    print(path)
    output = subprocess.check_output(["python3", "ls.py", path])
    for line in set(output.decode().split("\n")):
        if line == '' or line == '..' or line == '.':
            continue
        files.append(f"{path}/{line}")
print(files)

for path in files:
    print(path)
    output = subprocess.check_output(["python3", "cat.py", path])
    if b'pbctf{' in output:
        print("***********************")
        print(path)
        print(output)
        exit(0)
