from ptrlib import *
import random

def edit(index, offset, data):
    sock.sendlineafter("> ", "0")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(offset))
    sock.sendafter(": ", data)
def view(index):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(index))
    sock.recvline()
    sock.recvuntil(" = ")
    return sock.recvuntil("********")[:-9]
def leak(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
def subaction(action):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(": ", str(action))
def addrof(index): # my debug function
    sock.sendlineafter("> ", "4")
    sock.sendlineafter(": ", str(index))
    r = sock.recvregex("\((0x[0-9a-f]+)\) = char\[\] @ (0x[0-9a-f]+)")
    return int(r[0], 16), int(r[1], 16)

sock = Process("./deploy/bin/jheap")

rndbytes = lambda n: bytes([random.randrange(0, 0x100) for i in range(n)])

sizelist = []
for i in range(48):
    sizelist.append(int(sock.recvline()))
for i in range(3):
    print(i, addrof(i))

input("go?")
while True:
    index = random.randrange(0, 2)
    option = random.randrange(0, 4)
    if option == 0:
        print(f"[+] leak({index})")
        leak(index)
    elif option == 1:
        print(f"[+] subaction(1)")
        subaction(1)
    else:
        offset = random.randrange(0, sizelist[index])
        size = sizelist[index] - offset - 1
        #data = rndbytes(size)
        data = "A" * size
        print(f"[+] edit({index}, {size}, {data}) / {sizelist[index]}")
        edit(index, offset, data)

sock.interactive()
