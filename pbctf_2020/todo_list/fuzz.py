import random
import string
from ptrlib import *

NUM_CATEGORY = 6

def add(category, tasks):
    sock.sendlineafter(">>> ", "1")
    sock.sendlineafter(">>> ", str(category))
    sock.sendlineafter(": ", str(len(tasks)))
    for task in tasks:
        sock.sendline(task)
def view(category):
    sock.sendlineafter(">>> ", "2")
    sock.sendlineafter(">>> ", str(category))
    sock.recvline()
    tasks = []
    while True:
        l = sock.recvline()
        if b'Please select' in l: break
        tasks.append(l.lstrip())
    return tasks
def finish(category, removes):
    sock.sendlineafter(">>> ", "3")
    sock.sendlineafter(">>> ", str(category))
    for i in range(len(removes)):
        sock.sendlineafter(">>> ", str(removes[i]))
        for j in range(i, len(removes)):
            if removes[j] > removes[i]:
                removes[j] -= 1
    sock.sendlineafter(">>> ", "-1")

TODO = [[] for i in range(NUM_CATEGORY)]
randstr = lambda n: ''.join([
    random.choice(string.ascii_letters + string.digits)
    for i in range(n)
])

sock = Process("./todo")
input()

while True:
    category = random.randrange(0, NUM_CATEGORY)
    choice = random.randrange(0, 3)
    if choice == 0:
        #if len(TODO[category]) == 0:
            tasks = [randstr(random.randint(1, 0x100))
                     for i in range(random.randint(1, 10))]
            print(f"add({category}, {tasks})")
            add(category, tasks)
            TODO[category] += list(tasks)
    elif choice == 1:
        if len(TODO[category]) > 0:
            print(f"view({category})")
            view(category)
    elif choice == 2:
        if len(TODO[category]) > 0:
            target = random.sample(TODO[category],
                                   random.randint(1, len(TODO[category])))
            removes = [TODO[category].index(x)+1 for x in target]
            print(f"finish({category}, {removes})")
            finish(category, removes)
            for x in target:
                TODO[category].remove(x)

"""
while True:
    category = random.randrange(0, NUM_CATEGORY)
    choice = random.randrange(0, 3)
    if choice == 0:
        tasks = [randstr(random.randint(1, 0x80))
                 for i in range(random.randint(1, 10))]
        print(f"add({category}, {tasks})")
        add(category, tasks)
        TODO[category] += list(tasks)
    elif choice == 1:
        if len(TODO[category]) > 0:
            print(f"view({category})")
            view(category)
    elif choice == 2:
        if len(TODO[category]) > 0:
            target = random.sample(TODO[category],
                                   random.randint(1, len(TODO[category])))
            removes = [TODO[category].index(x)+1 for x in target]
            print(f"finish({category}, {removes})")
            finish(category, removes)
"""

sock.interactive()
