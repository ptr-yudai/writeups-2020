def spike_state(i):
    if i == -1 or i == len(on):
        return False
    else:
        return on[i]

def calc_state(candidate, key):
    for j in range(key + 1):
        candidate = ((candidate * 1103515245) + 12345) & 0x7fffffff
    return candidate
    
#on = [True, True, False, False, False, False, False, True, False, False, True]
#timer = [1, 2, 0, 0, 0, 5, 0, 0, 0, 0, 0]
#state = 138685467
on = [True, True, False, True, False, True, False, False, True, True, True]
timer = [0, 0, 10, 9, 0, 0, 6, 1, 0, 0, 0]
state = 2139500045

replay = []
tick = 0
pos = -1
goal = 8 * len(timer)
keyList = [
    0b01000, 0b11000, 0b01010, 0b11010,
    0b01100, 0b11100, 0b01110, 0b11110
]
emergency = [
    0b00000, 0b00010, 0b10000, 0b10010,
    0b00001, 0b00011, 0b10001, 0b10011,
]
while True:
    current_candidate = set()
    next_candidate = set()
    hit = [False, False]
    possible_state = {key: calc_state(state, key) for key in keyList}
    for n, t in enumerate(timer):
        if t == 0:
            if n == pos: # my position must be False
                hit[0] = True
                for key in possible_state:
                    if not ((possible_state[key]%100 < 5) ^ spike_state(pos)):
                        current_candidate.add(key)
            elif n == pos + 1: # next position should be False
                hit[1] = True
                for key in possible_state:
                    if not ((possible_state[key]%100 < 5) ^ spike_state(pos+1)):
                        next_candidate.add(key)
                #if n == 1:
                #    next_candidate.pop()
                #print(pos, next_candidate)
            
            for key in possible_state:
                possible_state[key] = calc_state(possible_state[key], key)

    print(tick, pos, current_candidate, next_candidate)
    # hoge
    #if pos == 9 and current_candidate and len(next_candidate) == 0:
    #    current_candidate.pop()

    if hit == [True, True]:
        possible = next_candidate.intersection(current_candidate)
        if len(possible) == 0:
            possible_state = {key: calc_state(state, key) for key in emergency}
            em_state = set()
            for key in possible_state:
                if not ((possible_state[key]%100 < 5) ^ spike_state(pos)):
                    em_state.add(key)
            replay.append(em_state.pop())
        else:
            replay.append(possible.pop())
    elif hit == [True, False] and spike_state(pos+1) == False:
        replay.append(current_candidate.pop())
    elif hit == [False, True]:
        if len(next_candidate) == 0:# and timer[pos+1] > 0:
            replay.append(0b00000)
        else:
            replay.append(next_candidate.pop())
    else:
        if spike_state(pos + 1) == True:
            replay.append(0b00000)
        else:
            replay.append(0b01000)

    for n, t in enumerate(timer):
        if t == 0:
            state = calc_state(state, replay[-1])
            if state % 100 < 5:
                on[n] ^= True
                timer[n] = 10
        else:
            timer[n] -= 1
    print(on, timer, state)

    if replay[-1] & 0b01000:
        tick += 1
        if pos < 5:
            if tick % 8 == 2 and tick > 2:
                pos += 1
        else:
            if tick % 9 == 4:
                pos += 1

    if pos == len(timer):
        break

output = '\n'.join(list(map(lambda x: bin(x)[2:].zfill(5), replay)))
print(output)
