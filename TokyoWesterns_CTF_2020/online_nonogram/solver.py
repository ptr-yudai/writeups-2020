#!/usr/bin/env python3

import sys, subprocess
from Nonogram import Nonogram

def solve(param):
    instance = Nonogram()
    instance.set_instance(*param)
    nliter, nclause, clauses = instance.build_rules()
    with open("./input.cnf", "w") as f:
        f.write("p cnf {} {}\n".format(nliter, nclause))
        f.write("\n".join(clauses))

    with open("/dev/null", "w") as f:
        subprocess.call(["./minisat", "input.cnf", "output.dat"], stdout=f)

    with open("./output.dat", "r") as f:
        if f.readline().strip() != "SAT":
            sys.exit(1)
        else:
            line = f.readline()
            total = instance.nrow * instance.ncol
            result = [[0] * instance.ncol for _ in range(instance.nrow)]
            for c in line.strip().split()[:total]:
                v = int(c)
                if v > 0:
                    v -= 1
                    result[v // instance.ncol][v % instance.ncol] = 1

    return result
