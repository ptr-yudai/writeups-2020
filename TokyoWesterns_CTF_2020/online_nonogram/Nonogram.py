class Nonogram():
    def __init__(self, nrow=0, ncol=0, crow=None,
                 ccol=None):
        self.nrow = nrow
        self.ncol = ncol
        self.crow = crow
        self.ccol = ccol
        self.clauses = []
        self.literal = list(range(1, nrow*ncol + 1))

    def set_instance(self, nrow, ncol, crow, ccol):
        self.nrow = nrow
        self.ncol = ncol
        self.crow = crow
        self.ccol = ccol
        self.literal = list(range(1, nrow*ncol + 1))

    def clear(self):
        self.clauses = []
        self.literal = list(range(1, self.nrow*self.ncol + 1))

    def build_row_rules(self):
        base = 1
        clauses = []
        for n in range(self.nrow):
            cons = self.crow[n]
            left_sum = 0
            right_sum = sum(cons) + len(cons) - 1

            # Handle 2 edge cases
            if not right_sum:
                for i in range(self.ncol):
                    clauses.append("-{} 0".format(base + i))
            elif right_sum == self.ncol:
                next_neg = base + cons.pop(0)
                for i in range(self.ncol):
                    if base + i == next_neg:
                        next_neg += cons.pop(0) + 1
                        clauses.append("-{} 0".format(base + i))
                    else:
                        clauses.append("{} 0".format(base + i))
            else:
                # General cases
                liters = [[] for _ in cons]
                idx_dic = {}
                AtoB = [[] for _ in range(self.ncol)]
                AtoNB = [[] for _ in range(self.ncol)]
                ordering = [[] for _ in range(self.ncol)]
                for i in range(len(cons)):
                    c = cons[i]
                    subrule = None
                    right_sum -= c
                    lbound = base + left_sum
                    rbound = base + self.ncol - right_sum

                    # Generate B literals
                    for start in range(lbound, rbound - c + 1):
                        liter = "r,{},{},{}".format(i, start, c)
                        self.literal.append(liter)
                        cur = str(len(self.literal))
                        liters[i].append(cur)
                        ordering[i].append((cur, start + c))

                        # For each constraint, the order must be preserved
                        flag = False
                        for j in range(i - 1, -1, -1):
                            for k in range(len(ordering[j]) - 1, -1, -1):
                                if start <= ordering[j][k][1]:
                                    clauses.append("-{} -{} 0".format(cur, ordering[j][k][0]))
                                elif k == len(ordering[j]) - 1:
                                    flag = True
                                    break
                                else:
                                    break
                            if flag:
                                break

                        # If B holds, then corresponding A holds
                        if start != base:
                            clauses.append("-{} -{} 0".format(cur, start - 1))
                        if start + c != base + self.ncol:
                            clauses.append("-{} -{} 0".format(cur, start + c))
                        for j in range(start, start + c):
                            clauses.append("-{} {} 0".format(cur, j))
                            AtoB[j - base].append(cur)

                        if start > base:
                            AtoNB[start - base - 1].append(cur)
                        if start + c < base + self.ncol:
                            AtoNB[start + c - base].append(cur)

                    left_sum += c + 1
                    right_sum -= 1
                del ordering

                # If A holds, then one of Bs that contains A holds.
                for i in range(len(AtoB)):
                    clauses.append("-{} {} 0".format(base + i, " ".join(AtoB[i])))
                    for elem in AtoNB[i]:
                        clauses.append("-{} -{} 0".format(base + i, elem))
                del AtoB
                del AtoNB

                # For each constraint, at least one B literal holds
                for sublst in liters:
                    clauses.append("{} 0".format(" ".join(sublst)))

                # For each constraint, at most one B literal holds
                for sublst in liters:
                    for j in range(len(sublst)):
                        for k in range(j + 1, len(sublst)):
                            clauses.append("-{} -{} 0".format(sublst[j], sublst[k]))
            base += self.ncol
        self.clauses += clauses

    def build_column_rules(self):
        base = 1
        clauses = []
        for n in range(self.ncol):
            cons = self.ccol[n]
            left_sum = 0
            right_sum = sum(cons) + len(cons) - 1

            # Handle 2 edge cases
            if not right_sum:
                for i in range(0, self.nrow * self.ncol, self.ncol):
                    clauses.append("-{} 0".format(base + i))
            elif right_sum == self.nrow:
                next_neg = base + cons.pop(0) * self.ncol
                for i in range(0, self.nrow * self.ncol, self.ncol):
                    if base + i == next_neg:
                        next_neg += (cons.pop(0) + 1) * self.ncol
                        clauses.append("-{} 0".format(base + i))
                    else:
                        clauses.append("{} 0".format(base + i))
            else:
                # General cases
                liters = [[] for _ in cons]
                idx_dic = {}
                AtoB = [[] for _ in range(self.nrow)]
                AtoNB = [[] for _ in range(self.nrow)]
                ordering = [[] for _ in range(self.nrow)]
                for i in range(len(cons)):
                    c = cons[i]
                    subrule = None
                    right_sum -= c
                    lbound = base + left_sum * self.ncol
                    rbound = base + (self.nrow - right_sum) * self.ncol

                    # Generate B literals
                    for start in range(lbound, rbound - (c - 1) * self.ncol, self.ncol):
                        liter = "c,{},{},{}".format(i, start, c)
                        self.literal.append(liter)
                        cur = str(len(self.literal))
                        liters[i].append(cur)
                        ordering[i].append((cur, start + c * self.ncol))

                        # For each constraint, the order must be preserved
                        flag = False
                        for j in range(i - 1, -1, -1):
                            for k in range(len(ordering[j]) - 1, -1, -1):
                                if start <= ordering[j][k][1]:
                                    clauses.append("-{} -{} 0".format(cur, ordering[j][k][0]))
                                elif k == len(ordering[j]) - 1:
                                    flag = True
                                    break
                                else:
                                    break
                            if flag:
                                break

                        # If B holds, then corresponding A holds
                        if start != base:
                            clauses.append("-{} -{} 0".format(cur, start - self.ncol))
                        if start + c * self.ncol != base + self.nrow * self.ncol:
                            clauses.append("-{} -{} 0".format(cur, start + c * self.ncol))
                        for j in range(start, start + c * self.ncol, self.ncol):
                            clauses.append("-{} {} 0".format(cur, j))
                            AtoB[(j - base) // self.ncol].append(cur)

                        if start > base:
                            AtoNB[(start - base) // self.ncol - 1].append(cur)
                        if start + c * self.ncol < base + self.nrow * self.ncol:
                            AtoNB[(start - base) // self.ncol + c].append(cur)

                    left_sum += c + 1
                    right_sum -= 1
                del ordering

                # If A holds, then one of Bs that contains A holds.
                for i in range(len(AtoB)):
                    clauses.append("-{} {} 0".format(base + i * self.ncol, " ".join(AtoB[i])))
                    for elem in AtoNB[i]:
                        clauses.append("-{} -{} 0".format(base + i * self.ncol, elem))
                del AtoB
                del AtoNB

                # For each constraint, at least one B literal holds
                for sublst in liters:
                    clauses.append("{} 0".format(" ".join(sublst)))

                # For each constraint, at most one B literal holds
                for sublst in liters:
                    for j in range(len(sublst)):
                        for k in range(j + 1, len(sublst)):
                            clauses.append("-{} -{} 0".format(sublst[j], sublst[k]))
            base += 1
        self.clauses += clauses

    def build_rules(self):
        self.clear()
        self.build_row_rules()
        self.build_column_rules()
        return len(self.literal), len(self.clauses), self.clauses
