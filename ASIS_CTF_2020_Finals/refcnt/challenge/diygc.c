#include <malloc.h>
#include "diygc.h"

/**
 * RC_put: Release object
 *  Call this function when the object is dereferenced.
 */
void RC_put(void *ptr) {
  RC_chunk *chunk;

  chunk = PTR2CHUNK(ptr);
  --(chunk->gc.ref_count);

  if (chunk->gc.ref_count <= 0) {
    free(chunk);
  }
}

/**
 * RC_get: Acquire object
 *  Call this function when the object is referenced.
 */
void RC_get(void *ptr) {
  RC_chunk *chunk;

  chunk = PTR2CHUNK(ptr);
  ++(chunk->gc.ref_count);
}

/**
 * RC_new: Create object
 *  Create new object
 */
void *RC_new(size_t size) {
  RC_chunk *chunk;
  void *ptr;

  chunk = (RC_chunk*)malloc(sizeof(RC_info) + size);
  chunk->gc.ref_count = 0;

  ptr = CHUNK2PTR(chunk);
  RC_get(ptr);

  return ptr;
}
