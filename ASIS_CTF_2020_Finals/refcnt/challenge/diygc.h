typedef struct {
  long ref_count;
} RC_info;

typedef struct __attribute__((packed)) {
  RC_info gc;
  char data[1];
} RC_chunk;

#define PTR2CHUNK(p) ((RC_chunk*)(p - sizeof(RC_info)))
#define CHUNK2PTR(c) ((void*)c->data)

void RC_put(void*);
void RC_get(void*);
void *RC_new(size_t);
