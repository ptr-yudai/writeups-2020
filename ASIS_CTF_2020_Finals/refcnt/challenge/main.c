#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include "diygc.h"
#define MAX_NOTE 5
#define MAX_SIZE 0x100

char *note[MAX_NOTE];
int size[MAX_NOTE];

__attribute__((constructor))
void setup(void) {
  setbuf(stdin, NULL);
  setbuf(stdout, NULL);
  setbuf(stderr, NULL);
  alarm(180);
}

void show_menu(void) {
  puts("1. new");
  puts("2. edit");
  puts("3. copy");
  puts("4. print");
  puts("5. delete");
  puts("x. Exit");
}

void readline(const char *msg, char *buf, int len) {
  int s;
  printf("%s", msg);
  if ((s = read(0, buf, len)) <= 0) exit(1);
  if (buf[s-1] == '\n') buf[s-1] = 0;
}

int getint_range(const char *msg, int min, int max) {
  int n;
  char buf[16];

  readline(msg, buf, 15);
  n = atoi(buf);
  if (n < min || n >= max) return -1;

  return n;
}

void new(void) {
  int index, len;

  if ((index = getint_range("Index: ", 0, MAX_NOTE)) == -1)
    return;
  if ((len  = getint_range("Size: ", 0, MAX_SIZE)) == -1)
    return;

  if (note[index])
    RC_put(note[index]);

  note[index] = RC_new(len);
  size[index] = len;
  puts("[+] new: OK!");
}

void edit(void) {
  int index;

  if ((index = getint_range("Index: ", 0, MAX_NOTE)) == -1)
    return;

  if (note[index]) {
    readline("Data: ", note[index], size[index]);
    puts("[+] edit: OK!");
  }
}

void copy(void) {
  int src, dest;

  if ((src = getint_range("From: ", 0, MAX_NOTE)) == -1)
    return;
  if ((dest = getint_range("To: ", 0, MAX_NOTE)) == -1)
    return;

  if (note[src]) {
    if (note[dest])
      RC_put(note[dest]);
    note[dest] = note[src];
    size[dest] = size[src];
    RC_get(note[src]);
    puts("[+] copy: OK!");
  }
}

void print(void) {
  int index;

  if ((index = getint_range("Index: ", 0, MAX_NOTE)) == -1)
    return;

  if (note[index])
    printf("[+] print: %s\n", note[index]);
  else
    puts("[-] print: Empty");
}

void delete(void) {
  int index;

  if ((index = getint_range("Index: ", 0, MAX_NOTE)) == -1)
    return;

  if (note[index]) {
    RC_put(note[index]);
    note[index] = NULL;
    puts("[+] delete: OK!");
  }
}

int main(void) {
  show_menu();
  while(1) {
    switch(getint_range("Choice: ", 1, 6)) {
    case 1: new(); break;
    case 2: edit(); break;
    case 3: copy(); break;
    case 4: print(); break;
    case 5: delete(); break;
    default: return 0;
    }
  }

  return 0;
}
