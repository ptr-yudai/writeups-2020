# refcnt
Author: ptr-yudai

## Title
refcnt

## Description
I wrote a malloc wrapper to make it more secure!

## Flag
`ASIS{r3f3r3nc3_c0unt1ng_1s_h4rd_4_pr0gr4mm3rs}`

## Deployment
```
docker-compose up
```
Distribute all files under `refcnt/distfiles` to the players.
