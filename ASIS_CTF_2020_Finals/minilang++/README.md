# minilang++
Author: ptr-yudai

## Title
minilang++

## Description
[minilang](https://github.com/drmenguin/minilang-interpreter) is now a bit faster?

## Flag
`ASIS{n3v3r_f0rg3t_2_us3_c0py_c9nstruct0r!!!}`

## Deployment
```
docker-compose up
```
Distribute all files under `minilang++/distfiles` to the players.
