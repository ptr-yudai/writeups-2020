# refcnt
Author: ptr-yudai

## Title
babyauth

## Description
Isolation is the most efficient way to protect software from attacker.

## Flag
`ASIS{PWN_the_IPC_2_PWN_the_CORE}`

## Deployment
```
docker-compose up
```
Distribute all files under `babyauth/distfiles` to the players.
